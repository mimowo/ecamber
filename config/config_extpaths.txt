BLAST_VER:BLAST+                                        # set BLAST_VER:BLASTALL to use the old version of BLAST (slower)

BLASTN_PATH:ECAMBER_PATH:/ext-tools/blastn              # BLAST+ blastn      --- corresponding to blastall blastn
BLASTP_PATH:ECAMBER_PATH:/ext-tools/blastp              # BLAST+ blastp      --- corresponding to blastall blastn
MAKEBLASTDB_PATH:ECAMBER_PATH:/ext-tools/makeblastdb # BLAST+ makeblastdb --- corresponding to formatdb

BLASTALL_PATH:ECAMBER_PATH:/ext-tools/blastall.exe       # blastall
FORMATDB_PATH:ECAMBER_PATH:/ext-tools/formatdb.exe       # formatdb

PRODIGAL_PATH:ECAMBER_PATH:/ext-tools/prodigal
