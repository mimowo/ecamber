\documentclass[xcolor=pdftex,dvipsnames,table]{beamer}
%\documentclass{beamer}
\usepackage{polski}
\usepackage[cp1250]{inputenc}
\usepackage{verbatim}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{caption}
\usepackage{tabularx}
\usepackage[absolute,overlay]{textpos}
\usepackage{graphicx}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage[labelformat=empty]{caption}
%\usepackage[table]{xcolor}
\usepackage{color}
\usetheme{Warsaw}

\algsetup{linenosize=\tiny}
\captionsetup[algorithm]{font=scriptsize,labelfont=scriptsize}
\captionsetup[algorithmic]{font=scriptsize,labelfont=scriptsize}

\definecolor{lightblue}{rgb}{0.6,0.7,1}

\title[eCAMBer]{eCAMBer:  \underline{e}fficient support for large-scale  \underline{c}omparative 
 \underline{a}nalysis of  \underline{m}ultiple  \underline{b}act\underline{er}ial strains}
\author[Michal Wozniak]{Michal Wozniak$^{1,2}$, Limsoon Wong$^2$ and Jerzy Tiuryn$^1$}

\institute{$^1$University of Warsaw\\$^2$National University of Singapore}
\date{9 October, 2013}

\DeclareMathOperator{\cH}{\mathcal{H}\xspace}

\begin{document}

\begin{frame}
\titlepage
\vspace{-0.5cm}
\begin{columns}[c]
	\column{2cm}
\begin{figure}[t]
\includegraphics[width=1.6cm, height=1.2cm]{images/UW.png}
\end{figure}
%	\framebox{\includegraphics[width=1.5cm, height=1.2cm]{images/UW.png}}
	\column{2cm}
\begin{figure}[t]
\includegraphics[width=2cm, height=1.1cm]{images/nus.png}
\end{figure}
%	\framebox{\includegraphics[width=1.7cm, height=1cm]{images/nus.png}}
\end{columns}
\end{frame}

\begin{frame}
\footnotesize
\tableofcontents
\end{frame}

\section{Introduction}

\subsection{Motivation and goals}
\begin{frame}{Annotation inconsistencies}
\scriptsize
There is a large number of observed inconsistencies are in the genome
annotations of bacterial strains. Moreover, it has been shows, that 
these inconsistencies are often not reflected by sequence discrepancies, 
but are caused by \textbf{wrongly annotated gene starts} as well as 
\textbf{mis-identified gene presence}:
\begin{itemize} \scriptsize
\item \textit{Consistency of gene starts among Burkholderia genomes}, BMC Genomics 2011
\item \textit{Using comparative genome analysis to identify problems in annotated microbial genomes}, Microbiology 2010
\end{itemize}
\begin{columns}
\column{0.6\textwidth}
\centering
\includegraphics[height=3cm, width=5.5cm]{images/mtb_incons.png}
\column{0.4\textwidth}
\centering
\includegraphics[height=3cm]{images/incon.png}
\end{columns}
\end{frame}

\begin{frame}{Example of annotation inconsistencies}
\scriptsize
There are 67 strains of M. tuberuculosis in the PATRIC database
\begin{itemize}\scriptsize
\item 67 with PATRIC annotations
\item 46 with RefSeq annotations
\end{itemize}
Annotations of the key drug resistance genes:
\begin{itemize}\scriptsize
\item rpoB: 3 strains with missing annotations in RefSeq 
\item katG: 5 strains with missing annotations in RefSeq (1 in PATRIC)
\item inhA: no strains with missing annotations in RefSeq
\item gyrA: no strains with missing annotations in RefSeq
\item rpsL: no strains with missing annotations in RefSeq (1 in PATRIC)
\item pncA: no strains with missing annotations in RefSeq (1 in PATRIC)
\end{itemize}
\end{frame}

\begin{frame}{Comparative analysis approaches}
It has also been argued, that the consistency and accuracy of 
annotations may be improved by comparative analysis of these 
annotations among bacterial strains:

\begin{itemize} \scriptsize
\item \textit{Genome majority vote improves gene predictions}, \\ PLoS Computational Biology 2011
\item \textit{Improving pan-genome annotation using whole genome multiple alignment},  \\ BMC Bioinformatics 2011
\item \textit{ORFcor: identifying and accommodating ORF prediction inconsistencies for phylogenetic analysis}, \\ PLoS ONE 2013
\item \textit{CAMBer: an approach to support comparative analysis of multiple bacterial strains}, \\ BMC Genomics 2011
\end{itemize}
\end{frame}

\begin{frame}{Overview of CAMBer}

\begin{columns}
\column{0.55\textwidth}

\includegraphics[width=0.99\textwidth]{images/blast.png}
\column{0.45\textwidth}
\includegraphics[width=0.99\textwidth]{images/closure.png}
\end{columns}

\scriptsize
A BLAST hit is acceptable if (default parameters):
\begin{itemize}
  \item the hit has one of the appropriate start codons: ATG, GTG, TTG, or the same start codon as in the
query sequence,
  \item BLAST e-value is smaller than $10^{-10}$,
  \item the length change is smaller than $0.2$,
  \item the threshold for the percentage of identity is $80\%$ for long sequences 
  and is adjusted for shorter sequences by the HSSP curve.
\end{itemize}
\end{frame}

\begin{frame}{Major issues with CAMBer}

Major issues with CAMBer:
\begin{itemize}
  \item It propagates annotation errors
  \item It uses each gene sequence (annotated or predicted) as a BLAST query
\end{itemize}

The number of gene sequences is much higher than the number of distinct gene 
sequences!
\begin{center}
\includegraphics[height=4cm]{images/f3.pdf}
\end{center}
\end{frame}

\begin{frame}{Goals}
Major goals for CAMBer and eCAMBer:
\begin{itemize}
  \item Goal 1: unification of annotations among bacterial strains,
  \item Goal 2: identification of annotation inconsistencies.
\end{itemize}
%\pause
Major goals for eCAMBer:
\begin{itemize}
  \item Goal 3: speeding up the closure procedure by avoiding repetitions 
  of sequences used as BLAST queries,
  \item Goal 4: cleaning up of propagated annotations errors.
\end{itemize}
\end{frame}

\section{Methodology}

\subsection{General schema of eCAMBer}
\begin{frame}{General schema of eCAMBer}
\footnotesize
Phase 1:
\begin{itemize}
  \item modified closure procedure
\end{itemize}

Phase 2:
\begin{itemize}
  \item modified \textit{refinement} procedure for splitting homologous gene families into orthologous gene clusters,
  \item the \textit{TIS voting} procedure for selecting the most reliable TIS,
  \item the \textit{clean up} procedure procedure for removal of multigene clusters that are 
  likely to be annotation errors propagated during the closure procedure.
\end{itemize}
\end{frame}

\subsection{Phase 1 in eCAMBer}
\begin{frame}{Schema of the closure procedure in eCAMBer}
\begin{columns}
\column{0.42\textwidth}
\vspace{-0.1cm}
\includegraphics[width=5.1cm]{images/f1.pdf}
\column{0.58\textwidth}
\input{include/pseudo1.tex}
\end{columns}
\end{frame}

\begin{frame}{Sequence consolidation graphs}
\begin{columns}
\column{0.4\textwidth}
\includegraphics[width=0.99\textwidth]{images/f2.pdf}
\column{0.6\textwidth}
\scriptsize
\begin{itemize}
\item (A) ORF consolidation graph $(V_O, E_O)$- nodes represent 
annotated or predicted ORFs, there is an edge $\{x, y\} \in E_O$ if 
there was an acceptable BLAST hit between the pair of ORFs,
\item (B) multigene consolidation graph $(V_M, E_M)$ - 
nodes represent multigenes, there is an edge $\{x, y\} \in E_M$ if 
there was an acceptable BLAST hit between any elements of the pair of multigenes,
\item (C) sequence consolidation graph $(V_S, E_S, E_B)$ - nodes represent
distinct gene sequences; there is a \textit{shared-end} edge $\{x, y\} \in E_S$ 
between a pair of sequence nodes if there is a multigene having two 
elements with these sequences; there is a BLAST-hit edge $\{x, y\} \in E_B$
between a pair of sequence nodes if there is an acceptable BLAST between
ORFs $x$ and $y$.
\end{itemize}
\end{columns}
\end{frame}

\subsection{Phase 2 in eCAMBer}
\begin{frame}{Refinement procedure}
%\input{include/pseudo2.tex}
\scriptsize
Subsequent steps of the procedure:
\begin{itemize}
\item for each strain sort multigenes by positions of stop codons,
\item for every pair of strains $(s_1, s_2)$:   $\{$in parallel$\}$
\begin{itemize} \scriptsize
  \item reconstruct the subgraph of the multigene consolidation graph for non-anchors,
  \item for each multigene $m$ on $s_1$ determine its neighbours, that belong to
  a multigene cluster with an element on $s_2$,
  \item for each non-anchor edge between a pair of multigenes on $s_1$ and 
  $s_2$ check if it is supported (remove if not supported).
\end{itemize}
\end{itemize}
\begin{columns}
\column{0.5\textwidth}
\centering
\includegraphics[height=4cm]{images/ref2.png}
\column{0.5\textwidth}
\centering
\includegraphics[height=4cm]{images/ref2.png}
\end{columns}
\end{frame}

\begin{frame}{TIS voting procedure}
\scriptsize
For each multigene $m$ in each multigene cluster $c$, we try 
to find a TIS (originally annotated or transferred)
that belongs to a connected component of the ORF consolidation graph, where
the connected component satisfies the following two conditions: 
\begin{itemize} \scriptsize
  \item (i) it has TISs (originally annotated or transferred) present in 
  at least 80\% of the multigenes in $c$; and 
  \item (ii) it has TISs originally annotated in at least 50\% of the multigenes 
  in $c$, or it has TISs originally 
  annotated in at least twice the number of multigenes in $c$ 
  than all other connected components in $c$. 
\end{itemize}

If such a TIS is found, it is selected as the TIS for $m$. If such a TIS is not found, but $m$ has an 
originally annotated TIS, then the originally annotated TIS is selected as the TIS for $m$. Otherwise,
the longest ORF in the multigene $m$ is selected. 
After the TIS voting procedure, every multigene has exactly one TIS selected.
\end{frame}

\begin{frame}{Clean up procedure}
\scriptsize
The input for this procedure consists of the set of multigene clusters $C^*$
and multigene annotations $M_s$, for each strain $s\in S$. For each multigene 
cluster $c\in C^*$ we compute the following features: 
\begin{itemize} \scriptsize
  \item (i) $l$, the median multigene length in $c$,
  \item (ii) $p$, the ratio of the number of strains with at least one element from $c$ to
the total number of strains;
\item (iii) $r$, the ratio of the number of originally annotated multigenes to the total
number of multigenes in $c$;
\item (iv) $v$, the ratio of the number of multigenes in the cluster that are overlapped
by a longer multigene to the total number of multigenes in the cluster.
\end{itemize}
Then, we update the set of multigene clusters $C^*$, by removing of multigene 
clusters for which: $(p<\frac{1}{3} \textrm{ or } r < \frac{1}{3})\textrm{ and }(l < 150 \textrm{ or } v>0.5)$.
\end{frame}

\subsection{Time complexity}
\begin{frame}{Closure procedure in CAMBer vs. eCAMBer}
\scriptsize
\begin{itemize}
  \item $k$ - number of strains
  \item $n$ - number of ORF sequences
  \item $d$ - number of distinct ORF sequences
  \item $O(kn\cdot k)$ of BLAST computations for CAMBer
  \item $O(d\cdot k)$ of BLAST computations for eCAMBer
  %\item $O((n\cdot k)^2)$ number of edges for ORF consolidation graph 
  %\item $O(d^2)$ number of edges for sequence consolidation graph 
\end{itemize}

\begin{block}{Case study of 64 M. tuberculosis strains}
\scriptsize
\begin{itemize}
  \item number of ORFs: 669620
  \item number of multigenes: 350774
  \item number of distinct ORF sequences: 60854
  \item number of edges in ORF consolidation graph:  23177547
  \item number of edges in multigene consolidation graph: 10875300
  \item number of edges in sequence consolidation graph: 139885
\end{itemize}
\end{block}
\end{frame}

\section{Results}

\subsection{Running times}
\begin{frame}{Comparison of running times}
\footnotesize
CAMBer vs. eCAMBer on four datasets from the CAMBer paper (using 4 processors):
\input{include/table1.tex}
\footnotesize
CAMBer vs. eCAMBer vs. Mugsy-Annotator on a single processor:
\input{include/table2.tex}
\end{frame}

\begin{frame}{Running times on large datasets}
Running times of eCAMBer on large datasets (using 20 processors):
\input{include/table3.tex}
\end{frame}

\begin{frame}{Comparison of graph sizes}
\footnotesize
Selected statistics for the largest dataset of 569 strains of \textit{E. coli}:
\begin{itemize}
\item 12.4mln nodes in the ORF consolidation graph (ORF annotations),
\item 1.6mln nodes in the sequence consolidation graph (unique ORF sequences),
\item 2.8bln edges in the ORF consolidation graph
\item 1.3mln shared-end edges in the sequence consolidation graph
\item 55.9mln BLAST-hit edges in the sequence consolidation graph
\end{itemize}
\end{frame}

\subsection{Evalution on the set of 20 E.coli strains}

\begin{frame}{Input dataset}
We use a dataset of 20 \textit{E.~coli} strains with manually curated
annotations, deposited in the ColiScope database. The annotations were
published with the work: 

\textit{Organized genome dynamics in the Escherichia coli species 
results in highly diverse adaptive paths} (PLoS Genet. 2009).

\begin{block}{Experimental support}
There are 923 genes with experimental support (EcoGene 3 database) 
for strain K-12 1655, out of which:
\begin{itemize} 
  \item 903 are present in the ColiScope annotations;
  \item 833 are present in the PATRIC annotations.
\end{itemize}
\end{block}
\end{frame}

\subsection{Annotation consistency}
\begin{frame}{Number of genes before and after}
\includegraphics[width=0.99\textwidth, height=5cm]{images/f4.pdf}
\scriptsize 
\newline
The mean absolute difference in the number of annotated multigenes between 
two neighbour strains (sorted in the order of increasing genome sizes):
\begin{itemize}
  \item 311 for the ColiScope annotations from ColiScope vs. 181 after eCAMBer
  \item 409 for the PATRIC annotations and 323 after applying eCAMBer.
\end{itemize}
\end{frame}

\begin{frame}{Inconsistencies in the ColiScope dataset}
\scriptsize
\begin{block}{Putative missing gene annotations}
\scriptsize
There are 73 gene families which have a multigene in every 
strain, and exactly one missing original annotation. The top 
four strains with the highest number of missing gene 
annotations of that type are: Sd197 (12), 2a 2457T (8), 
536 (7) and Sb227 (7). The most well-studied strain K-12 MG1655 
has four missing annotations of the above type.
\end{block}
\begin{block}{Inconsistent TIS annotations}
\scriptsize
There are 3923 pairs of annotated genes with different TISs, 
but with identical sequence (including 100bp. upstream region 
from the TIS of the longer annotation). This number was reduced 
to 482 after applying the TIS majority voting procedure and 
the clean up procedure.
\end{block}
\end{frame}

\subsection{Annotation accuracy}
\begin{frame}{Statistics for the TIS voting procedure}
\includegraphics[width=0.99\textwidth, height=5cm]{images/f5.pdf}
\scriptsize
\newline
There are 1134 gene families in the ColiScope dataset with consistent
TIS annotations (gold standard). For about 240 (depending on strain) 
of them annotations of TIS in the PATRIC database were unambiguous. 
In total 534 (74\% out of 739) changes were correct.
\end{frame}

\begin{frame}{Overall accuracy (f1 measure)}
\includegraphics[width=0.99\textwidth]{images/f6.pdf}
\end{frame}


\section{Summary}
\subsection{Limitations of eCAMBer}
\begin{frame}{Limitations of eCAMBer}
\begin{itemize}
\item eCAMBer only purely on the quality of original annotations. 
Thus, for example, eCAMBer cannot identify genes, whose annotations 
were missing for all strains;
\item eCAMBer excludes pseudogenes and non-protein coding genes from the analysis. 
This follows from the assumption that eCAMBer considers only genes that start with
start codon, end with stop codon, and have length divisible by 3.
\end{itemize}
\end{frame}

\subsection{Summary and conclusions}
\begin{frame}{Summary and conclusions}
\footnotesize
\begin{itemize}
\item eCAMBer is a tool to unify annotations among bacterial strains within the same species,
\item eCAMBer is more efficient than CAMBer and scales up to datasets comprising hundrends
of bacterial strains,
\item eCAMBer improves overall annotation consistency and accuracy,
\item it supports downloading genome sequences and genome annotations from the PATRIC database, 
  for the set of selected strains within a species,
\item eCAMBer generates output compatible with CAMberVis, a tool for simultaneous
visualization of multiple genome annotations of bacterial strains,
\item the project webpage: \textit{http://bioputer.mimuw.edu.pl/ecamber}.
\end{itemize}
\end{frame}

%\subsection{Acknowledgements}
\begin{frame}{Thank you}
\begin{center}
\Large
Thank you! \\
%Comments or questions?
\normalsize
You are welcome to give comments or ask questions.

\begin{figure}[t]
%\includegraphics[width=0.7\textwidth, height=5.5cm]{images/good.jpg}
%\includegraphics[width=0.7\textwidth, height=5.5cm]{images/prez.jpg}
\end{figure}

\end{center}
\end{frame}

\end{document}
