\documentclass[py@paper=a4paper, ptsize=8pt]{howto}
%\documentclass{article}
%\usepackage[T1]{fontenc}
%\usepackage{polski}
\let\ifpdf\relax
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{fixltx2e}
\usepackage{fancyvrb}
\usepackage{float}
\usepackage{xspace}
\usepackage[usenames,dvipsnames]{xcolor}
%\usepackage{hyperref}
\usepackage{url}
%\usepackage{amsfonts}

\newcommand{\ecamberhttp}{\small\url{http://bioputer.mimuw.edu.pl/ecamber}\normalsize}
\newcommand{\linkrepo}{\small\url{https://bitbucket.org/mimowo/ecamber}\normalsize}
\newcommand{\mails}{\small\url{m.wozniak@mimuw.edu.pl}\normalsize}
\newcommand{\cmd}[1]{\fcolorbox{white}{GreenYellow}{{\tt #1}}}

\newcommand{\mtu}{{\it M.~tuberculosis}\xspace}
\newcommand{\eco}{{\it E.~coli}\xspace}
\newcommand{\sau}{{\it S.~aureus}\xspace}

\DefineVerbatimEnvironment%
{MyVerbatim}{Verbatim}
{numbersep=1mm,
frame=lines,framerule=0.3mm, rulecolor=\color{GreenYellow},
fontsize=\footnotesize}

\date{27 March 2015}
\release{1.8}

\title{eCAMBer user's manual}
\author{Michal Wozniak}
\begin{document}

\maketitle
\tableofcontents

\section{About the software}

\subsection{Background}

\textasciitilde
Due to the fast progress in high-throughput sequencing technologies, the number 
of bacterial sequences grows rapidly 
({\it Loman et al. Nature Reviews Microbiology, 2012}). This enables new 
interesting comparative genome analyses. On the other hand, we observe a lot of 
inconsistencies in genome annotations among bacterial strains. It has been 
shown that a wide range of comparative analyses may be complicated or biased due 
to the common inconsistencies in genome annotations among closely related 
bacterial strains ({\it Derrick E. Wood et al. Biology Direct, 2012},
{\it John Dunbar et al. BMC Genomics, 12(1):125, 2011}).

It has also been shown that the use of the same tool to annotate a set of 
bacterial genomes increases annotation consistency ({\it John Dunbar et al. 
BMC Genomics, 12(1):125, 2011}), which can be further improved by comparative 
annotation among multiple genomes ({\it Poptsova et al., Microbiology, 2010}). 
However, even though there are many annotation tools dedicated to a single 
genome, there are relatively few tools supporting comparative annotation and 
analysis of multiple bacterial genomes

We have developed {\it eCAMBer}, a tool efficiently supporting comparative 
analysis of multiple bacterial strains within the same species (paper submitted 
to Bioinformatics 2013). In this work we achieve three major goals. First, 
eCAMBer is a highly optimized revision of our earlier tool, CAMBer 
({\it Wozniak M, Wong L, Tiuryn J. BMC Genomics 2011}), scaling it up for 
significantly larger datasets comprising hundreds of bacterial strains. Second, 
eCAMBer is capable of identifying and resolving annotation inconsistencies. 
Finally, eCAMBer improves the overall quality of annotations.

eCAMBer works in two phases. First, it unifies annotations of closely related 
species by homology transfer in the {\it closure procedure}. Second, eCAMBer
identifies and tries to resolve annotation inconsistencies in the three 
procedures: 
(i) {\it the refinement procedure} for splitting homologous gene families 
into orthologous gene clusters; 
(ii) {\it refinement procedure} for selecting the most reliable TIS; 
(iii) {\it TIS voting procedure} for removal of gene clusters that are 
likely to be annotation errors propagated during the 
{\it closure procedure}.

Details of the methodology are described in the eCAMBer paper. 

\subsection{Availability}

This software is an open source application (GPL 3 license). 
This is the project website:

\ecamberhttp

This is the project repository website:

\linkrepo

Please don't hesitate to contact us with any comments and suggestion or if you 
are interested in co-developing this software.

\subsection{About the authors}

This software was implemented by Michal Wozniak. All authors contributed to 
design of the method and analysis of results. Project idea and guidance came 
from prof. Limsoon Wong (National University of Singapore) and prof. Jerzy 
Tiuryn (University of Warsaw).

Corresponding author: Michal Wozniak (\mails)

\subsection{Installation}
This software is written in Python, thus Python 2 or 3 is required to run 
eCAMBer. The software does not need a classical installation, you only need to 
download and extract the zip package (versions for Windows, Linux and Mac OS
are available) from the project website:

\ecamberhttp

\subsection{Design of eCAMBer}

The software is designed as a set of executable scripts written in Python, 
which can be run via the console script {\tt ecamber.py}. 

Figure \ref{fig:structure} presents the hierarchy of folders in eCAMBer open
in Eclipse.

\begin{figure}[H]
\begin{center}
\includegraphics[height=14cm]{figures/folders2.jpg}
\caption{Hierarchy of folders in eCAMBer open in Eclipse.}
\label{fig:structure}
\end{center}
\end{figure}

The hierarchy of folders in eCAMBer (including the dataset folders): 

\begin{itemize}
  \item {\tt ecamber/} --- the main eCAMBer folder with the executable files
  \begin{itemize}
   \item {\tt ecamber.py} --- the main console script to run eCAMBer
   \item {\tt ext-tools/} --- BLAST+ or BLASTALL executable files
   \item {\tt config/} --- configuration files:
   \begin{itemize}
    \item {\tt config\_extpaths.txt} --- BLAST paths configuration file
    \item {\tt config\_resources.txt} --- configuration of the paths for datasets
    \item {\tt config\_params.txt} --- configuration of the eCAMBer parameters
    \item {\tt config\_results.txt} --- configuration of the paths with experiment results
   \end{itemize}
   \item {\tt datasets/} --- input datasets
   \begin{itemize}
     \item {\tt dataset1/} --- the folder with {\tt dataset1} input files (for example {\tt dataset1=ecoli20})
  	 \item {\tt dataset2/} --- the folder with {\tt dataset2} input files (for example {\tt dataset2=mtb})
   \end{itemize}
   \item {\tt src/}
   \begin{itemize}
    \item {\tt soft} --- the source code of eCAMBer
    \item {\tt soft/analysis\_closure} --- scripts to analyze output of the closure procedure
    \item {\tt soft/analysis\_input} --- scripts to analyze dataset input
    \item {\tt soft/analysis\_post} --- scripts to analyze results of post-processing procedures 
    \item {\tt soft/download} --- scripts for downloading the bacterial genomes and annotations from PATRIC
    \item {\tt soft/formats} --- scripts for formatting the downloaded data into eCAMBer input format
    \item {\tt soft/prepare} --- scripts for preparing input for the 1st iteration of the closure procedure
    \item {\tt soft/closure} --- scripts for computing the closure procedure
    \item {\tt soft/graph} --- scripts for constructing the sequence consolidation graph
    \item {\tt soft/components} --- scripts for computing the homologous gene clusters
    \item {\tt soft/refinement} --- scripts for computing the refinement procedure
    \item {\tt soft/tisvoting} --- scripts for computing the TIS voting procedure
    \item {\tt soft/cleanup} --- scripts for computing the clean up procedure
    \item {\tt soft/output} --- contains a script to generate input data for CAMBerVis
    \item {\tt soft/structs} --- structures used in eCAMBer
    \item {\tt soft/utils} --- implementation of common methods used in eCAMBer     
   \end{itemize}
  \end{itemize}
\end{itemize} 

\subsection{Dataset files}

Input files for eCAMBer consist of genome sequences and annotations for a set 
of bacterial strains. There is also file {\tt 'strains.txt'} required to specify 
the list of strains. The structure of input files for each dataset:

\begin{itemize}
\item {\tt anns\_input} --- contains a set of annotation files downloaded from PATRIC
\item {\tt anns\_parsed} --- contains a set of annotation files downloaded from PATRIC and parsed as eCAMBer input
\item {\tt genomes} --- contains a set of FASTA files
\item {\tt strains.txt} --- list of strains
\end{itemize} 

Each line of an annotation file should be in the following tab-separated 
format:

\begin{MyVerbatim}
gene_id start end strand gene_name contig_id
\end{MyVerbatim}

The last two pieces of information ({\tt gene\_name} and {\tt contig\_id}) are 
optional. Below we present an an example of the first 7 lines from the 
annotation file for the \mtu strain {\tt K-12 MG1655}.

\begin{MyVerbatim}
ECK0001   190   255    +   thrL
ECK0002   337   2799   +   thrA
ECK0003   2801  3733   +   thrB
ECK0004   3734  5020   +   thrC
ECK0005   5234  5530   +   yaaX
ECK0006   6459  5683   -   yaaA
ECK0007   7959  6529   -   yaaJ
\end{MyVerbatim}

The partial results of eCAMBer computations for each dataset are organized 
in the following structure:
\begin{itemize}
\item {\tt blast} --- BLAST results
\item {\tt cambervis} --- eCAMBer output formatted as CAMBerVis input
\item {\tt output} --- eCAMBer annotation output
\item {\tt genomes\_dbs} --- genomes formetted as BLAST databases
\item {\tt genomes\_tmp} --- FASTA genomes downloaded from PATRIC
\item {\tt results/exp-refseq-20-80-1e-10-YES-Q/} --- partial results for a combination of parameters
\begin{itemize}
\item {\tt gene\_names} --- gene names
\item {\tt excluded} --- gene annotations excluded from the analysis
\item {\tt analysis} --- results of automatic analysis of results
\item {\tt blast-tmp} --- temporal BLAST results
\item {\tt phase1} --- results for the closure procedure (phase 1)
\item {\tt phase2} --- results for the phase 2
\end{itemize}
\end{itemize}

Each line of the files with the result annotations has the following 
tab-delimited format:

\begin{MyVerbatim}
mg_cluster_id mg_id mg_lengths
\end{MyVerbatim}

Here, {\tt mg\_cluster} denotes the multigene cluster id, {\tt mg\_id} 
denotes the multigene id ({\tt end strand contig\_id}), {\tt mg\_lengths} 
denote different lengths of different putative genes, sharing the same
end, but corresponding to different putative TISs. Additionally, a star,
{\tt *} in front of the number representing the ORF length, denotes that its 
corresponding TIS was originally annotated. Similarly, a hash {\tt \#}, in 
front of the number representing the ORF length denotes, that the TIS was 
selected during the TIS voting procedure.

An example:
 \begin{MyVerbatim}
8204	255 + K-12_MG1655	ECK0001	#*66:ATG
1277	2799 + K-12_MG1655	ECK0002	#*2463:ATG
1153	3733 + K-12_MG1655	ECK0003	#*933:ATG
207	5020 + K-12_MG1655	ECK0004	#*1287:ATG
8028	5530 + K-12_MG1655	ECK0005	288:ATG #*297:GTG
2276	5683 - K-12_MG1655	ECK0006	#*777:ATG
3635	6529 - K-12_MG1655	ECK0007	#*1431:ATG
(...)
5850	1702700 + K-12_MG1655	ECK1620	*126:ATG #201:TTG
\end{MyVerbatim}
 
\section{Usage manual}

\subsection{Parameters}

There are two methods to specify parameters in eCAMBer:
\begin{itemize}
  \item -key\textvisiblespace value
  \item key=value
\end{itemize}


The two following commands (to download annotations from PATRIC) 
are equivalent:

\begin{itemize}
\item \cmd{python ecamber.py -a d -w 4} 
\item \cmd{python ecamber.py a=d w=4} 
\end{itemize}

Table \ref{tab:parameters} presents the list of eCAMBer parameters and their
descriptions.

\begin{table}
\begin{tabular}{l l l p{6cm}}
\toprule
Parameter & Default value & Example values (comma separated) & Description \\ 
\midrule
a & -- & d,pr,f,ph1,ph2,out & action \\
w & 1 & 1,2,3,\ldots & \# of threads (workets used) \\
d & -- & mtu2,ecoli20 & dataset \\
as & patric & patric,refseq,prodigal & annotation source \\
ep & exp & any string & prefix of the results folder \\
\midrule
m  & 100 & 1\ldots 100 & maximal number of iterations \\
be  & 10$^{10}$ & 0.0 \ldots 1.0 & BLAST e-value running parameter \\
te  & 100 & 0.0 \ldots 1.0 & BLAST e-value threshold parameter \\
lcm  & Y & Y,N & BLAST Low Complexity Masking  \\
hssp  & Y & Y,N & HSSP correction of PID \\
pid  & 80 & 30\ldots 100 & PID threshold for acceptable hits \\
\midrule
step & d & d,0,1,2,3,\ldots & (d)efault, step of phase 2\\
\midrule
tvc  & C & 1 \ldots n & (C)onservative; (R)elaxed \\
tvr  & 0.8 & 0.0 \ldots 1.0 & conservation threshold \\
\midrule
cur & 0.3334 & 0.0 \ldots 1.0 & annotation ratio \\
cup & 0.3334 & 0.0 \ldots 1.0 & conservation ratio \\ 
cul & 150 & 30 \ldots 500 & the median multigene length \\
cuv & 0.5 & 0.0 \ldots 1.0 & the overlapping ratio \\
\bottomrule
\end{tabular}
\caption{The list of eCAMBer parameters and their descriptions.}
\label{tab:parameters}
\end{table}

\subsection{Preparation of input annotations}

The input files for a dataset can be prepared manually according to the input 
format described above. However, eCAMBer supports two convenient ways to
prepare input annotations: (1) automatic download of annotations for the
PATRIC database; and (2) usage of Prodigal to generate the input annotations.

\subsubsection{Download of input annotations}

eCAMBer supports downloading of the input annotations from the PATRIC 
database (both PATRIC and RefSeq annotations can be downloaded), available 
under this ftp link: 

{\tt http://brcdownloads.vbi.vt.edu/patric2/}

In order to download the input dataset from PATRIC run:

\cmd{python ecamber.py -a d -w 4} 

Next, to format the downloaded data into eCAMBer input format:

\cmd{python ecamber.py -a f -d dir -w 4}

Here, {\tt dir} denotes the name of the folder to which the dataset files 
were downloaded (chosen during the previous phase).

\subsubsection{Prodigal input annotations}

Alternatively, input annotations can be prepared using Prodigal:

\cmd{python ecamber.py -a pr -d dir -w 4} 

\subsection{Phase 1: The closure procedure}

In order to run the closure procedure (phase 1), on Prodigal annotations, 
invoke that command:

\cmd{python ecamber.py -a ph1 -d dir -w 4 -as prodigal}
 
Alternatively, you can also execute the closure procedure step by step by 
running the two commands below. The first command prepares the input data for 
the $1$st iteration of the closure procedure:

\cmd{python ecamber.py -a p -d dir -w 4 -as prodigal}

Then, the following command starts the closure procedure itself:

\cmd{python ecamber.py -a c -d dir -w 4 -as prodigal -m 100}

Here, the default {\tt -as prodigal} parameter denotes the source of 
annotations, acceptable values comprise:
\begin{itemize}
  \item {\tt patric} --- PATRIC annotations, read from the folder {\tt anns\_parsed/patric/}
  \item {\tt refseq} --- RefSeq annotations, read from the folder {\tt anns\_parsed/refseq/}
  \item {\tt prodigal} --- Prodigal annotations, read from the folder {\tt anns\_parsed/prodigal/}
\end{itemize}


\subsection{Phase 2: refinement, TIS voting and clean up procedures}

In order to run the second phase of eCAMBer invoke that command:

\cmd{python ecamber.py -a ph2 -d dir -w 4 -as prodigal}

During this command three procedures are run subsequently: (1) refinement
procedure; (2) TIS voting procedure; (3) clean up procedure. 

Alternatively, you can also run the three procedures in any order using the
{\it step} parameter, which denotes the current step within the second 
phase of eCAMBer. The default value of this parameter is 'd', which means that
the refinement procedure will be performed as the 1st step; the TIS voting
procedure will be performed as the 2nd step; and the clean up procedure will
be performed as the 3rd (last) step. 

Below, we present, how to run each of the procedures of the 2nd phase manually.

\subsubsection{Step 0: The sequence consolidation graph and multigene clusters}

In this step eCAMBer constructs the sequence consolidation graph. and 
identifies the homologous clusters as connected components of the multigene
consolidation graph. To perform this step, run:

\cmd{python ecamber.py -a cc -d dir -w 4 -as prodigal}

\subsubsection{Step 1: Refinement procedure}

In this procedure eCAMBer splits multigene clusters with more than one multigene
per strain. To perform this step, run:

\cmd{python ecamber.py -a rf -d dir -w 4 -as prodigal -step 1}

\subsubsection{Step 2: TIS voting procedure}

In the TIS voting procedure, for each multigene, one TIS (originally annotated 
or predicted) is selected, that is most often annotated among the strains. To 
perform this step run:

\cmd{python ecamber.py -a tv -d dir -w 4 -as prodigal -step 2}

\subsubsection{Step 3: clean up procedure}

In the clean up procedure, eCAMBer, removes multigene clusters, that are likely
to be annotation errors propagated during the closure procedure. To perform 
this step, run:

\cmd{python ecamber.py -a cu -d dir -w 4 -as prodigal -step 3}

\subsection{Generating output}

The following command generates output annotations which includes input for 
CAMBerVis:

\cmd{python ecamber.py -a out -d dir -w 4 -as prodigal -step 3}

\section{Examples}

\subsection{2 strains of \mtu (included in eCAMBer package)}

We prepared one small example dataset incorporated into the eCAMBer package.
The input files for the dataset are in the folder {\tt ecamber/datasets/mtu2}.
To run eCAMBer on this dataset execute subsequently the following commands:
 
Formatting of the input:

\cmd{python ecamber.py -a f -w 4 -d mtu2}

The closure procedure (phase 1) of eCAMBer. 

\cmd{python ecamber.py -a ph1 -w 4 -d mtu2 -as refseq}

Phase 1 of eCAMBer (computing of gene clusters and refining annotations):

\cmd{python ecamber.py -a ph2 -w 4 -d mtu2 -as refseq}

Saving the output annotations to the folder {\tt ecamber/datasets/output} and
input for CAMBerVis to the folder {\tt ecamber/datasets/cambervis}:

\cmd{python ecamber.py -a out -w 4 -d mtu2 -as refseq}

\subsection{65 strains of \mtu (from PATRIC)}

Here we present a step-by-step example of applying eCAMBer to analize the 
dataset of 67 strains of \mtu strains, with genomes available in the PATRIC 
database.

\cmd{python ecamber.py -a d -w 4}

Then, eCAMBer will connect to the PATRIC ftp server and will provide you 
options to choose the database release:

\begin{MyVerbatim}
Platform: Linux
PATRIC: http://brcdownloads.vbi.vt.edu/patric2/
download_patric_genomes.py: downloads genome sequences and annotations from the PATRIC database.
0: genomes/      date: 16-Mar-2013 22:19 [current release]
1: genomes.Feb2012/      date: 05-Mar-2012 01:34
2: genomes.Jul2011/      date: 27-Jul-2011 10:03
3: genomes.Jun2011/      date: 08-Jun-2011 06:16
4: genomes.May2012/      date: 24-Jul-2012 21:19
5: genomes.Nov2012/      date: 24-Feb-2013 03:43
6: genomes.Oct2011/      date: 02-Dec-2011 11:07
7: genomes.Sept2012/     date: 05-Oct-2012 06:50
Select version's index: (default: 0 -current release):
\end{MyVerbatim} 

In this example we select the database from 24-Feb-2013 (number 5).

Next, eCAMBer asks to select the bacterial species. In this example we choose 
the default option --- \mtu. A list of all available \mtu strains will be 
displayed. We confirm download by 'd'. The dataset folder default name is 
'mtu65', it will be created at: {\tt ecamber/datasets/mtu65}.

Next, format the downloaded dataset as eCAMBer input:

\cmd{python ecamber.py -a f -w 4 -d mtu65}

Next, compute the closure procedure (phase 1) using annotations from PATRIC.
Use can also use annotations from RefSeq by specifying {\it -as refseq}:

\cmd{python ecamber.py -a ph1 -w 4 -d mtu65 -as patric}

Next, run the computing and cleaning of the gene clusters (phase 2):

\cmd{python ecamber.py -a ph2 -w 4 -d mtu65 -as patric}

Run this command to generate the output, including input for CAMBerVis:

\cmd{python ecamber.py -a out -w 4 -d mtu65 -as patric}

Copy the created folder {\tt ecamber/mtu65/cambervis} into the folder 
{\tt camber-vis/examples}. You may also change its name, for example: 
{\tt camber-vis/examples/mtu65}.

\subsection{20 strains of E. coli (from ColiScope)}

Download the dataset (with annotations formatted as eCAMBer input) from the 
eCAMBer project webpage. Unzip the package and put here:
{\tt ecamber/datasets/ecoli20}.

Next, run subsequently the following commands:

\cmd{python ecamber.py -a f -w 4 -d ecoli20 -as coliscope}

\cmd{python ecamber.py -a ph1 -w 4 -d ecoli20 -as coliscope}

\cmd{python ecamber.py -a ph2 -w 4 -d ecoli20 -as coliscope}

\cmd{python ecamber.py -a out -w 4 -d ecoli20 -as coliscope}

The last command generates the output. This output includes input for 
CAMBerVis. Copy the newly created folder 
{\tt ecamber/datasets/ecoli20/cambervis} into the folder 
{\tt camber-vis/examples}. You may also change its name, for example: 
{\tt camber-vis/examples/ecoli20}.

\end{document}
