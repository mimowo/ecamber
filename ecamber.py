import sys
import os
import time
import platform
import multiprocessing

def getCAMBerPathRun(run_path, cmd_path):
    full_path = os.path.abspath(os.path.join(run_path, cmd_path))
    ecamber_path = os.path.dirname(full_path)
    return ecamber_path + "/"

run_path = os.path.abspath(os.curdir)
cmd_path = sys.argv[0]
ecamber_path = getCAMBerPathRun(run_path, cmd_path)
os.chdir(ecamber_path)

sys.path += [ecamber_path + "/src/"]

from soft.utils.camber_params_utils import *
from soft.utils.camber_params_chk_utils import *
from soft.utils.camber_closure_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_graph_utils import *

if sys.version.startswith("3"):
    raw_input = input

def createCommandParams(attrs, parameters):
    new_cmd = ""
    for key in attrs:
        if key in parameters:
            new_cmd += " "+key+"="+parameters[key]
    return new_cmd


def showPATRIC(parameters):
    os.chdir(os.path.abspath("src/soft/download/"))

    params = ["W"]
    passed_params = createCommandParams(params, parameters)                
    
    os.system(python_cmd + " show_patric_genomes.py" + passed_params)
    os.chdir("../../../")

def downloadGenomes(parameters):
    os.chdir(os.path.abspath("src/soft/download/"))
    
    params = ["W", "PATRIC_FTP", "NCBI_HTTP"]
    passed_params = createCommandParams(params, parameters)

    if parameters.get("SOURCE", "NCBI") == "NCBI":
        if os.system(python_cmd + " download_ncbi_genomes.py" + passed_params): exit()
    else:
        if os.system(python_cmd + " download_patric_genomes.py" + passed_params): exit()
    os.chdir("../../../")

def output(parameters):
    os.chdir(os.path.abspath("src/soft/output/"))
    
    params = ["D", "EP", "W", "TE", "HSSP", "PID_TYPE", "PID", "PLC", "LCM", "AS", "STEP"]
    passed_params = createCommandParams(params, parameters)
    
    if os.system(python_cmd + " out1_cluster_names.py" + passed_params): exit()
    if os.system(python_cmd + " out2_standard_output.py" + passed_params): exit()
    if os.system(python_cmd + " out3_cambervis_input.py" + passed_params): exit()
    if os.system(python_cmd + " out4_gene_sequences.py" + passed_params): exit()
    if os.system(python_cmd + " out5_clusters_table.py" + passed_params): exit()
    if os.system(python_cmd + " out6_gwamar_input.py" + passed_params): exit()
    
    os.chdir("../../../")

def format_sequences(parameters):
    os.chdir(os.path.abspath("src/soft/formats/"))
    
    params = ["D", "W"]
    passed_params = createCommandParams(params, parameters)
    
    if os.system(python_cmd + " format1_sequences.py" + passed_params): exit()
    if os.system(python_cmd + " format2_databases.py" + passed_params): exit()
    os.chdir("../../../")

def format_annotations(parameters):
    os.chdir(os.path.abspath("src/soft/formats/"))
    
    params = ["D", "W"]
    passed_params = createCommandParams(params, parameters)
    if os.system(python_cmd + " format3_annotations.py" + passed_params): exit()
    os.chdir("../../../")

def prodigal(parameters):
    os.chdir(os.path.abspath("src/soft/prodigal/"))
    
    params = ["D", "W"]
    passed_params = createCommandParams(params, parameters)
    
    if os.system(python_cmd + " pro1_train_prodigal.py" + passed_params): exit()
    if os.system(python_cmd + " pro2_run_prodigal.py" + passed_params): exit()
    if os.system(python_cmd + " pro3_format_anns.py" + passed_params): exit()
    os.chdir("../../../")

def prepare(parameters):
    os.chdir(os.path.abspath("src/soft/prepare/"))

    params = ["D", "EP", "W", "TE", "HSSP", "PID_TYPE", "PID", "PLC", "LCM", "AS"]
    passed_params = createCommandParams(params, parameters)        
    
    t0_time = time.time()
    if os.system(python_cmd + " prepare1_annotations.py" + passed_params): exit()
    if os.system(python_cmd + " prepare2_contigs_mapping.py" + passed_params): exit()
    if os.system(python_cmd + " prepare3_ann_names.py" + passed_params): exit()
    t1_time = time.time()
    os.chdir("../../../")
    logExecutionTime(t1_time - t0_time, "prepare")
    

def closure(parameters):
    is_computed = False
    os.chdir(os.path.abspath("src/soft/closure/"))

    params = ["D", "EP", "W", "TE", "BE", "HSSP", "PID_TYPE", "PID", "PLC", "LCM", "AS"]
    passed_params = createCommandParams(params, parameters)                
    
    max_iteration = int(parameters["M"])
    
    t0_time = s_time = time.time()
    if os.system(python_cmd + " ext1_prepare_blast_queries.py" + passed_params): exit()
    if os.system(python_cmd + " ext2_compute_blasts.py" + passed_params): exit()
    if os.system(python_cmd + " ext3_update_blast_results.py" + passed_params): exit()
    
    t1_time = time.time()
    
    os.chdir("../../../")
    logExecutionTime(t1_time - t0_time, "blasts_update")
    os.chdir(os.path.abspath("src/soft/closure/"))

    for i in range(max_iteration):
        t0_time = time.time()
        os.chdir("../../../")
        iteration = currentClosureIteration()
        is_computed = isClosureComputed()
        os.chdir(os.path.abspath("src/soft/closure/"))
        
        if is_computed:
            print("The closure procedure is already computed.")
            break
        
        if os.system(python_cmd + " loop1_blast_queries.py" + passed_params): exit()
        if os.system(python_cmd + " loop2_compute_blasts.py" + passed_params): exit()
        
        t1_time = time.time()
        os.chdir("../../../")
        logExecutionTime(t1_time - t0_time, "closure_" + str(i) + "_blasts")
        os.chdir(os.path.abspath("src/soft/closure/"))
        
        if os.system(python_cmd + " loop3_update_seqs_mapping.py" + passed_params): exit()
        if os.system(python_cmd + " loop4_parse_blasts_comp.py" + passed_params): exit()
        if os.system(python_cmd + " loop5_parse_blasts_new.py" + passed_params): exit()
        if os.system(python_cmd + " loop6_merge_annotations.py" + passed_params): exit()
        t2_time = time.time()
        
        os.chdir("../../../")
        logExecutionTime(t2_time - t0_time, "closure_" + str(i) + "_tot")
        os.chdir(os.path.abspath("src/soft/closure/"))
        
    if i == max_iteration - 1 and is_computed == False:
        print("Additional")
        if os.system(python_cmd + " loop1_blast_queries.py" + passed_params): exit()
        if os.system(python_cmd + " loop3_update_seqs_mapping.py" + passed_params): exit()
        os.chdir("../../../")     

    e_time = time.time()
    print("Closure time: " + str(e_time-s_time))
    os.chdir("../../../")

def graph(parameters):
    os.chdir(os.path.abspath("src/soft/graph/"))
    
    params = ["D", "EP", "W", "TE", "HSSP", "PID_TYPE", "PID", "PLC", "LCM", "AS"]
    passed_params = createCommandParams(params, parameters)                
    
    t0_time = time.time()
    if os.system(python_cmd + " graph1_final_annotations.py" + passed_params): exit()
    if os.system(python_cmd + " graph2_mgsseq_mapping.py" + passed_params): exit()
    if os.system(python_cmd + " graph3_seq_blast_edges.py" + passed_params): exit()
    if os.system(python_cmd + " graph4_comb_it_blast_edges.py" + passed_params): exit()
    if os.system(python_cmd + " graph5_comb_blast_edges.py" + passed_params): exit()
    if os.system(python_cmd + " graph6_seq_multigene_edges.py" + passed_params): exit()
    if os.system(python_cmd + " graph7_comb_multigene_edges.py" + passed_params): exit()
    t1_time = time.time()
    os.chdir("../../../")
    logExecutionTime(t1_time - t0_time, "graph")
    
    
def components(parameters):
    os.chdir(os.path.abspath("src/soft/components/"))

    params = ["D", "EP", "W", "TE", "HSSP", "PID_TYPE", "PID", "PLC", "LCM", "AS"]
    passed_params = createCommandParams(params, parameters)                

    t0_time = time.time()
    if os.system(python_cmd + " cc1_compute_conn_comps.py" + passed_params): exit()
    if os.system(python_cmd + " cc2_save_details_strain.py" + passed_params): exit()
    t1_time = time.time()
    os.chdir("../../../")
    logExecutionTime(t1_time - t0_time, "components")
    
def tisvoting(parameters):
    os.chdir(os.path.abspath("src/soft/tisvoting/"))
    
    params = ["D", "EP", "W", "TE", "HSSP", "PID_TYPE", "PID", "PLC", "LCM", "AS", "STEP", "TVR", "TVC"]
    passed_params = createCommandParams(params, parameters)

    t0_time = time.time()
    if os.system(python_cmd + " tis1_compute_subcomponents.py" + passed_params): exit()
    if os.system(python_cmd + " tis2_count_annotations.py" + passed_params): exit()
    if os.system(python_cmd + " tis3_voted_annotations.py" + passed_params): exit()
    if os.system(python_cmd + " tis4_copy_graph.py" + passed_params): exit()
    t1_time = time.time()
    os.chdir("../../../")
    logExecutionTime(t1_time - t0_time, "tisvoting")
    

def refinement(parameters):
    os.chdir(os.path.abspath("src/soft/refinement/"))
    
    params = ["D", "EP", "W", "TE", "HSSP", "PID_TYPE", "PID", "PLC", "LCM", "AS", "STEP"]
    passed_params = createCommandParams(params, parameters)

    t0_time = time.time()
    if os.system(python_cmd + " rf1_classify_clusters.py" + passed_params): exit()
    if os.system(python_cmd + " rf2a_save_edges_strain_pairs.py" + passed_params): exit()
    if os.system(python_cmd + " rf2b_save_edges_strain_pairs.py" + passed_params): exit()
    if os.system(python_cmd + " rf3_remove_edges_strain_pairs.py" + passed_params): exit()
    if os.system(python_cmd + " rf4a_compute_conn_comps.py" + passed_params): exit()
    if os.system(python_cmd + " rf4b_compute_conn_comps.py" + passed_params): exit()
    if os.system(python_cmd + " rf5_save_ref_annotations.py" + passed_params): exit()
    if os.system(python_cmd + " rf6_copy_graph.py" + passed_params): exit()
    if os.system(python_cmd + " rf7_copy_rem_edges.py" + passed_params): exit()
    t1_time = time.time()
    os.chdir("../../../")
    logExecutionTime(t1_time - t0_time, "refinement")
    
    
def cleanup(parameters):
    os.chdir(os.path.abspath("src/soft/cleanup/"))
    
    params = ["D", "EP", "W", "TE", "HSSP", "PID_TYPE", "PID", "PLC", "LCM", "AS", "STEP", "CUP", "CUR", "CUL", "CUV"]
    passed_params = createCommandParams(params, parameters)

    t0_time = time.time()
    if os.system(python_cmd + " cleanup1_overlapped_mgs.py" + passed_params): exit()
    if os.system(python_cmd + " cleanup2_clusters_stats.py" + passed_params): exit()
    if os.system(python_cmd + " cleanup3_rem_clusters.py" + passed_params): exit()
    if os.system(python_cmd + " cleanup4_save_annotations.py" + passed_params): exit()
    if os.system(python_cmd + " cleanup5_mgsseq_mapping.py" + passed_params): exit()
    if os.system(python_cmd + " cleanup6_update_graphs.py" + passed_params): exit()
    t1_time = time.time()
    os.chdir("../../../")
    logExecutionTime(t1_time - t0_time, "cleanup")
    

def analysis_input(parameters):
    os.chdir(os.path.abspath("src/soft/analysis_input/"))
    
    params = ["D", "EP", "W", "TE", "HSSP", "PID_TYPE", "PID", "PLC", "LCM", "AS"]
    passed_params = createCommandParams(params, parameters)

    t0_time = time.time()
    if os.system(python_cmd + " input0_countORFs.py" + passed_params): exit()
    if os.system(python_cmd + " input1_strain_ann_counts.py" + passed_params): exit()
    if os.system(python_cmd + " input2_strain_genome_lengths.py" + passed_params): exit()
    if os.system(python_cmd + " input3_seq_counts.py" + passed_params): exit()
    t1_time = time.time()
    os.chdir("../../../")
    logExecutionTime(t1_time - t0_time, "analysis_closure")
    


def analysis_closure(parameters):
    os.chdir(os.path.abspath("src/soft/analysis_closure/"))
    
    params = ["D", "EP", "W", "TE", "HSSP", "PID_TYPE", "PID", "PLC", "LCM", "AS"]
    passed_params = createCommandParams(params, parameters)

    t0_time = time.time()
    if os.system(python_cmd + " analysis1_iter_gene_counts.py" + passed_params): exit()
    if os.system(python_cmd + " analysis2_strain_mgs_count.py" + passed_params): exit()
    if os.system(python_cmd + " analysis3_overlapped_mgs_strain.py" + passed_params): exit()
    if os.system(python_cmd + " analysis4_strain_overlapping_pairs.py" + passed_params): exit()
    if os.system(python_cmd + " analysis5_start_codons.py" + passed_params): exit()
    if os.system(python_cmd + " analysis6_strain_seq_counts.py" + passed_params): exit()
    #if os.system(python_cmd + " analysis7_tis_consistancy.py" + passed_params): exit()
    t1_time = time.time()
    os.chdir("../../../")
    logExecutionTime(t1_time - t0_time, "analysis_closure")
    

def analysis_post(parameters):
    os.chdir(os.path.abspath("src/soft/analysis_post/"))
    
    params = ["D", "EP", "W", "TE", "HSSP", "PID_TYPE", "PID", "PLC", "LCM", "AS", "STEP"]
    passed_params = createCommandParams(params, parameters)

    t0_time = time.time()
    if os.system(python_cmd + " postanalysis1_tis_consistancy.py" + passed_params): exit()
    if os.system(python_cmd + " postanalysis2_missing_genes_strains.py" + passed_params): exit()
    if os.system(python_cmd + " postanalysis3_save_cc_distribution.py" + passed_params): exit()
    if os.system(python_cmd + " postanalysis4_core-pan-genome.py" + passed_params): exit()
    if os.system(python_cmd + " postanalysis5_overlapped_mgs_strain.py" + passed_params): exit()
    if os.system(python_cmd + " postanalysis6_strain_overlapping_pairs.py" + passed_params): exit()
    if os.system(python_cmd + " postanalysis7_component_stats.py" + passed_params): exit()
#    if os.system(python_cmd + " postanalysis8_component_table.py" + passed_params): exit()
    if os.system(python_cmd + " postanalysis9_strain_mgs_count.py" + passed_params): exit()
    if os.system(python_cmd + " postanalysis10_start_codons.py" + passed_params): exit()
    #if os.system(python_cmd + " postanalysis11_tis_inconsistancy_exmples.py" + passed_params): exit()
    
    t1_time = time.time()
    os.chdir("../../../")
    logExecutionTime(t1_time - t0_time, "analysis_post")
    
def mutations(parameters):
    os.chdir(os.path.abspath("src/alnsoft/mutations/"))
    
    params = ["D", "EP", "W", "TE", "HSSP", "PID_TYPE", "PID", "PLC", "LCM", "AS", "STEP", "MUT_MAX_INDELS"]
    passed_params = createCommandParams(params, parameters)

    t0_time = time.time()
    
    if os.system(python_cmd + " muts0_save_ref_annotations.py" + passed_params): exit()
    if os.system(python_cmd + " muts0_save_strains_ordered.py" + passed_params): exit()
    if os.system(python_cmd + " muts1_point_prom_mutations.py" + passed_params): exit()
    if os.system(python_cmd + " muts2_point_aa_mutations.py" + passed_params): exit()
    if os.system(python_cmd + " muts3_combine_point_mutations.py" + passed_params): exit()
    if os.system(python_cmd + " muts4_gene_mutations.py" + passed_params): exit()
    t1_time = time.time()
    os.chdir("../../../")
    logExecutionTime(t1_time - t0_time, "mutations")

def phylogeny_rec(parameters):
    os.chdir(os.path.abspath("src/alnsoft/phylogeny_rec/"))
    
    params = ["D", "EP", "W", "TE", "HSSP", "PID_TYPE", "PID", "PLC", "LCM", "AS", "STEP","TREE_SOFT"]
    passed_params = createCommandParams(params, parameters)

    t0_time = time.time()
    if os.system(python_cmd + " resolve_all_subtrees.py" + passed_params): exit()
    t1_time = time.time()
    os.chdir("../../../")
    logExecutionTime(t1_time - t0_time, "phylogeny_rec")

def phylogeny_genes(parameters):
    os.chdir(os.path.abspath("src/alnsoft/phylogeny_genes/"))
    
    params = ["D", "EP", "W", "TE", "HSSP", "PID_TYPE", "PID", "PLC", "LCM", "AS", "STEP","TREE_SOFT"]
    passed_params = createCommandParams(params, parameters)

    t0_time = time.time()
    if os.system(python_cmd + " compute_tree.py" + passed_params): exit()
    t1_time = time.time()
    os.chdir("../../../")
    logExecutionTime(t1_time - t0_time, "phylogeny_new")
    
def phylogeny(parameters):
    os.chdir(os.path.abspath("src/alnsoft/phylogeny/"))
    
    params = ["D", "EP", "W", "TE", "HSSP", "PID_TYPE", "PID", "PLC", "LCM", "AS", "STEP","TREE_SOFT","RAXML_REP","RAXML_TYPE"]
    passed_params = createCommandParams(params, parameters)

    print(passed_params)

    t0_time = time.time()
    if os.system(python_cmd + " t1_prepare_alignments.py" + passed_params): exit()
    if os.system(python_cmd + " t2_compute_subset_tree.py" + passed_params): exit()
    if os.system(python_cmd + " t3_resolve_clusters.py" + passed_params): exit()
    t1_time = time.time()
    os.chdir("../../../")
    logExecutionTime(t1_time - t0_time, "phylogeny")

def phylogeny2(parameters):
    os.chdir(os.path.abspath("src/alnsoft/phylogeny/"))
    
    params = ["D", "EP", "W", "TE", "HSSP", "PID_TYPE", "PID", "PLC", "LCM", "AS", "STEP","TREE_SOFT","RAXML_REP","RAXML_TYPE"]
    passed_params = createCommandParams(params, parameters)

    print(passed_params)

    t0_time = time.time()
    if os.system(python_cmd + " t2_compute_subset_tree.py" + passed_params): exit()
    if os.system(python_cmd + " t3_resolve_clusters.py" + passed_params): exit()
    t1_time = time.time()
    os.chdir("../../../")
    logExecutionTime(t1_time - t0_time, "phylogeny2")

def alignments(parameters):
    dir = os.path.abspath("src/alnsoft/alignments/")
    os.chdir(dir)
    
    params = ["D", "EP", "W", "TE", "HSSP", "PID_TYPE", "PID", "PLC", "LCM", "AS", "STEP"]
    passed_params = createCommandParams(params, parameters)

    t0_time = time.time()
    
    #if os.system(python_cmd + " algs0_save_ref_annotations.py" + passed_params): exit()
    if os.system(python_cmd + " algs1_prepare_sequences_simple.py" + passed_params): exit()
    if os.system(python_cmd + " algs2_compute_alignments_aa.py" + passed_params): exit()
    if os.system(python_cmd + " algs3_find_promotors.py" + passed_params): exit()
    if os.system(python_cmd + " algs4_prepare_prom_sequences_simple.py" + passed_params): exit()
    if os.system(python_cmd + " algs5_compute_prom_alignments.py" + passed_params): exit()
    
    t1_time = time.time()
    os.chdir("../../../")
    logExecutionTime(t1_time - t0_time, "alignments")

def rna_muts(parameters):
    dir = os.path.abspath("src/alnsoft/rna_muts/")
    os.chdir(dir)
    
    params = ["D", "EP", "W", "GI"]
    passed_params = createCommandParams(params, parameters)

    t0_time = time.time()
    
    #if os.system(python_cmd + " algs0_save_ref_annotations.py" + passed_params): exit()
    if os.system(python_cmd + " rna1_blasts.py" + passed_params): exit()
    if os.system(python_cmd + " rna2_parse.py" + passed_params): exit()
    if os.system(python_cmd + " rna3_aln.py" + passed_params): exit()
    if os.system(python_cmd + " rna4_muts.py" + passed_params): exit()

    t1_time = time.time()
    os.chdir("../../../")
    logExecutionTime(t1_time - t0_time, "rna_muts")

def showHELP(parameters):
    print("HELP")
    for p in parameters:
        print(p + ":\t" + parameters[p])

    print("HELP-END")

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln = True)
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    ret = checkParameters(parameters)
    if ret < 0:
        exit()

    python_cmd = sys.executable 
    action = parameters["A"].upper()
    
    if action == "H":
        showHELP(parameters)
    elif action == "S":
        showPATRIC(parameters)
    elif action == "D":
        downloadGenomes(parameters)
    elif action == "F":
        format_sequences(parameters)
        format_annotations(parameters)
    elif action == "FS":
        format_sequences(parameters)
    elif action == "FA":
        format_annotations(parameters)
    elif action == "PR":
        format_sequences(parameters)
        prodigal(parameters)
    elif action == "P":
        prepare(parameters)
    elif action == "C":
        closure(parameters)
    elif action == "CL":
        graph(parameters)
        components(parameters)
    elif action == "RF":
        if parameters["STEP"].upper()=="D": parameters["STEP"] = "1"
        refinement(parameters)
    elif action == "TV":
        if parameters["STEP"].upper()=="D": parameters["STEP"] = "2"
        tisvoting(parameters)
    elif action == "CU":
        if parameters["STEP"].upper()=="D": parameters["STEP"] = "3"
        cleanup(parameters)
    elif action == "AI":
        analysis_input(parameters)
    elif action == "AC":
        analysis_closure(parameters)
    elif action == "AP":
        if parameters["STEP"].upper()=="D": parameters["STEP"] = "3"
        analysis_post(parameters)
    elif action == "AALL":
        analysis_input(parameters)
        analysis_closure(parameters)
        if parameters["STEP"].upper()=="D": parameters["STEP"] = "3"
        analysis_post(parameters)
    elif action == "CV":
        cambervis(parameters)
    elif action == "OUT":
        output(parameters)
    elif action == "PH1":
        prepare(parameters)
        closure(parameters)
    elif action == "PH2":
        graph(parameters)
        components(parameters)
        if parameters["DOREF"].upper() == "Y":
            parameters["STEP"] = "1"
            refinement(parameters)
            parameters["STEP"] = "2"
            tisvoting(parameters)
            parameters["STEP"] = "3"
            cleanup(parameters)
        else:
            parameters["STEP"] = "1"
            tisvoting(parameters)
            parameters["STEP"] = "2"
            cleanup(parameters)
    elif action == "ALL":
        #PH1
        format_sequences(parameters)
        format_annotations(parameters)
        prepare(parameters)
        closure(parameters)
        #PH2
        graph(parameters)
        components(parameters)
        if parameters["DOREF"] == "Y":
            parameters["STEP"] = "1"
            refinement(parameters)
            parameters["STEP"] = "2"
            tisvoting(parameters)
            parameters["STEP"] = "3"
            cleanup(parameters)
        else:
            parameters["STEP"] = "1"
            tisvoting(parameters)
            parameters["STEP"] = "2"
            cleanup(parameters)
        output(parameters)
        
    elif action == "ALN":
        alignments(parameters)    
    elif action == "PHYREC":
        phylogeny_rec(parameters)
    elif action == "PHYG":
        phylogeny_genes(parameters)
    elif action == "PHY":
        phylogeny(parameters)
    elif action == "PHY2":
        phylogeny2(parameters)
    elif action == "MUT":
        mutations(parameters)
    elif action == "PALL":
        alignments(parameters)
        phylogeny_genes(parameters)
        mutations(parameters)
    elif action == "PALL2": 
        alignments(parameters)
        phylogeny(parameters)
        mutations(parameters)
    elif action == "RNA":
        rna_muts(parameters)
    else:
        print("Value: '"+action+"' of the provided action '-a' parameter is not recognized.")
        
        