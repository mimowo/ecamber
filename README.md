### What is this repository for? ###

eCAMBer: efficient support for large-scale comparative analysis of multiple bacterial strains

Project webpage: http://bioputer.mimuw.edu.pl/ecamber/

Please don’t hesitate to contact us with any comments and suggestion or if you are interested in co-developing this
software

### How do I get set up? ###

This software is written in Python, thus Python 2 or 3 is required to run eCAMBer. 
User's manual is available at the project website.

### Who do I talk to?

m.wozniak@mimuw.edu.pl
