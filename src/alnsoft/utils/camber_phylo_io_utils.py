from alnsoft.structs.phylo_tree import *

def strainAureusNames(strain_id):
    if strain_id.startswith("aureus_") and strain_id != "aureus_M1015":
        strain_id = strain_id[7:]
    return strain_id

def getTreeStrainList(tree_line):
    tree_line = tree_line.strip().strip(";")
    tree_line = tree_line.replace("(", ";")
    tree_line = tree_line.replace(")", ";")
    tree_line = tree_line.replace(",", ";")
    strains_list_tmp = tree_line.split(";")
    strains_list = []
    for strain_id_tmp in strains_list_tmp:
        if strain_id_tmp.count(":") > 0: strain_id = strain_id_tmp.split(":")[0]
        else: strain_id = strain_id_tmp
        if len(strain_id) == 0: continue
        strains_list.append(strain_id)
    return strains_list

def splitStrSubtrees(strains_str):
    strains_strs = []
    curr_str = ""
    count = 0
    for i in range(0, len(strains_str), 1):
        if(strains_str[i]=="("):
            curr_str += strains_str[i]
            count += 1
        elif(strains_str[i]==")"):
            curr_str += strains_str[i]
            count -= 1
        elif(count == 0 and strains_str[i]==","):
            strains_strs.append(curr_str)
            curr_str = ""
        else:
            curr_str += strains_str[i]
    strains_strs.append(curr_str)
    
    return strains_strs

def readResTreeRec(tree, tree_str, strains_list, strains_map={}, e_len=None, support=False):
    last_index = tree_str.rfind(")")
    if last_index == -1:
        tokens = tree_str.split(":")
        strain_name = tokens[0]
        strain_name = strainAureusNames(strain_name)

        try:
            strain_index = strains_list.index(strain_name)
        except:
            return None
            print("error", strain_name, tree_str)
            exit()
        strain_label = strains_map.get(strain_name, strain_name)
        strain_node = PhyloTreeNode(strain_index)
        strain_node.setLabel(strain_label)

        if len(tokens) == 2:
            e_attrs = tokens[1].split("|")
            for e_attr in e_attrs:
                if e_attr in ["b", "w"]:
                    strain_node.ecolor = e_attr
                elif e_len == None:
                    try:
                        if support == False:
                            strain_node.e_len = float(e_attr)
                        else:
                            strain_node.e_len = float(1)
                    except:
                        pass
                else:
                    strain_node.e_len = e_len
        else:
            if e_len != None:
                strain_node.e_len = e_len

        tree.addLeafNode(strain_node)
        return strain_node
    else:
        node_id = tree.createNodeID("")
        node = PhyloTreeNode(node_id)
        if last_index + 2 < len(tree_str):
            attr_index = tree_str.rfind(":")
            if attr_index > 0:
                if support == False:
                    e_attrs = tree_str[attr_index+1:].split("|")
                else:
                    e_attrs = tree_str[last_index+1:attr_index].split("|")
                    #print(e_attrs, e_len, tree_str)
                for e_attr in e_attrs:
                    if e_attr in ["b", "w"]:
                        node.ecolor = e_attr
                    elif e_len == None:
                        try:
                            node.e_len = float(e_attr)
             #               print(node.e_len)
                        except:
                            pass
                    else:
                        node.e_len = e_len

            else:
                if e_len != None:
                    node.e_len = e_len
        tree.addInternalNode(node)
        subtrees_str = splitStrSubtrees(tree_str[1:last_index])
        
        for subtree_str in subtrees_str:
            subtree_node = readResTreeRec(tree, subtree_str, strains_list, strains_map, e_len, support)
            if subtree_node != None:
                node.addChildren(subtree_node)
                subtree_node.parent = node

        if len(node.children) == 0:
            return None
        return node

def readResTree(tree_text, strains_list=None, strains_map={}, e_len=None, support=False):
    if strains_list == None:
        strains_list = getTreeStrainList(tree_text)
     
    tree = PhyloTree(len(strains_list)) 
    root = readResTreeRec(tree, tree_text, strains_list, strains_map, e_len, support=support)
    tree.setRoot(root)

    tree.setLeavesByIDs(range(tree.leaves_count))
    return tree

def readPhyloTrees(input_fh, strains_list=None,  strains_map={}, e_len=None, support=False):
    trees_list = []
    lines = input_fh.readlines()

    tree_str = ""
    for line in lines:
        line = line.strip()
        if line.endswith(";"):
            tree_str += line;
            if len(tree_str) < 1:
                continue
            if line.startswith("#"):
                continue
            if strains_list == None:
                tree = readResTree(tree_str, strains_list=None, strains_map=strains_map, e_len=e_len, support=support)
            else:
                tree = readResTree(tree_str, strains_list=strains_list, strains_map=strains_map, e_len=e_len, support=support)
            trees_list.append(tree)
            tree_str = ""
        else:
            tree_str += line;
    return trees_list
