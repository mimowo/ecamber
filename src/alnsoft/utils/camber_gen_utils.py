import sys

from soft.utils.camber_seq_utils import *
from alnsoft.utils.camber_aln_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from alnsoft.structs.aln_filenames import *
from soft.utils.camber_utils import parameters_map

if sys.version.startswith("3"): raw_input = input

def intersection(list_a, list_b):
    return [val for val in list_a if val in list_b]

def readStrainsOrdered():
    global strains
    input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains_ordered.txt"
    if not os.path.exists(input_fn):
        input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains.txt"
    strains_fh = open(input_fn)
    strains = readStrainsFromFile(strains_fh)
    return strains

def saveStrainsOrdered(strains_ordered):
    output_fh = open(parameters_map["CAMBER_DATASET_PATH"] + "strains_ordered.txt", "w")
    for strain_id in strains_ordered:
        output_fh.write(strain_id + "\n")
    output_fh.close()    
    return None

def readClusterGeneRefIDs(input_fn=""):
    cluster_gene_ref_ids = {}
    #input_fn = parameters_map["DATASET_OUTPUT"] + "cluster_gene_ref_id.txt"
    #if not os.path.exists(input_fn):
    if len(input_fn)==0 or not os.path.exists(input_fn):    
        overwriteParameters(sys.argv)
        parameters = readParameters()
        closure_iteration = currentClosureIteration()
        ph2_iteration = currentPh2Iteration(default = 3) 
        input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration) + "cluster_gene_ref_id.txt"

    if os.path.exists(input_fn):
        input_fh = open(input_fn)
        for line in input_fh.readlines()[1:]:
            tokens = line.strip().split()
            if len(tokens) < 3: continue
            
            cluster_gene_ref_ids[tokens[0]] = tokens[2]
        input_fh.close()
    return cluster_gene_ref_ids

    
def readRefAnns(input_fh):
    ref_anns = {}
    
    for line in input_fh.readlines():
        line = line.strip()
        if line.startswith("#"):
            continue
        tokens = line.split("\t")
        cluster_id = tokens[0]
        mg_tokens = tokens[1].split()
        gene_id = tokens[2]
        
        if len(gene_id) <= 1:
            continue
        
        len_tokens = tokens[3].split()
        end = int(mg_tokens[0])
        strand = mg_tokens[1]
        contig_id = mg_tokens[2]
        
        length = 0
        
        for len_token in len_tokens:
            if not len_token.startswith("#"):
                continue
            len_token = len_token[1:]
            if len_token.startswith("*"):
                len_token = len_token[1:]
            len_token = len_token.split(":")[0]
            length = int(len_token)
        ref_anns[cluster_id] = (end, length, strand, contig_id, gene_id)
    
    return ref_anns

def readRefStrain():
    input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "ref_strain.txt"
    if not os.path.exists(input_fn): return ""
    
    input_fh = open(input_fn);
    line = input_fh.readline()
    input_fh.close()
    return line.strip()

def readRefStrains(strains_set=None):
    ref_strains = []
    input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "ref_strain.txt"
    if not os.path.exists(input_fn):
        
        while len(ref_strains) == 0:
            strain_input = raw_input("Please provide reference strain identifier(s) (separated by ';' if multiple): ").strip()
            strain_ids = strain_input.split(";")
            for strain_id in strain_ids:
                if strains_set == None or len(strains_set) == 0 or strain_id in strains_set:
                    ref_strains.append(strain_id)
            if len(ref_strains) == 0:
                print("Provided strain identifier(s) do not match strain identifiers in the dataset.")
        input_fh = open(input_fn, "w")
        for strain_id in ref_strains:
            input_fh.write(strain_id + "\n")
        input_fh.close()
            
        return ref_strains
    
    input_fh = open(input_fn);
    for line in input_fh.readlines():
        line = line.strip()
        ref_strains.append(line)
    input_fh.close()
    return ref_strains
