import sys
from collections import deque

from soft.utils.camber_params_utils import *
from soft.utils.camber_io_utils import *
from alnsoft.utils.camber_aln_utils import readSequencesMapFromAlignmentFile

def getTreeStrainList(tree_line):
    tree_line = tree_line.strip().strip(";")
    tree_line = tree_line.replace("(", ";")
    tree_line = tree_line.replace(")", ";")
    tree_line = tree_line.replace(",", ";")
    strains_list_tmp = tree_line.split(";")
    strains_list = []
    for strain_id_tmp in strains_list_tmp:
        if strain_id_tmp.count(":") > 0: strain_id = strain_id_tmp.split(":")[0]
        else: strain_id = strain_id_tmp
        if len(strain_id) == 0: continue
        strains_list.append(strain_id)
    return strains_list


def readStrainSubsetsMap(input_fn):
    subsets_map = {}
    input_fh = open(input_fn)
    lines = input_fh.readlines()
    input_fh.close()
    
    for line in lines:
        tokens = line.strip().split("\t")
        if len(tokens) >= 4:
            key_id = int(tokens[0])
            strains_tmp = set(tokens[1:])
            subsets_map[key_id] = strains_tmp
    return subsets_map


def findSubsetsTree(tree):
    queue = deque([])
    tree.root.computeSubstrains()
    subsets = []
    
    queue.append(tree.root)
    while queue:
        node = queue.popleft()
        if len(node.children) > 2:
            subsets.append(sorted(node.substrains))
        for child_node in node.children:
            queue.append(child_node)
    return subsets

def findNonBinarySubsetNode(tree, exc_str_subsets = None):
    queue = deque([])
    tree.root.computeSubstrains()
    
    queue.append(tree.root)
    while queue:
        node = queue.popleft()
        if len(node.children) > 2:
            if exc_str_subsets != None and not setToStr(node.substrains) in exc_str_subsets:
                return node
        for child_node in node.children:
            queue.append(child_node)
    return None

def findNonBinarySubset(tree):
    node = findNonBinarySubsetNode(tree)
    if node == None:
        return None
    return node.substrains
  
def readCurrentSubsetID(input_fn):
    input_fh = open(input_fn)
    line = input_fh.readline()
    input_fh.close()
    tokens = line.strip().split()
    if len(tokens) < 1:
        return "0"
    return tokens[0]

def setToStr(subset):
    txt = ""
    for strain_id in sorted(subset):
        txt += str(strain_id) + " "
    return txt.strip()

def replaceSubnode(tree, node, subtree):
    parent = node.parent
    subtree_subset = subtree.root.substrains
    subtree_subset_str = setToStr(subtree_subset)
    chilren_old = parent.children
    parent.children = []
    for child_node in chilren_old:
        child_subset = child_node.substrains
        child_subset_str = setToStr(child_subset)
        if subtree_subset_str == child_subset_str:
            subtree.root.parent = parent
            subtree.root.e_len = child_node.e_len 
            parent.children.append(subtree.root)
            
        else:
            parent.children.append(child_node)
    return tree


