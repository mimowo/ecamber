from soft.utils.camber_progress import shift

def readSequencesMapFromAlignmentFile(align_fh):
    seq_map = {};
    seq_map_rev = {};
    lines = align_fh.readlines();
    
    try: first_line = lines[0];
    except: return seq_map;
    
    tokens = first_line.split();
    seq_count = int(tokens[0]);

    for i in range(seq_count):
        tokens = lines[i + 1].split();
        seq_id = tokens[0]

        seq_map[seq_id] = "";
        seq_map_rev[i] = seq_id;

    for i in range(len(lines)):
        seq_num = (i % (seq_count + 1));
        if(seq_num > 0):
            parts = lines[i].split();
            seq_id = seq_map_rev[seq_num - 1];
            if(i <= seq_count):
                good_parts = parts[1:len(parts)];
            else:
                good_parts = parts;

            for part in good_parts:
                seq_map[seq_id] += part;

    return seq_map;

def savePHYLIPalignements(alns, output_fh, sorting = None):
    if not "OUTSTRAIN" in alns.keys():
        if sorting == None:
            strains_ord = list(alns.keys())
        else:
            strains_ord = sorted(alns.keys(), key=sorting)
    else:
        if sorting == None:
            strains_ord = ["OUTSTRAIN"] + list(set(alns.keys()) - set(["OUTSTRAIN"]))
        else:
            strains_ord = ["OUTSTRAIN"] + sorted(set(alns.keys()) - set(["OUTSTRAIN"]), key=sorting)
                
    strain_f_id = strains_ord[0]
    aln_len = len(alns[strain_f_id])
    output_fh.write(str(len(alns))+" " + str(aln_len) + "\n")
    combined_lines = {}
    for strain_id in strains_ord:
        combined_lines[strain_id] = ""
        
    for i in range(0, aln_len, 50):
        for strain_id in strains_ord:
            if i < 50: 
                if strain_id != "OUTSTRAIN":
                    combined_lines[strain_id] = shift(strain_id[:10], 10) + " "
                else:
                    combined_lines[strain_id] = shift("OUTSTRAIN", 10) + " "
            else:
                combined_lines[strain_id] = shift(" ", 10) + " "
            combined_lines[strain_id] += alns[strain_id][i:min(i+10, aln_len)] + " "
            if i+10 > aln_len:
                continue
            combined_lines[strain_id] += alns[strain_id][i+10:min(i+20, aln_len)] + " "
            if i+20 > aln_len:
                continue
            combined_lines[strain_id] += alns[strain_id][i+20:min(i+30, aln_len)] + " "
            if i+30 > aln_len:
                continue
            combined_lines[strain_id] += alns[strain_id][i+30:min(i+40, aln_len)] + " "
            if i+40 > aln_len:
                continue
            combined_lines[strain_id] += alns[strain_id][i+40:min(i+50, aln_len)] + " "
        for strain_id in strains_ord:
            output_fh.write(combined_lines[strain_id] + "\n")
        output_fh.write("\n")
        for strain_id in strains_ord:
            combined_lines[strain_id] = ""
    return None        
