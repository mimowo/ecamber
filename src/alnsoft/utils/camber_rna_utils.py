def readGeneIDs(input_fh, strain=None):
    gene_ids = {}

    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) < 4:
            continue
        gene_id = tokens[0]
        start = int(tokens[1]);
        end = int(tokens[2]);
        length = max(start, end) - min(start, end) + 1
        strand = tokens[3]
        if len(tokens) == 6:
            gene_name = tokens[4]
            contig_id = tokens[5]
        elif len(tokens) == 5:
            if strain != None and strain.sequence == None:
                gene_name = "x"
                contig_id = tokens[4]
            else:
                gene_name = tokens[4]
                contig_id = ""
        else:
            gene_name = "x"
            contig_id = ""
 
        gene_ids[gene_id] = (end, length, strand, contig_id, gene_id, gene_name)
    return gene_ids
