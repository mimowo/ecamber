from soft.structs.gene import Gene
from soft.utils.camber_seq_utils import translateSequence

def writeMutationsHeader(output_fh, strains_subset = None, only_strains = True, sep=" "):
    if not only_strains: header_line = "cluster_id" + "\t" + "aln_fn_id" + "\t" + "ann_id" + "\t" + "mut_type" + "\t" + "positions" + "\t" + "ref_positions"
    else: header_line = ""
    if strains_subset == None: return None
    for strain_id in strains_subset:      
        header_line += strain_id + sep
    header_line = header_line[:-1] + "\n"   
    output_fh.write(header_line);

def savePointMutations(output_fh, all_mutations, ref_anns, ref_strain, strains_list, cluster_gene_ref_ids={}, cluster_gene_names = {}, compress = True, rna=False):
    text_lines = []
    ref_strain_id = ref_strain.strain_id
    
    for (filename_id, cluster_id) in all_mutations:
        gene_name = cluster_gene_names.get(cluster_id, "x")
        gene_id = cluster_gene_ref_ids.get(cluster_id, "x")
        
        if cluster_id in ref_anns:
            ref_pres = "0"
            (end, length, strand, contig_id, gene_id_tmp) = ref_anns[cluster_id][:5]
            if gene_name == "x" and len(ref_anns[cluster_id]) == 6:
                gene_name = ref_anns[cluster_id][5]
            if strand == "+": start = end - length + 1
            else: start = start = end + length - 1
            gene = Gene(start, end, strand, ref_strain, gene_id_tmp, gene_name, contig_id)
            gene_seq_prom = gene.promotor()
            if rna:
                gene_seq_aa = gene.sequence()
            else:
                gene_seq_aa = translateSequence(gene.sequence())
            if len(gene_id) <= 1 and len(gene_id_tmp) > 1: gene_id = gene_id_tmp
            
            text_line = ">" + gene_id + "\t" + cluster_id + '\t' + gene_name
            text_line += "\t" + str(start) + "\t" + str(end) + "\t" + strand
            if len(contig_id) > 0 and contig_id!= ref_strain_id: text_line += "\t" +contig_id + "\n"
            else: text_line += "\n"
            text_line += gene_seq_prom +"\n"
            text_line += gene_seq_aa +"\n"
        else: 
            ref_pres = "1" 
            contig_id = "x"
            gene = None 
            gene_seq_prom = ""
            text_line = ">" + gene_id + "\t" + cluster_id + '\t' + gene_name + "\n"
            
        gene_mutations = all_mutations[(filename_id, cluster_id)]
        gene_mutations_sorted = sorted(gene_mutations, key=lambda mutation_tmp:mutation_tmp[0])
        
        for mutation_tmp in gene_mutations_sorted:
            (m_position, m_ref_position, m_vector, m_is_syn) = mutation_tmp
            
            state_strains = {}
            for strain_index in range(len(strains_list)):
                strain_id = strains_list[strain_index]
                if strain_id in m_vector: new_state = m_vector[strain_id]
                else: new_state = "?"
                    
                if not new_state in state_strains: state_strains[new_state] = []
                state_strains[new_state].append(strain_index)

            sel_state = sorted(state_strains, key=lambda state:-len(state_strains[state]))[0]
            text_line_tmp = ""
            text_line_tmp += str(m_ref_position) + "\t"
            
            if m_ref_position <= 0: text_line_tmp += str("p") + "\t"
            else: text_line_tmp += str("n") + "\t"

            if compress:
                desc_text_line = sel_state + " "
                
                for state in state_strains:
                    if state == sel_state: continue
                    desc_text_line += state + ":"
                    
                    k = len(state_strains[state])
                    i = 0
                    while i < k:
                        strain_index = state_strains[state][i]
                        desc_text_line += str(strain_index)
                        j = 1
                        while i+j<k and state_strains[state][i+j] == strain_index+j:
                            j = j + 1
                        i = i + j
                        
                        if j == 1: desc_text_line += ","
                        elif j == 2: desc_text_line += "," + str(state_strains[state][i-1]) + ","
                        else: desc_text_line += "_" + str(state_strains[state][i-1]) + ","
                        
                    if desc_text_line.endswith(","): desc_text_line = desc_text_line[:-1] + ";"
                desc_text_line = desc_text_line.strip(";")
                desc_text_line = desc_text_line.strip()
            else:
                desc_text_line = ""
                for strain_id in strains_list:
                    desc_text_line += m_vector.get(strain_id, "?")
            
            text_line_tmp += desc_text_line.strip() + "\n"
            text_line += text_line_tmp
        text_lines.append((ref_pres, text_line))

    writeMutationsHeader(output_fh, strains_list, only_strains=True, sep=" ")
    for (ref_pres, text_line) in sorted(text_lines):
        output_fh.write(text_line)

def saveGeneProfiles(output_fh, clusters, ref_anns, strains_list, cluster_gene_ref_ids = {}, cluster_gene_names={}, compress=True):
    text_lines = []
    
    for cluster in list(clusters.clusters.values()):
        cluster_id = cluster.cluster_id
        
        gene_name = cluster_gene_names.get(cluster_id, "x")
        gene_id = cluster_gene_ref_ids.get(cluster_id, "x")
        
        text_line = gene_id + "\t"
        text_line += cluster_id + "\t"    
        text_line += gene_name + "\t" 
        
        diff = False
        
        state_strains = {}
        for strain_index in range(len(strains_list)):
            strain_id = strains_list[strain_index]
            if len(cluster.nodesByStrain(strain_id)) > 0: new_state = "G"
            else: 
                new_state = "L"
                diff = True
                
            if not new_state in state_strains: state_strains[new_state] = []
            state_strains[new_state].append(strain_index)
        
        if not diff: continue
        
        if compress:
            sel_state = sorted(state_strains, key=lambda state:-len(state_strains[state]))[0]
            desc_text_line = sel_state + " "
            
            for state in state_strains:
                if state == sel_state: continue
                desc_text_line += state + ":"
                
                k = len(state_strains[state])
                i = 0
                while i < k:
                    strain_index = state_strains[state][i]
                    desc_text_line += str(strain_index)
                    j = 1
                    while i+j<k and state_strains[state][i+j] == strain_index+j:
                        j = j + 1
                    i = i + j
                
                    if j == 1: desc_text_line += ","
                    elif j == 2: desc_text_line += "," + str(state_strains[state][i-1]) + ","
                    else: desc_text_line += "_" + str(state_strains[state][i-1]) + ","
                        
                if desc_text_line.endswith(","): desc_text_line = desc_text_line[:-1] + ";"
            desc_text_line = desc_text_line.strip(";")
            desc_text_line = desc_text_line.strip()
                
        else:
            desc_text_line = ""
            for strain_id in strains_list:
                if len(cluster.nodesByStrain(strain_id)) > 0: desc_text_line += "G"
                desc_text_line += "L"
                
        text_line += desc_text_line.strip()
        text_lines.append(text_line)

    writeMutationsHeader(output_fh, strains_subset=strains_list, only_strains=True, sep=" ");
    for text_line in sorted(text_lines):
        output_fh.write(text_line + "\n")
        
        