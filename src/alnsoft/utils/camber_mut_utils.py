from soft.utils.camber_params_utils import *
from soft.utils.camber_io_utils import *
from alnsoft.utils.camber_aln_utils import *
from soft.utils.camber_utils import *
from alnsoft.utils.camber_gen_utils import readRefStrain
    
def filteroutMutationsRad(mutations, mrad):
    mutations_sorted = sorted(mutations, key=lambda mutation: mutation[0])
    mutations_final = []
    n = len(mutations)
    for i in range(n):
        curr_pos = mutations_sorted[i][0] 
        if i > 0:
            last_pos = mutations_sorted[i-1][0]
        else:
            last_pos = -mrad-1
        if i < n-1:
            next_pos = mutations_sorted[i+1][0]
        else:
            next_pos = n + mrad+1
        if curr_pos >= last_pos + mrad and curr_pos <= next_pos - mrad:
            mutations_final.append(mutations_sorted[i])
    return mutations_final


def readIdStrainsMap(input_fh):
    id_strains_map = {}
    for line in input_fh.readlines():
        tokens = line.strip().split()
        if len(tokens) < 2:
            continue
        seq_id = tokens[0]
        strain_id = tokens[1].split(".")[3]
        if not seq_id in id_strains_map:
            id_strains_map[seq_id] = set([])
        id_strains_map[seq_id].add(strain_id)
    return id_strains_map

def findRNAMutsFiles(params):
    filename_id, cluster_id, dna_aln_count, strains_subset, syn, mrad, all_strains_count, indel_limit, with_aa, acc_amb = params
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
 
    fasta_dna_folder = parameters_map["DATASET_MANUAL_ALNS"]
    alns_folder = parameters_map["DATASET_MANUAL_ALNS"]
    seq_map_dna_folder = parameters_map["DATASET_MANUAL_ALNS"]
    
    mutations_final = []
    
    if dna_aln_count < 2:
        sequences_aln_map_dna = readSequenceFromFile(fasta_dna_folder + "/" + str(filename_id) + ".fasta")
    else:
        align_fh = open(alns_folder + "/" + str(filename_id) + ".aln");
        sequences_aln_map_dna = readSequencesMapFromAlignmentFile(align_fh);

    map_dna_fh = open(seq_map_dna_folder + "/" + str(filename_id) + ".txt")
    id_strains_map_dna = readIdStrainsMap(map_dna_fh)
    map_dna_fh.close()

    ref_strain = readRefStrain()
     
    full_sequences_aln_map_dna = {}
    for seq_id in sequences_aln_map_dna.keys():
        seq_dna = sequences_aln_map_dna[seq_id]
        if strains_subset != None and len(strains_subset) > 0:
            for strain_id in (set(id_strains_map_dna[seq_id]) & set(strains_subset)):
                full_sequences_aln_map_dna[strain_id] = seq_dna
        else:
            for strain_id in set(id_strains_map_dna[seq_id]):
                full_sequences_aln_map_dna[strain_id] = seq_dna                
            
    strains_pres = set(full_sequences_aln_map_dna.keys()) 
    if len(strains_pres) < 0.8 * all_strains_count:
        return None
    if strains_subset != None and not strains_pres.issuperset(strains_subset):
        return None
    
    mutations_tmp = findSNPalignments(full_sequences_aln_map_dna, full_sequences_aln_map_dna, syn="R", mrad = 0, cluster_id=cluster_id, indel_limit=indel_limit, with_aa=with_aa, filename_id=filename_id, acc_amb=acc_amb, ref_strain=ref_strain)

    for mutation in mutations_tmp:
        (m_position, ref_position, m_vector, m_is_syn) = mutation
        if syn == "S" and m_is_syn == False:
            continue
        mutations_final.append(mutation)
          
    return mutations_final, filename_id, cluster_id


def findPromMutsFiles(params):
    filename_id, cluster_id, dna_aln_count, strains_subset, syn, mrad, all_strains_count, indel_limit, with_aa, acc_amb = params
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
 
    fasta_dna_folder = parameters_map["ALIGNMENTS_FOLDER_PROM_FASTA_NT"]
    alns_folder = parameters_map["ALIGNMENTS_FOLDER_PROM_ALN"]
    seq_map_dna_folder = parameters_map["ALIGNMENTS_FOLDER_PROM_MAP_NT"]
    
    mutations_final = []
    
    if dna_aln_count < 2:
        sequences_aln_map_dna = readSequenceFromFile(fasta_dna_folder + "/" + str(filename_id) + ".fasta")
    else:
        align_fh = open(alns_folder + "/" + str(filename_id) + ".aln");
        sequences_aln_map_dna = readSequencesMapFromAlignmentFile(align_fh);
    
   # print(sequences_aln_map_dna)


    map_dna_fh = open(seq_map_dna_folder + "/" + str(filename_id) + ".txt")
    id_strains_map_dna = readIdStrainsMap(map_dna_fh)
    map_dna_fh.close()

    ref_strain = readRefStrain()
     
    full_sequences_aln_map_dna = {}
    for seq_id in sequences_aln_map_dna.keys():
        seq_dna = sequences_aln_map_dna[seq_id]
        if strains_subset != None and len(strains_subset) > 0:
            for strain_id in (set(id_strains_map_dna[seq_id]) & set(strains_subset)):
                full_sequences_aln_map_dna[strain_id] = seq_dna
        else:
            for strain_id in set(id_strains_map_dna[seq_id]):
                full_sequences_aln_map_dna[strain_id] = seq_dna                
            
    strains_pres = set(full_sequences_aln_map_dna.keys()) 
    if len(strains_pres) < 0.8 * all_strains_count:
        return None
    if strains_subset != None and not strains_pres.issuperset(strains_subset):
        return None
    
    mutations_tmp = findSNPalignments(full_sequences_aln_map_dna, full_sequences_aln_map_dna, syn="R", mrad = 0, cluster_id=cluster_id, indel_limit=indel_limit, with_aa=with_aa, filename_id=filename_id, acc_amb=acc_amb, ref_strain=ref_strain)

    for mutation in mutations_tmp:
        (m_position, ref_position, m_vector, m_is_syn) = mutation
        if syn == "S" and m_is_syn == False:
            continue
        mutations_final.append(mutation)
          
    return mutations_final, filename_id, cluster_id


def findSNPsFiles(params):
    filename_id, cluster_id, aa_aln_count, strains_subset, syn, mrad, all_strains_count, indel_limit, with_aa, acc_amb = params
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    fasta_aa_folder = parameters_map["ALIGNMENTS_FOLDER_FASTA_AA"]
    fasta_dna_folder = parameters_map["ALIGNMENTS_FOLDER_FASTA_NT"]
    alns_folder = parameters_map["ALIGNMENTS_FOLDER_ALN_AA"]
    seq_map_aa_folder = parameters_map["ALIGNMENTS_FOLDER_MAP_AA"]
    seq_map_dna_folder = parameters_map["ALIGNMENTS_FOLDER_MAP_NT"]
    
    ref_strain = readRefStrain()
    
    mutations_final = []
    if aa_aln_count < 2 and syn == "N":
        return []
    elif aa_aln_count < 2:
        sequences_aln_map_aa = readSequenceFromFile(fasta_aa_folder + "/" + str(filename_id) + ".fasta")
    else:
        align_fh = open(alns_folder + "/" + str(filename_id) + ".aln");
        sequences_aln_map_aa = readSequencesMapFromAlignmentFile(align_fh);
    
    map_aa_fh = open(seq_map_aa_folder + "/" + str(filename_id) + ".txt")
    id_strains_map_aa = readIdStrainsMap(map_aa_fh)
    map_aa_fh.close()
    
    strains_pres = set([])
    for seq_id in sequences_aln_map_aa.keys():
        strains_pres.update(id_strains_map_aa[seq_id])

    if len(strains_pres) < 0.8 * all_strains_count:
        return None
    if strains_subset != None and not strains_pres.issuperset(strains_subset):
        return None

    full_sequences_aln_map_aa = {}
    for seq_id in sequences_aln_map_aa.keys():
        seq_aa = sequences_aln_map_aa[seq_id]
        if strains_subset != None and len(strains_subset) > 0:
            for strain_id in (set(id_strains_map_aa[seq_id]) & set(strains_subset)):
                full_sequences_aln_map_aa[strain_id] = seq_aa
        else:
            for strain_id in set(id_strains_map_aa[seq_id]):
                full_sequences_aln_map_aa[strain_id] = seq_aa
                
    sys.stdout.flush()
    
    if syn in ["B", "S"]:
        sequences_aln_map_dna = readSequenceFromFile(fasta_dna_folder + "/" + str(filename_id) + ".fasta")
        map_dna_fh = open(seq_map_dna_folder + "/" + str(filename_id) + ".txt")
        id_strains_map_dna = readIdStrainsMap(map_dna_fh)
        map_dna_fh.close()

        full_sequences_aln_map_dna = {}
        for seq_id in sequences_aln_map_dna.keys():
            seq_dna = sequences_aln_map_dna[seq_id]
            if strains_subset != None and len(strains_subset) > 0:
                for strain_id in (set(id_strains_map_dna[seq_id]) & set(strains_subset)):
                    full_sequences_aln_map_dna[strain_id] = seq_dna
            else:
                for strain_id in set(id_strains_map_dna[seq_id]):
                    full_sequences_aln_map_dna[strain_id] = seq_dna                
                
        mutations_tmp = findSNPalignments(full_sequences_aln_map_aa, full_sequences_aln_map_dna, syn=syn, mrad = mrad, cluster_id=cluster_id, indel_limit=indel_limit, with_aa=with_aa, filename_id=filename_id, acc_amb=acc_amb, ref_strain = ref_strain)
    
        for mutation in mutations_tmp:
            (m_position, m_ref_position, m_vector, m_is_syn) = mutation
            if syn == "S" and m_is_syn == False:
                continue
            mutations_final.append(mutation)
          
    else:
        
        mutations_tmp = findSNPalignments(full_sequences_aln_map_aa, None, syn=syn, mrad = mrad, cluster_id=cluster_id, indel_limit=indel_limit, with_aa=with_aa, filename_id=filename_id, acc_amb=acc_amb, ref_strain = ref_strain)
        for mutation in mutations_tmp:
            (m_position, m_ref_position, m_vector, m_is_syn) = mutation
            if syn == "S" and m_is_syn == False:
                continue
            mutations_final.append(mutation)
    return mutations_final, filename_id, cluster_id

def findSNPalignments(sequences_aln_map, sequences_map=None, syn = "B", mrad = 0, cluster_id = "", indel_limit = 0, with_aa = False, filename_id="", acc_amb=False, ref_strain = ""):
    mutations = [];
    
    if syn == "R":
        seq_list = list(sequences_aln_map)
        if len(sequences_aln_map) < 2:
            return [];
        aln_len = len(sequences_aln_map[seq_list[0]]);

        ref_indel_count = 0
                
        for i in range(aln_len):
            nuc_vector = {};
            nuc_set = {}
            indel = 0
            
            for genome_id in sequences_aln_map.keys():
                nuc_char = sequences_aln_map[genome_id][-i]
                nuc_vector[genome_id] = nuc_char
                if nuc_char != "-":
                    if not nuc_char in nuc_set:
                        nuc_set[nuc_char] = 0
                    nuc_set[nuc_char] += 1
                else:
                    indel += 1
                    if genome_id == ref_strain: ref_indel_count += 1
               #     indel_str[genome_id] += 1
            if indel > indel_limit: 
                continue
            
            if acc_amb == False:
                if len(nuc_set) == 2:
                    diff_count = 0
                    for nuc_char in nuc_set:
                        if nuc_set[nuc_char] >=2:
                            diff_count += 1
                    if diff_count == 2:
                        #position = (i + 1) - aln_len-1
                        #ref_position = position + aln_len - ref_indel_count
                        position = i
                        ref_position = position-ref_indel_count
                        if ref_position > 50:
                            continue
                        mutation = (-position, -ref_position, nuc_vector, False);
                        mutations.append(mutation);
            else:
                if len(nuc_set) >= 2:
                    #position = (i + 1) - aln_len-1
                    #ref_position = position + (aln_len - ref_indel_count)
                    position = i
                    ref_position = position-ref_indel_count
                    if ref_position > 50:
                        continue

                    mutation = (-position, -ref_position, nuc_vector, False);
                    mutations.append(mutation); 
        return mutations
    
    elif syn == "N":
        seq_list = list(sequences_aln_map)
        if len(sequences_aln_map) < 2:
            return [];
        aln_len = len(sequences_aln_map[seq_list[0]]);
        ref_indel_count = 0
    
        for i in range(aln_len):
            aa_vector = {};
            aa_set = {}
            indel = 0
            
            for genome_id in sequences_aln_map.keys():
                char = sequences_aln_map[genome_id][i]
                aa_vector[genome_id] = char
                if char != "-":
                    if not char in aa_set:
                        aa_set[char] = 0
                    aa_set[char] += 1
                else:
                    indel += 1
                    if genome_id == ref_strain:
                        ref_indel_count += 1
            if indel > indel_limit: 
                continue
            
            if acc_amb == False:
                if len(aa_set) == 2:
                    diff_count = 0
                    for aa_char in aa_set:
                        if aa_set[aa_char] >=2:
                            diff_count += 1
                    if diff_count == 2:
                        position = i + 1
                        ref_position = position-ref_indel_count
                        mutation = (position, ref_position, aa_vector, False);
                        mutations.append(mutation);
            else:
                if len(aa_set) >= 2:
                    position = i + 1
                    ref_position = position-ref_indel_count
#                    print("x", position, ref_position)
                    mutation = (position, ref_position, aa_vector, False);
                    mutations.append(mutation); 
        return mutations
    else:
        genomes_list = list(sequences_aln_map)
        if len(sequences_aln_map) < 2:
            return [];
    
        aln_len = len(sequences_aln_map[genomes_list[0]]);
    
        offset_map = {}
        for genome_id in genomes_list:
            offset_map[genome_id] = 0
    
        for i in range(aln_len):
            aa_vector = {};
            codon_vector = {}
            aa_set = set([])
            indel = 0
            
            for genome_id in genomes_list:
                aa_char = sequences_aln_map[genome_id][i]
                if aa_char == "-":
                    offset_map[genome_id] += 1
                    codon = "---"
                    indel += 1
                else:
                    aa_set.add(aa_char)
                    offset = offset_map[genome_id]
                    codon = sequences_map[genome_id][(i-offset)*3:(i-offset)*3 + 3]
                # if len(codon) < 3:
                #     print("XXX", cluster_id, filename_id, genome_id, aln_len, len(sequences_map[genome_id]), i, offset)
                #     print((i-offset)*3,(i-offset)*3 + 3)

                codon_vector[genome_id] = codon 
                aa_vector[genome_id] = aa_char
                
            if indel > indel_limit:
                continue
            for j in [0,1,2]:
                nuc_vector = {}
                nuc_set = {}
                problem = False
                for genome_id in genomes_list:
                    codon = codon_vector[genome_id]
                    try:
                        nuc_char = codon_vector[genome_id][j]
                        nuc_vector[genome_id] = nuc_char
                    except:
                        # print(genome_id, j, len(codon_vector), len(genomes_list), codon_vector)
                        # print(len(sequences_aln_map["WEB067"]))
                        # print(len(sequences_map["WEB067"]))
                        problem = True
                        break

                    if nuc_char != "-": 
                        if not nuc_char in nuc_set:
                            nuc_set[nuc_char] = 0
                        nuc_set[nuc_char] += 1
                
                if problem:
                    continue
                if acc_amb == False:
                    if (len(nuc_set) == 2):
                        diff_count = 0
                        for nuc_char in nuc_set:
                            if nuc_set[nuc_char] > 1:
                                diff_count += 1
                        if diff_count == 2:
                            if len(aa_set) == 1:
                                is_syn = True
                            else:
                                is_syn = False
                            if with_aa == False:
                                position = i*3 + 1 + j
                                ref_position = position - 3*offset_map.get(ref_strain, 0)
                                mutation = (position, ref_position, nuc_vector, is_syn);
                                mutations.append(mutation);
                            else:
                                m_vector = {}
                                for genome_id in genomes_list:
                                    m_vector[genome_id] = nuc_vector[genome_id] + ":" + aa_vector[genome_id]
                                position = i*3 + 1 + j
                                ref_position = position - 3*offset_map.get(ref_strain, 0) 
                                mutation = (position, ref_position, m_vector, is_syn);
                                mutations.append(mutation);
                else:
                    if (len(nuc_set) >= 2):
                        if len(aa_set) == 1:
                            is_syn = True
                        else:
                            is_syn = False
                        if with_aa == False:
                            position = i*3 + 1 + j
                            ref_position = position - 3*offset_map.get(ref_strain, 0)
                            mutation = (position, ref_position, nuc_vector, is_syn);
                            mutations.append(mutation);
                        else:
                            m_vector = {}
                            for genome_id in genomes_list:
                                m_vector[genome_id] = nuc_vector[genome_id] + ":" + aa_vector[genome_id]
                            position = i*3 + 1 + j
                            ref_position = position - 3*offset_map.get(ref_strain, 0)
                            mutation = (position, ref_position, m_vector, is_syn);
                            mutations.append(mutation);                    
        if mrad > 0:
            mutations = filteroutMutationsRad(mutations, mrad)
        return mutations
    return [];
