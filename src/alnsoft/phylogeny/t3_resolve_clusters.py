import sys
import os
import copy
import time
from collections import deque
from StringIO import StringIO
from multiprocessing import Pool
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.structs.aln_filenames import *
from alnsoft.structs.phylo_tree import *
from alnsoft.utils.camber_aln_utils import *
from alnsoft.utils.camber_phylo_utils import *
from alnsoft.utils.camber_phylo_io_utils import *

thr = 0.000000001

def transformTree(tree, clusters_map):
    for leaf_node in tree.root.getLeaves():
        strains_tmp = list(clusters_map[leaf_node.label].split(" "))
        if len(strains_tmp) == 1:
            leaf_node.label = strains_tmp[0]
            leaf_node.node_id = strains_tmp[0]
        else:
            for strain_id in strains_tmp:
                new_leaf = PhyloTreeNode(strain_id)
                new_leaf.e_len = 0.00001
                new_leaf.label = strain_id
                leaf_node.addChild(new_leaf)
    return tree

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln = True)
    
    phylo_dir = parameters["PHYLOGENY_FOLDER_MUTATIONS"] 
    tree_soft = parameters["TREE_SOFT"]

    print(tree_soft)

    cluster_strains = []
    clusters_map = {}

    input_fh = open(phylo_dir + "/clusters_map.txt")
    for line in input_fh.readlines():
        tokens = line.strip().split("\t")
        clusters_map[tokens[0]] = tokens[1].replace(" ", ",")
        cluster_strains.append(tokens[0])
    input_fh.close()
    
    input_fn = phylo_dir + 'tree_raw_'+tree_soft.lower()+".txt"
    output_fn = phylo_dir + 'tree'+tree_soft.upper()+".txt"
    input_fh = open(input_fn)
    output_fh = open(output_fn, "w")
    trees = readPhyloTrees(input_fh, strains_list=cluster_strains, strains_map = {})
    tree = trees[0]
    tree = transformTree(tree, clusters_map)
    flat_tree = tree.flatten(thr)
    text_line = flat_tree.toString(lengths=False, strains=None, int_labels=False) + ";\n"
    output_fh.write(text_line);
    output_fh.close();
    input_fh.close()

    print("t3_resolve_clusters.py. Finished.")
    