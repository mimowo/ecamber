import os
import sys
import time
import random
from StringIO import StringIO
from multiprocessing import Pool
import multiprocessing

sys.path.append("src/")
sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.structs.aln_filenames import *
from alnsoft.utils.camber_aln_utils import *
from alnsoft.utils.camber_phylo_utils import *
from alnsoft.utils.camber_mut_utils import findSNPsFiles

SAMPLE_THR = 50000*200
ratio = 0.98

def filteroutStrains(connected_alignment):
    connected_alignment_res = {}
    strains_filtered = []
    for strain_id in connected_alignment:
        sequence = connected_alignment[strain_id]
        if sequence.count("X") == len(sequence):
            strains_filtered.append(strain_id)
            print(strain_id)
        else:
            connected_alignment_res[strain_id] = sequence

    return connected_alignment_res, strains_filtered

def readIdMgsMap(input_fh):
    id_mgs_map = {}
    for line in input_fh.readlines():
        tokens = line.strip().split("\t")
        if len(tokens) < 2:
            continue
        seq_id = tokens[0]
        mg_id = tokens[1]
        if not seq_id in id_mgs_map:
            id_mgs_map[seq_id] = set([])
        id_mgs_map[seq_id].add(mg_id)
    return id_mgs_map

def readIdStrainsMap(input_fh):
    id_strains_map = {}
    for line in input_fh.readlines():
        tokens = line.strip().split()
        if len(tokens) < 4:
            continue
        seq_id = tokens[0]
        strain_id = tokens[4]
        if not seq_id in id_strains_map:
            id_strains_map[seq_id] = set([])
        id_strains_map[seq_id].add(strain_id)
    return id_strains_map

def orderedSampleWithoutReplacement(seq, k):
    if not 0<=k<=len(seq):
        raise ValueError('Required that 0 <= sample_size <= population_size')

    numbersPicked = 0
    for i,number in enumerate(seq):
        prob = (k-numbersPicked)/(len(seq)-i)
        if random.random() < prob:
            yield number
            numbersPicked += 1

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    print("phy2_connect_point_mutations.py.")
    
    strains = readStrainsInfo()
    
    if "SYN" in parameters: syn = parameters["SYN"]
    else: syn = parameters["SYN"]
       
    if "MRAD" in parameters: mrad = int(parameters["MRAD"])
    else: mrad = 0
        
    progress = ShowProgress("Load strain sequences")
    strains_list = strains.allStrains()
    
    print(len(strains_list))

    progress.setJobsCount(strains.count())

    acc_amb = False
    fns_to_search = []
    input_fh = open(parameters_map["ALIGNMENTS_FOLDER"] + "aligned_clusters.txt")
    for line in input_fh.readlines():
        tokens = line.strip().split()
        if len(tokens) < 1:
            continue
        filename_id = tokens[0]
        cluster_id = tokens[1]

        aa_seq_aln_count = int(tokens[len(tokens)-3])
        aa_aln_count = int(tokens[len(tokens)-1])
        if syn == "N" and aa_aln_count < 2:
            continue
        if aa_seq_aln_count < ratio*strains.count():
            continue
        aln_fn = parameters_map["ALIGNMENTS_FOLDER_ALN_AA"] + "/" + str(filename_id) + ".aln"
        if not os.path.exists(aln_fn) and aa_aln_count >= 2:
            print("file does not exist: ", aln_fn)
            exit()
            
        fns_to_search.append((filename_id, cluster_id, aa_aln_count, None, syn, mrad, strains.count(), 0, False, acc_amb))
    input_fh.close()
    
    progress = ShowProgress("Identify point mutations")
    n = len(fns_to_search)
    progress.setJobsCount(n)
    
    connected_alignment = {}
    for strain_id in strains_list:
        strain_short_id = strains.strain(strain_id).short_name
        connected_alignment[strain_short_id] = ""
    connected_alignment_len = 0
    
    TASKS =  fns_to_search
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())

    cc_mut = 0
    cc_ch = 0
    
    if WORKERS>1:
        pool = Pool(processes=WORKERS)
    
        for r in pool.imap(findSNPsFiles, TASKS):
            if r == None:
                progress.update()
                continue            
            mutations_t, filename_id, cluster_id = r
            cc_ch += 1
            if len(mutations_t) > 0:
                cc_mut += 1
            for mutation in mutations_t:
                
                (m_position,ref_position, m_vector, m_is_syn) = mutation
                for strain_id in strains_list:                  
                    strain_short_id = strains.strain(strain_id).short_name
                    if strain_id in m_vector:
                        connected_alignment[strain_short_id] += m_vector[strain_id]
                    else:
                        connected_alignment[strain_short_id] += "X"
                connected_alignment_len += 1
            progress.update()
    else:
        for TASK in TASKS:
            r = findSNPsFiles(TASK)
            if r == None:
                progress.update()
                continue
            
            mutations_t, filename_id, cluster_id = r
            cc_ch += 1
            if len(mutations_t) > 0:
                cc_mut += 1
            for mutation in mutations_t:    
                (m_position,ref_position, m_vector, m_is_syn) = mutation
                for strain_id in strains_list:                  
                    strain_short_id = strains.strain(strain_id).short_name
                    if strain_id in m_vector:
                        connected_alignment[strain_short_id] += m_vector[strain_id]
                    else:
                        connected_alignment[strain_short_id] += "X"
                connected_alignment_len += 1
            progress.update()
    
    print("Strains count: ", strains.count())    
    print("Gene clusters: ", len(TASKS))
    print("Gene clusters in all substrains: ", cc_ch)
    print("Gene clusters with at least one mutation: ", cc_mut)
    print("Number of mutations: ", len(connected_alignment[strain_id]))
    print("Number of characters in multiple alignement: ",connected_alignment_len * strains.count())
    print("SAMPLE_THE: ", SAMPLE_THR)

    if connected_alignment_len * strains.count() > SAMPLE_THR:
        allids = list(range(connected_alignment_len))
        print(len(allids))

        tosample = min(connected_alignment_len, int(SAMPLE_THR / strains.count()))
        print(tosample)
        subids = orderedSampleWithoutReplacement(allids, tosample)

        connected_alignment_tmp = {}
        for strain_id in connected_alignment: connected_alignment_tmp[strain_id] = ""
        for i in subids:
            for strain_id in connected_alignment:
                connected_alignment_tmp[strain_id] += connected_alignment[strain_id][i]

        connected_alignment = connected_alignment_tmp

    print("h0", len(connected_alignment))

    connected_alignment, strains_filtered = filteroutStrains(connected_alignment)

    print("h1", len(strains_list))
    print("h2", len(connected_alignment))
    mutations_map = connected_alignment
    cluster_ids = {}
    cluster_ids_rev = {}
    cluster_seqs = {}
    cluster_seqs_rev = {}
    
    cluster_id = 0
    for strain_id in mutations_map:
        seq = mutations_map[strain_id]
        if seq in cluster_seqs_rev:
            cluster_id_ex = cluster_seqs_rev[seq]
            cluster_ids_rev[strain_id] = cluster_seqs_rev[seq]
            cluster_ids[cluster_id_ex].add(strain_id)
        else:
            cluster_id += 1
            cluster_seqs_rev[seq] = cluster_id
            cluster_ids_rev[strain_id] = cluster_id
            cluster_ids[cluster_id] = set([strain_id])
            cluster_seqs["c"+str(cluster_id)] = seq
            

    phylo_dir =  parameters["PHYLOGENY_FOLDER_MUTATIONS"] 
    ensure_dir(phylo_dir)

    output_fh = open(phylo_dir + 'strains_filtered.txt', "w");
    for strain_id in strains_filtered:
        output_fh.write(strain_id + "\n")
    output_fh.close()

    output_fh = open(phylo_dir + 'alignment_cluster.txt', "w");
    savePHYLIPalignements(cluster_seqs, output_fh, sorting = lambda cluster_id_str:int(cluster_id_str[1:]))
    output_fh.close()

    output_fh = open(phylo_dir + 'clusters_map.txt', "w");
    for cluster_id in sorted(cluster_ids):
        text_line = "c" + str(cluster_id) + "\t"
        text_line += " ".join(cluster_ids[cluster_id]) + "\n"
        output_fh.write(text_line)
    output_fh.close()

    print("phy2_connect_point_mutations.py. Finished.")
