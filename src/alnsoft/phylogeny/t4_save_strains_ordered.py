import sys
import os
import copy
import time
from collections import deque
from StringIO import StringIO
from multiprocessing import Pool
import multiprocessing

sys.path.append("../../")

from drsoft.utils.gwamar_params_utils import *
from drsoft.utils.gwamar_utils import *
from drsoft.utils.gwamar_muts_io_utils import *
from drsoft.utils.gwamar_tree_io_utils import *
from drsoft.utils.gwamar_strains_io_utils import *

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    phylo_dir = parameters["PREBROAD_TREE_DIR"]
    input_dir = parameters["PREBROAD_OUTPUT_DIR"]
  
    input_fh = open(input_dir + "/strains.txt")
    strains = readStrains(input_fh)
    input_fh.close()

    output_fn = input_dir + "strains_ordered.txt"
    
    if os.path.exists(output_fn):
        print("File " + output_fn + " already exists. First remove it.")
        exit()
    
    input_fn = input_dir + 'tree.txt'
    
    if os.path.exists(input_fn):
        input_fh = open(input_fn)
        tree_line = input_fh.readline()
        input_fh.close()
        strains_list = getTreeStrainList(tree_line)
    else:
        strains_list = strains.allStrains()

    output_fh = open(output_fn, "w")
    for strain_id in strains_list:
        output_fh.write(strain_id + "\n")
    output_fh.close()

    print("t4_save_strains_ordered.py")
    