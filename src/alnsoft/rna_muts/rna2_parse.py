import sys
import os
import multiprocessing


sys.path.append("../../")
from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_closure_utils import *
from soft.structs.blast_hit import *
from soft.structs.gene import *
from soft.utils.camber_graph_utils import *
from alnsoft.utils.camber_rna_utils import *

def writeClustersRNA(fasta_fn, map_fn, seq_clusters):
    clust_id = 0
    fasta_fh = open(fasta_fn, "w")
    map_fh = open(map_fn, "w")
    for seq in seq_clusters:
        writeFASTAseq(fasta_fh, seq, str(clust_id))
            
        for gene_full_id in seq_clusters[seq]:
            map_fh.write(str(clust_id) + "\t" + gene_full_id + "\n")

        clust_id += 1

    map_fh.close()  
    fasta_fh.close()
    
    return None


def clusterSeqs(seqs_map):
    seq_clusters = {}
    
    for mg_id, seq in seqs_map.iteritems():
        if not seq in seq_clusters: seq_clusters[seq] = set([])
        
        seq_clusters[seq].add(mg_id)
    return seq_clusters

  
def parseParsedBlastHitRNA(gene, tokens, strain):
    if len(tokens) <= 1:
        return None
    gene_start = int(tokens[8])
    gene_end = int(tokens[9])
    query_id = tokens[0]

    if gene_start < gene_end:
        gene_strand = "+"
        query_start = int(tokens[6])
        query_end = int(tokens[7])
    else:
        gene_strand = "-"
        query_start = int(tokens[6])
        query_end = int(tokens[7])

    contig_id = tokens[1]
    evalue = tokens[10]
    aln_len = int(tokens[3])
    identity_b = float(tokens[2])
    identities = int(round(identity_b*aln_len/100))
    mismatches = int(tokens[4])
    residues = identities+mismatches
    hit_gene = Gene(gene_start, gene_end, gene_strand, strain, contig_id=contig_id)
    blast_hit = BlastHit(gene, contig_id, hit_gene, evalue_b=evalue, identities=identities, identity_b=identity_b, aln_len=aln_len, residues=residues, query_start=query_start, query_end=query_end, mismatches=mismatches, query_id=query_id)
    return blast_hit

def rateBlastHitDnaRNA(blast_hit, strain, len_p, id_p, evalue_tr=10e-10):
    hssp_b=True
    pid_method=0

    if strain.sequence != None:
        strain_seq = strain.sequence
        rev_strain_seq = strain.rev_sequence
    else:
        strain_seq = strain.sequences_map[blast_hit.contig_id]
        rev_strain_seq = strain.rev_sequences_map[blast_hit.contig_id]
    
    if float(blast_hit.evalue_b) > float(evalue_tr):
        return None
    
    if blast_hit.query_len != None:
        query_len = blast_hit.query_len
    elif blast_hit.query_gene != None: 
        query_len = blast_hit.query_gene.length()
    else:
        query_len = len(blast_hit.query)  
        
    if pid_method == 0:
        pid = float(blast_hit.identities)*100.0/float(query_len)
    elif pid_method == 1:
        pid = float(blast_hit.identity_b)*100.0
    
    if pid < id_p:
        return None
    if hssp_b:
        res_c = int(blast_hit.residues / 3)
        if pid < hssp(res_c, id_p - 19.5):
            return None
    
    accepted = True
    
    if int(blast_hit.query_start) > 12: 
        accepted = False
    else:
        hit_gene = blast_hit.hit_gene        
        max_acceptable_len = (1+len_p) * query_len
        n = len(strain_seq)

        start = hit_gene.start
        end = hit_gene.end
        length = hit_gene.length()
            
        if min(start, end) < 0 or max(start, end) >= n or length < 50:
            accepted = False
        elif (length >= (1-len_p) * query_len and length <= (1+len_p)* query_len):
            ext_hit_gene = Gene(start, end, hit_gene.strand, strain)
            ext_hit_gene.contig_id = blast_hit.hit_gene.contig_id
            return ext_hit_gene
                   
    return None


def parseBlastResultsRNA(params):
    strain_id, gene_id, query_len = params

    overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    
    readStrainSequences(strain)
    strain.calcRevSequence()

    input_fn = parameters["DATASET_RNA_BLAST"] + gene_id +  "/blast-" + strain_id + ".txt"
    if not os.path.exists(input_fn) or os.path.getsize(input_fn) < 2:
        return strain_id, None
    
    input_fh = open(input_fn)
    lines = input_fh.readlines()
    input_fh.close()

    line0 = lines[0]
    tokens = line0.split()
    if len(tokens) == 0:
        return strain_id, None
    query_id = tokens[0]

    blast_hit = parseParsedBlastHitRNA(None, tokens, strain)
    if blast_hit == None:
        strain_id, None
    blast_hit.query_len = query_len
    gene_hit = rateBlastHitDnaRNA(blast_hit, strain, 0.1, 80, 10e-10)
    if gene_hit != None:
        gene_hit.seq = gene_hit.sequence()

    return strain_id, gene_hit
    
if __name__ == '__main__':
    overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    strains = readStrainsInfo()
    strains_list = strains.allStrains()

    ensure_dir(parameters["DATASET_RNA_ALNS"])
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())

    input_fh = open( parameters["DATASET_RNA_INPUT"] + "/ref_anns.txt")
    gene_ids = readGeneIDs(input_fh)
    input_fh.close()


    progress = ShowProgress("Processed RNA BLASTs")
    progress.setJobsCount(len(strains_list)*len(gene_ids))
    
    for gene_id in gene_ids:

        query_fn = parameters["DATASET_RNA_FASTA"] + "/" + gene_id + ".fasta"
        query_seq_map = readSequenceFromFile(query_fn)
        query_len = len(list(query_seq_map.values())[0])

        TASKS = []
        for strain_id in strains_list:
            TASKS.append((strain_id, gene_id, query_len))

        seq_map_nt = {}


        if WORKERS > 1:
            pool = multiprocessing.Pool(processes=WORKERS)
            for strain_id, gene_hit in pool.imap_unordered(parseBlastResultsRNA, TASKS):
                if gene_hit!= None:
                    seq_map_nt[gene_hit.strain_unique_id] = gene_hit.seq
                progress.update(desc=strain_id)
            pool.close()
            pool.join()
        else:
            for strain_id in TASKS:
                strain_id, gene_hit = parseBlastResultsRNA(strain_id)
                if gene_hit!= None:
                    seq_map_nt[gene_hit.strain_unique_id] = gene_hit.seq
                progress.update(desc=strain_id)

        seq_clusters = clusterSeqs(seq_map_nt)

        fasta_fn_nt = parameters["DATASET_RNA_ALNS"] + str(gene_id) + ".fasta"
        map_fn_nt = parameters["DATASET_RNA_ALNS"] + str(gene_id) + ".txt" 

        writeClustersRNA(fasta_fn_nt, map_fn_nt, seq_clusters)

    print("rna2_parse.py: Finished.")
        
