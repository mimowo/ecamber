import os
import sys
import time
sys.path.append("../../../../camber2/src/")

import threading
try:
    from queue import Queue
except ImportError:
    from Queue import Queue

from soft.utils.camber_io_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_progress import CAMBerProgress
from ressoft.utils.mut_utils import *

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
parameters = readParametersFromFile("../parameters-phylo.txt", parameters)
parameters = readParametersFromFile("../parameters-manual.txt", parameters)


strains = readStrainsInfo()
genomes_set = strains.allStrains()

seq_id = "23S_rRNA"
input_fn = parameters["MANUAL_TESTS_FOLDER"]+seq_id+"_input.txt"
input_fh = open(input_fn)
seq = readSequenceFromFile(input_fh)
input_fh.close()


def clustal(input_fn, output_fn):
    clustal_exe = os.path.abspath(parameters["CLUSTAL_PATH"])
    #clustal_exe = parameters["CLUSTAL_PATH"]
    os.system(clustal_exe + " -INFILE=" + input_fn + " -OUTPUT=PHYLIP " + " -OUTFILE=" + output_fn);

print(seq)
if False:
    ensure_dir(parameters["BLASTS_MANUAL_FOLDER"])  
    results_dir = parameters["BLASTS_MANUAL_FOLDER"] + seq_id  
    ensure_dir(results_dir)  
    tr_evalue = parameters["BLAST_EVALUE"]
    
    for genome_id in genomes_set:
        genome_fn = parameters["GENOMES"]+'seq-' + genome_id + ".fasta";
        print(genome_fn)
        query_fn = parameters["MANUAL_TESTS_FOLDER"]+seq_id+"_input.txt";
        output_fn = results_dir + "/blast-" + genome_id + ".txt";
        blast_exe = parameters["BLAST_PATH"]   
        standardDnaBlastList(blast_exe, query_fn, genome_fn, output_fn, tr_evalue);

if False:
    readStrainSequences(strains,genomes_set)
    hits_fn = parameters["MANUAL_TESTS_FOLDER"]+seq_id+"_hits.fasta";
    hits_fh = open(hits_fn, "w")
    for genome_id in genomes_set:
        blast_fn = parameters["BLASTS_MANUAL_FOLDER"] + seq_id + "/blast-" + genome_id + ".txt";
        blast_fh = open(blast_fn)
        blast_hits = parseParsedBlastResultsFile(None, blast_fh, genome_id)
        blast_fh.close()
        if len(blast_hits) >= 1:
            sorted_hits = sorted(list(blast_hits), key=lambda blast_hit:-blast_hit.identities)
            seq = sorted_hits[0].hit_gene.sequence()
            print(genome_id+" "+str(sorted_hits[0].identities)+" "+seq)
            writeFASTAseq(hits_fh, seq, strains.getGenome(genome_id).short_name)
    hits_fh.close()

if False:
    print("end")
    input_fn = parameters["MANUAL_TESTS_FOLDER"]+seq_id+"_hits.fasta";
    output_fn = parameters["MANUAL_TESTS_FOLDER"]+seq_id+"_hits.aln";
    clustal(input_fn, output_fn)
    
if True:
    mut_fh = open(parameters["MANUAL_TESTS_FOLDER"]+seq_id+"_muts.aln", "w")
    aln_fn = parameters["MANUAL_TESTS_FOLDER"]+seq_id+"_hits.aln";
    aln_fh = open(aln_fn)
    aligns_seq = readSequencesFromAlignmentFile(aln_fh);
    all_mutations = findMutationsInRNAAlignment(aligns_seq);
    aln_fh.close()
    for mutation in all_mutations:
        text = ""
        text += seq_id + "\t" 
        text += "x" + "\t"  
        #gene_name = conn_comp.geneName(genomes_list) 
        text += seq_id + "\t" 
        text += "snp-n"
        text += "\t"
        text += str(mutation.p)
        text += "\t"
        for strain_id in strains.allStrains():
            strain = strains.strain(strain_id)
            if strain_id in mutation.nv:
                text += mutation.nv[strain_id] + "\t"
            else:
                plas_id = ""
                for plasmid_id in strain.plasmids:
                    if plasmid_id in mutation.nv:
                        plas_id = plasmid_id
                if plas_id == "":
                    text += "?" + "\t"
                else:
                    text += mutation.nv[plas_id] + "\t"
            
        mut_fh.write(text + "\n")
        
    mut_fh.write(str(len(all_mutations)));
    mut_fh.close();


    
    