import sys
import os
import multiprocessing

sys.path.append("../../")
from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.gene import *
from soft.utils.camber_graph_utils import *
from alnsoft.utils.camber_mut_io_utils import *
from alnsoft.utils.camber_aln_utils import readSequencesMapFromAlignmentFile
from alnsoft.utils.camber_mut_utils import findSNPalignments
from alnsoft.utils.camber_gen_utils import readRefStrains
from alnsoft.utils.camber_rna_utils import *


def readIdStrainsMapMan(input_fh):
    seq_map = {}
    
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split()
        if len(tokens) < 2: continue
        seq_id = tokens[0]
        strain_id = tokens[1].split(".")[-1]
        if not seq_id in seq_map: seq_map[seq_id] = set([])
        seq_map[seq_id].add(strain_id)
    return seq_map

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains_ordered.txt"
    if not os.path.exists(input_fn):
        input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains.txt"
    input_fh = open(input_fn)
    strains = readStrainsFromFile(input_fh)
    input_fh.close()

    strains_list = strains.allStrains()

    strains_subset = strains_list
    all_strains_count = len(strains_list)
    indel_limit = int(0.1*all_strains_count)

    ensure_dir(parameters["DATASET_RNA_MUTS"])
        
    input_fh = open( parameters["DATASET_RNA_INPUT"] + "/ref_anns.txt")
    gene_ids = readGeneIDs(input_fh)
    input_fh.close()

    fasta_dna_folder = parameters_map["DATASET_RNA_ALNS"]
    alns_folder = parameters_map["DATASET_RNA_ALNS"]
    seq_map_dna_folder = parameters_map["DATASET_RNA_ALNS"]
    
    syn = "N"


    mutations_final = {}
    
    for gene_id in gene_ids:

        sequences_aln_map_dna = readSequenceFromFile(fasta_dna_folder + "/" + gene_id + ".fasta")
        if len(sequences_aln_map_dna) <= 1:
            print("only one seqeunce", gene_id)
            continue
        else:
            align_fh = open(alns_folder + "/" + gene_id + ".aln");
            sequences_aln_map_dna = readSequencesMapFromAlignmentFile(align_fh);

        map_dna_fh = open(seq_map_dna_folder + "/" + gene_id + ".txt")
        id_strains_map_dna = readIdStrainsMapMan(map_dna_fh)
        map_dna_fh.close()

        ref_strain_id = readRefStrains(set(strains_list))[0]
        if len(ref_strain_id) < 1:
            print("Reference strain not defined! Edit file '<datasets>/<dataset>/ref_strain.txt'")
            exit(-1)
        
        ref_strain = strains.strain(ref_strain_id)
        readStrainSequences(ref_strain)

        full_sequences_aln_map_dna = {}
        for seq_id in sequences_aln_map_dna.keys():
            seq_dna = sequences_aln_map_dna[seq_id]
            if strains_subset != None and len(strains_subset) > 0:
                for strain_id in (set(id_strains_map_dna[seq_id]) & set(strains_subset)):
                    full_sequences_aln_map_dna[strain_id] = seq_dna
            else:
                for strain_id in set(id_strains_map_dna[seq_id]):
                    full_sequences_aln_map_dna[strain_id] = seq_dna                
                
        strains_pres = set(full_sequences_aln_map_dna.keys()) 
        if len(strains_pres) < 0.6 * all_strains_count:
            print("here 1a", strains_pres, all_strains_count)
            exit()
        if strains_subset != None and not strains_pres.issubset(strains_subset):
            print("here 1b", len(strains_pres), len(strains_subset))
            exit()
        
        mutations_all = findSNPalignments(full_sequences_aln_map_dna, full_sequences_aln_map_dna, syn=syn, mrad = 0, cluster_id=gene_id, indel_limit=indel_limit, filename_id=gene_id, acc_amb=True, ref_strain=ref_strain_id)

        for mutation in mutations_all:
            (m_position, m_ref_position, m_vector, m_is_syn) = mutation
            if not (gene_id, gene_id) in mutations_final:
                mutations_final[(gene_id, gene_id)] = []
            mutations_final[(gene_id, gene_id)].append(mutation)

    output_fn = parameters["DATASET_RNA_MUTS"] + "rna_mutations.txt"
    output_fh = open(output_fn, "w");
    savePointMutations(output_fh, mutations_final, gene_ids, ref_strain, strains_list, cluster_gene_ref_ids={}, cluster_gene_names = {}, compress=True, rna=True) 
    output_fh.close()

    print("rna4_muts.py. Finished.")
