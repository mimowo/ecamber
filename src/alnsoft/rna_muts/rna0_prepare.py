import sys
import os
import multiprocessing


sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_format_utils import convertFeatureTab, convertGenBank
from alnsoft.utils.camber_gen_utils import readRefStrains

def convertAnnsRNA(strain_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    strains = readStrainsInfo()
    
    acc_map = {}
    
    input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "contig_acc_map.txt"
    if os.path.exists(input_fn):
        input_fh = open(input_fn)
        for line in input_fh.readlines():
            tokens = line.split()
            if len(tokens) < 2:
                continue
            acc_map[tokens[0]] = tokens[1]
        input_fh.close()
    
    strain = strains.strain(strain_id)
    readStrainSequences(strain)
    
    #for src in ["refseq"]:
    #for src in [""]:
    src= parameters["AS"]
    input_dir = parameters["DATASET_ANNS_INPUT"] + src + "/"
    output_dir = parameters["DATASET_RNA_INPUT"]
    
    input_fn = input_dir + strain_id + ".tab"
    if not os.path.exists(input_fn): input_fn = input_dir + strain_id + ".txt"
    if not os.path.exists(input_fn): input_fn = input_dir + strain_id + ".cds"
    
    output_fn = output_dir + "/ref_anns" + ".txt"
    print(input_fn)

    if os.path.exists(input_fn) and os.path.getsize(input_fn) > 10:
        output_fh = open(output_fn, "w")
        input_fh = open(input_fn)
        convertFeatureTab(strain, input_fh, output_fh, acc_map, "rRNA")
        input_fh.close()
        output_fh.close()

    return strain_id

def readAnnotationsRNA(input_fh, strain):
    annotation = Annotation()

    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split("\t")
        if len(tokens) < 4: continue
        gene_id = tokens[0];
        start = int(tokens[1]);
        end = int(tokens[2]);
        strand = tokens[3]
        if len(tokens) == 6:
            gene_name = tokens[4]
            contig_id = tokens[5]
        elif len(tokens) == 5:
            if strain.sequence == None:
                gene_name = gene_id
                contig_id = tokens[4]
            else:
                gene_name = tokens[4]
                contig_id = ""
        else:
            gene_name = gene_id
            contig_id = ""
        gene = Gene(start, end, strand, strain, gene_id=gene_id, gene_name=gene_name, contig_id = contig_id)

        annotation.addGene(gene)

    return annotation

if __name__ == '__main__':

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    strains = readStrainsInfo()

    strains_list = strains.allStrains()
    
    intput_dir = parameters["DATASET_RNA_INPUT"]
    fasta_dir= parameters["DATASET_RNA_FASTA"]
    ensure_dir(intput_dir)
    ensure_dir(fasta_dir)

    ref_strain_id = readRefStrains(set(strains_list))[0]
    if len(ref_strain_id) < 1:
        print("Reference strain not defined! Edit file '<datasets>/<dataset>/ref_strain.txt'")
        exit(-1)

    ref_strain = strains.strain(ref_strain_id)   
    readStrainSequences(ref_strain) 
    convertAnnsRNA(ref_strain_id)

    input_fn = intput_dir + "/ref_anns.txt"
    input_fh = open(input_fn)
    annotation = readAnnotationsRNA(input_fh, ref_strain)
    input_fh.close()

    for gene in annotation.genes.values():
        seq = gene.sequence()
        if len(seq) < 30:
            continue
        output_fn = fasta_dir + "/"+ gene.gene_id + ".fasta"
        output_fh = open(output_fn, "w")
        writeFASTAseq(output_fh, seq, gene.unique_id)
        output_fh.close()

    print(len(annotation.genes))
    print("rna0_prepare.py: Finished.")
    
