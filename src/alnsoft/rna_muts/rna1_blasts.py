import os
import sys
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_graph_utils import *
from soft.utils.camber_closure_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_io_utils import ensure_dir
from soft.utils.camber_progress import ShowProgress
from soft.utils.camber_coding_utils import codeDNAseq, decodedDNAseqLength
from alnsoft.utils.camber_rna_utils import *

def runBlastallBlastnRNA(blast_exe, query_fn, genome_fn, output_fn, tr_evalue, masking = False):
    command = blast_exe + " -p blastn -e "+str(tr_evalue)
    if masking == False:
        command += " -F F "
    command += " -d " + genome_fn + " -i " + query_fn + " -o " + output_fn + " -m 8";
    os.system(command);

def runBlastnRNA(blast_exe, query_fn, genome_fn, output_fn, tr_evalue, masking = False):
    command = blast_exe + " -evalue "+str(tr_evalue)
    if masking == True:
        command += " -dust no " 
    command += " -db " + genome_fn + " -query " + query_fn + " -out " + output_fn + " -outfmt 6";
    os.system(command);

def blastDnaRNA(params):
    strain_id, gene_id = params

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    results_tmp_dir = parameters["DATASET_RNA_BLAST"] + gene_id + "/"
    ensure_dir(results_tmp_dir)
  
    genome_db = parameters["DATASET_GENOMES_DBS"] + strain_id
    
    query_fn = parameters["DATASET_RNA_FASTA"] + "/" + gene_id + ".fasta"
    src_output_fn = results_tmp_dir + "/tmp-" + strain_id + ".txt";
    dst_output_fn = results_tmp_dir + "/blast-" + strain_id + ".txt";
    
    if os.path.exists(dst_output_fn) or not os.path.exists(query_fn) or os.path.getsize(query_fn) < 2:
        return strain_id, gene_id
    
    masking = False
    tr_evalue = 10e-10

    if parameters["BLAST_VER"].upper() == "BLAST+":
        blastn_exe = parameters["BLASTN_PATH"]
        runBlastnRNA(blastn_exe, query_fn, genome_db, src_output_fn, tr_evalue, masking=masking);

    elif parameters["BLAST_VER"].upper() == "BLASTALL": 
        blastall_exe = parameters["BLASTALL_PATH"]
        runBlastallBlastnRNA(blastall_exe, query_fn, genome_db, src_output_fn, tr_evalue, masking=masking);
    os.rename(src_output_fn, dst_output_fn)
    return strain_id, gene_id

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)

    ensure_dir(parameters["DATASET_RNA_BLAST"])

    strains = readStrainsInfo()

    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    strains_list = list(strains.allStrains())
    
    input_fh = open( parameters["DATASET_RNA_INPUT"] + "/ref_anns.txt")
    gene_ids = readGeneIDs(input_fh)
    input_fh.close()

    TASKS = []
    for strain_id in strains_list:
        for gene_id in gene_ids:
            TASKS.append((strain_id, gene_id))

    progress = ShowProgress("Computed BLASTs")
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap_unordered(blastDnaRNA, TASKS):
            progress.update(desc=str(r))
        pool.close()
        pool.join()
    else:
        for strain_id in TASKS:
            r = blastDnaRNA(strain_id)
            progress.update(desc=str(r))

    print("man1_blasts.py. Finished.")
