import sys
import os
import multiprocessing

sys.path.append("../../")
from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_closure_utils import *
from soft.structs.blast_hit import *
from soft.structs.gene import *
from soft.utils.camber_graph_utils import *
from alnsoft.utils.camber_rna_utils import *

def muscle(input_fn, output_fn, matrix=None, muscle_exe = "muscle", output_tmp_fn = ""):
    if output_tmp_fn == "":
        command =  muscle_exe + " -quiet" + " -in " + input_fn + " -phyiout " + output_fn
        os.system(command);
    else:
        command =  muscle_exe + " -quiet" + " -in " + input_fn + " -phyiout " + output_tmp_fn
        os.system(command);
        os.rename(output_tmp_fn, output_fn)

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    strains = readStrainsInfo()
    
    input_fh = open( parameters["DATASET_RNA_INPUT"] + "/ref_anns.txt")
    gene_ids = readGeneIDs(input_fh)
    input_fh.close()

    progress = ShowProgress("Aligned RNAs")
    progress.setJobsCount(len(gene_ids))

    for gene_id in gene_ids:
        progress.update(gene_id)

        input_fn = parameters["DATASET_RNA_ALNS"] +"/"+ gene_id + ".fasta"
        seq_map = readSequenceFromFile(input_fn)
        if len(seq_map) <= 1:
            print("rna3_aln.py: only one sequence", gene_id)
            continue

        output_fn = parameters["DATASET_RNA_ALNS"] +"/"+ gene_id + ".aln"
        output_tmp_fn = parameters["DATASET_RNA_ALNS"] +"/tmp-"+ gene_id + ".aln" 
        if os.path.exists(output_fn): 
            continue
        if os.path.exists(output_tmp_fn):
            os.remove(output_tmp_fn)

        if platform.system().upper().count("WIN") > 0:
            muscle_exe = os.path.abspath(parameters["MUSCLE_PATH"])
        else:
            muscle_exe = parameters["MUSCLE_PATH"]
        muscle(input_fn, output_fn, None, muscle_exe, output_tmp_fn=output_tmp_fn)

    print("rna3_aln.py. Finished.")
