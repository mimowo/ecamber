import os
import sys
import time
import multiprocessing

sys.path.append("../../../../camber2/src/")

from soft.utils.camber_params_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_utils import *
from soft.structs.gene import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_progress import ShowProgress
from alnsoft.utils.camber_mut_utils import *

def muscle(input_fn, output_fn):
    muscle_exe = os.path.abspath(parameters["MUSCLE_PATH"])
    #clustal_exe = parameters["CLUSTAL_PATH"]
    os.system(muscle_exe + " -INFILE=" + input_fn + " -OUTPUT=PHYLIP " + " -OUTFILE=" + output_fn);
    os.system(muscle_exe + " -INFILE=" + input_fn + " -OUTPUT=PHYLIP " + " -OUTFILE=" + output_fn);
    muscle_exe = os.path.abspath(parameters["MUSCLE_PATH"])
    command = muscle_exe + " -in " + input_fn + " -phyiout " + output_fn
    
    os.system(command);

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
   
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    strains = readStrainsInfo()
    strains_set = strains.allStrains()

    if not "SEQ_ID" in parameters:
        print("No SEQ_ID specified!")
        exit()
        
    seq_id = parameters["SEQ_ID"]

    input_fn = parameters["MANUAL_TESTS_FOLDER"] + seq_id + "/blast_seqs_"+parameters["AT"]+".fasta";
    output_fn = parameters["MANUAL_TESTS_FOLDER"] + seq_id + "/blast_seqs_"+parameters["AT"]+".aln";
    muscle(input_fn, output_fn)
        
    