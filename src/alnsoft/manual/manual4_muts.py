import os
import sys
import time
import multiprocessing

sys.path.append("../../../../camber2/src/")

from soft.utils.camber_params_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_utils import *
from soft.structs.gene import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_progress import ShowProgress
from alnsoft.utils.camber_mut_io_utils import *
from alnsoft.utils.camber_aln_utils import readSequencesMapFromAlignmentFile
from alnsoft.utils.camber_mut_utils import findSNPalignments

def readIdStrainsMapMan(input_fh):
    seq_map = {}
    
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) < 2: continue
        seq_id = tokens[1]
        strain_id = tokens[0]
        if not seq_id in seq_map: seq_map[seq_id] = set([])
        seq_map[seq_id].add(strain_id)
    return seq_map

def findSNPsFilesManAA(gene_seq_id):
    strains_subset = None
    mutations_final = []
    syn = "N"
    all_strains_count = len(strains.allStrains())
    indel_limit = int(0.1*all_strains_count)

    align_fh = open(parameters["MANUAL_TESTS_FOLDER"] + gene_seq_id + "/blast_seqs_aa.aln");
    sequences_aln_map_aa = readSequencesMapFromAlignmentFile(align_fh);

    map_aa_fh = open(parameters["MANUAL_TESTS_FOLDER"] + gene_seq_id + "/blast_map.txt")
    id_strains_map_aa = readIdStrainsMapMan(map_aa_fh)
    map_aa_fh.close()
    
    strains_pres = set([])
    print(len(id_strains_map_aa), id_strains_map_aa)
    
    for seq_id in sequences_aln_map_aa.keys():
        strains_pres.update(id_strains_map_aa[seq_id])
    if len(strains_pres) < 0.8 * all_strains_count:
        return None
    if strains_subset != None and not strains_pres.issuperset(strains_subset):
        return None
    
    full_sequences_aln_map_aa = {}
    for seq_id in sequences_aln_map_aa.keys():
        seq_aa = sequences_aln_map_aa[seq_id]
        if strains_subset != None and len(strains_subset) > 0:
            for strain_id in (set(id_strains_map_aa[seq_id]) & set(strains_subset)):
                full_sequences_aln_map_aa[strain_id] = seq_aa
        else:
            for strain_id in set(id_strains_map_aa[seq_id]):
                full_sequences_aln_map_aa[strain_id] = seq_aa
 #   print(full_sequences_aln_map_aa)
    
    sys.stdout.flush()
    
    if syn in ["B", "S"]:
        sequences_aln_map_dna = readSequenceFromFile(parameters["MANUAL_TESTS_FOLDER"] + gene_seq_id + "/blast_seqs_nt.aln")
        map_dna_fh = open(parameters["MANUAL_TESTS_FOLDER"] + gene_seq_id + "/blast_map.txt")
        id_strains_map_dna = readIdStrainsMapMan(map_dna_fh)
        map_dna_fh.close()

        full_sequences_aln_map_dna = {}
        for seq_id in sequences_aln_map_dna.keys():
            seq_dna = sequences_aln_map_dna[seq_id]
            if strains_subset != None and len(strains_subset) > 0:
                for strain_id in (set(id_strains_map_dna[seq_id]) & set(strains_subset)):
                    full_sequences_aln_map_dna[strain_id] = seq_dna
            else:
                for strain_id in set(id_strains_map_dna[seq_id]):
                    full_sequences_aln_map_dna[strain_id] = seq_dna                
                
        mutations_tmp = findSNPalignments(full_sequences_aln_map_aa, full_sequences_aln_map_dna, syn=syn, indel_limit=indel_limit, acc_amb=True)
    
        for mutation in mutations_tmp:
            (m_position, m_ref_position, m_vector, m_is_syn) = mutation
            if syn == "S" and m_is_syn == False:
                continue
            mutations_final.append(mutation)
          
    else:
        
        mutations_tmp = findSNPalignments(full_sequences_aln_map_aa, None, syn=syn, mrad = 0, acc_amb=True, indel_limit=indel_limit)
    #    print(mutations_tmp)
        for mutation in mutations_tmp:
            (m_position, m_ref_position, m_vector, m_is_syn) = mutation
            if syn == "S" and m_is_syn == False:
                continue
            mutations_final.append(mutation)
     #   print(mutations_final)
            
    return mutations_final, gene_seq_id, gene_seq_id, gene_seq_id


if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    strains = readStrainsInfo()
    strains_set = strains.allStrains()

    if not "SEQ_ID" in parameters:
        print("No SEQ_ID specified!")
        exit()
        
    seq_id = parameters["SEQ_ID"]
        
    mut_fh = open(parameters["MANUAL_TESTS_FOLDER"] + seq_id + "/muts_"+parameters["AT"]+".txt", "w")
    writeMutationsHeader(mut_fh, strains_set);

    all_mutations = {}
        
    if parameters["AT"] == "aa":
        r = findSNPsFilesManAA(seq_id)
        if r != None:            
            mutations_t, filename_id, cluster_id, gene_name = r
            all_mutations[filename_id] = []
            for mutation in mutations_t:
                (m_position, m_ref_position, m_vector, m_is_syn) = mutation
                all_mutations[filename_id].append((cluster_id, gene_name, mutation))
        else:
            all_mutations = {}
    else:
        
        all_mutations = {}

    output_fn = parameters["MANUAL_TESTS_FOLDER"] + seq_id + '/mutations_'+parameters["AT"]+'.txt'
    output_fh = open(output_fn, "w");
    writeMutationsHeader(output_fh, strains_set)
    for filename_id in all_mutations:
        gene_mutations = all_mutations[filename_id]
        gene_mutations_sorted = sorted(gene_mutations, key=lambda mutation_tmp:mutation_tmp[2][0])
        for mutation_tmp in gene_mutations_sorted:
            cluster_id, gene_name, mutation = mutation_tmp
            #saveMutation(output_fh, mutation, strains_set, cluster_id, filename_id, gene_name)
    output_fh.close()
