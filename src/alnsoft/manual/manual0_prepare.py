import os
import sys
import time
import multiprocessing

sys.path.append("../../../../camber2/src/")

from soft.utils.camber_params_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_utils import *
from soft.structs.gene import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_progress import ShowProgress
from alnsoft.utils.camber_mut_utils import *
    

if __name__ == '__main__':

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    ensure_dir(parameters["MANUAL_TESTS_FOLDER"])
    print(parameters["MANUAL_TESTS_FOLDER"])
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    strains = readStrainsInfo()
    strains_set = strains.allStrains()

    strain = strains.strain("H37Rv")
    readStrainSequences(strain)

    gene = Gene(759807, 763325, "+", strain, "rpoB", contig_id="H37Rv")
        
    print(gene.sequence())
    print(translateSequence(gene.sequence()))
    
    if gene == None: 
        exit()

    readStrainSequences(strain)
    seq_id = gene.gene_id 
    ensure_dir(parameters["MANUAL_TESTS_FOLDER"] + seq_id)
    
    seq_nt = gene.sequence()
    seq_aa = translateSequence(seq_nt)
    
    input_fn = parameters["MANUAL_TESTS_FOLDER"] + seq_id + "/input_nt.fasta"
    input_fh = open(input_fn, "w")
    writeFASTAseq(input_fh, seq_nt, str(1))
    input_fh.close()
    
    input_fn = parameters["MANUAL_TESTS_FOLDER"] + seq_id + "/input_aa.fasta"
    input_fh = open(input_fn, "w")
    writeFASTAseq(input_fh, seq_aa, str(1))
    input_fh.close()
    
            
