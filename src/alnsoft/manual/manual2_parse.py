import os
import sys
import time
import multiprocessing

sys.path.append("../../../../camber2/src/")

from soft.utils.camber_params_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_utils import *
from soft.structs.gene import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_progress import ShowProgress
from alnsoft.utils.camber_mut_utils import *

def parseBlast(params):
    strain_id, seq_id = params
    
    overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    readStrainSequences(strain)
    
    blast_fn = parameters["MANUAL_TESTS_FOLDER"] + seq_id + "/blasts_"+parameters["BT"]+"/blast-" + strain_id + ".txt"
    blast_fh = open(blast_fn)
    blast_hits = parseParsedBlastResultsFile(None, blast_fh, strain)
    blast_fh.close()
    
    if len(blast_hits) >= 1:
        sorted_hits = sorted(list(blast_hits), key=lambda blast_hit:-blast_hit.identities)
        seq = sorted_hits[0].hit_gene.sequence()
    else:
        return strain_id, ""

    return strain_id, seq

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
        
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    strains = readStrainsInfo()
    strains_set = strains.allStrains()

    if not "SEQ_ID" in parameters:
        print("No SEQ_ID specified!")
        exit()
        
    seq_id = parameters["SEQ_ID"]
    
    TASKS = []
    for strain_id in strains_set:
        TASKS.append((strain_id, seq_id))    
    
    hit_seqs = {}
    n = len(TASKS)
    progress = ShowProgress("Parsed BLASTs")
    progress.setJobsCount(n)
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap_unordered(parseBlast, TASKS):
            strain_id, hit_seq = r
            if len(hit_seq) > 0: hit_seqs[strain_id] = hit_seq
            
            progress.update(desc=strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = parseBlast(TASK)
            strain_id, hit_seq = r
            if len(hit_seq) > 0: hit_seqs[strain_id] = hit_seq
            progress.update(desc=strain_id)
    
    
    seq_nt_fn = parameters["MANUAL_TESTS_FOLDER"] + seq_id + "/" + "blast_seqs_nt.fasta";
    seq_nt_fh = open(seq_nt_fn, "w")
    
    seq_aa_fn = parameters["MANUAL_TESTS_FOLDER"] + seq_id + "/" + "blast_seqs_aa.fasta";
    seq_aa_fh = open(seq_aa_fn, "w")
    
    map_fn = parameters["MANUAL_TESTS_FOLDER"] + seq_id + "/" + "blast_map.txt";
    map_fh = open(map_fn, "w")
    
    count = 0
    for strain_id in hit_seqs:
        seq_nt = hit_seqs[strain_id]
        seq_aa = translateSequence(seq_nt)
        writeFASTAseq(seq_nt_fh, seq_nt, str(count))
        writeFASTAseq(seq_aa_fh, seq_aa, str(count))
        map_fh.write(strain_id + "\t" + str(count) + "\t" + str(len(seq_nt)) + "\n")
        count += 1
    seq_aa_fh.close()
    seq_nt_fh.close()
    map_fh.close()
