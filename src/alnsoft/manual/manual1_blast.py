import os
import sys
import time
import multiprocessing

sys.path.append("../../../../camber2/src/")

from soft.utils.camber_params_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_utils import *
from soft.structs.gene import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_progress import ShowProgress
from alnsoft.utils.camber_mut_utils import *
    
def blastDna(params):
    strain_id, seq_id = params
    
    overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    query_fn = parameters["MANUAL_TESTS_FOLDER"] + seq_id+"/input_"+parameters["BT"]+".fasta"
    results_dir = parameters["MANUAL_TESTS_FOLDER"] + seq_id + "/blasts_"+parameters["BT"]+"/"
    
    tr_evalue = 10e-5#parameters["BE"]
    genome_db = parameters["DATASET_GENOMES_DBS"] + strain_id
    
    src_output_fn = results_dir + "/tmp-" + strain_id + ".txt"
    dst_output_fn = results_dir + "/blast-" + strain_id + ".txt"
    
    if os.path.exists(dst_output_fn) or not os.path.exists(query_fn) or os.path.getsize(query_fn) < 2:
        return strain_id
    
    masking = False
    if "LCM" in parameters and parameters["LCM"].upper() == "Y":
        masking =True
    
    if parameters["BLAST_VER"].upper() == "BLAST+":
        if parameters["BT"].lower() == "nt": blastn_exe = parameters["BLASTN_PATH"]
        else: blastn_exe = parameters["TBLASTN_PATH"]
        
        runBlastn(blastn_exe, query_fn, genome_db, src_output_fn, tr_evalue, masking=masking);

    elif parameters["BLAST_VER"].upper() == "BLASTALL": 
        blastall_exe = parameters["BLASTALL_PATH"]
        runBlastallBlastn(blastall_exe, query_fn, genome_db, src_output_fn, tr_evalue, masking=masking);
    
    os.rename(src_output_fn, dst_output_fn)
    return strain_id


if __name__ == '__main__':

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    strains = readStrainsInfo()
    strains_set = strains.allStrains()

    if not "SEQ_ID" in parameters:
        print("No SEQ_ID specified!")
        exit()
        
    seq_id = parameters["SEQ_ID"]
    
    ensure_dir(parameters["MANUAL_TESTS_FOLDER"] + seq_id + "/blasts_"+parameters["BT"]+"/")
        
    TASKS = []
    for strain_id in strains_set:
        TASKS.append((strain_id, seq_id))    
    
    n = len(TASKS)
    progress = ShowProgress("Computed BLASTs")
    progress.setJobsCount(n)
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap_unordered(blastDna, TASKS):
            progress.update(desc=r)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = blastDna(TASK)
            progress.update(desc=r)
