from collections import deque

class PhyloTreeNode:
    def __init__(self, node_id):
        self.visited = False
        self.node_id = node_id
        self.label = None
        self.children = []
        self.parent = None 
        self.e_len = 1.0
    def addChild(self, child_node):
        self.children.append(child_node)
    def addChildren(self, children_nodes):
        self.children.append(children_nodes)
    def setLabel(self, label):
        self.label = label
    def getEdgeLen(self):
        return self.e_len
    def setEdgeLen(self, e_len):
        self.e_len = e_len
    def setParent(self, parent):
        self.parent = parent
    def is_leaf(self):
        return len(self.children) == 0
    def getLeaves(self):
        queue = deque([])
        if len(self.children) == 0: return [self]
        queue.append(self)
        leaves = []
        while queue:
            node = queue.popleft()
            if node == None: continue
            children_list = list(node.children)
            if len(children_list) == 0: leaves.append(node)
            else:
                for child in children_list: queue.append(child)
        return leaves

        
    
    def flatten(self, thr = 0.0, min_val = 0.00001):
        if self.getEdgeLen() > thr:
            curr_node = PhyloTreeNode(self.node_id)
            curr_node.setLabel(self.label)
            curr_node.setEdgeLen(max(min_val, self.e_len))
            for child_node in self.children:
                res = child_node.flatten(thr)
                for c in res:
                    curr_node.addChild(c)
            return [curr_node]
        else:
            if len(self.children) == 0:
                leaf_node = PhyloTreeNode(self.node_id)
                leaf_node.setLabel(self.label)
                leaf_node.setEdgeLen(max(min_val,self.e_len))
                return [leaf_node]
            else:
                forest = []
                for child_node in self.children:
                    res = child_node.flatten(thr)
                    for c in res:
                        forest.append(c)        
                return forest;

    def computeSubstrains(self):
        self.substrains = []
        if len(self.children) == 0:
            if len(self.label) > 0: self.substrains = [self.label]
            else: self.substrains = [self.node_id]
            return self.substrains
        else:
            for child_node in self.children:
                self.substrains += child_node.computeSubstrains()
        return self.substrains
    

    def toString(self, lengths=True, is_root = True, strains = None, int_labels= False, strains_map={}):
        n = len(self.children)
        if n==0:
            if lengths == False:
                if strains == None:
                    strain_id_tmp = str(self.label)
                    strain_id = strains_map.get(strain_id_tmp, strain_id_tmp)
                    return strain_id_tmp
                else:
                    strain_id_tmp = str(self.label)
                    #strain_id = strains.getStrain(self.label)
                    return strains_map.get(strain_id_tmp, strain_id_tmp)
            else:
                if strains == None:
                    strain_id_tmp = str(self.label)
                    strain_id = strains_map.get(strain_id_tmp, strain_id_tmp)
                    return strain_id + ":" + str("%.5f" % self.e_len)
                else:
                    #strain_id = str(strains.getStrain(self.node_id))
                    strain_id_tmp = str(self.label)
                    strain_id = strains_map.get(strain_id_tmp, strain_id_tmp)
                    return strain_id + ":" + str("%.5f" % self.e_len)
        else:
            text = "("
            i = 0
            for child in self.children:
                text += child.toString(lengths = lengths, is_root = False, strains = strains, int_labels=int_labels, strains_map=strains_map)
                i += 1
                if i < n:
                    text += ","
            text += ")"
            if int_labels == True:
                text +="S" + str(self.node_id)
                
            if lengths == True and is_root == False:
                text += ":" + str("%.5f" % self.e_len)
            return text

class PhyloTree:
    def __init__(self, leaves_count):
        self.root_id = 0
        self.root = None
        self.nodes = {}
        self.leaves_count = leaves_count
        self.nodes_count = leaves_count
        self.leaves = set([])
        self.leaf_ids = set([])
    def setLeavesByNodes(self, leaves):
        self.leaves = leaves 
        for leaf in leaves:
            self.leaf_ids.add(leaf.node_id)
    def setLeavesByIDs(self, leaf_ids):
        self.leaf_ids = leaf_ids
        for leaf_id in leaf_ids:
            self.leaves.add(self.nodes[leaf_id])
    def setRoot(self, root):
        self.root_id = root.node_id
        self.root = root
    def addLeafNode(self, node):
        self.nodes[node.node_id] = node
    def addInternalNode(self, node):
        self.nodes[node.node_id] = node
        self.nodes_count += 1
    def createNodeID(self, node_name):
        return self.nodes_count
    def getText(self):
        text = ""
        for strain_id in range(0, self.nodes_count, 1):
            text += str(strain_id)+" "+ str(self.nodes[strain_id].n_zero)+" "+ str(self.nodes[strain_id].n_one)
            text += " children: ";
            node = self.nodes[strain_id]
            for child in node.children:
                text += str(child.node_id)+",";
            text += "\n"
        return text

    def toString(self, lengths=True, strains = None, int_labels = False, strains_map={}):
        return self.root.toString(is_root = True, lengths = lengths, strains=strains, int_labels=int_labels, strains_map=strains_map)
            
    def __str__(self):
        txt = "TREE ROOT ID: " + str(self.root_id) + "\n"
        for i in self.nodes:
            node_i = self.nodes[i]
            parent = node_i.parent
            if parent == None: 
                txt += str(i) + "\t" + str("ROOT:")+str("%.5f" % 0.0)+"\t" #+ str(len(node_i.children)) +":"
            else:
                txt += str(i) + "\t" + str(parent.node_id) + ":"+str("%.5f" % node_i.e_len)+"\t" #+ str(len(node_i.children)) +":"
            for node_j in node_i.children:
                txt += str(node_j.node_id) + ","
            txt = txt[:-1] + "\n"
             
        return txt
            
    def flatten(self, thr=0.0):
        flatten_forest = self.root.flatten(thr)
        flat_root = None
        if len(flatten_forest) == 1:
            flat_root = list(flatten_forest)[0]
        else:
            flat_root = PhyloTreeNode("x")
            for c in flatten_forest:
                flat_root.addChild(c)
        leaves_count = len(flat_root.getLeaves())
        flat_tree = PhyloTree(leaves_count)
        flat_tree.setRoot(flat_root)
        return flat_tree



