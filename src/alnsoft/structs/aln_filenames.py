class AlnFilenames:

    def __init__(self):
        self.map = {}
        self.rev_map = {}
        self.curr_id = 0
    def addMappingByLine(self, line):
        line = line.strip()
        tokens = line.split("\t")
        if len(tokens) >= 2:
            fn_id = int(tokens[0])
            mix_cluster_id = tokens[1]
        self.map[fn_id] = mix_cluster_id
        self.rev_map[mix_cluster_id] = fn_id
    def hasClusterID(self, mix_cluster_id):
        return mix_cluster_id in list(self.rev_map.keys())
    def addMapping(self, mix_cluster_id):
        while self.curr_id in self.map:
            self.curr_id += 1
        self.map[self.curr_id] = mix_cluster_id
        self.rev_map[mix_cluster_id] = self.curr_id
        return self.curr_id
    def getMapping(self, mix_cluster_id):
        return self.rev_map[mix_cluster_id]

def clusterLongestMixID(cluster):
    mix_id = ""
    names_list = []
    for multigene in list(cluster.nodes.values()):
        names_list.append(multigene.mg_longest_unique_id)
    sorted_names_list = sorted(names_list, key=lambda mg_id:mg_id)
    for name in sorted_names_list:
        mix_id += name + ","
    return mix_id

def clusterSelMixID(cluster):
    mix_id = ""
    names_list = []
    for multigene in list(cluster.nodes.values()):
        multigene_id = multigene.getGeneID()
        #print(multigene_id, multigene.mg_unique_id, multigene.sel_lengths)
        names_list.append(multigene_id)
    sorted_names_list = sorted(names_list, key=lambda mg_id:mg_id)
    for name in sorted_names_list:
        mix_id += name + ","
    return mix_id


