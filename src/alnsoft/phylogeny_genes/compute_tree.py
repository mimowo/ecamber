import math
import sys
import time
import multiprocessing
from multiprocessing import Pool

sys.path.append("src/")
sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_graph_utils import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.structs.aln_filenames import *
from alnsoft.utils.camber_aln_utils import *
from alnsoft.utils.camber_phylo_utils import *
from alnsoft.structs.phylo_tree import *
from soft.structs.clusters import *

class cluster_node:
    def __init__(self,vec,left=None,right=None,distance=0.0,n_id=None,count=1):
        self.left=left
        self.right=right
        self.vec=vec
        self.id=n_id
        self.distance=distance
        self.count = count

def L2dist(v1,v2):
    dist = 0.0
    n = len(v1)
    for i in range(n): dist += abs(v1[i]-v2[i])
    return dist
    #return math.sqrt(sum((v1-v2)**2))

def hcluster(features,distance=L2dist):
    distances={}
    currentclustid=-1 # clusters are initially just the individual rows
    clust=[cluster_node(features[i],n_id=i) for i in range(len(features))]

    while len(clust)>1:
        lowestpair=(0,1)
        closest=distance(clust[0].vec,clust[1].vec)
        
        for i in range(len(clust)-1):  # loop through every pair looking for the smallest distance
            for j in range(i+1,len(clust)):
                # distances is the cache of distance calculations
                if (clust[i].id,clust[j].id) not in distances: 
                    distances[(clust[i].id,clust[j].id)]=distance(clust[i].vec,clust[j].vec)

                d=distances[(clust[i].id,clust[j].id)]

                if d < closest:
                    closest=d
                    lowestpair=(i,j)
        
        clust1 = clust[lowestpair[0]]
        clust2 = clust[lowestpair[1]]
        # calculate the weighted average of the two clusters
        mergevec=[(clust1.vec[i]*clust1.count+clust2.vec[i]*clust2.count)/(clust1.count + clust2.count) \
            for i in range(len(clust1.vec))]

        # create the new cluster
        newcluster=cluster_node(mergevec,
                             left=clust1,
                             right=clust2,
                             distance=closest,
                             n_id=currentclustid,
                             count = clust1.count + clust2.count)

        # cluster ids that weren't in the original set are negative
        currentclustid-=1
        del clust[lowestpair[1]]
        del clust[lowestpair[0]]
        clust.append(newcluster)

    return clust[0]


if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    strains = readStrainsInfo()
    
    ensure_dir(parameters["PHYLOGENY_FOLDER"])
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    strains_list = strains.allStrains()
    
    graph_iteration = currentClosureIteration()
    comp_iteration = currentPh2Iteration(default = 3)
    
    results_map = {}
    clusters = MGClusters()
    
    TASKS = []
    for strain_id in strains.allStrains():
        strain = strains.strain(strain_id)
        TASKS.append((strain, graph_iteration, comp_iteration, False, False))
    
    s_time = time.time()
    progress = ShowProgress("Read multigene clusters")
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = Pool(processes = WORKERS)    
        for strain_id, cluster_mapping in pool.imap(readMultigeneClusterStrain, TASKS):
            
            for cluster_id in cluster_mapping:
                if not cluster_id in clusters.clusters:
                    cluster = MGCluster(cluster_id)
                    clusters.addCluster(cluster)
                else:
                    cluster = clusters.getCluster(cluster_id)
                for multigene in cluster_mapping[cluster_id]:
                    cluster.addMultigene(multigene)
        
            progress.update()
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:    
            strain_id, cluster_mapping = readMultigeneClusterStrain(TASK)
            
            for cluster_id in cluster_mapping:
                if not cluster_id in clusters.clusters:
                    cluster = MGCluster(cluster_id)
                    clusters.addCluster(cluster)
                else:
                    cluster = clusters.getCluster(cluster_id)
                for multigene in cluster_mapping[cluster_id]:
                    cluster.addMultigene(multigene)
                
            progress.update()        
    
    e_time = time.time()
    clusters.classifyClusters(strains_list)
    
    print("READ TIME", e_time - s_time)
    
    clusters_list = list(clusters.anchors.values())
    
    n = len(strains_list)
    k = len(clusters_list)
    
    features = []
    for i in range(n): features.append([0]*k)

    for strain_index in range(n):
        strain_id = strains_list[strain_index]
        cluster_profile = []
        for cluster in clusters_list:
            cluster_profile.append(len(cluster.nodesByStrain(strain_id)))
            
        features[strain_index] = cluster_profile
    
    tree = hcluster(features)
    
    phylo_tree = PhyloTree(n)
    res_node_id = phylo_tree.createNodeID(tree.id)
    res_node = PhyloTreeNode(res_node_id)
    phylo_tree.setRoot(res_node)
    
    queue = deque([])
    queue.append((tree.left, "l", res_node))
    queue.append((tree.right, "r", res_node))
    
    while queue:
        (node, site, parent_res_node) = queue.popleft()
        if node == None: continue
        
        children_list = []
        if node.left != None and node.right != None: 
            if site == "l":
                node_left_id = phylo_tree.createNodeID(node.left.id)
                res_node_left = PhyloTreeNode(node_left_id)
            
                parent_res_node.addChild(res_node_left)
                phylo_tree.addInternalNode(res_node_left)

                queue.append((node.left, "l", res_node_left))
                queue.append((node.right,"r", res_node_left))
            else:
                node_right_id = phylo_tree.createNodeID(node.right.id)
                res_node_right = PhyloTreeNode(node_right_id)
            
                parent_res_node.addChild(res_node_right)
                phylo_tree.addInternalNode(res_node_right)
            
                queue.append((node.left, "l", res_node_right))
                queue.append((node.right,"r", res_node_right))              
        elif node.left == None and node.right == None:
            node_leaf_id = node.id
            res_leaf_node = PhyloTreeNode(node_leaf_id)
            res_leaf_node.setLabel(strains_list[node_leaf_id])
            parent_res_node.addChild(res_leaf_node)
            res_leaf_node.children = []
            phylo_tree.addLeafNode(res_leaf_node)
        else:
            print("error")
    
    print(phylo_tree.toString(lengths=False, strains=None, int_labels=False))
    
    output_fh = open(parameters["PHYLOGENY_FOLDER"] + 'phylo_tree_genes.txt', "w")
    output_fh.write(phylo_tree.toString(lengths=False, strains=None, int_labels=False) + ";\n")
    output_fh.close()    


