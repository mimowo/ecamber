import sys
import time

sys.path.append("src/")
sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from alnsoft.structs.aln_filenames import *
from soft.utils.camber_progress import shift
from alnsoft.utils.camber_gen_utils import readStrainsOrdered


def readGeneProfilesFreq(input_fh):
    gene_profiles_freq = {}
    
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) != 2: continue
        gene_profile = tokens[0]
        if gene_profile.count("L") < 2 or gene_profile.count("G") < 2: continue
        gene_profiles_freq[gene_profile] = int(tokens[1])
        
    return gene_profiles_freq

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)

    phase = 1
    strains = readStrainsOrdered()
    strains_list = list(strains.allStrains())
    n = len(strains_list)
    
    input_fh = open(parameters["PHYLOGENY_FOLDER"] + "bin_profiles_genes_freqs.txt")
    gene_profiles_freq = readGeneProfilesFreq(input_fh)
    input_fh.close()
    
    distance = {}
    #for i in range(n): distance[(i,i)] = 0
    for i in range(0, n-1, 1):
        for j in range(i+1, n, 1):
            distance[(i, j)] = 0
    
    for gene_profile in gene_profiles_freq:
        count = gene_profiles_freq[gene_profile]
        for i in range(0, n-1, 1):
            for j in range(i+1, n, 1):
                if gene_profile[i] != gene_profile[j]:
                    distance[(i, j)] += count
     
    text = "x\t"
    for strain_id1 in strains_list: text += strain_id1 + "\t"
    text += "\n"    
        
    for i in range(n):
        strain_id1 = strains_list[i]
        text += shift(strain_id1, 15)
        for j in range(n):
            strain_id2 = strains_list[j]
            if i != j:
                if (i, j) in distance: text += str(distance[(i, j)]) + "\t"
                else: text += str(distance[(j, i)]) + "\t"
            else: text += str(0) + "\t" 
        text += "\n"         
        
    output_fn = parameters["PHYLOGENY_FOLDER"] + "distances.txt"
    output_fh = open(output_fn, "w")                    
    output_fh.write(text)       
    output_fh.close()
     
