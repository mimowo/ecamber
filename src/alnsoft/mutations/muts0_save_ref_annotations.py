import sys
import time
import shutil
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_graph_utils import *
from soft.structs.clusters import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *
from alnsoft.utils.camber_gen_utils import readRefStrains

def saveClusterGeneNames(output_fh, clusters, strains_ord):
    
    text_line = "cluster_id\tgene_name\n"
    output_fh.write(text_line)
          
    for cluster in list(clusters.clusters.values()):
        cluster.geneName(strains_ord)
        
        if len(cluster.gene_name) <= 1: continue
        text_line  = cluster.cluster_id + "\t"
        text_line += cluster.gene_name + "\t"

        if text_line.endswith("\t"): text_line = text_line[:-1]

        output_fh.write(text_line + "\n")
    return None


def saveClusterGeneRefIDs(output_fh, clusters, ref_strain_ids_tmp):
    if len(ref_strain_ids_tmp) == 0: return None
    cluster_gene_names = readClusterGeneNames()
    
    text_line = "cluster_id\tgene_name\t"
    for strain_id in ref_strain_ids_tmp:
        text_line += strain_id + "\t"
    text_line = text_line[:-1] + "\n"

    output_fh.write(text_line)
    out_lines = []
          
    for cluster in list(clusters.clusters.values()):
        gene_name = cluster_gene_names.get(cluster.cluster_id, "x")
        text_line = cluster.cluster_id + "\t" + gene_name + "\t"
        found_id = False
        text_line_ids = ""

        for ref_strain_id in ref_strain_ids_tmp:
            
            if not ref_strain_id in cluster.nodes_by_strain: mg_ref_id = "x"

            multigenes = cluster.nodesByStrain(ref_strain_id).values()
            if len(multigenes) == 0: mg_ref_id = "x"
            else: 
                ref_multigene = list(multigenes)[0]
                mg_ref_id = ref_multigene.ann_id
                if len(mg_ref_id) > 1: found_id = True
            text_line_ids += mg_ref_id + "\t"
            
        if found_id: 
            text_line += text_line_ids[:-1] + "\n"
            out_lines.append((text_line_ids, text_line))
            
    for (gene_id, text_line) in sorted(out_lines):
        output_fh.write(text_line)
            
    return None

def saveClusterGeneRefID(output_fh, clusters, ref_strain_ids_tmp):
    if len(ref_strain_ids_tmp) == 0: return None
    
    print(ref_strain_ids_tmp)
    cluster_gene_names = readClusterGeneNames()
    
    text_line = "cluster_id\tgene_name\tgene_ref_id\n"
    output_fh.write(text_line)
    
    out_lines = []
          
    for cluster in list(clusters.clusters.values()):
        gene_name = cluster_gene_names.get(cluster.cluster_id, "x")
        text_line  = cluster.cluster_id + "\t" + gene_name + "\t"

        for ref_strain_id in ref_strain_ids_tmp:
            multigenes = cluster.nodesByStrain(ref_strain_id).values()
            if len(multigenes) > 0:
                ref_multigene = list(multigenes)[0]
                mg_ref_id = ref_multigene.ann_id
                if len(mg_ref_id) > 1: 
                    text_line += mg_ref_id + "\n"
                    out_lines.append((mg_ref_id, text_line))
                    break
                
    for (gene_id, text_line) in sorted(out_lines):
        output_fh.write(text_line)
        
    return None

if __name__ == '__main__':
    print("algs0_saveRefAnnotations.py: saves details on multigene clusters.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    ensure_dir(parameters["DATASET_OUTPUT"])
    
    strains = readStrainsInfo()
    strains_list = strains.allStrains()
    ref_strain_ids = readRefStrains(set(strains_list))
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default = 3)
    
    ensure_dir(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_CLEANUP", ph2_iteration))
    
    clusters = MGClusters()
    
    TASKS = []
    for strain_id in strains_list:
        strain = strains.strain(strain_id)
        TASKS.append((strain, closure_iteration, ph2_iteration, False, True))
    
    progress = ShowProgress("Multigene clusters read")
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes = WORKERS)    
        for strain_id, cc_mapping in pool.imap(readMultigeneClusterStrain, TASKS):
            
            for cluster_id in cc_mapping:
                if not cluster_id in clusters.clusters:
                    cluster = MGCluster(cluster_id)
                    clusters.addCluster(cluster)
                else:
                    cluster = clusters.getCluster(cluster_id)
                for multigene in cc_mapping[cluster_id]:
                    cluster.addMultigene(multigene)
            
            progress.update(desc = strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            strain_id, cc_mapping = readMultigeneClusterStrain(TASK)
            
            for cluster_id in cc_mapping:
                if not cluster_id in clusters.clusters:
                    cluster = MGCluster(cluster_id)
                    clusters.addCluster(cluster)
                else:
                    cluster = clusters.getCluster(cluster_id)
                for multigene in cc_mapping[cluster_id]:
                    cluster.addMultigene(multigene)
            
            progress.update(desc = strain_id)
    
    clusters.classifyClusters(strains_list)
    
    output_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration) + "cluster_gene_names.txt"
    output_fh = open(output_fn, "w")
    saveClusterGeneNames(output_fh, clusters, strains_list)
    output_fh.close()
    
    dst_fn = parameters["DATASET_OUTPUT"] + "cluster_gene_names.txt"
    shutil.copy(output_fn, dst_fn)
    
    output_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration) + "cluster_gene_ref_id.txt"
    output_fh = open(output_fn, "w")
    saveClusterGeneRefID(output_fh, clusters, ref_strain_ids + strains_list)
    output_fh.close()

    dst_fn = parameters["DATASET_OUTPUT"] + "cluster_gene_ref_id.txt"
    shutil.copy(output_fn, dst_fn)
    
    output_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration) + "cluster_gene_ref_ids.txt"
    output_fh = open(output_fn, "w")
    saveClusterGeneRefIDs(output_fh, clusters, ref_strain_ids)
    output_fh.close()

    dst_fn = parameters["DATASET_OUTPUT"] + "cluster_gene_ref_ids.txt"
    shutil.copy(output_fn, dst_fn)
    
    print("algs0_saveRefAnnotations.py. Finished.")
