import os
import sys
import time
from StringIO import StringIO
from multiprocessing import Pool
import multiprocessing

sys.path.append("src/")
sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.structs.aln_filenames import *
from alnsoft.utils.camber_aln_utils import *
from alnsoft.utils.camber_gen_utils import *
from alnsoft.utils.camber_mut_utils import *
from alnsoft.utils.camber_mut_io_utils import savePointMutations

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    strains = readStrainsOrdered()
    strains_list = strains.allStrains()
    
    ref_strain_id = readRefStrain()
    if len(ref_strain_id) < 1:
        print("Reference strain not defined! Edit file '<datasets>/<dataset>/ref_strain.txt'")
        exit(-1)
    
    ref_strain = strains.strain(ref_strain_id)
    readStrainSequences(ref_strain)
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default = 3)
    
    cluster_gene_names = readClusterGeneNames()
    cluster_gene_ref_ids = readClusterGeneRefIDs()

    ref_input_fh = open(getItPath(parameters_map, closure_iteration, "DATASET_RESULTS_EXP_UNIANNS", ph2_iteration)+ref_strain_id+".txt")
    ref_anns = readRefAnns(ref_input_fh)
    ref_input_fh.close()
    
    syn = "N"
    
    ensure_dir(parameters["MUTATIONS_RESULTS"])
    
    strains_subset = strains_list

    strains_min_count = int(strains.count()*float(parameters["ALN_STRAINS_FRAC"]))
    indel_limit = int((float(parameters["MUT_MAX_INDELS"]))*strains.count())

    print("indel limit", indel_limit, "strains min count", strains_min_count)

    acc_amb = True
    fns_to_search = []
    input_fh = open(parameters_map["ALIGNMENTS_FOLDER"] + "aligned_clusters.txt")
    for line in input_fh.readlines():
        tokens = line.strip().split()
        if len(tokens) < 1:
            continue
        filename_id = tokens[0]
        cluster_id = tokens[1]

        aa_seq_count = int(tokens[-3])
        aa_diff_seq_count = int(tokens[-1])
        
        if syn == "N" and aa_diff_seq_count < 2: continue
        if aa_seq_count < strains_min_count: continue
    
        aln_fn = parameters_map["ALIGNMENTS_FOLDER_ALN_AA"] + "/" + str(filename_id) + ".aln"
        if not os.path.exists(aln_fn) and aa_diff_seq_count >= 2:
            print("file does not exist: ", aln_fn)
            exit()
        
        fns_to_search.append((filename_id, cluster_id, aa_seq_count, [], syn, 0, strains.count(), indel_limit, True, acc_amb))
    input_fh.close()
    
    progress = ShowProgress("Identify aa point mutations")
    n = len(fns_to_search)
    progress.setJobsCount(n)
    
    all_mutations = {}
    
    TASKS =  fns_to_search
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())

    cc_mut = 0
    cc_ch = 0
    
    
    if WORKERS>1:
        pool = Pool(processes=WORKERS)
    
        for r in pool.imap(findSNPsFiles, TASKS):
            if r == None:
                progress.update()
                continue
            mutations_t, filename_id, cluster_id = r
            cc_ch += 1
            if len(mutations_t) > 0:
                cc_mut += 1
            all_mutations[(filename_id, cluster_id)] = []
            for mutation in mutations_t:
                all_mutations[(filename_id, cluster_id)].append(mutation)
            progress.update()
    else:
        for TASK in TASKS:
            r = findSNPsFiles(TASK)
            if r == None:
                progress.update()
                continue
            mutations_t, filename_id, cluster_id = r
            cc_ch += 1
            if len(mutations_t) > 0:
                cc_mut += 1
            all_mutations[(filename_id, cluster_id)] = []
            for mutation in mutations_t:
                all_mutations[(filename_id, cluster_id)].append(mutation)
            progress.update()
                    
    print("Checked clusters: ", len(TASKS))
    print("Checked clusters in all substrains: ", cc_ch)
    print("Checked clusters with any mutations: ", cc_mut)
    print(len(all_mutations), indel_limit, len(fns_to_search))
    
    if parameters["COMPRESS_MUTS"] == "Y": compress = True
    else: compress = False

    output_fn = parameters["MUTATIONS_RESULTS"] + 'point_aa_mutations.txt'
    output_fh = open(output_fn, "w");
    savePointMutations(output_fh, all_mutations, ref_anns, ref_strain, strains_list, cluster_gene_ref_ids=cluster_gene_ref_ids, cluster_gene_names = cluster_gene_names, compress=compress)
    output_fh.close()

