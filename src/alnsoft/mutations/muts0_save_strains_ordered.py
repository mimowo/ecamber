import os
import sys
import time
from StringIO import StringIO
from multiprocessing import Pool
import multiprocessing

sys.path.append("src/")
sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.structs.aln_filenames import *
from alnsoft.utils.camber_phylo_utils import *


if __name__ == '__main__':
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    #species = "sta_plas_184"

    strains = readStrainsInfo()
    
    output_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains_ordered.txt"
    
    if os.path.exists(output_fn):
        print("File " + output_fn + " already exists. First remove it.")
        exit()
    
    input_fn = parameters["PHYLOGENY_FOLDER"] + 'tree.txt'
    
    if os.path.exists(input_fn):
        input_fh = open(input_fn)
        tree_line = input_fh.readline()
        input_fh.close()
        strains_list = getTreeStrainList(tree_line)
    else:
        strains_list = strains.allStrains()

    output_fh = open(output_fn, "w")
    for strain_id in strains_list:
        output_fh.write(strain_id + "\n")
    output_fh.close()
