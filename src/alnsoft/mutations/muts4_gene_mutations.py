import os
import sys
import time
from StringIO import StringIO
from multiprocessing import Pool
import multiprocessing

sys.path.append("src/")
sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.structs.aln_filenames import *
from alnsoft.utils.camber_aln_utils import *
from alnsoft.utils.camber_gen_utils import *
from alnsoft.utils.camber_mut_utils import *
from soft.structs.clusters import *
from alnsoft.utils.camber_mut_io_utils import saveGeneProfiles
from soft.utils.camber_graph_utils import readMultigeneClusterStrain

if __name__ == '__main__':

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    strains = readStrainsOrdered()
    
    ref_strain_id = readRefStrain()
    if len(ref_strain_id) < 1:
        print("Reference strain not defined! Edit file '<datasets>/<dataset>/ref_strain.txt'")
        exit(-1)
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default = 3)

    ref_input_fh = open(getItPath(parameters_map, closure_iteration, "DATASET_RESULTS_EXP_UNIANNS", ph2_iteration)+ref_strain_id+".txt")
    ref_anns = readRefAnns(ref_input_fh)
    ref_input_fh.close()
    
    cluster_gene_names = readClusterGeneNames()
    cluster_gene_ref_ids = readClusterGeneRefIDs()

    ensure_dir(parameters["MUTATIONS_RESULTS"])

    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    strains_list = sorted(list(strains.allStrains()), key=lambda strain_id:-strains.strain(strain_id).totalGenomeLength())

    results_map = {}
    clusters = MGClusters()
    
    TASKS = []
    for strain_id in strains.allStrains():
        strain = strains.strain(strain_id)
        TASKS.append((strain, closure_iteration, ph2_iteration, False, False))
    
    progress = ShowProgress("Read multigene clusters")
    progress.setJobsCount(len(TASKS))
        
    strains_set = set([])
    
    if WORKERS > 1:
        pool = Pool(processes = WORKERS)    
        for strain_id, cluster_mapping in pool.imap(readMultigeneClusterStrain, TASKS):
            
            for cluster_id in cluster_mapping:
                if not cluster_id in clusters.clusters:
                    cluster = MGCluster(cluster_id)
                    clusters.addCluster(cluster)
                else:
                    cluster = clusters.getCluster(cluster_id)
                for multigene in cluster_mapping[cluster_id]:
                    cluster.addMultigene(multigene)
                
            progress.update()
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:    
            strain_id, cluster_mapping = readMultigeneClusterStrain(TASK)
            
            for cluster_id in cluster_mapping:
                if not cluster_id in clusters.clusters:
                    cluster = MGCluster(cluster_id)
                    clusters.addCluster(cluster)
                else:
                    cluster = clusters.getCluster(cluster_id)
                for multigene in cluster_mapping[cluster_id]:
                    cluster.addMultigene(multigene)
                        
            progress.update()        
    
    clusters.classifyClusters(strains_subset = strains_list)
    
    strains_ord = strains.allStrains()
    
    if parameters["COMPRESS_MUTS"] == "Y": compress = True
    else: compress = False

    output_fn = parameters["MUTATIONS_RESULTS"] + 'gene_profiles.txt';
    output_fh = open(output_fn, "w");
    saveGeneProfiles(output_fh, clusters, ref_anns, strains_list, cluster_gene_ref_ids, cluster_gene_names, compress=compress)
    output_fh.close()
