import os
import sys
import time
from StringIO import StringIO
from multiprocessing import Pool
import multiprocessing


sys.path.append("src/")
sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.structs.aln_filenames import *
from alnsoft.utils.camber_aln_utils import *
from alnsoft.utils.camber_gen_utils import *
from alnsoft.utils.camber_mut_utils import *
from alnsoft.utils.camber_mut_io_utils import writeMutationsHeader

def addPointMutations(input_fh, all_mutations):
    line0 = input_fh.readline().strip()
    gene_details = ""
    for line in input_fh.readlines():
        line = line.strip()
        m_pos_details = True
        if line.startswith(">"): 
            line = line[1:]
            m_pos_details = False
        
        tokens = line.split("\t")
        if len(tokens) == 0: continue
        elif len(tokens) == 1: sequence = tokens[0]
        elif m_pos_details:    
            m_position = int(tokens[0])
            m_type = tokens[1]
            m_desc = tokens[2]
            all_mutations[gene_details][m_position] = (m_type, m_desc)
        else:
            gene_details = line
            if not gene_details in all_mutations:
                all_mutations[gene_details] = {}
    
    return all_mutations

if __name__ == '__main__':
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    strains = readStrainsOrdered()
    strains_list = strains.allStrains()
    
    ensure_dir(parameters["MUTATIONS_RESULTS"])
    
    if "SYN_M" in parameters: syn_desc = parameters["SYN_M"].lower()
    else: syn_desc = "n"

    snp_muts_fn = parameters["DATASET_RNA_MUTS"] + "rna_mutations.txt"
    if not os.path.exists(snp_muts_fn):
        all_mutations = {}
    else:
        snp_muts_fh = open(snp_muts_fn);
        all_mutations = addPointMutations(snp_muts_fh, {})
        snp_muts_fh.close()
    print(len(all_mutations))
        
    snp_muts_fn = parameters["MUTATIONS_RESULTS"] + 'point_aa_mutations.txt';
    snp_muts_fh = open(snp_muts_fn);
    all_mutations = addPointMutations(snp_muts_fh, all_mutations)
    snp_muts_fh.close()
    print(len(all_mutations))
    
    snp_muts_fn = parameters["MUTATIONS_RESULTS"] + 'point_prom_mutations.txt';
    snp_muts_fh = open(snp_muts_fn);
    all_mutations = addPointMutations(snp_muts_fh, all_mutations)
    snp_muts_fh.close()
    print(len(all_mutations))

    all_muts_fn = parameters["MUTATIONS_RESULTS"] + 'point_mutations.txt';
    all_muts_fh = open(all_muts_fn, "w");
    writeMutationsHeader(all_muts_fh, strains_subset=strains_list, only_strains=True, sep=" ");

    for gene_datails in sorted(all_mutations):
        all_muts_fh.write(">" + gene_datails + "\n")
        for m_position in sorted(all_mutations[gene_datails]):
            m_type, m_desc = all_mutations[gene_datails][m_position]
            all_muts_fh.write(str(m_position) + "\t" + m_type + "\t" + m_desc + "\n")
    all_muts_fh.close()
    
    
