import sys
import time
import multiprocessing
from multiprocessing import Pool

sys.path.append("src/")
sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_graph_utils import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.structs.aln_filenames import *
from soft.structs.clusters import MGClusters, MGCluster

def writeClusterMulti(params):
    fasta_fn = params[0]
    map_fn = params[1]
    seq_map = params[2]

    if os.path.exists(map_fn) and os.path.exists(fasta_fn):
        return None

    clustered_seqs = {}
    clust_id = 0
    fasta_fh = open(fasta_fn, "w")
    map_fh = open(map_fn, "w")
    for mg_id in seq_map:
        seq = seq_map[mg_id]
        if not seq in clustered_seqs:
            writeFASTAseq(fasta_fh, seq, str(clust_id))
            map_fh.write(str(clust_id) + "\t" + mg_id + "\n")
            clustered_seqs[seq] = clust_id
            clust_id += 1
        else:
            clust_tmp_id = clustered_seqs[seq]
            map_fh.write(str(clust_tmp_id) + "\t" + mg_id + "\n")
            
    map_fh.close()  
    fasta_fh.close()
    
    return None

def getSequenceMap(cluster, aln_type = "aa"):
    seq_map = {}
    for multigene in list(cluster.nodes.values()):
        multigene_id = multigene.getGeneStrainID()
        if aln_type.lower() == "aa": 
            seq_map[multigene_id] = translateSequence(multigene.sequence())[:-1]
        else:
            seq_map[multigene_id] = multigene.sequence()    
    return seq_map

def clusterSeqs(seqs_map):
    seq_clusters = {}
    
    for mg_id, seq in seqs_map.iteritems():
        if not seq in seq_clusters: seq_clusters[seq] = set([])
        
        seq_clusters[seq].add(mg_id)
    return seq_clusters

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    strains = readStrainsInfo()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    strains_list = sorted(list(strains.allStrains()), key=lambda strain_id:-strains.strain(strain_id).totalGenomeLength())
    
    graph_iteration = currentClosureIteration()
    comp_iteration = currentPh2Iteration(default = 3)

    results_map = {}
    clusters = MGClusters()
    
    TASKS = []
    for strain_id in strains.allStrains():
        strain = strains.strain(strain_id)
        TASKS.append((strain, graph_iteration, comp_iteration, False, False, True))
    
    s_time = time.time()
    progress = ShowProgress("Read multigene clusters")
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = Pool(processes = WORKERS)    
        for strain_id, cluster_mapping in pool.imap(readMultigeneClusterStrain, TASKS):
            
            for cluster_id in cluster_mapping:
                if not cluster_id in clusters.clusters:
                    cluster = MGCluster(cluster_id)
                    clusters.addCluster(cluster)
                else:
                    cluster = clusters.getCluster(cluster_id)
                for multigene in cluster_mapping[cluster_id]:
                    cluster.addMultigene(multigene)
        
            progress.update()
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:    
            strain_id, cluster_mapping = readMultigeneClusterStrain(TASK)
            
            for cluster_id in cluster_mapping:
                if not cluster_id in clusters.clusters:
                    cluster = MGCluster(cluster_id)
                    clusters.addCluster(cluster)
                else:
                    cluster = clusters.getCluster(cluster_id)
                for multigene in cluster_mapping[cluster_id]:
                    cluster.addMultigene(multigene)
                
            progress.update()        
    
    e_time = time.time()
    print("READ TIME", e_time - s_time)
    clusters.classifyClusters(strains_subset = strains_list)
    
    ensure_dir(parameters["ALIGNMENTS_FOLDER"])
    ensure_dir(parameters["ALIGNMENTS_FOLDER_FASTA_AA"])
    ensure_dir(parameters["ALIGNMENTS_FOLDER_MAP_AA"])
    ensure_dir(parameters["ALIGNMENTS_FOLDER_FASTA_NT"])
    ensure_dir(parameters["ALIGNMENTS_FOLDER_MAP_NT"])
            
    print("Update alignement files mapping...")
    strains_min_count = int(strains.count()*float(parameters["ALN_STRAINS_FRAC"]))
    
    clusters_to_align = {}
    new_ids = {}
    
    progress = ShowProgress("Select multigene clusters to align")
    progress.setJobsCount(len(clusters.clusters))
  
    for cluster in list(clusters.clusters.values()):
        progress.update()
        if len(cluster.strains()) < strains_min_count: continue
        
        filename_id = cluster.cluster_id.replace(":", "_")
        new_ids[filename_id] = cluster.cluster_id
        clusters_to_align[cluster.cluster_id] = cluster   

    print(("clusters to align: " + str(len(clusters_to_align))))

    progress = ShowProgress("Saved sequences to align")
    progress.setJobsCount(len(new_ids))
    
    out_text = ""
    for filename_id in sorted(new_ids):
        progress.update()

        cluster_id = new_ids[filename_id]
        cluster = clusters_to_align[cluster_id]
        
        seq_map_nt = getSequenceMap(cluster, aln_type="nt")
        fasta_fn_nt = parameters["ALIGNMENTS_FOLDER_FASTA_NT"] + str(filename_id) + ".fasta"
        map_fn_nt = parameters["ALIGNMENTS_FOLDER_MAP_NT"] + str(filename_id) + ".txt"        
        seq_map_aa = getSequenceMap(cluster, aln_type="aa")
        fasta_fn_aa = parameters["ALIGNMENTS_FOLDER_FASTA_AA"] + str(filename_id) + ".fasta"
        map_fn_aa = parameters["ALIGNMENTS_FOLDER_MAP_AA"] + str(filename_id) + ".txt"
        
        clustered_seqs_nt = clusterSeqs(seq_map_nt)
        clustered_seqs_aa = clusterSeqs(seq_map_aa)
        
        writeClusterMulti((fasta_fn_aa, map_fn_aa, seq_map_aa))
        writeClusterMulti((fasta_fn_nt, map_fn_nt, seq_map_nt))
            
        out_text += str(filename_id) + "\t" 
        out_text += cluster.cluster_id + "\t"
        out_text += str(len(cluster.strains())) + "\t"
        out_text += str(len(clustered_seqs_nt)) + "\t"
        out_text += str(len(clustered_seqs_aa)) + "\n"
        
    output_fh = open(parameters["ALIGNMENTS_FOLDER"] + "aligned_clusters.txt", "w")
    output_fh.write(out_text)
    output_fh.close()
