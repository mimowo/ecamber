import sys
import time
import multiprocessing
from multiprocessing import Pool

sys.path.append("src/")
sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_graph_utils import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from soft.structs.clusters import *

def readPromStrainSubsetsMap(input_fh):
    lines = input_fh.readlines()
    line0 = lines[0]
    strains_list = line0.split()
    strains_map = {}
    for line in lines[1:]:
        line = line.strip()
        tokens = line.strip().split("\t")
        if len(tokens) < 3: continue
        
        cluster_id = tokens[1]
        strains_desc = tokens[2]
        
        strains_map[cluster_id] = set([])
        for strain_index in range(len(strains_desc)):
            if strains_desc[strain_index] == "1":
                strain_id = strains_list[strain_index]
                strains_map[cluster_id].add(strain_id)
        
    return strains_map

def writeClusterMulti(params):
    fasta_fn = params[0]
    map_fn = params[1]
    seq_map = params[2]

    if os.path.exists(map_fn) and os.path.exists(fasta_fn):
        return None
    
    clustered_seqs = {}
    clust_id = 0
    fasta_fh = open(fasta_fn, "w")
    map_fh = open(map_fn, "w")
    for mg_id in seq_map:
        seq = seq_map[mg_id]
        if not seq in clustered_seqs:
            writeFASTAseq(fasta_fh, seq, str(clust_id))
            map_fh.write(str(clust_id) + "\t" + mg_id + "\n")
            clustered_seqs[seq] = clust_id
            clust_id += 1
        else:
            clust_tmp_id = clustered_seqs[seq]
            map_fh.write(str(clust_tmp_id) + "\t" + mg_id + "\n")
            
    map_fh.close()  
    fasta_fh.close()

    return None

def getPromSequenceMap(cluster, strains_subset, aln_type = "aa"):
    seq_map = {}
    for multigene in list(cluster.nodes.values()):
        gene_id = multigene.getGeneStrainID()
        strain_id = gene_id.split(".")[3]
        if not strain_id in strains_subset: continue
        seq_map[gene_id] = multigene.promotor()
    return seq_map

def clusterSeqs(seqs_map):
    seq_clusters = {}
    for mg_id in seqs_map:
        seq = seqs_map[mg_id]
        if not seq in seq_clusters: seq_clusters[seq] = set([])
        seq_clusters[seq].add(mg_id)
    return seq_clusters

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    strains = readStrainsInfo()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    strains_list = strains.allStrains()
    
    graph_iteration = currentClosureIteration()
    comp_iteration = currentPh2Iteration(default = 3)

    results_map = {}
    clusters = MGClusters()
    
    TASKS = []
    for strain_id in strains.allStrains():
        strain = strains.strain(strain_id)
        TASKS.append((strain, graph_iteration, comp_iteration, False, False, True))
    
    progress = ShowProgress("Read multigene clusters")
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = Pool(processes = WORKERS)    
        for strain_id, cluster_mapping in pool.imap(readMultigeneClusterStrain, TASKS):
            
            for cluster_id in cluster_mapping:
                if not cluster_id in clusters.clusters:
                    cluster = MGCluster(cluster_id)
                    clusters.addCluster(cluster)
                else:
                    cluster = clusters.getCluster(cluster_id)
                for multigene in cluster_mapping[cluster_id]:
                    cluster.addMultigene(multigene)
        
            progress.update()
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:    
            strain_id, cluster_mapping = readMultigeneClusterStrain(TASK)
            
            for cluster_id in cluster_mapping:
                if not cluster_id in clusters.clusters:
                    cluster = MGCluster(cluster_id)
                    clusters.addCluster(cluster)
                else:
                    cluster = clusters.getCluster(cluster_id)
                for multigene in cluster_mapping[cluster_id]:
                    cluster.addMultigene(multigene)
                
            progress.update()        
    
    clusters.classifyClusters(strains_subset = strains_list)
    
    ensure_dir(parameters["ALIGNMENTS_FOLDER"])
    ensure_dir(parameters["ALIGNMENTS_FOLDER_PROM_FASTA_NT"])
    ensure_dir(parameters["ALIGNMENTS_FOLDER_PROM_MAP_NT"])
            
    comps_to_align = {}
    new_ids = {}
    
    input_fh = open(parameters["ALIGNMENTS_FOLDER"] + "promotor_strains.txt")
    strain_subsets = readPromStrainSubsetsMap(input_fh)
    input_fh.close()
    
    strains_min_count = int(strains.count()*float(parameters["ALN_PROM_STRAINS_FRAC"]))
#        
    progress = ShowProgress("Select clusters to align")
    progress.setJobsCount(len(clusters.anchors))
  
    for anchor in list(clusters.anchors.values()):
        progress.update()
        strains_subset = strain_subsets.get(anchor.cluster_id, set([]))

        if len(strains_subset) < strains_min_count: continue

        cluster_id = anchor.cluster_id
        filename_id = cluster_id.replace(":", "_")

        new_ids[filename_id] = cluster_id
        comps_to_align[cluster_id] = anchor        
        
    print(("comps to align: " + str(len(comps_to_align))))

    progress = ShowProgress("Saved cluster promotor sequences to align")
    progress.setJobsCount(len(new_ids))
    
    out_text = ""
    for filename_id in sorted(new_ids):
        mix_cluster_id = new_ids[filename_id]
        cluster = comps_to_align[mix_cluster_id]

        strains_subset = strain_subsets.get(cluster.cluster_id, set([]))
        
        if len(strains_subset) < strains_min_count: continue
        
        seq_map_dna = getPromSequenceMap(cluster, strains_subset, aln_type = "nt")
        clustered_seqs_dna = clusterSeqs(seq_map_dna)
        
        fasta_fn_dna = parameters["ALIGNMENTS_FOLDER_PROM_FASTA_NT"] + str(filename_id) + ".fasta"
        map_fn_dna = parameters["ALIGNMENTS_FOLDER_PROM_MAP_NT"] + str(filename_id) + ".txt"        
        
        writeClusterMulti((fasta_fn_dna, map_fn_dna, seq_map_dna))
                
        out_text += str(filename_id) + "\t" 
        out_text += cluster.cluster_id + "\t"
        out_text += str(len(strains_subset)) + "\t"
        out_text += str(len(clustered_seqs_dna)) + "\t"
        out_text += str(len(clustered_seqs_dna)) + "\n"
        progress.update()
        
    output_fh = open(parameters["ALIGNMENTS_FOLDER"] + "aligned_promotors.txt", "w")
    output_fh.write(out_text)
    output_fh.close()
