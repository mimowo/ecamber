import sys
import time
import multiprocessing
from multiprocessing import Pool

sys.path.append("src/")
sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_graph_utils import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.structs.aln_filenames import *

def clustal(input_fn, output_fn, matrix=None, clustal_exe = "clustalw"):
    os.system(clustal_exe + " -INFILE=" + input_fn + " -OUTPUT=PHYLIP " + " -OUTFILE=" + output_fn);

def muscle(input_fn, output_fn, matrix=None, muscle_exe = "muscle", output_tmp_fn = ""):
    if output_tmp_fn == "":
        command =  muscle_exe + " -quiet" + " -in " + input_fn + " -phyiout " + output_fn
        os.system(command);
    else:
        command =  muscle_exe + " -quiet" + " -in " + input_fn + " -phyiout " + output_tmp_fn
        os.system(command);
        os.rename(output_tmp_fn, output_fn)

def alignCluster(filename_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
 
    try:
        input_fn = parameters["ALIGNMENTS_FOLDER_FASTA_AA"] +"/"+ filename_id + ".fasta"
        output_fn = parameters["ALIGNMENTS_FOLDER_ALN_AA"] +"/"+ filename_id + ".aln"
        output_tmp_fn = parameters["ALIGNMENTS_FOLDER_ALN_AA"] +"/tmp-"+ filename_id + ".aln" 
        if os.path.exists(output_fn): 
            return filename_id
        if os.path.exists(output_tmp_fn):
            os.remove(output_tmp_fn)
    
        if parameters["ALN_PROGRAM"].upper().startswith("CLUSTAL"): 
            clustal_exe = os.path.abspath(parameters["CLUSTAL_PATH"])
            clustal(input_fn, output_fn, parameters["MATRIX"], output_tmp_fn=output_tmp_fn)
        else:
            if platform.system().upper().count("WIN") > 0:
                muscle_exe = os.path.abspath(parameters["MUSCLE_PATH"])
            else:
                muscle_exe = parameters["MUSCLE_PATH"]
            muscle(input_fn, output_fn, None, muscle_exe, output_tmp_fn=output_tmp_fn)
    except Exception:
        exc = sys.exc_info()[1]
        print("Error: " + str(exc))
        print("Error: " + str(sys.exc_info()[0]))
        print("Error: " + str(sys.exc_info()))
        sys.stdout.flush()
    return filename_id

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    strains = readStrainsInfo()
    
    ensure_dir(parameters["ALIGNMENTS_FOLDER_ALN_AA"])

    filenames_to_align = []
    input_fh = open(parameters["ALIGNMENTS_FOLDER"] + "aligned_clusters.txt")
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split()
        if len(tokens) == 0: continue
        if int(tokens[-1]) <= 1: continue
        
        filenames_to_align.append(tokens[0])
    input_fh.close()
    
    progress = ShowProgress("Aligned clusters")
    progress.setJobsCount(len(filenames_to_align))
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    print("workers", WORKERS)
    pool = Pool(processes=WORKERS) 
    
    TASKS = []
    for filename_id in filenames_to_align:
        TASKS.append(filename_id)
    
    if WORKERS > 1:
        for filename_id in pool.imap(alignCluster, TASKS):
            progress.update(desc=filename_id)
    else:
        for TASK in TASKS:
            filename_id = alignCluster(TASK)
            progress.update(desc=filename_id)
    
    print("")

