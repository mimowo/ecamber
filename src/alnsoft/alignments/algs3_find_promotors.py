import sys
import time
import multiprocessing
from multiprocessing import Pool

sys.path.append("src/")
sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_graph_utils import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.utils.camber_aln_utils import *
from alnsoft.utils.camber_mut_utils import *


def findPromotorsMap(params):
    filename_id, cluster_id, diff_seq_count, strains_subset, syn, mrad, all_strains_count, indel_limit, with_aa, acc_amb = params
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    fasta_aa_folder = parameters["ALIGNMENTS_FOLDER_FASTA_AA"]
    alns_folder = parameters["ALIGNMENTS_FOLDER_ALN_AA"]
    seq_map_aa_folder = parameters["ALIGNMENTS_FOLDER_MAP_AA"]
    
    if diff_seq_count <= 1:
        sequences_aln_map_aa = readSequenceFromFile(fasta_aa_folder + "/" + str(filename_id) + ".fasta")
    else:
        align_fh = open(alns_folder + "/" + str(filename_id) + ".aln");
        sequences_aln_map_aa = readSequencesMapFromAlignmentFile(align_fh);

    map_aa_fh = open(seq_map_aa_folder + "/" + str(filename_id) + ".txt")
    id_strains_map_aa = readIdStrainsMap(map_aa_fh)
    map_aa_fh.close()
    
    strain_min_count = int(float(parameters["ALN_STRAINS_FRAC"]) * all_strains_count)
    
    strains_pres = set([])
    for seq_id in sequences_aln_map_aa.keys():
        strains_pres.update(id_strains_map_aa[seq_id])
    if len(strains_pres) < strain_min_count: return None
    
    if strains_subset != None and not strains_pres.issuperset(strains_subset):
        return None

    full_sequences_aln_map_aa = {}
    for seq_id, seq_aa in sequences_aln_map_aa.iteritems():
        if strains_subset != None and len(strains_subset) > 0:
            for strain_id in (set(id_strains_map_aa[seq_id]) & set(strains_subset)):
                full_sequences_aln_map_aa[strain_id] = seq_aa
        else:
            for strain_id in set(id_strains_map_aa[seq_id]):
                full_sequences_aln_map_aa[strain_id] = seq_aa
    

    minus_count_map = {}
    minus_counts = {}
    
    most_common_minus_count = 0
    most_common_count = 0
    
    for strain_id, seq_aa in full_sequences_aln_map_aa.iteritems():
        minus_count = 0
        i = 0 
        while i<len(seq_aa) and seq_aa[i] == "-":
            minus_count += 1
            i += 1

        minus_count_map[strain_id] = minus_count
        if not minus_count in minus_counts:
            minus_counts[minus_count] = 0
        minus_counts[minus_count] += 1
        
        if minus_counts[minus_count] > most_common_count:
            most_common_count = minus_counts[minus_count]
            most_common_minus_count = minus_count
    
    promotors_set = set([])
    for strain_id in minus_count_map:
        if minus_count_map[strain_id] == most_common_minus_count:
            promotors_set.add(strain_id)
            
    return promotors_set, filename_id, cluster_id


if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    strains = readStrainsInfo()
    strains_min_count = int(strains.count()*float(parameters["ALN_STRAINS_FRAC"]))

    print(strains_min_count)
    syn = "N"

    strains_list = strains.allStrains()

    acc_amb = True
    fns_to_search = []
    input_fh = open(parameters_map["ALIGNMENTS_FOLDER"] + "aligned_clusters.txt")
    for line in input_fh.readlines():
        tokens = line.strip().split()
        if len(tokens) < 1:
            continue
        filename_id = tokens[0]
        cluster_id = tokens[1]
        
        aa_seq_count = int(tokens[-3])
        diff_aa_seq_count = int(tokens[-1])

        if aa_seq_count < strains_min_count: continue
        
        fns_to_search.append((filename_id, cluster_id, diff_aa_seq_count, [], syn, 0, strains.count(), 0, True, acc_amb))
    input_fh.close()
        
    progress = ShowProgress("Find strains with consistent starts")
    n = len(fns_to_search)
    progress.setJobsCount(n)
    
    all_mutations = {}
    
    TASKS =  fns_to_search
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    cc_mut = 0
    cc_ch = 0
    
    promotors_strains = {}
    
    if WORKERS>1:
        pool = Pool(processes=WORKERS)
    
        for r in pool.imap(findPromotorsMap, TASKS):
            if r == None:
                progress.update()
                continue
            promotors_set, filename_id, cluster_id = r
            promotors_strains[(filename_id, cluster_id)] = promotors_set
            progress.update()
    else:
        for TASK in TASKS:
            r = findPromotorsMap(TASK)
            if r == None:
                progress.update()
                continue
            promotors_set, filename_id, cluster_id = r
            promotors_strains[(filename_id, cluster_id)] = promotors_set
            progress.update()
    
    output_fh = open(parameters["ALIGNMENTS_FOLDER"] + "promotor_strains.txt", "w")
    text_line = ""
    for strain_id in strains_list:
        text_line += strain_id + " "
    text_line = text_line.strip() + "\n"
    output_fh.write(text_line)
    
    for (filename_id, cluster_id) in promotors_strains:
        text_line = str(filename_id) + "\t" + str(cluster_id) + "\t"
        for strain_index in range(strains.count()):
            strain_id = strains_list[strain_index]
            if strain_id in promotors_strains[(filename_id, cluster_id)]:
                text_line += "1"
            else:
                text_line += "0"
        output_fh.write(text_line + "\n")
    output_fh.close()
