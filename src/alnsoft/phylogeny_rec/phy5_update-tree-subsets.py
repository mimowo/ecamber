import os
import sys
import time
from StringIO import StringIO
from multiprocessing import Pool
import multiprocessing

sys.path.append("src/")
sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.structs.aln_filenames import *
from alnsoft.utils.camber_aln_utils import *
from alnsoft.utils.camber_phylo_utils import *
from alnsoft.utils.camber_phylo_io_utils import readPhyloTree

def findSubsetsTreeText(tree_text, strains_list=None):
    tree = readPhyloTree(tree_text, strains_list)
    subsets = findSubsetsTree(tree)
    return subsets

def findSubsetsTrees(input_fh, strains_list=None):
    lines = input_fh.readlines()
    tree_txt = ""
    count = 0
    subsets = []
    for line in lines:
        line = line.strip()
        tree_txt += line
        if line.endswith(";"):
            subsets += findSubsetsTreeText(tree_txt, strains_list)
            count += 1
            tree_txt = ""
    if tree_txt != "":
        subsets += findSubsetsTreeText(tree_txt, strains_list)
        count += 1
    return subsets

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    print("phy5_update-tree-subsets.py.")
    
    strains = readStrainsInfo()
    
    if "SYN" in parameters: syn = parameters["SYN"]
    else: syn = parameters["SYN"]
       
    if "MRAD" in parameters: mrad = int(parameters["MRAD"])
    else: mrad = 0
    
    if "CURR" in  parameters:
        curr_subset_id = int(parameters["CURR"])
    else:
        curr_subset_id = int(readCurrentSubsetID(parameters["PHYLOGENY_FOLDER"] + "current_subset-"+syn.lower()+"_" + str(mrad)+".txt"))
    
    subsets_old_map = readStrainSubsetsMap(parameters["PHYLOGENY_FOLDER"] + "subsets-"+syn.lower()+"_" + str(mrad)+'.txt')
    
    input_fn = parameters["PHYLOGENY_FOLDER_FLATPHYLOTREE"] + 'flatphylotree-' +syn.lower()+"_"+str(curr_subset_id)+"_" + str(mrad)+'.txt'
    input_fh = open(input_fn)
    subsets_new = findSubsetsTrees(input_fh)
    input_fh.close()
    
    max_id = -1
    
    for key_id in subsets_old_map.keys():
        if int(key_id) >= max_id:
            max_id = int(key_id)
    
    max_id += 1

    subsets_old = subsets_old_map.values()
    subsets_old_text = set([])
    
    for subset in subsets_old:
        subsets_old_text.add(setToStr(subset))
    
    subsets_sorted = sorted(subsets_new, key=lambda subset:-len(subset))
    
    output_fh = open(parameters["PHYLOGENY_FOLDER"] + "subsets-"+syn.lower()+"_" + str(mrad)+'.txt', "a")
    new_subset_map = {}
    for subset in subsets_sorted:
        if setToStr(subset) in subsets_old_text:
            continue
        new_subset_map[max_id] = subset
        text_line = str(max_id)
        for strain_id in subset:
            text_line += "\t" + str(strain_id)
        output_fh.write(text_line + "\n")
        max_id += 1
    output_fh.close()
        
    print("phy5_update-tree-subsets.py. Finished.")
    