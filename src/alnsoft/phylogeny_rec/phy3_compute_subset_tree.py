import os
import sys
import time
import multiprocessing
from StringIO import StringIO

sys.path.append("src/")
sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.structs.aln_filenames import *
from alnsoft.utils.camber_aln_utils import *
from alnsoft.utils.camber_phylo_utils import *

def ensure_rem(filename):
    if os.path.exists(filename):
        os.remove(filename)

def computeTreeDNAML(phylip_path, input_fn, outfile_fn, outtree_fn, sufix = ""):
    os.chdir(phylip_path)   
    ensure_rem(outfile_fn)
    ensure_rem(outtree_fn)
    tmp_fn = "tmpfile" + sufix
    tmp_fh = open(tmp_fn, "w")
    tmp_fh.write(input_fn + "\n")
    tmp_fh.write("F" + "\n")
    tmp_fh.write(outfile_fn + "\n")
    tmp_fh.write("Y" + "\n")
    tmp_fh.write("F" + "\n")
    tmp_fh.write(outtree_fn + "\n")
    tmp_fh.close()
    if platform.system().count("WIN") > 0:
        os.system(phylip_path + "dnamlk < " + tmp_fn)
    else:
        os.system(phylip_path + "/dnamlk < " + tmp_fn)

def computeTreePROML(phylip_path, input_fn, outfile_fn, outtree_fn, sufix = ""):
    os.chdir(phylip_path)
    ensure_rem(outfile_fn)
    ensure_rem(outtree_fn) 

    tmp_fn = "tmpfile" + sufix
    tmp_fh = open(tmp_fn, "w")
    tmp_fh.write(input_fn + "\n")
    tmp_fh.write("F" + "\n")
    tmp_fh.write(outfile_fn + "\n")
    tmp_fh.write("Y" + "\n")
    tmp_fh.write("F" + "\n")
    tmp_fh.write(outtree_fn + "\n")
    tmp_fh.close()
    if platform.system().count("dows") > 0:
        os.system("promlk < " + tmp_fn)
    else:
        os.system(phylip_path + "/promlk < " + tmp_fn)

    return None

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    print("phy3_compute_subset_tree.py.")
    
    strains = readStrainsInfo()
    
    if "SYN" in parameters: syn = parameters["SYN"]
    else: syn = parameters["SYN"]
       
    if "MRAD" in parameters: mrad = int(parameters["MRAD"])
    else: mrad = 0
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    progress = ShowProgress("Load strain sequences")
    genomes_list = sorted(list(strains.allStrains()), key=lambda strain_id:-strains.strain(strain_id).totalGenomeLength())
    progress.setJobsCount(len(genomes_list))
    
    subsets_map = readStrainSubsetsMap(parameters["PHYLOGENY_FOLDER"] + "subsets-"+syn.lower()+"_" + str(mrad)+'.txt')
    
    if "CURR" in  parameters:
        curr_subset_id = int(parameters["CURR"])
    else:
        curr_subset_id = int(readCurrentSubsetID(parameters["PHYLOGENY_FOLDER"] + "current_subset-"+syn.lower()+"_" + str(mrad)+'.txt'))

    strains_subset = subsets_map[curr_subset_id]
    strains_ord = sorted(strains.allStrains())
    
    input_fn = parameters["PHYLOGENY_FOLDER_MUTATIONS"] + 'snp-mutations-' +syn.lower()+"_"+str(curr_subset_id)+"_" + str(mrad)+'.txt'
    outfile_fn = parameters["PHYLOGENY_FOLDER_PHYLOFILE"] + 'phylofile-' +syn.lower()+"_"+str(curr_subset_id)+"_" + str(mrad)+'.txt'
    outtree_fn = parameters["PHYLOGENY_FOLDER_PHYLOTREE"] + 'phylotree-' +syn.lower()+"_"+str(curr_subset_id)+"_" + str(mrad)+'.txt'
    
    phylip_path = parameters["PHYLIP_PATH"]
    if platform.system().count("WIN") > 0:
        if platform.system().upper().count("CYGWIN") > 0:
            input_fn.replace("/cygdrive/c/", "C:/")
            outfile_fn.replace("/cygdrive/c/", "C:/")
            outtree_fn.replace("/cygdrive/c/", "C:/")
            abs_phylip_path = phylip_path        
    else:    
        abs_phylip_path = os.path.abspath(phylip_path)
    
    print(phylip_path)
    print(abs_phylip_path)
    
    if syn in ["S", "B"]:
        computeTreeDNAML(abs_phylip_path, input_fn, outfile_fn, outtree_fn, sufix = "_"+syn.lower()+"_"+str(curr_subset_id))
    else:
        computeTreePROML(abs_phylip_path, input_fn, outfile_fn, outtree_fn, sufix = "_"+syn.lower()+"_"+str(curr_subset_id))
    
    print("phy3_compute_subset_tree.py. Finished.")
    