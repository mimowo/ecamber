import os
import sys
import time

sys.path.append("src/")
sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.structs.aln_filenames import *
from alnsoft.utils.camber_aln_utils import *
from alnsoft.utils.camber_phylo_utils import *

python_exe = sys.executable + " "

curr_dir = os.path.dirname(os.path.abspath(__file__))
os.chdir(curr_dir)

if __name__ == '__main__':
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    os.chdir(curr_dir)    
    print(os.getcwd())
    
    if "SYN" in parameters: syn = parameters["SYN"]
    else: syn = parameters["SYN"]
       
    if "MRAD" in parameters: mrad = int(parameters["MRAD"])
    else: mrad = 0
    
    input_fn = parameters["PHYLOGENY_FOLDER"] + "current_subset-"+syn.lower()+"_" + str(mrad)+'.txt'

    if "CURR" in parameters: curr_subset_id = int(parameters["CURR"])
    elif os.path.exists(input_fn): curr_subset_id = int(readCurrentSubsetID(input_fn))
    else: curr_subset_id = 0  

    params_txt = ""
    parameters["CURR"] = str(curr_subset_id)
    for param_key in parameters:
        params_txt += param_key + "=" + parameters[param_key] + " "
    
    if not os.path.exists(input_fn):
        os.system(python_exe + curr_dir + "/phy1_init_strain_subsets.py " +params_txt)
    
    finished = False
    
    while curr_subset_id < 1000 and not finished:
        print("current itaration: " + str(curr_subset_id))
        params_txt = ""
        parameters["CURR"] = str(curr_subset_id)
        for param_key in parameters:
            params_txt += param_key + "=" + parameters[param_key] + " "
        
        if os.system(python_exe + curr_dir + "/phy2_connect_point_mutations.py " +params_txt): exit()
        if os.system(python_exe + curr_dir + "/phy3_compute_subset_tree.py " +params_txt): exit()
        if os.system(python_exe + curr_dir + "/phy4_flatten-tree.py " +params_txt): exit()
        if os.system(python_exe + curr_dir + "/phy5_update-tree-subsets.py " +params_txt): exit()
        
        subsets_old_map = readStrainSubsetsMap(parameters["PHYLOGENY_FOLDER"] + "subsets-"+syn.lower()+"_" + str(mrad)+'.txt')
        max_id = 1
        for key_id in subsets_old_map.keys():
            if int(key_id) >= max_id:
                max_id = int(key_id)
        
        curr_subset_id += 1
        
        if curr_subset_id > max_id:
            finished = True
            print("Finished", curr_subset_id)
            break
    
    if finished:
        if os.system(python_exe + curr_dir + "/phy6_combine-trees.py " +params_txt): exit()
