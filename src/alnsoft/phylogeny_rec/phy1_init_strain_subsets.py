import os
import sys
import time
from StringIO import StringIO
from multiprocessing import Pool
import multiprocessing

sys.path.append("src/")
sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.structs.aln_filenames import *
from alnsoft.utils.camber_aln_utils import *
from alnsoft.utils.camber_phylo_utils import *

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    print("phy1_init_strain_subsets.py.")
    
    strains = readStrainsInfo()
    
    ensure_dir(parameters["PHYLOGENY_FOLDER"])
    ensure_dir(parameters["PHYLOGENY_FOLDER_PSTREE"])
    ensure_dir(parameters["PHYLOGENY_FOLDER_PHYLOTREE"])
    ensure_dir(parameters["PHYLOGENY_FOLDER_PHYLOFILE"])
    ensure_dir(parameters["PHYLOGENY_FOLDER_MUTATIONS"])
    ensure_dir(parameters["PHYLOGENY_FOLDER_FLATPHYLOTREE"])
    
    genomes_list = sorted(list(strains.allStrains()))
    
    if "SYN" in parameters: syn = parameters["SYN"]
    else: syn = parameters["SYN"]
       
    if "MRAD" in parameters: mrad = int(parameters["MRAD"])
    else: mrad = 0

    output_fn = parameters["PHYLOGENY_FOLDER"] + "current_subset-"+syn.lower()+"_" + str(mrad).lower()+'.txt'
    if os.path.exists(output_fn):
        print("File already exists: " + output_fn)
        exit()
    
    output_fh = open(output_fn, "w")
    output_fh.write("0\n")
    output_fh.close()
    
    output_fn = parameters["PHYLOGENY_FOLDER"] +  "subsets-"+syn.lower()+"_" + str(mrad).lower()+'.txt'
    if os.path.exists(output_fn):
        print("File already exists: " + output_fn)
        exit()
    
    output_fh = open(output_fn, "w")
    text_line = "0"
    for strain_id in genomes_list:
        text_line += "\t" + strain_id
    output_fh.write(text_line + "\n")
    output_fh.close()
    
    print("phy1_init_strain_subsets.py. Finished.")
    