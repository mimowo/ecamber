import os
import sys
import time
from StringIO import StringIO
from multiprocessing import Pool
import multiprocessing

sys.path.append("src/")
sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.structs.aln_filenames import *
from alnsoft.utils.camber_aln_utils import *
from alnsoft.utils.camber_phylo_utils import *
from alnsoft.utils.camber_mut_utils import findSNPsFiles

def readIdMgsMap(input_fh):
    id_mgs_map = {}
    for line in input_fh.readlines():
        tokens = line.strip().split("\t")
        if len(tokens) < 2:
            continue
        seq_id = tokens[0]
        mg_id = tokens[1]
        if not seq_id in id_mgs_map:
            id_mgs_map[seq_id] = set([])
        id_mgs_map[seq_id].add(mg_id)
    return id_mgs_map

def readIdStrainsMap(input_fh):
    id_strains_map = {}
    for line in input_fh.readlines():
        tokens = line.strip().split()
        if len(tokens) < 4:
            continue
        seq_id = tokens[0]
        strain_id = tokens[4]
        if not seq_id in id_strains_map:
            id_strains_map[seq_id] = set([])
        id_strains_map[seq_id].add(strain_id)
    return id_strains_map


if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    print("phy2_connect_point_mutations.py.")
    
    strains = readStrainsInfo()
    
    if "SYN" in parameters: syn = parameters["SYN"]
    else: syn = parameters["SYN"]
       
    if "MRAD" in parameters: mrad = int(parameters["MRAD"])
    else: mrad = 0
        
    progress = ShowProgress("Load strain sequences")
    strains_list = strains.allStrains()
    
    progress.setJobsCount(strains.count())
    
    subsets_map = readStrainSubsetsMap(parameters["PHYLOGENY_FOLDER"] + "subsets-"+syn.lower()+"_" + str(mrad)+'.txt')
    if "CURR" in  parameters:
        curr_subset_id = int(parameters["CURR"])
    else:
        curr_subset_id = int(readCurrentSubsetID(parameters["PHYLOGENY_FOLDER"] + "current_subset-"+syn.lower()+"_" + str(mrad)+'.txt'))

    strains_subset = subsets_map[curr_subset_id]

    acc_amb = False
    fns_to_search = []
    input_fh = open(parameters_map["ALIGNMENTS_FOLDER"] + "aligned_clusters.txt")
    for line in input_fh.readlines():
        tokens = line.strip().split()
        if len(tokens) < 1:
            continue
        filename_id = tokens[0]
        cluster_id = tokens[1]

        aa_seq_aln_count = int(tokens[len(tokens)-3])
        aa_aln_count = int(tokens[len(tokens)-1])
        if syn == "N" and aa_aln_count < 2:
            continue
        if aa_seq_aln_count < 0.95*strains.count():
            continue
        aln_fn = parameters_map["ALIGNMENTS_FOLDER_ALN_AA"] + "/" + str(filename_id) + ".aln"
        if not os.path.exists(aln_fn) and aa_aln_count >= 2:
            print("file does not exist: ", aln_fn)
            exit()
            
        fns_to_search.append((filename_id, cluster_id, aa_aln_count, strains_subset, syn, mrad, strains.count(), 0, False, acc_amb))
    input_fh.close()
    
    progress = ShowProgress("Identify point mutations")
    n = len(fns_to_search)
    progress.setJobsCount(n)
    
    connected_alingment = {}
    for strain_id in strains_subset:
        strain_short_id = strains.strain(strain_id).short_name
        connected_alingment[strain_short_id] = ""
    
    TASKS =  fns_to_search
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())

    cc_mut = 0
    cc_ch = 0
    
    if WORKERS>1:
        pool = Pool(processes=WORKERS)
    
        for r in pool.imap(findSNPsFiles, TASKS):
            if r == None:
                progress.update()
                continue            
            mutations_t, filename_id, cluster_id = r
            cc_ch += 1
            if len(mutations_t) > 0:
                cc_mut += 1
            for mutation in mutations_t:
                
                (m_position,ref_position, m_vector, m_is_syn) = mutation
                for strain_id in strains_subset:
                    strain_short_id = strains.strain(strain_id).short_name
                    connected_alingment[strain_short_id] += m_vector[strain_id]
            progress.update()
    else:
        for TASK in TASKS:
            r = findSNPsFiles(TASK)
            if r == None:
                progress.update()
                continue
            
            mutations_t, filename_id, cluster_id = r
            cc_ch += 1
            if len(mutations_t) > 0:
                cc_mut += 1
            for mutation in mutations_t:
                (m_position, ref_position, m_vector, m_is_syn) = mutation
                for strain_id in strains_subset:
                    strain_short_id = strains.strain(strain_id).short_name
                    connected_alingment[strain_short_id] += m_vector[strain_id]
            progress.update()
        
    print("Checked clusters: ", len(TASKS))
    print("Checked clusters in all substrains: ", cc_ch)
    print("Checked clusters with any mutations: ", cc_mut)
    print("Number of mutations: ", len(connected_alingment[strain_id]))
    
    connected_alingment_ids = {}
    ids_map = {}
    index = 0
    for strain_id in connected_alingment.keys():
        connected_alingment_ids[str(index)] = connected_alingment[strain_id]
        ids_map[str(index)] = strain_id
        index += 1
#    print(syn_count, nons_count)
    output_fn = parameters["PHYLOGENY_FOLDER_MUTATIONS"] + 'snp-mutations-' +syn.lower()+"_"+str(curr_subset_id)+"_" + str(mrad)+'.txt'
    output_fh = open(output_fn, "w");
    savePHYLIPalignements(connected_alingment_ids, output_fh, sorting = lambda strain_id:strain_id)
    output_fh.close()

    output_fn = parameters["PHYLOGENY_FOLDER_MUTATIONS"] + 'snp-strain-ids-' +syn.lower()+"_"+str(curr_subset_id)+"_" + str(mrad)+'.txt'
    output_fh = open(output_fn, "w");
    for index in  ids_map:
        output_fh.write(str(index) + "\t" + ids_map[index] + "\n")
    output_fh.close()

    print("phy2_connect_point_mutations.py. Finished.")
