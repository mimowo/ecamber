import os
import sys
import time
from StringIO import StringIO

sys.path.append("src/")
sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.structs.aln_filenames import *
from alnsoft.utils.camber_aln_utils import *
from alnsoft.utils.camber_phylo_utils import *
from alnsoft.utils.camber_phylo_io_utils import readPhyloTree


def readPhyloTreeFile(input_fh, strains):
    lines = input_fh.readlines()
    tree_txt = ""    
    for line in lines:
        tree_txt += line.strip()
        if ";" in line:
            #tree = readPhyloTree(tree_txt, strains)
            tree = readPhyloTree(tree_txt)
            tree_txt = ""       
    if tree_txt != "":
        #tree = readPhyloTree(tree_txt, strains)
        tree = readPhyloTree(tree_txt)        
    return tree

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    print("phy6_combine-trees.py.")
    
    strains = readStrainsInfo()
    
    if "SYN" in parameters: syn = parameters["SYN"]
    else: syn = parameters["SYN"]
       
    if "MRAD" in parameters: mrad = int(parameters["MRAD"])
    else: mrad = 0

    subsets_map = readStrainSubsetsMap(parameters["PHYLOGENY_FOLDER"] + "subsets-"+syn.lower()+"_" + str(mrad)+'.txt')
        
    subsets_old_map = readStrainSubsetsMap(parameters["PHYLOGENY_FOLDER"] + "subsets-"+syn.lower()+"_" + str(mrad)+'.txt')
    subsets_old_map_rev = {}
    
    for key_id in subsets_old_map.keys():
        subtree_text =  setToStr(subsets_old_map[key_id])
        subsets_old_map_rev[subtree_text] = key_id
    
    input_fh = open(parameters["PHYLOGENY_FOLDER_FLATPHYLOTREE"] + 'flatphylotree-' +syn.lower()+"_"+str(0)+"_" + str(mrad)+'.txt')
    tree = readPhyloTreeFile(input_fh, strains)
    input_fh.close()
    findSubsetsTree(tree)
    
    last_subset_str = setToStr(tree.root.substrains)
    
    exc_str_subsets = set([last_subset_str])
    
    counter = 0
    
    output_fh = open(parameters["PHYLOGENY_FOLDER"] + 'combined-tree-' +syn.lower()+"_" + str(mrad)+"_"+str(counter)+'.txt', "w")
    output_fh.write(tree.toString(lengths=False) + ";\n")
    output_fh.close()
    
    while True:
        node = findNonBinarySubsetNode(tree, exc_str_subsets)
        if node == None:           
            break
        subset = node.substrains
        subset_str = setToStr(subset)
        exc_str_subsets.add(subset_str)
        
        if not subset_str in subsets_old_map_rev:
            exit()
        else:
            subset_id = subsets_old_map_rev[subset_str]
            input_fn = parameters["PHYLOGENY_FOLDER_FLATPHYLOTREE"] + 'flatphylotree-' +syn.lower()+"_"+str(subset_id)+"_" + str(mrad)+'.txt'
            if os.path.exists(input_fn):
                input_fh = open(input_fn)
                subtree = readPhyloTreeFile(input_fh, strains)
                input_fh.close()
                findSubsetsTree(subtree)
                
                tree = replaceSubnode(tree, node, subtree)
                
        counter += 1
        output_fh = open(parameters["PHYLOGENY_FOLDER"] + 'combined-tree-' +syn.lower()+"_" + str(mrad)+"_"+str(counter)+'.txt', "w")
        output_fh.write(tree.toString(lengths=False) + ";\n")
        output_fh.close()
    
    output_fh = open(parameters["PHYLOGENY_FOLDER"] + 'phylo_tree_muts.txt', "w")
    output_fh.write(tree.toString(lengths=False) + ";\n")
    output_fh.close()    

    print("phy6_combine-trees.py. Finished.")
     
            
            
        