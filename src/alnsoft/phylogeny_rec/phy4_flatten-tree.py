import os
import sys
import time
from StringIO import StringIO
from multiprocessing import Pool
import multiprocessing

sys.path.append("src/")
sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.structs.aln_filenames import *
from alnsoft.utils.camber_aln_utils import *
from alnsoft.utils.camber_phylo_utils import *
from alnsoft.utils.camber_phylo_io_utils import *


def flatTreeTxt(tree_text, output_fh, strains_map = {}):
    tree = readPhyloTree(tree_text, strains_map=strains_map)
    flat_tree = tree.flatten(0.00001)
    flat_tree_txt = flat_tree.toString(lengths=True, strains_map=strains_map)
    output_fh.write(flat_tree_txt+";\n")
    
    queue = deque([])

    queue.append(tree.root)
    while queue:
        node = queue.popleft()
    #    if len(node.children) == 0:
     #       print(node.node_id) 
        for child_node in  node.children:
            queue.append(child_node)

def flatTrees(input_fh, output_fh, strains_map={}):
    lines = input_fh.readlines()
    tree_txt = ""
    count = 0
    for line in lines:
        tree_txt += line.strip()
        if ";" in line:
            flatTreeTxt(tree_txt, output_fh, strains_map)
            count += 1
            tree_txt = ""
    if tree_txt != "":
        flatTreeTxt(tree_txt, output_fh, strains_map)
        count += 1


if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    
    print("phy4_flatten-tree.py.")
    
    strains = readStrainsInfo()
    
    if "SYN" in parameters: syn = parameters["SYN"]
    else: syn = parameters["SYN"]
       
    if "MRAD" in parameters: mrad = int(parameters["MRAD"])
    else: mrad = 0
    
    subsets_map = readStrainSubsetsMap(parameters["PHYLOGENY_FOLDER"] + "subsets-"+syn.lower()+"_" + str(mrad)+'.txt')
    if "CURR" in  parameters:
        curr_subset_id = int(parameters["CURR"])
    else:
        curr_subset_id = int(readCurrentSubsetID(parameters["PHYLOGENY_FOLDER"] + "current_subset-"+syn.lower()+"_" + str(mrad)+'.txt'))

    strains_map = {}
    input_fn = parameters["PHYLOGENY_FOLDER_MUTATIONS"] + 'snp-strain-ids-' +syn.lower()+"_"+str(curr_subset_id)+"_" + str(mrad)+'.txt'
    input_fh = open(input_fn);
    for line in input_fh.readlines():
        tokens = line.strip().split("\t")
        if len(tokens) < 2:
            continue
        index = tokens[0]
        strain_id = tokens[1]
        strains_map[index] = strain_id
    input_fh.close()

    input_fn = parameters["PHYLOGENY_FOLDER_PHYLOTREE"] + 'phylotree-' +syn.lower()+"_"+str(curr_subset_id)+"_" + str(mrad)+'.txt'
    output_fn = parameters["PHYLOGENY_FOLDER_FLATPHYLOTREE"] + 'flatphylotree-' +syn.lower()+"_"+str(curr_subset_id)+"_" + str(mrad)+'.txt'
    
    if os.path.exists(input_fn):
        input_fh = open(input_fn)
        output_fh = open(output_fn, "w")
        flatTrees(input_fh, output_fh, strains_map)
        output_fh.close();
        input_fh.close()
    else:
        text_line = "("
        strains_subset = subsets_map[curr_subset_id]
        for strain_id in strains_subset:
            text_line += strain_id + ","
        text_line = text_line[:-1] + ");"
        
        output_fh = open(output_fn, "w")
        output_fh.write(text_line + "\n")
        output_fh.close();
        

    print("phy4_flatten-tree.py. Finished.")
    