import sys
import os
import multiprocessing
from soft.structs.multigene import createMultigeneId


sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import ensure_dir, readStrainsFromFile
from soft.utils.camber_format_utils import convertFeatureTab, convertGenBank


def convertAnns(strain_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains_tmp.txt"
    if not os.path.exists(input_fn):
        input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains.txt"
    input_fh = open(input_fn)
    strains = readStrainsFromFile(input_fh)
    input_fh.close()
    
    strain = strains.strain(strain_id)
    
    readStrainSequences(strain)
    #annotation = readOrgAnnotations(strain, src="prodigal")
    input_fn = parameters["DATASET_ANNS_PARSED"] + "/prodigal/" + strain_id + ".txt"
    strain_id, gene_ann = readOrgAnnotations(strain, src="prodigal")
    
    mg_ann = Annotation()
    
    for gene in gene_ann.genes.values():
        #mg_id = createStrainMultigeneId(gene.end, gene.strand, strain_id, gene.contig_id)
        multigene = Multigene(gene.start, gene.end, gene.strand, strain, gene.gene_id, gene.gene_id, gene.contig_id)
        multigene.addGene(gene)
        mg_ann.addMultigene(multigene)
    
    ensure_dir(parameters["ECAMBER_DATASET_PATH"] + "/gmv/after_gmv/")
    input_fn = parameters["ECAMBER_DATASET_PATH"] + "/gmv/gene-predictions.txt"
    input_fh = open(input_fn)
    
    for line in input_fh.readlines()[1:]:
        line = line.strip()
        tokens = line.split("\t")
        if len(tokens) < 4: continue
        strain_id_tmp, contig_id = tokens[0].split(".")[:2]
        #cluster_id_tmp = tokens[1]
        if strain_id_tmp != strain_id: continue
        
        left_b = int(tokens[5])
        right_b = int(tokens[6])
        
        left_b_old = int(tokens[3])
        right_b_old = int(tokens[4])
        
        if left_b == left_b_old and right_b == right_b_old:
            continue
        print(left_b, right_b, tokens[8])
        
        if tokens[7] == "1": 
            strand = "+"
            start, end = left_b, right_b
        else: 
            strand = "-"
            start, end = right_b, left_b
            
            
        mg_id = createMultigeneId(end, strand, contig_id, strain_id)
        gene = Gene(start, end, strand, strain, "x", "x", contig_id, True)
        
        if not mg_id in mg_ann.multigenes:
            print(mg_id, list(mg_ann.multigenes.keys())[:2])
        else:
            mg = mg_ann.multigenes[mg_id]
            gene_len = gene.length()
            if gene_len in mg.lengths:
                mg.genes[gene_len].selected = True
                mg.sel_lengths.add(gene_len)
            else:
                mg.addGene(gene)
    input_fh.close()
    
    for mg in mg_ann.multigenes.values():
        if len(mg.genes) == 1:
            for gene in mg.genes.values():
                gene.selected = True
                mg.sel_lengths.add(gene.length())
        
    print(len(mg_ann.multigenes))
        
    output_fn = parameters["ECAMBER_DATASET_PATH"] + "/gmv/after_gmv/" + strain_id + ".txt"
    output_fh = open(output_fn, "w")
    saveMgsAnnotations(output_fh, genes=None, multigenes=mg_ann.multigenes.values(), show_start_codons=True, cluster_ids=False, smr=0.0, std=False, cluster_gene_names={})             
    output_fh.close()
                
    return strain_id

if __name__ == '__main__':

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains_tmp.txt"
    if not os.path.exists(input_fn):
        input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains.txt"
    input_fh = open(input_fn)
    strains = readStrainsFromFile(input_fh)
    input_fh.close()
    
    ensure_dir(parameters["DATASET_ANNS_PARSED"] + "/prodigal/")
        
    strains_list = strains.allStrains()
    #strains_list = []
    print("pro3_format_anns.py: Formats PRODIGAL annotations.")

    n = len(strains_list)
    progress = ShowProgress("Formatted PRODIGAL annotations")
    progress.setJobsCount(n)
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())

    TASKS = strains_list
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)
        for r in pool.imap_unordered(convertAnns, TASKS):
            progress.update(desc=r)    
        pool.close() 
        pool.join()
    else:
        for TASK in TASKS:
            r = convertAnns(TASK)
            progress.update(desc=r)
        
    print("pro3_format_anns.py: Finished.")

