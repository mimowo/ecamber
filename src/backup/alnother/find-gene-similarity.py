import os
import sys
import time
from StringIO import StringIO

sys.path.append("../../../../ecamber/src/")
from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.structs.aln_filenames import *
from alnsoft.utils.camber_aln_utils import *
from alnsoft.utils.camber_phylo_utils import *

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
parameters = readParametersFromFile("../../../config/config_aln_paths.txt", parameters)
parameters = readParametersFromFile("../../../config/config_aln_params.txt", parameters)

phase = 0
if "PHASE" in parameters:
    phase = int(parameters["PHASE"])

ensure_dir(parameters["MUTATIONS_RESULTS"])
pangenome_dir = parameters["CAMBER_PARAM_RESULTS_FOLDER"]+"pangenome-all/"
fasta_dir = pangenome_dir +"fasta/"
info_dir = pangenome_dir +"info/"
mapping_dir = pangenome_dir +"mapping/"
results_dir = pangenome_dir +"results/"

diffTime("params")
strains = readStrainsInfo()
diffTime("info")
print((strains.allStrains()))

diffTime("sequences")
#readExtAnnotations(strains.allStrains())
diffTime("annotations")
multigene_graph = MultigeneGraph()
diffTime()
#input_fn = parameters_map["CLOSURE_RESULTS_GRAPHS"] + "mult_details.txt"
input_fn = parameters_map["CLOSURE_RESULTS_GRAPHS"] + "cluster-comb-det-" + str(phase) + ".txt"
input_fh = open(input_fn)
multigene_graph = readMultigenesGraphFast2(strains, multigene_graph, input_fh)
input_fh.close()
diffTime("graph")


for genome_id in strains.allGenomes():
    print((len(multigene_graph.nodes_by_strain[genome_id])))
diffTime("strain")

input_fn2 = parameters_map["CLOSURE_RESULTS_GRAPHS"] + "cluster-comb-det-" + str(phase) + ".txt"
input_fh2 = open(input_fn2)
clusters = readClusters2(multigene_graph, input_fh=input_fh2)
clusters.classifyClusters()
input_fh2.close()
diffTime("classified")

strains_ord = strains.getOrderedGenomes(strains.allGenomes())

cluster_fh = open(input_fn2)
clusters, id_cluster_map = readClusters(cluster_fh, strains)
cluster_fh.close()

anns_fn = parameters_map["CLOSURE_RESULTS"] + "names_anns.txt"
if os.path.exists(anns_fn):
    anns_fh = open(anns_fn)
    names_mapping = readNamesMapping(anns_fh)
    anns_fh.close()
else:
    names_mapping = None

mut_file = parameters["MUTATIONS_RESULTS"] + 'gene-sim-mutations-' + str(phase) + '.txt';
mut_fh = open(mut_file, "w");
writeMutationsHeader(mut_fh, strains);

mg_id_mapping = {}
mg_id_mapping_rev = {}

blast_hits = {}

def readMappings(strain_id):
    if not strain_id in mg_id_mapping:
        mg_id_mapping[strain_id] = {}
        mg_id_mapping_rev[strain_id] = {}
    mapping_output_fn = mapping_dir+"all-"  + strain_id+".txt"
    mapping_output_fh = open(mapping_output_fn)
    for line in mapping_output_fh.readlines():
        line = line.strip()
        tokens = line.split("\t")
        if tokens >= 1:
            mg_ass_id= tokens[0]
            mg_id = tokens[1]
            mg_id_mapping[strain_id][mg_ass_id] = mg_id
            mg_id_mapping_rev[strain_id][mg_id] = mg_ass_id
    mapping_output_fh.close()

def readAABlastHits(strain1_id, strain2_id):
    input_fn = results_dir + strain1_id+"/blast-"  + strain1_id+"-"+strain2_id + ".txt"
    input_fh = open(input_fn)
    
    blast_hits_map = {}
    
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split()
        if len(tokens) >= 1:
            mg1_ass_id = tokens[0]
            mg2_ass_id = tokens[1]
            b_pid = float(tokens[2])
    
            gene_start = int(tokens[8])
            gene_end = int(tokens[9])
    
            if gene_start<gene_end:
                gene_strand = "+"
                query_start = int(tokens[6])
                query_end = int(tokens[7])
            else:
                gene_strand = "-"
                query_start = int(tokens[6])
                query_end = int(tokens[7])
            query_len = query_end - query_start + 1
            gene_len = gene_end - gene_start + 1
            evalue = tokens[10]
            aln_len = int(tokens[3])
            identity_b = float(tokens[2])
            identities = int(round(identity_b*aln_len/100))
            mismatches = int(tokens[4])
            residues = identities+mismatches
            pid = float(identities)/float(max(query_len, gene_end))
            thr = hssp(residues, 100*0.5-19.5)
            if not mg1_ass_id in blast_hits_map:
                blast_hits_map[mg1_ass_id] = set([])
            blast_hits_map[mg1_ass_id].add((mg2_ass_id, b_pid, pid, thr))
            
    input_fh.close()
    return blast_hits_map
    
diffTime("reading mappings")

for strain_id in strains.allGenomes():
    readMappings(strain_id)

diffTime("reading hits")

for strain1_id in ["TW20", "Mu50"]:#strains.allGenomes():
    diffTime(strain1_id)
    for strain2_id in strains.allGenomes():
        if strain1_id == strain2_id:
            continue
        blast_hits[(strain1_id, strain2_id)] = readAABlastHits(strain1_id, strain2_id)

diffTime("components")

for cluster in list(clusters.clusters.values()):
    text = ""
    
    text += cluster.name + "\t"    
    text += "x" + "\t"    
    gene_id = cluster.name
        
    if not clusters == None:
        mapped_gene_id = mapID(gene_id, clusters, id_cluster_map, strains_ord)
        if mapped_gene_id == "x":
            mapped_gene_id = gene_id
    else:
        mapped_gene_id = gene_id

    gene_name = ""
    if names_mapping != None:
        gene_name = mapGeneName(gene_id, clusters, id_cluster_map, strains_ord, names_mapping)
        if gene_name == "x":
            gene_name = mapped_gene_id

    #gene_name = cluster.geneName(genomes_list) 
    text += gene_name + "\t" 
    
    text += "gene" + "\t"
    text += "0" + "\t"
    diff = False
    
    for strain_id in strains.allGenomes():
        if str(cluster.name) == "2446" and strain_id=="A5937":
            print(str(cluster.name), strain_id, len(cluster.nodesByStrain(strain_id)))
        strain = strains.strain(strain_id)
        if len(cluster.nodesByStrain(strain_id)) > 0:
            text += "G:"+strain_id+":"+str(len(cluster.nodesByStrain(strain_id)))
        else:
            diff = True
            max_pid = 0.0
            max_real_pid = 0.0
            max_thr = 0.0
            for strain2_id in cluster.nodes_by_strain:
                
                if not (strain2_id, strain_id) in blast_hits:
                    continue
                for mg_node in list(cluster.nodes_by_strain[strain2_id].values()):
                    #print(mg_node.multigene.mg_longest_unique_id)
                    mg_ass_id = mg_id_mapping_rev[strain2_id][mg_node.multigene.mg_unique_id]
                    if mg_ass_id in blast_hits[(strain2_id, strain_id)]:
                        hits_set = blast_hits[(strain2_id, strain_id)][mg_ass_id]
                        for hit in hits_set:
                            (mg_ass_id2, pid, real_pid, thr) = hit
                            if real_pid > max_real_pid:
                                max_pid = pid
                                max_real_pid = real_pid
                                max_thr = thr
            text += "L:"+strain_id+":"+str(max_pid)+":"+str(max_real_pid)+":"+str(max_thr)
        text += "\t"
    if diff:
        mut_fh.write(text + "\n")
        
mut_fh.close()
