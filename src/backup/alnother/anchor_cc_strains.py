import sys
import time
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_graph_utils import *
from soft.structs.cluster import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *

def readMultigeneClusterStrainLocal(params):
    strain_id = params[0]
    iteration = params[1] 
    overwriteParameters(sys.argv)
    readParameters() 
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    
    return readMultigeneClusterStrain(strain, iteration)
    
    
if __name__ == '__main__':
    print("analysis_anns_table.py: read and saves connected components.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    parameters = overwriteParameters(sys.argv)
    parameters = readParametersFromFile("config/config_aln_paths.txt", parameters)
    
    ensure_dir(parameters["DATASET_RESULTS_EXP_ANALYSIS"])
    strains = readStrainsInfo()
    strains_list = strains.allStrains()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    iteration = lastIterationWithChange()
    
    clusters = ConnectedComponents()
    
    TASKS = []
    for strain_id in strains_list:
        TASKS.append((strain_id, iteration))
    
    progress = ShowProgress("Read connected components")
    progress.setJobsCount(len(TASKS))
        
    pool = multiprocessing.Pool(processes = WORKERS)    
    for strain_id, cluster_mapping in pool.imap(readMultigeneClusterStrainLocal, TASKS):
        
        for cluster_id in cluster_mapping:
            if not cluster_id in clusters.clusters:
                cluster = ConnectedComponent(cluster_id)
                clusters.addCluster(cluster)
            else:
                cluster = clusters.getCluster(cluster_id)
            for multigene in cluster_mapping[cluster_id]:
                cluster.addMultigene(multigene)
        
        progress.update(desc = strain_id)
    pool.close()
    
    clusters.classifyClusters(strains_list)
    
    ensure_dir(parameters["CONSISTANCY_FOLDER"])
    
    cc_lines = {}
    cc_ann_lines = {}
    
    for cluster in clusters.anchors.values():
        cluster_id = cluster.cluster_id
        strains_subset = sorted(cluster.strains())
        strains_ann_subset = sorted(cluster.ann_strains())
        
        if len(strains_ann_subset) == 0:
            continue
        
        text_line = cluster_id + "\t"
        for strain_id in strains_subset:
            text_line += strain_id + ";"
        text_line = text_line[:-1]
        
        text_ann_line = cluster_id + "\t"
        for strain_id in strains_ann_subset:
            text_ann_line += strain_id + ";"
        text_ann_line = text_ann_line[:-1]
        
        cc_lines[cluster_id] = text_line
        cc_ann_lines[cluster_id] = text_ann_line    
    
    output_fn = parameters["CONSISTANCY_FOLDER"] + "anochor_cc_strains.txt"
    output_fh = open(output_fn, "w")
    for cluster_id in sorted(cc_lines, key=lambda cluster_id:int(cluster_id)):
        text_line = cc_lines[cluster_id]
        output_fh.write(text_line + "\n")
    output_fh.close()

    output_fn = parameters["CONSISTANCY_FOLDER"] + "anochor_cc_ann_strains.txt"
    output_fh = open(output_fn, "w")
    for cluster_id in sorted(cc_ann_lines, key=lambda cluster_id:int(cluster_id)):
        text_line = cc_ann_lines[cluster_id]
        output_fh.write(text_line + "\n")
    output_fh.close()

    
    print("analysis_anns_table.py: Finished.")
        
        
        
        
        
        