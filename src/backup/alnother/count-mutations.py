import os
import sys
import time
from StringIO import StringIO

sys.path.append("../../../../ecamber/src/")
from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.structs.aln_filenames import *
from alnsoft.utils.camber_aln_utils import *
from alnsoft.utils.camber_phylo_utils import *

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
parameters = readParametersFromFile("../../../config/config_aln_paths.txt", parameters)
parameters = readParametersFromFile("../../../config/config_aln_params.txt", parameters)

phase = 0
if "PHASE" in parameters:
    phase = int(parameters["PHASE"])

diffTime("params")
strains = readStrainsInfo()
diffTime("info")
#readStrainSequences(strains,strains.allGenomes())
#diffTime("sequences")
#readExtAnnotations(strains.allStrains())
diffTime("annotations")
multigene_graph = MultigeneGraph()
diffTime()
input_fn = parameters_map["CLOSURE_RESULTS_GRAPHS"] + "mult_details.txt"
input_fh = open(input_fn)
multigene_graph = readMultigenesGraphEff(strains.allGenomes(), multigene_graph, input_fh)
input_fh.close()
diffTime("graph")

for strain_id in strains.allStrains():
    print(len(multigene_graph.nodes_by_strain[strain_id]))
diffTime("strain")

input_fn = parameters_map["CLOSURE_RESULTS_GRAPHS"] + "cluster-det-" + str(phase) + ".txt"
input_fh = open(input_fn)
clusters = readClusters(multigene_graph, input_fh=input_fh)
clusters.classifyClusters()
input_fh.close()
diffTime("classified")

strains_ord = strains.getOrderedGenomes(strains.allGenomes())

cluster_fh = open(input_fn)
clusters, id_cluster_map = readClusters(cluster_fh, strains)
cluster_fh.close()

anns_fn = parameters_map["CLOSURE_RESULTS"] + "names_anns.txt"
if os.path.exists(anns_fn):
    anns_fh = open(anns_fn)
    names_mapping = readNamesMapping(anns_fh)
    anns_fh.close()
else:
    names_mapping = None

aln_filenames = AlnFilenames()
filenames_fn = parameters_map["PHYLOGENY_FOLDER"] + "filenames.txt"
filenames_fh = open(filenames_fn)

lines = filenames_fh.readlines()
filenames_fh.close();
for line in lines:
    aln_filenames.addMappingByLine(line)

#all_mutations = loadMutationsFromAlignments(list(clusters.anchors.values()))

ident_pos, indel_muts, point_muts = countMutationsFromAlignments(list(clusters.anchors_in_all_strain.values()), aln_type="AA", gaps_as_aa=True)
print(len(clusters.clusters),  len(clusters.anchors_in_all_strain), len(clusters.cluster_in_all_strain))
print(ident_pos, indel_muts, point_muts)



