import sys
import os
import time
import platform

def getCamberPathRun():
    curr_path = os.path.abspath(sys.argv[0])
    curr_path = os.path.abspath(os.path.join(curr_path, ".."))
    tmp_path = curr_path
    camber_path = curr_path
    last_tmp_path = tmp_path
    it_count = 0
    while len(tmp_path) > 1 and it_count < 20:
        it_count += 1
        curr_dir = os.path.basename(tmp_path)
        curr_path = os.path.abspath(os.path.join(tmp_path, ".."))
        sys.stdout.flush()
        
        if curr_dir.startswith("ecamber"):
            camber_path = tmp_path + "/"
            break
        elif os.path.exists(curr_path + "/ecamber/"):
            camber_path = curr_path + "/ecamber/"
            break
        else:
            tmp_path = curr_path
        if last_tmp_path == tmp_path or it_count == 20:
            print("Cannot determine CAMBer path")
            exit()
    return camber_path;

sys.path += ["src/", "../", "../../"]

run_path = os.path.abspath(os.curdir)
camber_path = getCamberPathRun()
os.chdir(camber_path)

from soft.utils.camber_params_utils import *
from soft.utils.camber_closure_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_graph_utils import *
from soft.utils.camber_params_chk_utils import *

if sys.version.startswith("3"): raw_input = input

def createCommandParams(attrs, parameters):
    new_cmd = ""
    for key in attrs:
        if key in parameters:
            new_cmd += " "+key+"="+parameters[key]
    return new_cmd

def mutations(parameters):
    os.chdir(os.path.abspath("src/alnsoft/mutations/"))
    
    params = ["D", "EP", "W", "TE", "HSSP", "PID_TYPE", "PID", "PLC", "LCM", "AS"]
    passed_params = createCommandParams(params, parameters)

    t0_time = time.time()
    if os.system(python_cmd + " muts0-save-strains-ordered.py" + passed_params): exit()
    if os.system(python_cmd + " muts1-point_prom_mutations.py" + passed_params): exit()
    if os.system(python_cmd + " muts2-find-point_mutations.py" + passed_params): exit()
    if os.system(python_cmd + " muts3-gene-mutations.py" + passed_params): exit()
    if os.system(python_cmd + " muts4-combine-mutations.py" + passed_params): exit()
    t1_time = time.time()
    os.chdir("../../../")
    logExecutionTime(t1_time - t0_time, "mutations")
    


def phylogeny_muts(parameters):
    os.chdir(os.path.abspath("src/alnsoft/phylogeny/"))
    
    params = ["D", "EP", "W", "TE", "HSSP", "PID_TYPE", "PID", "PLC", "LCM", "AS"]
    passed_params = createCommandParams(params, parameters)

    t0_time = time.time()
    if os.system(python_cmd + " resolve_all_subtrees.py" + passed_params): exit()
    t1_time = time.time()
    os.chdir("../../../")
    logExecutionTime(t1_time - t0_time, "phylogeny_new")

def phylogeny_genes(parameters):
    os.chdir(os.path.abspath("src/alnsoft/phylogeny_genes/"))
    
    params = ["D", "EP", "W", "TE", "HSSP", "PID_TYPE", "PID", "PLC", "LCM", "AS"]
    passed_params = createCommandParams(params, parameters)

    t0_time = time.time()
    if os.system(python_cmd + " compute_tree.py" + passed_params): exit()
    t1_time = time.time()
    os.chdir("../../../")
    logExecutionTime(t1_time - t0_time, "phylogeny_new")
    

def alignments(parameters):
    dir = os.path.abspath("src/alnsoft/alignments/")
    os.chdir(dir)
    
    params = ["D", "EP", "W", "TE", "HSSP", "PID_TYPE", "PID", "PLC", "LCM", "AS", "STEP"]
    passed_params = createCommandParams(params, parameters)

    t0_time = time.time()
    if os.system(python_cmd + " algs1_prepare_sequences_simple.py" + passed_params): exit()
    if os.system(python_cmd + " algs2_compute_alignments.py" + passed_params): exit()
    if os.system(python_cmd + " algs3_find-promotors.py" + passed_params): exit()
    if os.system(python_cmd + " algs4_prepare_prom_sequences_simple.py" + passed_params): exit()
    if os.system(python_cmd + " algs5_compute_prom_alignments.py" + passed_params): exit()
    
    t1_time = time.time()
    os.chdir("../../../")
    logExecutionTime(t1_time - t0_time, "alignments")
    

if __name__ == '__main__':
    os.chdir(run_path)
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    parameters = readParametersFromFile("config/config_aln_params.txt", parameters)
    parameters = readParametersFromFile("config/config_aln_paths.txt", parameters)
        
    WORKERS = int(parameters["W"])
    
    ret = checkParameters(parameters)
    if ret < 0:
        exit()
            
    python_cmd = sys.executable 
    action = parameters["ACTION_RES"]
    
    if action == "ALN":
        alignments(parameters)    
    elif action == "PHY":
        phylogeny_muts(parameters)
    elif action == "PHYG":
        phylogeny_genes(parameters)
    elif action == "MUT":
        mutations(parameters)
    elif action == "ALL":
        alignments(parameters)
        phylogeny_genes(parameters)
        mutations(parameters)
        
