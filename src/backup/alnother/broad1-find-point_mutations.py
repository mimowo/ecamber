import os
import sys
import time
from StringIO import StringIO
from multiprocessing import Pool
import multiprocessing

sys.path.append("../../../../ecamber/src/")
from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.structs.aln_filenames import *
from alnsoft.utils.camber_aln_utils import *
from alnsoft.utils.camber_phylo_utils import *
from alnsoft.utils.camber_mut_utils import *

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    parameters = readParametersFromFile("config/config_aln_paths.txt", parameters)
    parameters = readParametersFromFile("config/config_aln_params.txt", parameters)

    strains = readStrainsOrdered()
    
    syn = "B"
    
    if len(parameters["MATRIX"]) > 2 and parameters["ALN_TYPE"].lower() == "aa":
        parameters["MUTATIONS_EXP_RESULTS"] += "-"+parameters["MATRIX"] + "/"
    else:
        parameters["MUTATIONS_EXP_RESULTS"] += "/"
    
    ensure_dir(parameters["MUTATIONS_RESULTS"])
    ensure_dir(parameters["MUTATIONS_EXP_RESULTS"])
    
    progress = ShowProgress("Load strain sequences")
    strains_list = sorted(list(strains.allStrains()), key=lambda strain_id:-strains.strain(strain_id).totalGenomeLength())
    progress.setJobsCount(len(strains_list))
    
    strains_subset = strains_list
    all_strains_count = len(strains_list)
    aln_type = parameters["ALN_TYPE"].lower()
    
    indel_limit = int(0.10*all_strains_count)
    
    acc_amb = True
    fns_to_search = []
    input_fh = open(parameters_map["ALIGNMENTS_FOLDER"] + "aligned_clusters.txt")
    for line in input_fh.readlines():
        tokens = line.strip().split()
        if len(tokens) < 1:
            continue
        filename_id = int(tokens[0])
        cluster_id = tokens[1]
        gene_name = tokens[2]
        aa_seq_aln_count = int(tokens[len(tokens)-3])
        aa_aln_count = int(tokens[len(tokens)-1])
        
        if syn == "N" and aa_aln_count < 2:
            continue
#        if gene_name in ["rpoB"]:
#            print(gene_name, aa_seq_aln_count, aa_aln_count, line, filename_id)
#        if gene_name != "rpoB":
#            continue
        if aa_seq_aln_count < 0.9*len(strains_list):
            continue
        fns_to_search.append((filename_id, cluster_id, gene_name, aa_aln_count, [], syn, 0, all_strains_count, indel_limit, True, acc_amb))
    input_fh.close()
    
    for params in fns_to_search:
        filename_id = params[0]
        aa_aln_count = params[3]
        aln_fn = parameters_map["ALIGNMENTS_FOLDER_ALN_AA"] + "/" + str(filename_id) + ".aln"
        if not os.path.exists(aln_fn) and aa_aln_count >= 2:
            print("file does not exist: ", aln_fn)
            exit()
    
    print(len(fns_to_search))
    
    progress = ShowProgress("Identify point mutations")
    n = len(fns_to_search)
    progress.setJobsCount(n)
    
    all_mutations = {}
    
    TASKS =  fns_to_search
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())

    cc_mut = 0
    cc_ch = 0
    
    
    if WORKERS>1:
        pool = Pool(processes=WORKERS)
    
        for r in pool.imap(findSNPsFiles, TASKS):
            if r == None:
                progress.update()
                continue
            mutations_t, filename_id, cluster_id, gene_name = r
            cc_ch += 1
            if len(mutations_t) > 0:
                cc_mut += 1
            all_mutations[filename_id] = []
            for mutation in mutations_t:
                all_mutations[filename_id].append((cluster_id, gene_name, mutation))
            progress.update()
    else:
        for TASK in TASKS:
            r = findSNPsFiles(TASK)
            if r == None:
                progress.update()
                continue            
            mutations_t, filename_id, cluster_id, gene_name = r
            cc_ch += 1
            if len(mutations_t) > 0:
                cc_mut += 1
            all_mutations[filename_id] = []
            for mutation in mutations_t:
                (m_position, m_vector, m_is_syn) = mutation
                all_mutations[filename_id].append((cluster_id, gene_name, mutation))
                
            progress.update()
        
    print("Checked components: ", len(TASKS))
    print("Checked components in all substrains: ", cc_ch)
    print("Checked components with any mutations: ", cc_mut)
    print(len(all_mutations), indel_limit, len(fns_to_search))

    output_fn = parameters["MUTATIONS_EXP_RESULTS"] + 'snp-mutations-' +syn.lower()+'.txt'
    output_fh = open(output_fn, "w");
    writeMutationsHeader(output_fh, strains, strains_list)
    for filename_id in all_mutations:
        gene_mutations = all_mutations[filename_id]
        gene_mutations_sorted = sorted(gene_mutations, key=lambda mutation_tmp:mutation_tmp[2][0])
        for mutation_tmp in gene_mutations_sorted:
            cluster_id, gene_name, mutation = mutation_tmp
            saveMutation(output_fh, mutation, strains_list, cluster_id, filename_id, gene_name)
    output_fh.close()



