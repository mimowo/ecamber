import os
import sys
import time
from StringIO import StringIO

sys.path.append("../../../../ecamber/src/")
from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.structs.aln_filenames import *
from alnsoft.utils.camber_aln_utils import *
from alnsoft.utils.camber_phylo_utils import *

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
parameters = readParametersFromFile("../../../config/config_aln_paths.txt", parameters)
parameters = readParametersFromFile("../../../config/config_aln_params.txt", parameters)

aln_filenames = AlnFilenames()
filenames_fn = parameters["PHYLOGENY_FOLDER"] + "filenames.txt"
if not os.path.exists(filenames_fn):
    fh = open(filenames_fn, 'w')
    fh.close()

filenames_fh = open(filenames_fn)
comps_ids_aligned = set()
lines = filenames_fh.readlines()
for line in lines:
    if not line:
        continue
    line = line.strip()
    tokens = line.split("\t")
    if len(tokens) >=2:
        if tokens[1] in comps_ids_aligned:
            print("suplicated: "+ comps_ids_aligned)
        else:
            comps_ids_aligned.add(tokens[1])
print("END")
