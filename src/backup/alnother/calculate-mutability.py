import os
import sys
import time
from StringIO import StringIO

sys.path.append("../../../../ecamber/src/")
from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.structs.aln_filenames import *
from alnsoft.utils.camber_aln_utils import *
from alnsoft.utils.camber_phylo_utils import *

parameters = overwriteParameters(sys.argv)
parameters = readParameters()
parameters = readParametersFromFile("../../../config/config_aln_paths.txt", parameters)
parameters = readParametersFromFile("../../../config/config_aln_params.txt", parameters)

phase = 0
if "PHASE" in parameters:
    phase = int(parameters["PHASE"])

if len(parameters["MATRIX"]) > 2 and parameters["ALN_TYPE"].upper() == "AA":
    parameters["PHYLOGENY_EXP_FOLDER"] += "-"+parameters["MATRIX"] + "/"
    parameters["ALN_ALIGNMENTS_FOLDER"] += "-"+parameters["MATRIX"] + "/"
    parameters["MUTATIONS_EXP_RESULTS"] += "-"+parameters["MATRIX"] + "/"
else:
    parameters["PHYLOGENY_EXP_FOLDER"] += "/"
    parameters["ALN_ALIGNMENTS_FOLDER"] += "/"
    parameters["MUTATIONS_EXP_RESULTS"] += "/"

ensure_dir(parameters["MUTATIONS_RESULTS"] )
ensure_dir(parameters["MUTATIONS_EXP_RESULTS"] )

strains = readStrainsInfo()

multigene_graph = MultigeneGraph()
input_fn = parameters_map["CLOSURE_RESULTS_GRAPHS"] + "cluster-comb-det-" + str(phase) + ".txt"
input_fh = open(input_fn)
multigene_graph = readMultigenesGraphFast2(strains, multigene_graph, input_fh)
input_fh.close()

input_fh = open(input_fn)
clusters = readClusters2(multigene_graph, input_fh=input_fh)
clusters.classifyClusters()
input_fh.close()

strains_ord = strains.getOrderedGenomes(strains.allGenomes())

cluster_fh = open(input_fn)
clusters, id_cluster_map = readClusters(cluster_fh, strains)
cluster_fh.close()

anns_fn = parameters_map["CLOSURE_RESULTS"] + "names_anns.txt"
if os.path.exists(anns_fn):
    anns_fh = open(anns_fn)
    names_mapping = readNamesMapping(anns_fh)
    anns_fh.close()
else:
    names_mapping = None

aln_filenames = AlnFilenames()
filenames_fn = parameters_map["ALIGNMENTS_FOLDER"] + "filenames_"+parameters["ALN_TYPE"].lower()+".txt"
filenames_fh = open(filenames_fn)

lines = filenames_fh.readlines()
filenames_fh.close();
for line in lines:
    aln_filenames.addMappingByLine(line)

aln_type = parameters["ALN_TYPE"].upper()

clusters_to_search = []
for anchor_comp in list(clusters.anchors.values()):
    if len(anchor_comp.strains) >= 0.9*len(strains.allStrains()):
        clusters_to_search.append(anchor_comp)

#print("To search: ", len(clusters_to_search), len(clusters.anchors))

if aln_type == "AA":
    mutabilities = calculateMutability(clusters_to_search, aln_type=aln_type, gaps_as_aa=True)
else:
    mutabilities = calculateMutability(clusters_to_search, aln_type=aln_type, gaps_as_aa=False)    

ensure_dir(parameters["MUTATIONS_EXP_RESULTS"])
mut_file = parameters["MUTATIONS_EXP_RESULTS"] + 'mutabilities-'+parameters["ALN_TYPE"]+"-" + str(phase) + '.txt';
mut_fh = open(mut_file, "w");

progress = ShowProgress("Saving mutations")
n = len(mutabilities)
progress.setJobsCount(n)    

for cluster_id in mutabilities:
    mutability = mutabilities[cluster_id]
    mut_fh.write(str(cluster_id) + "\t"+str(mutability)+"\n");
mut_fh.close();

