import os
import sys
import time
from StringIO import StringIO

sys.path.append("../../../../ecamber/src/")
from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from alnsoft.structs.aln_filenames import *
from alnsoft.utils.camber_aln_utils import *
from alnsoft.utils.camber_phylo_utils import *


parameters = overwriteParameters(sys.argv)
parameters = readParameters()
parameters = readParametersFromFile("../../../config/config_aln_paths.txt", parameters)
parameters = readParametersFromFile("../../../config/config_aln_params.txt", parameters)

phase = 0
if "PHASE" in parameters:
    phase = int(parameters["PHASE"])

clustal_exe = os.path.abspath(parameters["CLUSTAL_PATH"])


diffTime("params")
strains = readStrainsInfoWithPlasmids()
diffTime("info")
print(strains.strains)
print(strains.short_names)
strains_count = len(strains.strains) - len(strains.plasmids)
print(strains_count)
readStrainSequences(strains,strains.allStrains())
diffTime("sequences")
#readExtAnnotations(strains.allStrains())
diffTime("annotations")
multigene_graph = MultigeneGraph()
diffTime()
input_fh = open(parameters_map["CLOSURE_RESULTS_GRAPHS"] + "cluster-comb-det-" + str(phase) + ".txt")
multigene_graph = readMultigenesGraphFast(strains, multigene_graph, input_fh)
input_fh.close()
diffTime("graph")

for strain_id in strains.allStrains():
    print(len(multigene_graph.nodes_by_strain[strain_id]))
diffTime("strain")

clusters = readClusters(phase, multigene_graph)
clusters.classifyClusters()
diffTime("classified")


aln_filenames = AlnFilenames()
filenames_fn = parameters["PHYLOGENY_FOLDER"] + "filenames.txt"
filenames_fh = open(filenames_fn)
lines = filenames_fh.readlines()
for line in lines:
    aln_filenames.addMappingByLine(line)
filenames_fh.close()


output_fh = open(parameters_map["PHYLOGENY_FOLDER"] + "all-diff-genes.txt", "w")

for anchor in list(clusters.anchors_in_all_strain.values()):
    rv_mgs = list(anchor.nodesByStrain(strains.refStrain()).values())
    if len(rv_mgs) > 0 and rv_mgs[0].multigene.ann_id != "x":
        cc_ann = rv_mgs[0].multigene.ann_id
    else:
        cc_ann = anchor.name  
    
    mix_cluster_id = clusterLongestMixID(anchor)
    if not aln_filenames.hasClusterID(mix_cluster_id):
        aln_filenames.addMapping(mix_cluster_id)
        filename_id = aln_filenames.getMapping(mix_cluster_id)
    filename_id = aln_filenames.getMapping(mix_cluster_id)

    debug = False
    if debug == True:
        continue;
    all_diff = True
    seqs = set([])
    count = 0
    for mg_node in list(anchor.nodes.values()):
        count += 1
        mg = mg_node.multigene
        if not mg.sequence() in seqs:
            seqs.add(mg.sequence())
        else:
            all_diff = False
    print(anchor.name, filename_id, len(seqs))
    if len(seqs) >= strains_count - 3:
        output_fh.write(cc_ann + " " + filename_id + "\n")
    
output_fh.close()

