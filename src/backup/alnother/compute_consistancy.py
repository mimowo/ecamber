import sys
import time
import os
import multiprocessing
from collections import deque
from alnsoft.utils.camber_tree_utils import readPhyloTree

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_graph_utils import *
from soft.structs.cluster import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *
    
def readTree(input_fh, strains):
    line = input_fh.readline()
    tree_txt = line.strip()
    tree = readPhyloTree(tree_txt, strains)
    return tree
    
if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    parameters = readParametersFromFile("config/config_aln_paths.txt", parameters)

    strains = readStrainsInfo()
    strains_list = sorted(strains.allStrains())
    
    if len(parameters["MATRIX"]) > 2 and parameters["ALN_TYPE"].lower() == "aa":
        parameters["MUTATIONS_EXP_RESULTS"] += "-"+parameters["MATRIX"] + "/"
    else:
        parameters["MUTATIONS_EXP_RESULTS"] += "/"
    if not "SYN" in parameters:
        parameters["SYN"] = "B"
    syn = parameters["SYN"]   
    if "MRAD" in parameters:
        mrad = int(parameters["MRAD"])
    else:
        mrad = 0
            
    input_fn = parameters["PHYLOGENY_FOLDER_FLATPHYLOTREE"] + 'flatphylotree-' +syn.lower()+"_"+str(0)+"_" + str(mrad)+'.txt'
    input_fh = open(input_fn)
    tree = readPhyloTree(input_fh, strains_list)
    input_fh.close()
    
    print(tree.toString())
    cc_strains = {}
    cc_ann_strains = {}
        
    input_fn = parameters["CONSISTANCY_FOLDER"] + 'anochor_cc_strains.txt'
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        tokens = line.strip().split("\t")
        if len(tokens) < 2:
            continue
        cluster_id = tokens[0]
        strain_tokens = tokens[1].split(";")
        cc_strains[cluster_id] = set(strain_tokens)
    input_fh.close()
    
    input_fn = parameters["CONSISTANCY_FOLDER"] + 'anochor_cc_ann_strains.txt'
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        tokens = line.strip().split("\t")
        if len(tokens) < 2:
            continue
        cluster_id = tokens[0]
        strain_tokens = tokens[1].split(";")
        cc_ann_strains[cluster_id] = set(strain_tokens)
    input_fh.close()
    
    
    changes_map = {}
    tot_changes = 0
    tot_ann_changes = 0
    decreased = 0
    increased = 0
    count_no_change = 0
    strain_no_change = 0
        
    for cluster_id in set(cc_strains.keys()) & set(cc_ann_strains.keys()):
        sub_strains = cc_strains[cluster_id]
        sub_ann_strains = cc_ann_strains[cluster_id]
        
        strains_profile = []
        for strain_index in tree.leaf_ids:
            strain_id = strains_list[strain_index]
            if strain_id in sub_strains and strain_id != "RGTB327":
                strains_profile.append('1')
            else:
                strains_profile.append('0')
        tree.setGenePresenceProfile(strains_profile)
        changes = tree.calcChanges()            
                
        strains_profile = []
        for strain_index in tree.leaf_ids:
            strain_id = strains_list[strain_index]
            if strain_id in sub_ann_strains:
                strains_profile.append('1')
            else:
                strains_profile.append('0')                
                
        tree.setGenePresenceProfile(strains_profile)        
        ann_changes = tree.calcChanges()
        
        tot_changes += changes
        tot_ann_changes += ann_changes
        changes_map[cluster_id] = (changes, ann_changes)
        
        if changes < ann_changes:
            decreased += 1
        elif changes > ann_changes:
            increased += 1
        elif len(sub_strains) == len(sub_ann_strains):
            strain_no_change += 1
        else:
            count_no_change += 1
        
    output_fh = open(parameters["CONSISTANCY_FOLDER"] + 'consistancy_cmp.txt', "w")
    for cluster_id in sorted(changes_map.keys()):
        changes, ann_changes = changes_map[cluster_id]
        output_fh.write(cluster_id +"\t" + str(changes) + "\t" + str(ann_changes) + "\n")
    output_fh.close()
    
    print(tot_changes, tot_ann_changes)
    print(decreased, strain_no_change, count_no_change, increased)
     