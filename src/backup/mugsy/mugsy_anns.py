import sys
import os
import shutil
sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_seq_utils import translateSequence

if __name__ == '__main__':
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    
    print("Creates input for CAMBerVis")
    print("Input folder will be stored at: "+parameters["DATASET_OUTPUT"])
    
    ensure_dir(parameters["DATASET_OUTPUT"])
    ensure_dir(parameters["DATASET_OUTPUT"]+"/genes_aa/")
    ensure_dir(parameters["DATASET_OUTPUT"]+"/genes_dna/")
    
    progress = ShowProgress("Genome annotations")
    n = len(strains.allStrains())
    progress.setJobsCount(n)
    
    text_lines = []
    gene_id_tmp = 0
    
    for strain_id in strains.allStrains():
        strain = strains.strain(strain_id)

        readStrainSequences(strain)
        
        strain_id, annotations = readOrgAnnotations(strain)
        genes_sorted = sorted(annotations.genes.values(), key=lambda gene:gene.end)
        for gene in genes_sorted:
            start, end = gene.start, gene.end
          #  print(strain_id, gene.gene_id, gene.contig_id)
            ann_id =  gene.gene_id + "_" + str(gene_id_tmp)
            gene_id_tmp += 1
            if len(gene.contig_id) > 1:
                if strain.sequence == None:
                    #seq_id = strain_id + "." + gene.contig_id
                    seq_id = gene.contig_id
                else:
                    seq_id = gene.contig_id
            else:
                if strain.sequence == None:
                    #seq_id = strain_id + "." + strain_id
                    seq_id = strain_id
                else:
                    seq_id = strain_id
                    
            seq_id = seq_id.replace("-", "_")
            
            if gene.strand == "+":
                left_bound, right_bound = start, end
                text_line = ann_id + "\t" + seq_id + "\t" + str(left_bound-1) + "\t" + str(right_bound) + "\t" + gene.strand + "\n"                    
            else:
                left_bound, right_bound = end, start
                text_line = ann_id + "\t" + seq_id + "\t" + str(left_bound-1) + "\t" + str(right_bound) + "\t" + gene.strand + "\n"
            
            text_lines.append(text_line)
        progress.update()
        
    output_fn = parameters["ECAMBER_DATASET_PATH"] + "/" + parameters["D"] +".txt"
    print(output_fn)
    output_fh = open(output_fn, "w")
    for text_line in text_lines:
        output_fh.write(text_line)
    output_fh.close()
    print("Copying unified annotations.")

