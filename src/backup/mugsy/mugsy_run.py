import sys
import os
import shutil
sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_seq_utils import translateSequence

if __name__ == '__main__':
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    
    print("Creates input for CAMBerVis")
    print("Input folder will be stored at: "+parameters["DATASET_OUTPUT"])

    dataset = parameters["ECAMBER_DATASET_PATH"]

    fastas_txt = ""
    for strain_id in strains.allStrains():
        fastas_txt += "$dataset/genomes/" + strain_id.replace("-", "_") + ".fasta" + " "
    fastas_txt = fastas_txt[:-1]
    
    text_lines = [
'export MUGSY_INSTALL="/home/misias/mugsy/mugsy"\n',
'dataset="' + parameters["D"] + '"\n',
'echo $dataset\n',
'\n',
'rm -rf annotator/$dataset.index annotator/$dataset.fsa annotator/$dataset.maf\n',
'cat $dataset/genomes/*.fasta > annotator/$dataset.fsa\n',
'cp $dataset/$dataset.txt annotator/$dataset.txt\n',
'\n',
'\n',
'mkdir /home/misias/mugsy/$dataset/output\n',
'time ./mugsy/mugsy --directory /home/misias/mugsy/$dataset/output --prefix $dataset ' + fastas_txt + ' > $dataset"-step0.txt"\n',
'cat $dataset/output/$dataset.maf | perl -ne' + "'s/^s(\s+)[^\.]+\./s$1/;print'" + ' > annotator/$dataset".maf"\n',
'\n',
'\n',
'cd annotator\n',
'\n',
'\n',
'time ./mafindex.pl $dataset.index < $dataset.maf > ../$dataset"-step1.txt"\n',
'time ./featureindex.pl $dataset.index < $dataset.txt > ../$dataset"-step2.txt"\n',
'time ./mapfeatures.pl $dataset.index $dataset.fsa < $dataset.txt > ../$dataset"-step3.txt"\n'
]
        
    output_fn = dataset + "/" + parameters["D"] +".sh"
    output_fh = open(output_fn, "w")
    output_fh.writelines(text_lines)
    output_fh.close()
    print("Copying unified annotations.")

