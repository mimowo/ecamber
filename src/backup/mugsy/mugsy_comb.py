import sys
import os
import multiprocessing
from soft.structs.multigene import createMultigeneId


sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import ensure_dir, readStrainsFromFile
from soft.utils.camber_format_utils import convertFeatureTab, convertGenBank

input_dataset = "prodigal"

def convertAnns(strains):
    mugsy_strain_map = {}
    contig_strain_map = {}
    for strain_id in strains.allStrains():
        strain = strains.strain(strain_id)
        readStrainSequences(strain)
        if strain.sequence == None:
            for contig_id in strain.sequences_map:
                contig_strain_map[contig_id] = strain_id
                mugsy_strain_map[contig_id.replace("-", "_")] = contig_id
        else: 
            contig_strain_map[strain_id] = strain_id
        mugsy_strain_map[strain_id.replace("-", "_")] = strain_id
        strain_id, gene_ann = readOrgAnnotations(strain, src="prodigal")
    
        mg_ann_tmp = Annotation()
    
        for gene in gene_ann.genes.values():
            multigene = Multigene(gene.start, gene.end, gene.strand, strain, gene.gene_id, gene.gene_id, gene.contig_id)
            multigene.addGene(gene)
            mg_ann_tmp.addMultigene(multigene)
        strain.annotation.multigenes = mg_ann_tmp.multigenes
    
    print("read annotations!!")
    
    counter = 0
    ensure_dir(parameters["ECAMBER_DATASET_PATH"] + "/mugsy/after_mugsy_"+input_dataset+"/")
    for edit_fn in os.listdir(parameters["ECAMBER_DATASET_PATH"] + "/mugsy/run_"+input_dataset):
        if not edit_fn.endswith(".edits.out"): continue
        input_fn = parameters["ECAMBER_DATASET_PATH"] + "/mugsy/run_"+input_dataset + "/" + edit_fn
        input_fh = open(input_fn)
        
        if counter % 500 == 0:
            print(counter)
        counter += 1
        
        for line in input_fh.readlines()[1:]:
            line = line.strip()
            if line.startswith(">"):
                break
            tokens = line.split("\t")
          #  print("tokens", tokens)
            if len(tokens) < 4: continue
            gene_id = tokens[0]
            contig_id = mugsy_strain_map.get(tokens[1], tokens[1])
            strain_id = contig_strain_map.get(contig_id, contig_id)
            strain = strains.strain(strain_id)
            strand = tokens[5]

            left_b = int(tokens[2])
            right_b = int(tokens[3])

            if strand == "+": 
                start, end = left_b+1, right_b
            else:
                start, end = right_b, left_b+1

            mg_id = createMultigeneId(end, strand, contig_id, strain_id)
            gene = Gene(start, end, strand, strain, "x", "x", contig_id, True)
            if not gene.startCodon() in ["ATG","GTG","TTG"] or not gene.stopCodon() in ["TAA", "TAG", "TGA"]:
                print(gene.startCodon(), gene.stopCodon(), edit_fn, line)
            
            if not mg_id in strain.annotation.multigenes:
                multigene = Multigene(start, end, strand, strain, ann_id="x", gene_name="x", contig_id=contig_id)
                multigene.addGene(gene)
                strain.annotation.multigenes[mg_id] = multigene
            else:
                mg = strain.annotation.multigenes[mg_id]
                gene_len = gene.length()
                if gene_len in mg.lengths:
                    mg.genes[gene_len].selected = True
                    mg.sel_lengths.add(gene_len)
                else:
                    mg.addGene(gene)
                    
        input_fh.close()
#        

#            
    for strain_id in strains.allStrains():
        strain = strains.strain(strain_id)
        for mg in strain.annotation.multigenes.values():
            if len(mg.genes) == 1:
                for gene in mg.genes.values():
                    gene.selected = True
                    mg.sel_lengths.add(gene.length())
        
        output_fn = parameters["ECAMBER_DATASET_PATH"] + "/mugsy/after_mugsy_"+input_dataset+"/" + strain_id + ".txt"
        output_fh = open(output_fn, "w")
        saveMgsAnnotations(output_fh, genes=None, multigenes=strain.annotation.multigenes.values(), show_start_codons=True, cluster_ids=False, smr=0.0, std=False, cluster_gene_names={})             
        output_fh.close()
                
    return strain_id

if __name__ == '__main__':

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains_tmp.txt"
    if not os.path.exists(input_fn):
        input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains.txt"
    input_fh = open(input_fn)
    strains = readStrainsFromFile(input_fh)
    input_fh.close()
    
    strains_list = strains.allStrains()
    #strains_list = []
    print("pro3_format_anns.py: Formats Mugsy annotations.")

    convertAnns(strains)
    
        
    print("pro3_format_anns.py: Finished.")

