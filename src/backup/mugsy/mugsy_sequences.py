import sys
import os

import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *

def convertSequence(strain_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
        
    input1_fn = parameters["DATASET_GENOMES"]+"seq-"+strain_id+".fasta"
    input2_fn = parameters["DATASET_GENOMES"]+strain_id+".fasta"
    output_fn = parameters["DATASET_GENOMES_MUGSY"] + strain_id.replace("-", "_")+".fasta"
    
    if os.path.exists(input1_fn) or os.path.exists(input2_fn):
        if os.path.exists(input1_fn): seq_map = readSequenceFromFile(input1_fn)
        else: seq_map = readSequenceFromFile(input2_fn)
        
        total_length = 0
        max_length = 0
    
        output_fh = open(output_fn, "w")        
        if len(seq_map) == 1:
            seq_upper = list(seq_map.values())[0].upper()
            total_length = len(seq_upper)
            max_length = total_length
            writeFASTAseq(output_fh, seq_upper, strain_id.replace("-", "_"));
        else:
            for seq_id in seq_map:
                seq_upper = seq_map[seq_id].upper()
                if len(seq_upper) > 0:
                    total_length += len(seq_upper)
                    max_length = max(len(seq_upper), max_length)
                    writeFASTAseq(output_fh, seq_upper, seq_id.replace("-", "_"));
                else:
                    print("ZERO LENGTH SEQ: " + strain_id)
        output_fh.close()
    else:
        total_length = 0
        max_length = 0
        
        seq_map = readSequenceFromFile(output_fn)
            
        if len(seq_map) == 1:
            seq_upper = list(seq_map.values())[0].upper()
            total_length = len(seq_upper)
            max_length = total_length
        else:
            for seq_id in seq_map:
                seq_upper = seq_map[seq_id].upper()
                if len(seq_upper) > 0:
                    total_length += len(seq_upper)
                    max_length = max(len(seq_upper), max_length)
                else:
                    print("ZERO LENGTH SEQ: " + strain_id)
                
    return strain_id, total_length, max_length

if sys.version.startswith("3"):
    raw_input = input

if __name__ == '__main__':

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains_tmp.txt"
    if not os.path.exists(input_fn):
        input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains.txt"
    input_fh = open(input_fn)
    strains_all = readStrainsFromFile(input_fh)
    input_fh.close()
    
    WORKERS = 1#min(int(parameters["W"]), multiprocessing.cpu_count())
    ensure_dir(parameters["DATASET_GENOMES_MUGSY"])
    
    print("format_sequences.py: formats sequences to CAMBer input format.")
    
    strains_list = list(strains_all.allStrains())
    
    progress = ShowProgress("Formatted sequences")
    n = len(strains_list)
    progress.setJobsCount(n)
    
    TASKS = strains_list
    strain_tot_len = {}
    strain_max_len = {}
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap_unordered(convertSequence, TASKS):
            strain_id = r[0]
            strain_tot_len[strain_id] = r[1]
            strain_max_len[strain_id] = r[2]
            progress.update(desc=strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = convertSequence(TASK)
            strain_id = r[0]
            strain_tot_len[strain_id] = r[1]
            strain_max_len[strain_id] = r[2]
            progress.update(desc=strain_id)
    
    short_tot_strains_exc = set([])
    long_tot_strains_exc = set([])
    short_max_strains_exc = set([])

    medial_length = median(list(strain_tot_len.values()))
    
    for strain_id in strains_list:
        seq_length = strain_tot_len[strain_id]
        if seq_length < 0.5*medial_length:
            short_tot_strains_exc.add(strain_id)
    for strain_id in strains_list:
        seq_length = strain_tot_len[strain_id]
        if seq_length > 1.5*medial_length:
            long_tot_strains_exc.add(strain_id)
            
            
    if len(short_tot_strains_exc) > 0:
        print("Exclude these strains, because their genome sequences are shorter than 50% of the median sequence length: " + str(medial_length))
        for strain_id in short_tot_strains_exc:
            print(strain_id + "\t" + str(strain_tot_len[strain_id]))
        print("Edit manually file '"+parameters_map["ECAMBER_DATASET_PATH"] + "strains.txt'" + " if you want to include them, by removing # characters.")

    if len(long_tot_strains_exc) > 0:
        print("Exclude these strains, because their genome sequences are longer than 50% of the median sequence length: " + str(medial_length))
        for strain_id in long_tot_strains_exc:
            print(strain_id + "\t" + str(strain_tot_len[strain_id]))
        print("Edit manually file '"+parameters_map["ECAMBER_DATASET_PATH"] + "strains.txt'" + " if you want to include them, by removing # characters.")
            
    output_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains.txt"
    output_fh = open(output_fn, "w")
    for strain_id in strains_list:
        if strain_id in short_tot_strains_exc | long_tot_strains_exc: #| short_max_strains_exc:
            output_fh.write("#" + strain_id + "\n")
        else:
            output_fh.write(strain_id + "\n")
    output_fh.close()    
    
    print("format_sequences.py: Finished.")
    