import sys
import os
import multiprocessing
import re

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import ensure_dir, readStrainsFromFile
from soft.utils.camber_format_utils import *

def readSequencesFromFile(genome_fn):
    seq_list = []
    seq_map = {}
    genome_fh = open(genome_fn)
    genome_lines = genome_fh.readlines();
    genome_fh.close()
    
    accession = ""
    for line in genome_lines[0:]:
        parts = line.split();
        if len(parts) >= 1:
            if parts[0].startswith(">"):
                if accession != "":
                    seq_map[accession] = ''.join(seq_list)
                    seq_list = []
                acc_tokens = parts[0].split("|")
                if len(acc_tokens) >= 4:
                    accession = acc_tokens[3]
                else:
                    accession = parts[0][1:]
                #acc_tok = accession.split(".")
                #accession = acc_tok[0]
            elif len(parts[0]) >= 1:
                seq_list.append(parts[0])
    if len(seq_list) > 0:
        seq_map[accession] = ''.join(seq_list)
    return seq_map;

def convertGISdata(strain, input_fn, ann_fh):
    strain.calcRevSequence()
    seq_map = readSequencesFromFile(input_fn)
    out_lines = []
    sequence = strain.sequence
    rev_sequence = strain.rev_sequence
    
    curr_id = 0
    n = len(sequence)
   # print(len(sequence), len(rev_sequence))
    
    for gene_tmp_id in seq_map:
        gene_id = strain.strain_id + "_" + gene_tmp_id[len("GERMS-WGS.pl_"):]
        if curr_id % 100 == 0:
            pass
       #     print(curr_id, gene_tmp_id)
        curr_id += 1
        gene_seq = seq_map[gene_tmp_id]
        count = 0
        gene_stop_codon = gene_seq[-3:]
        gene_start_codon = gene_seq[:3]
      #  print(gene_stop_codon, gene_start_codon, len(seq_map), gene_id)
        
        if not gene_stop_codon in ["TAG", "TAA", "TGA"]:
            continue
        if not gene_start_codon in ["ATG", "GTG", "CTG"]:
            continue
     #   print(gene_seq)
        matches_p = [(m.start(), m.end()) for m in re.finditer(gene_seq, sequence)]
        for (start, end) in matches_p:
            if count > 0:
                out_line = gene_id + "_" + str(count) + "\t"
            else:
                out_line = gene_id + "\t"
            out_line += str(start+1) + "\t" + str(end) + "\t" + "+" + "\t"
            out_line += strain.strain_id
            count += 1
            out_lines.append(out_line)
        matches_p = [(m.start(), m.end()) for m in re.finditer(gene_seq, rev_sequence)]
        for (start, end) in matches_p:
            
            if count > 0:
                out_line = gene_id + "_" + str(count) + "\t"
            else:
                out_line = gene_id + "\t"
            out_line += str(n-start) + "\t" + str(n-end+1) + "\t" + "-" + "\t"
            out_line += strain.strain_id
            count += 1
            out_lines.append(out_line)

#            seq_res = complementarySequence(sequence[n-end:n-start])
#            print(start, end, n-end, n-start, n-end -( n-start), (start-end)/3)
#            print(seq_res)
#            print(gene_seq)

            
            
        #if not gene_seq.startswith("ATG")
    #    print(gene_seq)

    
    for out_line in out_lines:
        ann_fh.write(out_line + "\n")
        
    return None

def convertAnns(strain_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains_tmp.txt"
    if not os.path.exists(input_fn):
        input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains.txt"
    input_fh = open(input_fn)
    strains = readStrainsFromFile(input_fh)
    input_fh.close()
    
    strain = strains.strain(strain_id)
    
    strain_fn = parameters_map["DATASET_GENOMES"] + strain.strain_id + ".fasta"
    strain_seq_map = readSequenceFromFile(strain_fn)
    if len(strain_seq_map) == 1: 
        strain.setSequence(list(strain_seq_map.values())[0])
    else:
        strain.setSequences(strain_seq_map)
    
    input_fn = parameters["ECAMBER_DATASET_PATH"]+"/anns_tmp/" + strain_id + "_prodigal_output/" + strain_id + "_prodigal_output-joined.prodigal.ffn"
    output_fn = parameters["DATASET_ANN_CDS_OTHER"] + strain_id+".txt"
    
    if os.path.exists(input_fn):
        
        ann_fh = open(output_fn, "w")
        convertGISdata(strain, input_fn, ann_fh)
        ann_fh.close()
    else:
        ann_fh = open(output_fn, "w")
        ann_fh.close() 
    return strain_id

if __name__ == '__main__':

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains_tmp.txt"
    if not os.path.exists(input_fn):
        input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains.txt"
    input_fh = open(input_fn)
    strains = readStrainsFromFile(input_fh)
    input_fh.close()
    
    ensure_dir(parameters["DATASET_ANN_CDS_OTHER"])
        
    strains_list = strains.allStrains()
    print("format_anns_mage.py: formats ColiScope annotations")
                 
    n = len(strains_list)
    progress = ShowProgress("Formatted ColiScope annotations")
    progress.setJobsCount(n)
    
    WORKERS = 6#min(int(parameters["W"]), multiprocessing.cpu_count())

    TASKS = strains_list
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)
        for r in pool.imap_unordered(convertAnns, TASKS):
            progress.update(desc=r)    
        pool.close() 
        pool.join()
    else:
        for TASK in TASKS:
            r = convertAnns(TASK)
            progress.update(desc=r)
    print("format_anns_mage.py: Finished.")
