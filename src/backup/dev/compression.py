import sys

from soft.utils.camber_seq_utils import reverseString
from soft.utils.camber_params_utils import overwriteParameters, readParameters
from soft.utils.camber_utils import readStrainsInfo, readStrainSequences

def charChoice(lset):
    km = 0
    f = {}
    for (k, s) in lset:
        if k > km: km = k
    for (k, s) in lset:
        if k == km:
            c = s[k]
            if not c in f:
                f[c] = 0
            f[c] += 1
    if len(f) == 0:
        return ""
    cm = max(f.values())
    for cc in f:
        if f[cc] == cm:
            return cc
    return ""

def superstring(seqs, lmax=5):
    lset = set([])
    j = 0
    for seq in seqs:
        j += 1
        k = len(seq)
        for i in range(0, k-lmax, 1):
            s = seq[i:i+lmax]
            lset.add((0, s))
        print("j", j, len(lset))
    sequence = ""
    
    i = 0
    while i<10000:
        lsetn = set([])
        c = charChoice(lset)
        if c == "":
            break
        sequence += c
        for e in lset:
            (k, s) = e
            if s[k] == c and k+1<lmax:
                lsetn.add((k+1, s))
            elif s[k] == c and k+1==lmax:
                pass
            else:
                lsetn.add((0, s))
        lset = lsetn
        i += 1
        print(i, sequence, len(lset))
    return sequence
    
if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    strains_list = list(strains.allStrains())
    
    seqs = []
    tot_len = 0
    
    for strain_id in strains_list:
        strain = strains.strain(strain_id)
        readStrainSequences(strain)
        if strain.sequence == None:
            for seq in strain.sequences_map.values():
                seqs.append(seq)
                tot_len += len(seq)
        else:
            seqs.append(strain.sequence)
            tot_len += len(strain.sequence)
        
        print("done", strain_id, len(seqs), tot_len)
    superseq = superstring(seqs, 100)
    print(len(superseq))
