import sys
import time
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_graph_utils import *
from soft.structs.clusters import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *

def saveClustersDistr(output_fh, clusters, strains):
    strains_ordered = sorted(strains.allStrains())
    
    for cluster in list(clusters.clusters.values()):
        text_line = cluster.cluster_id + "\t"
        for strain_id in cluster.strains():
            multigenes = cluster.nodesByStrain(strain_id).values()
            ann_mgs_count = 0
            
            for multigene in multigenes:
                if len(multigene.ann_id) > 1:
                    ann_mgs_count += 1
            mgs_count = len(multigenes)
            text_line += strain_id + ":" + str(mgs_count) +":" + str(ann_mgs_count) + ";"
        text_line = text_line[:-1]
        output_fh.write(text_line + "\n")
    return None


def readMultigeneClusterStrainLocal(params):
    overwriteParameters(sys.argv)
    readParameters() 
        
    strain_id = params[0]
    closure_iteration = params[1] 
    ph2_iteration = params[2]
    
    input_fh = open(parameters_map["ECAMBER_DATASET_PATH"] + "strains_tmp.txt")
    strains = readStrainsFromFile(input_fh)
    input_fh.close()
    strain = strains.strain(strain_id)
    
    return readMultigeneClusterStrain(strain, closure_iteration, ph2_iteration)
    
    
if __name__ == '__main__':
    print("analysis_anns_table.py: read and saves connected components.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    input_fh = open(parameters_map["ECAMBER_DATASET_PATH"] + "strains_tmp.txt")
    strains = readStrainsFromFile(input_fh)
    input_fh.close()
    strains_list = strains.allStrains()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=3)
    
    clusters = MGClusters()
    
    TASKS = []
    for strain_id in strains_list:
        TASKS.append((strain_id, closure_iteration, ph2_iteration))
    
    progress = ShowProgress("Read connected components")
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes = WORKERS)    
        for strain_id, cc_mapping in pool.imap(readMultigeneClusterStrainLocal, TASKS):
            
            for cluster_id in cc_mapping:
                if not cluster_id in clusters.clusters:
                    cluster = MGCluster(cluster_id)
                    clusters.addCluster(cluster)
                else:
                    cluster = clusters.getCluster(cluster_id)
                for multigene in cc_mapping[cluster_id]:
                    cluster.addMultigene(multigene)
            
            progress.update(desc = strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            strain_id, cc_mapping =readMultigeneClusterStrainLocal(TASK)
            
            for cluster_id in cc_mapping:
                if not cluster_id in clusters.clusters:
                    cluster = MGCluster(cluster_id)
                    clusters.addCluster(cluster)
                else:
                    cluster = clusters.getCluster(cluster_id)
                for multigene in cc_mapping[cluster_id]:
                    cluster.addMultigene(multigene)
            
            progress.update(desc = strain_id)
        
    cluster_sizes = [0]*len(strains.allStrains())

    for cluster in clusters.clusters.values():
        cluster_sizes[len(cluster.strains())-1] += 1
    
    output_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_ANALYSIS_POST", ph2_iteration) + "cluster_sizes_distr_tmp.txt"
    output_fh = open(output_fn, "w")
    for i in range(len(strains.allStrains())):
        output_fh.write(str(i+1) + "\t" + str(cluster_sizes[i]) + "\n")
    output_fh.close()
    
    print("analysis_anns_table.py: Finished.")
