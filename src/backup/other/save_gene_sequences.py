import sys
import os
import shutil
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_seq_utils import translateSequence

def saveGeneSequences(strain_id):
    overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=3)

    strain = strains.strain(strain_id)
    readStrainSequences(strain)
    
    gene_ids = set([])
    
    input_fh = open(parameters["DATASET_OUTPUT"] + "gene_ids.txt")
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) < 1: continue
        gene_ids.add(tokens[0])
    input_fh.close()
    
    cluster_gene_names = {}
    
    input_fn = parameters["DATASET_OUTPUT"] + "cluster_gene_names.txt"
    if os.path.exists(input_fn):
        input_fh = open(input_fn)
        for line in input_fh.readlines()[1:]:
            tokens = line.strip().split()
            if len(tokens) < 1:
                continue
            cluster_gene_names[tokens[0]] = tokens[1]
        input_fh.close()
    

    text_lines = []

    strain_id, annotations = readUniAnnotationsIter(strain, closure_iteration, ph2_iteration)
    
    mutligenes_sorted = sorted(annotations.multigenes.values(), key=lambda multigene:mgIntoSortableTriple(multigene.mg_unique_id))
    for multigene in mutligenes_sorted:
        if not multigene.ann_id in gene_ids : continue

      #  print(multigene.ann_id)
        
        start, end = multigene.getStartEnd()
        
        ann_id =  multigene.ann_id + "|" + multigene.cluster_id + "|"+ cluster_gene_names.get(multigene.cluster_id,"x") + "|" + multigene.strand + "|" + str(start) + "|" + str(end) + "|" + str(multigene.contig_id)
        sequence_dna = multigene.sequence()
        sequence_aa = translateSequence(sequence_dna)
        text_lines.append(">" + ann_id)
        curr = 0;
        while(curr < len(sequence_aa)):
            text_lines.append(sequence_aa[curr:min(curr + 80, len(sequence_aa))])
            curr += 80;  
        text_lines.append("")
    return strain_id, text_lines

if __name__ == '__main__':
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    
    print("Creates input for CAMBerVis")
    print("Input folder will be stored at: "+parameters["DATASET_OUTPUT"])
    
    ensure_dir(parameters["DATASET_OUTPUT"])
    ensure_dir(parameters["DATASET_OUTPUT"]+"/genes_aa/")
    ensure_dir(parameters["DATASET_OUTPUT"]+"/genes_dna/")
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=3)
        
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    progress = ShowProgress("Gene sequences saved")
    n = len(strains.allStrains())
    progress.setJobsCount(n)
    
    TASKS = strains.allStrains()
    text_lines = []
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap_unordered(saveGeneSequences, TASKS):
            strain_id, new_lines = r
            text_lines += new_lines
            progress.update(desc=strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = saveGeneSequences(TASK)
            strain_id, new_lines = r
            text_lines += new_lines
            progress.update(desc=strain_id)

    output_aa_fh = open(parameters["DATASET_OUTPUT"]+"/genes_aa.fasta", "w")
    for text_line in text_lines:
        output_aa_fh.write(text_line + "\n")
    output_aa_fh.close()
    print("Gene sequences saved.")



