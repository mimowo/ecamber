import sys
import time
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_graph_utils import *
from soft.structs.clusters import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *

def saveClusterGeneNames(output_fh, clusters, strains):

    strains_ordered = sorted(strains.allStrains())
    
    text_line = "cluster_id\tgene_name"

    output_fh.write(text_line + "\n")
          
    for cluster in list(clusters.clusters.values()):
        cluster.geneName(strains_ordered)
        
        if len(cluster.gene_name) <= 1: continue
        text_line  = cluster.cluster_id + "\t"
        text_line += cluster.gene_name + "\t"

        if text_line.endswith("\t"):
            text_line = text_line[:-1]

        output_fh.write(text_line + "\n")
    return None


def saveClusterNames(output_fh, clusters, strains):

    strains_ordered = sorted(strains.allStrains())
    
    text_line = "cluster_id\tgene_name"
    for strain_id in strains_ordered:
        text_line += "\t" + strain_id
        
    output_fh.write(text_line + "\n")
          
    for cluster in list(clusters.clusters.values()):
        cluster.geneName(strains_ordered)
        
        text_line  = cluster.cluster_id + "\t"
        text_line += cluster.gene_name + "\t"
            
        for strain_id in strains_ordered:
            mgs = cluster.nodesByStrain(strain_id).values()
            for multigene in mgs:
                if len(multigene.ann_id) > 1:
                    text_line += multigene.ann_id + ";"
                else:
                    text_line += strain_id + ":x;";
            if len(mgs) == 0:
                text_line += strain_id + ":y;";
            if text_line.endswith(";"):
                text_line = text_line[:-1]
            text_line += "\t"
        if text_line.endswith("\t"):
            text_line = text_line[:-1]

        output_fh.write(text_line + "\n")
    return None

def readMultigeneClusterStrainLocal(params):
    overwriteParameters(sys.argv)
    parameters = readParameters() 
        
    strain_id = params[0]
    closure_iteration = params[1]
    
    ph2_iteration = currentPh2Iteration(default=3)
     
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
  #  readStrainSequences(strain)
    
    strain_id, cc_mapping = readMultigeneClusterStrain(strain, closure_iteration, ph2_iteration)
    
    return strain_id, cc_mapping
    
    
if __name__ == '__main__':
    print("cleanup2_component_stats.py: saves details on connected components.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    strains_list = strains.allStrains()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=3)
    
    ensure_dir(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_CLEANUP", ph2_iteration))
    
    clusters = MGClusters()
    
    TASKS = []
    for strain_id in strains_list:
        TASKS.append((strain_id, closure_iteration))
    
    progress = ShowProgress("Gene names saved")
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes = WORKERS)    
        for strain_id, cc_mapping in pool.imap(readMultigeneClusterStrainLocal, TASKS):
            
            for cluster_id in cc_mapping:
                if not cluster_id in clusters.clusters:
                    cluster = MGCluster(cluster_id)
                    clusters.addCluster(cluster)
                else:
                    cluster = clusters.getCluster(cluster_id)
                for multigene in cc_mapping[cluster_id]:
                    cluster.addMultigene(multigene)
            
            progress.update(desc = strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            strain_id, cc_mapping =readMultigeneClusterStrainLocal(TASK)
            
            for cluster_id in cc_mapping:
                if not cluster_id in clusters.clusters:
                    cluster = MGCluster(cluster_id)
                    clusters.addCluster(cluster)
                else:
                    cluster = clusters.getCluster(cluster_id)
                for multigene in cc_mapping[cluster_id]:
                    cluster.addMultigene(multigene)
            
            progress.update(desc = strain_id)
    
    clusters.classifyClusters(strains_list)    
    
    output_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration) + "cluster_names.txt"
    output_fh = open(output_fn, "w")
    saveClusterNames(output_fh, clusters, strains)
    output_fh.close()
    
    output_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration) + "cluster_gene_names.txt"
    output_fh = open(output_fn, "w")
    saveClusterGeneNames(output_fh, clusters, strains)
    output_fh.close()
    
    print("out1_cluster_names.py. Finished.")
