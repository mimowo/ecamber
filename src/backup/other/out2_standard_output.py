import sys
import os
import shutil
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *

def readStrainGeneNames(parameters, strain_id):
    cluster_gene_names = {}
    input_fn = parameters_map["DATASET_RESULTS_EXP_NAMES"] + strain_id + ".txt"
    if os.path.exists(input_fn):
        input_fh = open(input_fn)
        for line in input_fh.readlines()[1:]:
            tokens = line.strip().split()
            if len(tokens) < 1:
                continue
            cluster_gene_names[tokens[0]] = tokens[1]
        input_fh.close()
    return cluster_gene_names
    

def saveOutputAnnotations(strain_id):
    overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    closure_iteration = currentClosureIteration()

    strain = strains.strain(strain_id)
    readStrainSequences(strain)
    
    
    strain_gene_names = readStrainGeneNames(parameters, strain_id)
    
    strain_id, annotations = readAllAnnotationsIter(strain, 0)
    output_fn = parameters["DATASET_OUTPUT"]+'/'+parameters["AS"]+"_before"+'/' + strain_id + ".txt"
    output_fh = open(output_fn, "w")
    saveMgsAnnotations(output_fh, genes=annotations.genes, multigenes=None, show_start_codons=False, cluster_ids=False, smr=0.0, std=True, cluster_gene_names=strain_gene_names)
    output_fh.close()

    cluster_gene_names = readClusterGeneNames()

    strain_id, annotations = readUniAnnotationsIter(strain, closure_iteration, 3)
    output_fn = parameters["DATASET_OUTPUT"]+'/'+parameters["AS"]+"_after"+'/' + strain_id + ".txt"
    output_fh = open(output_fn, "w")
    saveMgsAnnotations(output_fh, genes=None, multigenes=annotations.multigenes.values(), show_start_codons=False, cluster_ids=True, smr=0.0, std=True, cluster_gene_names=cluster_gene_names)
    output_fh.close()
    return strain_id

if __name__ == '__main__':
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    
    print("Creates input for CAMBerVis")
    print("Input folder will be stored at: "+parameters["DATASET_OUTPUT"])
    
    ensure_dir(parameters["DATASET_OUTPUT"])
    ensure_dir(parameters["DATASET_OUTPUT"]+'/'+parameters["AS"]+"_before"+'/')
    ensure_dir(parameters["DATASET_OUTPUT"]+'/'+parameters["AS"]+"_after"+'/')
        
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    progress = ShowProgress("Saved output annotations")
    n = len(strains.allStrains())
    progress.setJobsCount(n)
    
    TASKS = strains.allStrains()
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap_unordered(saveOutputAnnotations, TASKS):
            progress.update(desc=r)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = saveOutputAnnotations(TASK)
            progress.update(desc=r)
      
    print("Saved output annotations.")



