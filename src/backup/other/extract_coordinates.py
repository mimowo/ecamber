import sys
import os
import multiprocessing
import re

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_format_utils import *

def extractCoordinates(strain_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    readStrainSequences(strain)
    input_fn = parameters["ECAMBER_DATASET_PATH"] + "to_extract/sequences.fasta"

    strain.calcRevSequence()
    seq_map = readSequenceFromFile(input_fn)
    out_lines = []

   # print(strain.sequences_map)

    if len(strain.sequences_map) == 0:
        sequences_map = {"x": strain.sequence}
        rev_sequences_map = {"x": strain.rev_sequence}
    else:
        sequences_map = strain.sequences_map
        rev_sequences_map = strain.rev_sequences_map

    for contig_id in sequences_map:
        sequence = sequences_map[contig_id]
        rev_sequence = rev_sequences_map[contig_id]

        curr_id = 0
        n = len(sequence)
        
        for gene_tmp_id in seq_map:
            gene_id = gene_tmp_id
           #     print(curr_id, gene_tmp_id)
            curr_id += 1
            gene_seq = seq_map[gene_tmp_id]
            count = 0
         #   print(len(gene_seq), len(sequence))
            gene_stop_codon = gene_seq[-3:]
            gene_start_codon = gene_seq[:3]
          #  print(gene_stop_codon, gene_start_codon, len(seq_map), gene_id)
            
           # if not gene_stop_codon in ["TAG", "TAA", "TGA"]:
            #    continue
            #if not gene_start_codon in ["ATG", "GTG", "CTG"]:
             #   continue
            print(len(gene_seq), len(sequence))
            matches_p = [(m.start(), m.end()) for m in re.finditer(gene_seq, sequence)]
            print(len(matches_p))
            for (start, end) in matches_p:
              #  print(start, end)
                if count > 0:
                    out_line = gene_id + "_" + str(count) + "\t"
                else:
                    out_line = gene_id + "\t"
                out_line += str(start+1) + "\t" + str(end) + "\t" + "+" + "\t"
                out_line += strain.strain_id
                count += 1
                out_lines.append(out_line)
            print(rev_sequence[:10], sequence[:10])
            print(len(gene_seq), len(rev_sequence))
            matches_p = [(m.start(), m.end()) for m in re.finditer(gene_seq, rev_sequence)]
            for (start, end) in matches_p:
                
                if count > 0:
                    out_line = gene_id + "_" + str(count) + "\t"
                else:
                    out_line = gene_id + "\t"
                out_line += str(n-start) + "\t" + str(n-end+1) + "\t" + "-" + "\t"
                out_line += strain.strain_id
                count += 1
                out_lines.append(out_line)

        for out_line in out_lines:
            print(out_line)
            
    return strain_id

if __name__ == '__main__':

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    

    strains = readStrainsInfo()
         
    strains_list = ["K-12_MG1566"]
                 
    n = len(strains_list)
    progress = ShowProgress("Formatted annotations")
    progress.setJobsCount(n)
    
    TASKS = ["K-12_MG1655"]

    for TASK in TASKS:
        strain_id = extractCoordinates(TASK)
        progress.update(desc=strain_id)
    print("extract_coordinates.py: Finished.")

