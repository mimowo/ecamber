import sys
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_seq_utils import translateSequence
from soft.utils.camber_io_utils import ensure_dir
from soft.utils.camber_closure_utils import *
from alnsoft.utils.camber_gen_utils import readRefStrains

if __name__ == '__main__':
    print("prepare_annotations.py: Initialization of the closure procedure.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()

    strains = readStrainsInfo()
    strains_list = strains.allStrains()

    ref_strain_id = readRefStrains(set(strains_list))[0]
    if len(ref_strain_id) < 1:
        print("Reference strain not defined! Edit file '<datasets>/<dataset>/ref_strain.txt'")
        exit(-1)

    print(strains_list)

    ref_strain = strains.strain(ref_strain_id)   
    readStrainSequences(ref_strain) 

    ref_strain_id, annotation = readOrgAnnotations(ref_strain)

    for gene in annotation.genes.values():
        if gene.gene_id == "D505_04539":
            seq = gene.sequence()
            aa_seq = translateSequence(seq[:-1])
            up_seq = gene.upstream(up_length=100)
            down_seq = gene.downstream(down_length=100)
            print(gene.gene_id, gene.start, gene.end, gene.strand, gene.contig_id)
            
            print(len(up_seq), up_seq)
            print(len(down_seq), down_seq)
         #   print("|".join([up_seq, seq, down_seq]))
            print(len(seq), seq)
            print(len(aa_seq), aa_seq)

    print(len(ref_strain.sequences_map))
        
    print("prepare_annotations.py: Finished.")
