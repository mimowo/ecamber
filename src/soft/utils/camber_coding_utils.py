import time

def getCodingMap():
    bp_map = {
          "AAA":"!",
          "AAT":"#",
          "AAG":"$",
          "AAC":"%",
          "ATA":"&",
          "ATT":"|",
          "ATG":"(",
          "ATC":")",
          "AGA":"*",
          "AGT":"+",
          "AGG":",",
          "AGC":"-",
          "ACA":".",
          "ACT":"/",
          "ACG":"0",
          "ACC":"1", 

          "TAA":"2",
          "TAT":"3",
          "TAG":"4",
          "TAC":"5",
          "TTA":"6",
          "TTT":"7",
          "TTG":"8",
          "TTC":"9",
          "TGA":":",
          "TGT":";",
          "TGG":"<",
          "TGC":"=",
          "TCA":">",
          "TCT":"?",
          "TCG":"@",
          "TCC":"[",
     
          "GAA":"]",
          "GAT":"^",
          "GAG":"_",
          "GAC":"~",
          "GTA":"a",
          "GTT":"b",
          "GTG":"c",
          "GTC":"d",
          "GGA":"e",
          "GGT":"f",
          "GGG":"g",
          "GGC":"h",
          "GCA":"i",
          "GCT":"j",
          "GCG":"k",
          "GCC":"l",
                   
          "CAA":"m",
          "CAT":"n",
          "CAG":"o",
          "CAC":"p",
          "CTA":"q",
          "CTT":"r",
          "CTG":"s",
          "CTC":"t",
          "CGA":"u",
          "CGT":"v",
          "CGG":"w",
          "CGC":"x",
          "CCA":"y",
          "CCT":"z",
          "CCG":"{",
          "CCC":"}",
          
          "NNN":"'",
          
          "ANN":"`",
          "TNN":"\\",
          "GNN":"\"",
          "CNN":"E",
          "NNA":"F",
          "NNT":"I",
          "NNG":"J",
          "NNC":"L"
          }
    return bp_map

def mapCodingMapRev(bp_map):
    bp_map_rev = {}
    for bp_key in bp_map:
        bp_map_rev[bp_map[bp_key]] = bp_key
    return bp_map_rev
    

def codeTriple(bp_triple, bp_map):
    return bp_map.get(bp_triple, bp_triple)

def decodeChar(char, bp_map_rev):
    return bp_map_rev.get(char, char)

def decodedDNAseqLength(sequence, bp_map_rev):
    seq_dec_length = 0
    for i in range( len(sequence)):
        if not sequence[i] in bp_map_rev:
            seq_dec_length += 1
        else:
            seq_dec_length += 3
    return seq_dec_length

def decodeDNAseq(sequence, bp_map_rev):
    n = len(sequence)
    return ''.join([bp_map_rev.get(sequence[i], sequence[i]) for i in range(n)])

def codeDNAseq(sequence, bp_map):
    n = len(sequence)
    return ''.join([codeTriple(sequence[3*i:3*i+3], bp_map) for i in range((n//3) + 1)])

if __name__ == '__main__':
    bp_map = getCodingMap()
    bp_map_rev = mapCodingMapRev(bp_map)
    seq = "ATGTNNGNNNNGNNANNNNXNAAAAGXXXXTGMMMACTTTATATTTTATGACGGTCATTATACTTATGCTTGTATTATTTGAAGTCGTATTTTCAGTCTCCGTTTACCGATATTATTATAATGGTATTGTGCAATATGTAGAATCTCATGCGAAGACAAGTACACGATTTTTCTCTGAATACAATTCATTATACTTTATTCGTTTGCAAGAATATAGCGGGGATATAATTGAGAGTTTTCAGTTAGAAGGAACCGAATTACAATTAATTGATCGACATGGTACGATTATACAATCGTCAAGTGGAGAAAAGACAGAGGGTAAAGTAGTTATTCCGTATTCGTTACTAGAAGGAGAAATGTACCACCAAGTGACTACTACGAAGGACAAAGTGAAACAATTAGAAGTGATTAGTCCGCTGATTCATCAAGGACAAACGATTGGTGTTTTAAAATATACGACTGTATTAACACATGTAAATGCCAAGATTATTGAAATTATTATGTTTACGATTTGTGTAGGTATTGTTATTTCAGGAATTGTATTCTTAATTAGTAGACGATTAGCGAATTCATTTGTTAAACCAATCGAATCTATAATTCATGCTTCTTCTCAAATTGCAGAAGGTACATTAAAGAAGAAAATTAAAGAAGATTATCCTGGTGAATTAGGTGAGTTAGCACATAGTTTAAATCATATGTCTGATAAAATAGAAAAAGCGGAACAGATGAAAAATGAATTTATTGCTTCTATTTCTCATGAAATACGAACCCCTTTAACTGGAATTAAGGGTTGGAGTGAAACGTTAAAAACGGTAGATCATTTAACGGAAGAAGAAATAAAGCAAGGCATGGGGATTATTTCAGGTGAGACAGACAGATTAATTCATTTAGTAGAAGAATTGCTAGATTTTTCAAGGTTGCAATCAAATCATTTTAATTTATATAAACAAAAGGTGCAATTATACGATATACTAGAGGAGACGATTTGGCAATTAACTCCTAATGCTGAAGAGAAGAAAATGCAATTCATCAAAACTATAGAACGAATTGAATTGATAGGAGATCGAAATAGGCTAAAGCAAATTTTCTTGAATATTGTTCAAAATGCCATTAAATATTCACATGAAAATGGTAAAGTATATATTGAAGCGACTAAAAATGAAGGACAAGCAGTCATAAAGGTGAAAGACGATGGGATTGGAATTGCTAAAGAGCATTTACCGTATATAGAACAGTCATTTTACCAGATTAATAATCATGCTACAGGTGCTGGTCTTGGTTTAGCTATCGTTAAAAAAATGGTGGAGCTTCATGGAGGTACAATAAATATTATAAGTAAAGAAGGAATAGGAACGACCATTTTGATAAAACTCCCATTATAA"

    s_time = time.time()
    
    for i in range(10000):
        coded_seq = codeDNAseq(seq, bp_map)
        decoded_seq = decodeDNAseq(seq, bp_map)
        length = decodedDNAseqLength(coded_seq, bp_map_rev)

        
    e_time = time.time()
    
    print("X", len(seq), seq)
    print("Y", len(coded_seq), coded_seq)
    print("Z", len(decoded_seq), decoded_seq)
    

