import math

def correctGeneSeq(gene_seq, alt_starts = False):
    from soft.utils.camber_seq_utils import findProteinSequenceLength
    
    if not alt_starts:
        if not gene_seq[:3] in ["ATG", "GTG", "TTG"]:
            return -2;
    if not gene_seq[-3:] in ["TAG", "TAA", "TGA"]:
        return -3;

    length = findProteinSequenceLength(gene_seq, start=0, alt_starts = alt_starts);
    #print(length, len(gene_seq), gene_seq)
    if not length == len(gene_seq):
        return -4
    return 0;

def correctGene(gene, alt_starts = False, is_cds=True):
    if is_cds:
        if not gene.length() % 3 == 0:
            return -1;
        gene_seq = gene.sequence()
        return correctGeneSeq(gene_seq, alt_starts)
    else:
        gene_seq = gene.sequence()
        if len(gene_seq) < 30:
            return -5
        return 0

def hssp(l, n):
    if l <= 11:
        return 100.0
    elif l <= 450:
        return min(100.0, n + 480*l**(-0.32*(1+math.exp(-float(l)/1000.0))))
    else:
        return 19.5 + n

