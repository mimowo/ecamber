import sys
import os
import platform
from soft.utils.camber_params_utils import readParameters

sys.path.append("../../")

from soft.utils.camber_exttools import checkBLASTPaths, checkMusclePaths

#if sys.version.startswith("3"): raw_input = input

def checkPrepareAction(parameters):
    if not os.path.exists(parameters["DATASET_GENOMES_DBS"]):
        print("First format sequences, setting '-a f'")
        return -7
         
    input_fn = parameters["DATASET_ANNS_PARSED_SRC"]
    if not os.path.exists(input_fn):
        print("Annotation source " + input_fn + " is not available. Specify '-as' parameter correctly. Available options:")
        for ann_source in os.listdir(parameters["DATASET_ANNS_PARSED"]):
            print(ann_source)
        return -8
    return 0

def checkClosureAction(parameters):
    exp_path = parameters["DATASET_RESULTS_EXP_PHASE1"]
    if not os.path.exists(exp_path):
        print("This folder does not exist: " + exp_path)
        print("First prepare the experiment files, by setting ACTION=P.")
        return -11
    return 0

def checkGraphAction(parameters):
    return 0

def checkComponentsAction(parameters):
    return 0

def checkRefinementAction(parameters):
    return 0

def checkVotingAction(parameters):
    return 0

def checkCleanUpAction(parameters):
    return 0

def checkOutputAction(parameters):
    return 0

def checkAlignAction(parameters):
    checkMusclePaths(parameters)
    return 0

def checkPhylogenyAction(parameters):
    return 0

def checkMutationsAction(parameters):
    return 0

def checkParameters(parameters):
    
    if not "A" in parameters:
        print("Define -a (action) in the parameters")
        print("d - download data from PATRIC")
        print("f - format downloaded data as input")
        print("ph1 - first phase of eCAMBer (closure procedure)")
        print("ph2 - second phase of eCAMBer (refinement, TIS voting, clean up)")
        print("out - generates output")
        print("ai,ac,ap - post-processing analyzes")
        print("aln - compute multiple alignments of gene amino acid sequences and promotors (requires muscle)")
        print("phyg - computes phylogenetic tree based on gene cluster presence")
        print("mut - detects point mutations (in gene amino acid sequences and promotors)")
        print("Example: python ecamber.py -a f -d mtu2 -w 4")
        ret = -5
        
    action = parameters["A"]
    if action.upper() != "D":
        dataset_path = parameters["ECAMBER_DATASET_PATH"]
        if not os.path.exists(dataset_path):
            print("There is no such dataset: " + dataset_path +". Define the 'DATASET' parameter!")
            return -6
        
    if action == "D":
        return 0
    elif action == "F":
        checkBLASTPaths(parameters)    
    elif action == "P":
        checkPrepareAction(parameters)
    elif action == "C":
        checkBLASTPaths(parameters)
        checkClosureAction(parameters)
    elif action == "PH1":
        checkBLASTPaths(parameters)
        checkPrepareAction(parameters)
        checkClosureAction(parameters)
    elif action == "G":
        checkGraphAction(parameters)
    elif action == "CC":
        checkComponentsAction(parameters) 
    elif action == "RF":
        checkRefinementAction(parameters)
    elif action == "TV":
        checkVotingAction(parameters)
    elif action == "CU":
        checkCleanUpAction(parameters)
    elif action == "PH2":
        checkGraphAction(parameters)
        checkComponentsAction(parameters)
        checkRefinementAction(parameters)
        checkVotingAction(parameters)
        checkCleanUpAction(parameters)
    elif action == "OUT":
        checkOutputAction(parameters)
    elif action == "ALN":
        parameters = readParameters(config_aln=True)
        checkAlignAction(parameters)
    elif action == "PHY":
        parameters = readParameters(config_aln=True)
        checkPhylogenyAction(parameters)
    elif action == "MUT":
        parameters = readParameters(config_aln=True)
        checkMutationsAction(parameters)
    return 0

