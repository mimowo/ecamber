import sys
import time
import platform
import os
import shutil

sys.path += ["src/", "../", "../../"]

from soft.utils.camber_params_utils import *
from soft.utils.camber_progress import ShowProgress
from soft.structs.strains import Strains
from soft.utils.camber_io_utils import readGeneAnnotationsFromFile, readMgAnnotationsFromFile, readMgUniAnnotationsFromFile, readOrgAnnotationsFromFile
from soft.structs.gene import Gene
from soft.utils.camber_seq_utils import findProteinSequenceLength
from soft.utils.camber_closure_utils import hssp
from soft.utils.camber_io_utils import ensure_dir
from soft.utils.camber_io_utils import readSequenceFromFile
from soft.structs.annotation import Annotation
from soft.structs.multigene import Multigene, createStrainMultigeneId

strains = Strains()

def logExecutionTime(time_sec, comment):
    output_dir = parameters_map["DATASET_RESULTS_EXP"]
    ensure_dir(output_dir)
    output_fn = output_dir +"exec_times.log"
    output_fh = open(output_fn, "a")
    output_fh.write(str(time_sec) + " " + str(comment) + "\n")
    output_fh.close()
    return None

def readStrainsInfo():
    global strains
    from soft.utils.camber_io_utils import readStrainsFromFile
    input_fh = open(parameters_map["ECAMBER_DATASET_PATH"] + "strains.txt")
    strains = readStrainsFromFile(input_fh);
    input_fh.close()
    return strains

def currentClosureIteration():
    input_fh = open(parameters_map["DATASET_RESULTS_EXP_PHASE1"] +"ph1_it.txt")
    line = input_fh.readline();
    word = line.split()[0]
    iteration = int(word)
    input_fh.close()
    return iteration;

def lastIterationWithChange():
    input_fh = open(parameters_map["DATASET_RESULTS_EXP_PHASE1"] +"ph1_it.txt")
    line = input_fh.readline();
    input_fh.close()
    tokens = line.split()
    if len(tokens) == 0:
        iteration = 0
    elif len(tokens) == 1:
        iteration = int(tokens[0])
    else:
        iteration = max(int(tokens[0]), 1) 
    
    return iteration;

#def lastPh2Iteration(closure_iteration):
#    input_fh = open(getItPath(parameters_map, closure_iteration, "DATASET_RESULTS_EXP_PHASE2_IT") + "ph2_step.txt")
#    line = input_fh.readline()
#    input_fh.close()
#    tokens = line.split()
#    if len(tokens) == 0:
#        iteration = 0
#    else:
#        iteration = int(tokens[0])
#    return iteration

def currentPh2Iteration(closure_iteration=None, default=0):
    if "STEP" in parameters_map:
        try: 
            return int(parameters_map["STEP"])
        except:
            if closure_iteration != None:
                i=0
                input_fh = open(getItPath(parameters_map, closure_iteration, "DATASET_RESULTS_EXP_PHASE2_IT") + "/"+str(0))

                while i<100 and os.path.exists(input_fh):
                    i+=1
                    input_fh = open(getItPath(parameters_map, closure_iteration, "DATASET_RESULTS_EXP_PHASE2_IT") + "/"+str(i))
                if i == 100: return default
                else: return i
            else:
                return default
    else:
        return default

def isClosureComputed():
    input_fh = open(parameters_map["DATASET_RESULTS_EXP_PHASE1"] +"ph1_it.txt")
    line = input_fh.readline();
    if not line:
        return False
    tokens = line.split()
    if len(tokens) > 1:
        return True
    input_fh.close()
    return False


def readStrainSequences(strain):
    input1_fn = parameters_map["DATASET_GENOMES"]+"seq-" + strain.strain_id + ".fasta"
    input2_fn = parameters_map["DATASET_GENOMES"] + strain.strain_id + ".fasta"
    if os.path.exists(input1_fn): strain_seq_map = readSequenceFromFile(input1_fn)
    elif os.path.exists(input2_fn): strain_seq_map = readSequenceFromFile(input2_fn)
    else: strain_seq_map = {}
        
    if len(strain_seq_map) == 1: 
        strain.setSequence(list(strain_seq_map.values())[0])
    else:
        strain.setSequences(strain_seq_map)
    return strain.strain_id, strain_seq_map

def readOrgAnnotations(strain, src=None):
    gt = "cds"
    if src == None:
        input_fn = parameters_map["DATASET_ANNS_PARSED_SRC"] + "/" + strain.strain_id + ".txt"
    else:
        input_fn = parameters_map["DATASET_ANNS_PARSED"] + src + "/" + strain.strain_id + ".txt"

    annotation = readOrgAnnotationsFromFile(input_fn, strain, check=True, annotation=None, gt=gt)
    strain.setAnnotation(annotation)
    return strain.strain_id, annotation

def readNewAnnotationsIter(strain, iteration):
    if iteration == 0:
        input_fn = getItPath(parameters_map, iteration, "DATASET_RESULTS_EXP_ALL")+"/"+strain.strain_id + ".txt"
        annotation = readGeneAnnotationsFromFile(input_fn, strain, False)
        strain.setNewAnnotation(annotation)   
    else:
        input_fn = getItPath(parameters_map, iteration, "DATASET_RESULTS_EXP_NEW")+"/"+strain.strain_id + ".txt"
        annotation = readGeneAnnotationsFromFile(input_fn, strain, False)
        strain.setNewAnnotation(annotation)
    return strain.strain_id, annotation

def readAllAnnotationsIter(strain, iteration):
    input_fn = getItPath(parameters_map, iteration, "DATASET_RESULTS_EXP_ALL")+"/"+strain.strain_id + ".txt"
    
    if os.path.exists(input_fn):
        all_annotation = readGeneAnnotationsFromFile(input_fn, strain, False)
    else:
        all_annotation = Annotation()
        for it in range(0, iteration+1, 1):
            if it == 0:
                input_fn = getItPath(parameters_map, it, "DATASET_RESULTS_EXP_ALL")+"/"+strain.strain_id + ".txt"
            else:
                input_fn = getItPath(parameters_map, it, "DATASET_RESULTS_EXP_NEW")+"/"+strain.strain_id + ".txt"
            all_annotation = readGeneAnnotationsFromFile(input_fn, strain, False, annotation = all_annotation)
        
    strain.setAllAnnotation(all_annotation)
    return strain.strain_id, all_annotation

def readAllMgAnnotationsIter(strain, iteration):
    input_fn = getItPath(parameters_map, iteration, "DATASET_RESULTS_EXP_ALL")+"/"+strain.strain_id + ".txt"
    if os.path.exists(input_fn):
        all_annotation = readMgAnnotationsFromFile(input_fn, strain, False)
    else:
        all_annotation = Annotation()
        for it in range(0, iteration+1, 1):
            if it == 0:
                input_fn = getItPath(parameters_map, it, "DATASET_RESULTS_EXP_ALL")+"/"+strain.strain_id + ".txt"
            else:
                input_fn = getItPath(parameters_map, it, "DATASET_RESULTS_EXP_NEW")+"/"+strain.strain_id + ".txt"
            all_annotation = readMgAnnotationsFromFile(input_fn, strain, False)
        
    strain.setAllAnnotation(all_annotation)
    return strain.strain_id, all_annotation

def readFinalAnnotationsIter(strain, iteration):
    input_fn = getItPath(parameters_map, iteration, "DATASET_RESULTS_EXP_FINAL")+"final-"+strain.strain_id+".txt"
    annotation = readGeneAnnotationsFromFile(input_fn, strain, False)
    strain.setFinalAnnotation(annotation)
    return strain.strain_id, annotation

def readPostAnnotationsIter(strain, iteration):
    input_fn = getItPath(parameters_map, iteration, "DATASET_RESULTS_EXP_POST_CCDET")+strain.strain_id+".txt"
    annotation = readMgUniAnnotationsFromFile(input_fn, strain, False)
    strain.setUniAnnotation(annotation)
    return strain.strain_id, annotation

def readUniAnnotationsIter(strain, closure_iteration, ph2_iteration):
    input_fn = getItPath(parameters_map, closure_iteration, "DATASET_RESULTS_EXP_UNIANNS", ph2_iteration)+strain.strain_id+".txt"
    annotation = readMgUniAnnotationsFromFile(input_fn, strain, False)
    strain.setUniAnnotation(annotation)
    return strain.strain_id, annotation

def readClusterGeneNames(input_fn = ""):
    cluster_gene_names = {}
#    input_fn = parameters_map["DATASET_OUTPUT"] + "cluster_gene_names.txt"
    if len(input_fn)==0 or not os.path.exists(input_fn):    
        overwriteParameters(sys.argv)
        parameters = readParameters()
        closure_iteration = currentClosureIteration()
        ph2_iteration = currentPh2Iteration(default = 3) 
        input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration) + "cluster_gene_names.txt"
    if os.path.exists(input_fn):
        input_fh = open(input_fn)
        for line in input_fh.readlines()[1:]:
            tokens = line.strip().split()
            if len(tokens) < 1:
                continue
            cluster_gene_names[tokens[0]] = tokens[1]
        input_fh.close()
    return cluster_gene_names

def readFinalMgAnnotationsIter(strain, iteration):
    input_fn = getItPath(parameters_map, iteration, "DATASET_RESULTS_EXP_FINAL")+"final-"+strain.strain_id+".txt"
    final_annotation = readMgAnnotationsFromFile(input_fn, strain, False)
    
    strain.setFinalAnnotation(final_annotation)
    return strain.strain_id, final_annotation

def runBlastallBlastn(blast_exe, query_fn, genome_fn, output_fn, tr_evalue, masking = False):
    command = blast_exe + " -p blastn -e "+str(tr_evalue)
    if masking == False:
        command += " -F F "
    command += " -d " + genome_fn + " -i " + query_fn + " -o " + output_fn + " -m 8";
    os.system(command);

def runBlastn(blast_exe, query_fn, genome_fn, output_fn, tr_evalue, masking = False):
    command = blast_exe + " -evalue "+str(tr_evalue)
    if masking == True:
        command += " -dust no " 
    command += " -db " + genome_fn + " -query " + query_fn + " -out " + output_fn + " -outfmt 6";
    os.system(command);

def runBlastp(blast_exe, query_fn, genome_fn, output_fn, tr_evalue, masking = False):
    command = blast_exe + " -evalue "+str(tr_evalue)
    if masking == True:
        command += " -dust no " 
    command += " -db " + genome_fn + " -query " + query_fn + " -out " + output_fn + " -outfmt 6";
    os.system(command);


def runBlastallBlastp(blast_exe, query_fn, genome_fn, output_fn, tr_evalue): 
    command = blast_exe + " -p tblastn -e "+str(tr_evalue)+" -F F -C 0 ";
    command += " -d " + genome_fn + " -i " + query_fn + " -o " + output_fn + " -m 8";
    os.system(command)
    
def rateBlastHitsProt(blast_hits):    
    return [],[]

def rateBlastHitDna(blast_hit, strain, len_p, id_p, evalue_tr=10e-10, pid_method=0, hssp_b=True, id_p_pseudo = 0.0, alt_start=None, is_cds=True):
    if strain.sequence != None:
        strain_seq = strain.sequence
        rev_strain_seq = strain.rev_sequence
    else:
        strain_seq = strain.sequences_map[blast_hit.contig_id]
        rev_strain_seq = strain.rev_sequences_map[blast_hit.contig_id]
    
    if float(blast_hit.evalue_b) > float(evalue_tr):
        return None
    
    if blast_hit.query_len != None:
        query_len = blast_hit.query_len
    elif blast_hit.query_gene != None: 
        query_len = blast_hit.query_gene.length()
    else:
        query_len = len(blast_hit.query)  
        
    if pid_method == 0:
        pid = float(blast_hit.identities)*100.0/float(query_len)
    elif pid_method == 1:
        pid = float(blast_hit.identity_b)*100.0
    
    if pid < id_p:
        return None
    if hssp_b:
        res_c = int(blast_hit.residues / 3)
        if pid < hssp(res_c, id_p - 19.5):
            return None
    
    accepted = True
    
    if int(blast_hit.query_start) > 12: 
        accepted = False
    else:
        hit_gene = blast_hit.hit_gene        
        max_acceptable_len = (1+len_p) * query_len
        n = len(strain_seq)

        if is_cds:
            if(hit_gene.strand == "+"):
                prot_start = hit_gene.start - blast_hit.query_start + 1
                length = findProteinSequenceLength(strain_seq, start=prot_start-1, alt_start=alt_start, max_acceptable_len=max_acceptable_len);
                start = prot_start
                end = prot_start + length - 1
            else:
                prot_start = hit_gene.start + blast_hit.query_start - 1
                length = findProteinSequenceLength(rev_strain_seq, start=n-prot_start, alt_start=alt_start, max_acceptable_len=max_acceptable_len);
                start = prot_start - length + 1
                end = prot_start
        else:
            start = hit_gene.start
            end = hit_gene.end
            length = hit_gene.length()
            
        if min(start, end) < 0 or max(start, end) >= n or length < 50:
            accepted = False
        elif (length >= (1-len_p) * query_len and length <= (1+len_p)* query_len):
            ext_hit_gene = Gene(start, end, hit_gene.strand, strain)
            blast_hit.addBlastHitExtended(ext_hit_gene)
            blast_hit.is_pseudo = False
            return blast_hit
        
    if accepted == False and id_p_pseudo > 0.0:
        res_c = int(blast_hit.residues / 3)
        if pid >= hssp(res_c, id_p_pseudo - 19.5):
            blast_hit.addBlastHitExtended(blast_hit.hit_gene)
            blast_hit.is_pseudo = True
            return blast_hit
               
    return None

def saveMgsAnnotations(output_fh, genes=None, multigenes=None, show_start_codons = False, cluster_ids=False, smr=0.0, std=False, cluster_gene_names={}):
    if genes != None:
        multigenes = {}
        for gene in list(genes.values()):
            multigene_id = createStrainMultigeneId(gene.end, gene.strand, gene.strain_id, gene.contig_id)
            if not multigene_id in multigenes:
                multigene = Multigene(gene.start, gene.end, gene.strand, gene.strain, contig_id=gene.contig_id)
                multigenes[multigene_id] = multigene
            else:
                multigene = multigenes[multigene_id]
            multigene.addGene(gene)
        multigenes_set = set(multigenes.values())
    else:
        multigenes_set = multigenes
         
    mg_desc_list = []
    multigenes_sorted = sorted(multigenes_set, key=lambda multigene: (multigene.contig_id, multigene.end, multigene.strand))

    if std == False:
        for multigene in multigenes_sorted:
            mg_desc = ""
            if cluster_ids == True:
                mg_desc += multigene.cluster_id + "\t"
            mg_desc += multigene.mg_strain_unique_id+"\t"+multigene.ann_id + "\t"
            genes_sorted = sorted(multigene.genes.values(), key=lambda gene:gene.length())
            for gene in genes_sorted:
                if gene.length() < smr*multigene.length() and not gene.length() in multigene.ann_lengths:
                    continue
                if gene.selected == True or gene.length() in multigene.sel_lengths:
                    mg_desc += "#"
                if len(gene.gene_id) > 1:
                    mg_desc += "*"
                mg_desc += str(gene.length())
                if show_start_codons:
                    mg_desc += ":" + gene.startCodon()
                mg_desc += " "
            mg_desc = mg_desc[:-1]
            mg_desc_list.append(mg_desc)
    else:
        for multigene in multigenes_sorted:
            start, end = multigene.getStartEnd()
            mg_desc = ""
            if cluster_ids == True:
                mg_desc += multigene.cluster_id + "\t"
            mg_desc += multigene.ann_id + "\t"
            if hasattr(multigene, "cluster_id") and multigene.cluster_id != None:
                mg_desc += cluster_gene_names.get(multigene.cluster_id, "x") + "\t"
            else:
                mg_desc += cluster_gene_names.get(multigene.ann_id, "x") + "\t"
            mg_desc += str(start) + "\t"
            mg_desc += str(end) + "\t"
            mg_desc += str(multigene.strand) + "\t"
            mg_desc += str(multigene.contig_id)
            mg_desc_list.append(mg_desc)
        
    for mg_desc in mg_desc_list:
        output_fh.write(mg_desc + "\n")
    return None


def tail(input_fh, lines=1, _buffer=4098):
    lines_found = []
    block_counter = -1

    while len(lines_found) < lines:
        try:
            input_fh.seek(block_counter * _buffer, os.SEEK_END)
        except IOError:
            input_fh.seek(0)
            lines_found = input_fh.readlines()
            break
        lines_found = input_fh.readlines()
        if len(lines_found) > lines:
            break
        block_counter -= 1
    return lines_found[-lines:]

def median(mylist):
    sorts = sorted(mylist)
    n = len(sorts)
    if n == 0:
        return 0.0
    m = int(n/2)
    if n % 2 == 1:
        return (sorts[m] + sorts[m-1]) / 2.0
    return sorts[m]

