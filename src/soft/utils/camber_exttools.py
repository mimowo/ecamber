import os
import sys
import platform

def find_program(prog_filename, error_on_missing=False):
    bdirs = ['$HOME/Environment/local/bin/',
             '$HOME/bin/',
             '/share/apps/bin/',
             '/usr/local/bin/',
             '/usr/bin/']
    paths_tried = []
    for d in bdirs:
        p = os.path.expandvars(os.path.join(d, prog_filename))
        paths_tried.append(p)
        if os.path.exists(p):
            return p
    if error_on_missing:
        raise Exception("*** ERROR: '%s' not found on:\n  %s\n" % (prog_filename, "\n  ".join(paths_tried)))
    else:
        return None

def checkBLASTPaths(parameters):
    ret = 0
    print("Platform: " + platform.system())
    if parameters["BLAST_VER"].upper() == "BLASTALL":
        if platform.system().upper().count("LINUX") > 0:
            if not os.path.exists(parameters["BLASTALL_PATH"]):
                blast_path = find_program(parameters["BLASTALL_PATH"], False)
                if blast_path is None: ret = -1
            if not os.path.exists(parameters["FORMATDB_PATH"]):
                formatdb_path = find_program(parameters["FORMATDB_PATH"], False)
                if formatdb_path is None: ret = -2
        else:
            if not os.path.exists(parameters["BLASTALL_PATH"]): ret = -1
            elif not os.path.exists(parameters["BLASTALL_PATH"]): ret = -2
    else:
        if platform.system().upper().count("LINUX") > 0:
            if not os.path.exists(parameters["BLASTN_PATH"]):
                blast_path = find_program(parameters["BLASTN_PATH"], False)
                if blast_path is None: ret = -3
            if not os.path.exists(parameters["MAKEBLASTDB_PATH"]):
                formatdb_path = find_program(parameters["MAKEBLASTDB_PATH"], False)
                if formatdb_path is None: ret = -4
        else:
            if not os.path.exists(parameters["BLASTN_PATH"]): ret = -3
            elif not os.path.exists(parameters["MAKEBLASTDB_PATH"]): ret = -4
            
    if ret == -1:
        print("The path to the BLASTALL program is not specified correctly: " + parameters["BLASTALL_PATH"])
        print("Please specify the path in the file: config/parameters-extpaths.txt")
    elif ret == -2:
        print("The path to the FORMATDB program is not specified correctly: " + parameters["FORMATDB_PATH"])
        print("Please specify the path in the file: config/parameters-extpaths.txt")
    elif ret == -3:
        print("The path to BLASTN is not specified correctly: " + parameters["BLASTN_PATH"])
        print("Please specify the path in the file: config/parameters-extpaths.txt")
    elif ret == -4:
        print("The path to the MAKEBLASTDB program is not specified correctly: " + parameters["MAKEBLASTDB_PATH"])
        print("Please specify the path in the file: config/parameters-extpaths.txt")
    
    if ret < 0:
        exit(ret)
        
    return ret

def checkMusclePaths(parameters):
    if platform.system().upper().count("LINUX") > 0:
        if not os.path.exists(parameters["MUSCLE_PATH"]):
            muscle_path = find_program(parameters["MUSCLE_PATH"], False)
            if muscle_path is None:
                return -3
    elif not os.path.exists(parameters["MUSCLE_PATH"]):
        return -3
            
    return 0



def checkPHYLIPPaths(parameters):
    if not os.path.exists(parameters["PHYLIP_PATH"]):
        return -3
    return 0


