import socket
import os
import sys
try:
    from urllib.request import urlopen
except ImportError:
    from urllib2 import urlopen

if sys.version.startswith("3"): raw_input = input

def intparse(string):
    try: return int(string)
    except Exception: return None


def downloadStrainsList(ncbi_link, timeout=60, species_name=""):
	print(ncbi_link)
	try:
	    socket.setdefaulttimeout(timeout)
	    online_fh = urlopen(ncbi_link, timeout=timeout)
	    lines = online_fh.readlines()
	    online_fh.close()
	except Exception:
	    exc = sys.exc_info()[1]
	    print("Error: " + str(exc))
	    print("Error: " + str(sys.exc_info()[0]))
	    print("Error: " + str(sys.exc_info()))
	    print("Time of "+str(timeout) + "s was exceeded. Cannot connect to the database.")

	print("lines: ", len(lines))

	ret = []
	for line in lines:
		line = line.strip()
		tokens = line.split("\t")
		strain_full_id = tokens[0]
		if strain_full_id.count("#") > 0 or len(strain_full_id) <= len(species_name):
			continue
		status = tokens[-1]
		date = tokens[-3]
		print(strain_full_id, status, date)
		ret.append(strain_full_id)
	return ret

def downloadStrainFiles(params):
    strain_id = params[0]
    full_strain_id = params[1]
    genomes_dir = params[2]
    cds_refseq_dir = params[3]
    cds_patric_dir = params[4]
    gb_refseq_dir = params[5]
    gb_patric_dir = params[6]
    patric_db = params[7]    
    
    log_lines = []

    out_seq_fn = genomes_dir + "/"+strain_id+".fasta"    
    if not os.path.exists(out_seq_fn) or os.path.getsize(out_seq_fn)<10:
        http_fn = patric_db + "/" + full_strain_id + "/" + full_strain_id+ ".fna"
        log = downloadFile(http_fn, out_seq_fn)
        if len(log) > 0:
            log_lines.append("SEQUENCE GENOME: " + log)
            if os.path.exists(out_seq_fn):
                os.remove(out_seq_fn)
            
    if len(cds_refseq_dir) > 0:
        out_fea_fn = cds_refseq_dir+"/"+strain_id+".tab"
        if not os.path.exists(out_fea_fn) or os.path.getsize(out_fea_fn)<10:
            http_fn = patric_db + "/" + full_strain_id + "/" + full_strain_id+ ".RefSeq.features.tab"
            log = downloadFile(http_fn, out_fea_fn)
            if len(log) > 0:
                log_lines.append("REFSEQ: " + log)
                if os.path.exists(out_fea_fn):
                    os.remove(out_fea_fn)
    
    if len(cds_patric_dir) > 0:
        out_fea_fn = cds_patric_dir+"/"+strain_id+".tab"            
        if not os.path.exists(out_fea_fn) or os.path.getsize(out_fea_fn)<10:
            #http_fn = patric_db + "/" + full_strain_id + "/" + full_strain_id+ ".PATRIC.cds.tab"
            http_fn = patric_db + "/" + full_strain_id + "/" + full_strain_id+ ".PATRIC.features.tab"
            log = downloadFile(http_fn, out_fea_fn)
            if len(log) > 0:
                log_lines.append("PATRIC: " + log)
                if os.path.exists(out_fea_fn):
                    os.remove(out_fea_fn)

    if len(gb_refseq_dir) > 0:
        out_fea_fn = gb_refseq_dir+"/"+strain_id+".txt"
        if not os.path.exists(out_fea_fn) or os.path.getsize(out_fea_fn)<10:
            http_fn = patric_db + "/" + full_strain_id + "/" + full_strain_id+ ".RefSeq.gbf"
            log = downloadFile(http_fn, out_fea_fn)
            if len(log) > 0:
                log_lines.append("REFSEQ: " + log)
                if os.path.exists(out_fea_fn):
                    os.remove(out_fea_fn)
    
    if len(gb_patric_dir) > 0:
        out_fea_fn = gb_patric_dir+"/"+strain_id+".txt"            
        if not os.path.exists(out_fea_fn) or os.path.getsize(out_fea_fn)<10:
            http_fn = patric_db + "/" + full_strain_id + "/" + full_strain_id+ ".PATRIC.gbf"
            log = downloadFile(http_fn, out_fea_fn)
            if len(log) > 0:
                log_lines.append("PATRIC: " + log)
                if os.path.exists(out_fea_fn):
                    os.remove(out_fea_fn)

            
    return strain_id, log_lines

def retriveStrainID(strain_full_id, species_name):
	return strain_full_id[len(species_name):]

def strainIDs(all_strains):
    ret = []
    for strain_full_id in all_strains:        
        strain_id = retriveStrainID(strain_full_id)
        if len(strain_id) > 0:
            ret.append((strain_id, strain_full_id))
    return ret

def selectStrains(strain_det_list, species_name):    
    if len(strain_det_list) == 0:
        print("No strain names containing " + species_name + " as their substring.")
        sys.exit()
        
    selected_subset = set(strain_det_list)
    ans = ""
    n = len(strain_det_list)
    while not ans.lower() in ["q", "d"]:
        for i in range(n):
            full_strain_id = strain_det_list[i]
            if full_strain_id in selected_subset:
                print("[X] "+str(i+1) +": "+ full_strain_id + "\t" + retriveStrainID(full_strain_id, species_name))
            else:
                print("[ ] "+str(i+1) +": "+ full_strain_id + "\t" + retriveStrainID(full_strain_id, species_name))
        
        print("Press (d)ownload selected / (a)ll /(n)one / (Int) index to toggle selection / (q)uit")
        sys.stdout.flush()
        ans = raw_input("Choose action: ").lower().strip()
        if ans == "d":
            continue
        elif ans == "q":
            exit()
        elif ans == "a":
            selected_subset = set(strain_det_list).copy()
        elif ans == "n":
            selected_subset = set([])
        elif ans.isdigit():
            index = int(ans)-1
            if index < 0 or index >= n:
                continue 
            strain_full_id = strain_det_list[index]
            if strain_full_id in selected_subset:
                selected_subset.remove(strain_full_id)
            else:
                selected_subset.add(strain_full_id)
        
    print("Genomes to download: " + str(len(selected_subset)))
    return selected_subset
