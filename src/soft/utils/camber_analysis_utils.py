from soft.utils.camber_params_utils import parameters_map

def readGenomeLengths(strains):
    input_fh = open(parameters_map["DATASET_RESULTS_EXP_ANALYSIS"]+"analysis_strain_genome_lengths.txt")
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) < 2:
            continue
        strain_id = tokens[0]
        genome_length = int(tokens[1])
        strain = strains.strain(strain_id)
        strain.genome_length = genome_length
    input_fh.close()
    return None