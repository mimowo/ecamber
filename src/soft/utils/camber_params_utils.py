import sys
import os
import platform

from soft.utils.camber_exttools import *

parameters_map = {}

key_map = {"ACTION":"A", "a": "A",
           "WORKERS": "W", "w": "W",
           "DATASET": "D", "d": "D",
           "EXP_PREFIX": "EP", "e": "EP", "ep":"EP",
           "ANN_SOURCE": "AS", "as": "AS",
           "MAX_IT": "M", "m": "M",
           "PID_DNA": "PID", "pid": "PID",
           "BEVALUE": "BE", "be": "BE",
           "TEVALUE": "TE", "te": "TE",
           "PH2_IT": "STEP", "step": "STEP", "STEP":"STEP",
           "tvc": "TVC",
           "tvr": "TVR",
           "cur": "CUR",
           "cup": "CUP",
           "cul": "CUL",
           "cuv": "CUV",
           "doref": "DOREF",
           "tree_soft":"TREE_SOFT", "ts":"TREE_SOFT", "TS":"TREE_SOFT"
           }

def shortKey(key_tmp):
    return key_map.get(key_tmp, key_tmp)

def readParametersFromFile(input_fn, parameters_map):
    input_fh = open(input_fn);
    lines = input_fh.readlines()
    input_fh.close()

    for line in lines:
        line = line.strip().split("#")[0].strip()
        tokens = line.split(":")
        if len(tokens) < 2:
            continue
        key_tmp = tokens[0]
        key = shortKey(key_tmp)
        if key == "ECAMBER_PATH":
            pass
        else:
            value = ""
            for token in tokens[1:]:
                value += parameters_map.get(token, token)
                    
            if not key in parameters_map:
                if key == "ECAMBER_DATASET_PATH":
                    parameters_map[key] = os.path.abspath(value)+"/"
                elif key in ["BLASTALL_PATH", "FORMATDB_PATH", "BLASTN_PATH", "TBLASTN_PATH", "MUSCLE_PATH", "BLASTP_PATH", "MAKEBLASTDB_PATH"]:
                    if platform.system().upper().count("CYGWIN") > 0:
                        parameters_map[key] = os.path.relpath(value) + ".exe"
                    elif platform.system().upper().count("WINDOWS") > 0:
                        parameters_map[key] = os.path.relpath(value) + ".exe"
                    else:
                        parameters_map[key] = value
                else:
                    parameters_map[key] = value

    return parameters_map;

def getCAMBerPath(run_path, cmd_path):
    full_path = os.path.abspath(os.path.join(run_path, cmd_path))
    ecamber_path = os.path.dirname(full_path)

    last_dir = os.path.split(ecamber_path)
    while len(ecamber_path) > 1:
        path_tokens = os.path.split(ecamber_path)
        if path_tokens[1] == "ecamber":
            return ecamber_path + "/"
        ecamber_path = path_tokens[0]

    return ecamber_path;

def getItPath(parameters, it, pathtype, comp_it=0):
    if pathtype == "DATASET_RESULTS_EXP_PHASE1_IT":
        return parameters["DATASET_RESULTS_EXP_PHASE1"] +"/" + str(it) + "/"
    elif pathtype == "DATASET_RESULTS_EXP_PHASE2_IT":
        return parameters["DATASET_RESULTS_EXP_PHASE2"] +"/" + str(it) + "/"
        
    elif pathtype == "DATASET_RESULTS_EXP_ALL":
        return parameters["DATASET_RESULTS_EXP_PHASE1"] +"/" + str(it) + "/all_anns/"
    elif pathtype == "DATASET_RESULTS_EXP_NEW":
        return parameters["DATASET_RESULTS_EXP_PHASE1"] +"/" + str(it) + "/new_anns/"
    elif pathtype == "DATASET_RESULTS_EXP_BLAST_ACC":
        return parameters["DATASET_RESULTS_EXP_PHASE1"] +"/" + str(it) + "/blasts_acc/"
    
    elif pathtype == "DATASET_RESULTS_EXP_PH2_IT":
        return parameters["DATASET_RESULTS_EXP_PHASE2"] + "/" + str(it) + "/step_" + str(comp_it) + "/"
    elif pathtype == "DATASET_RESULTS_EXP_MGSSEQ":
        return parameters["DATASET_RESULTS_EXP_PHASE2"] +"/" + str(it) + "/step_" + str(comp_it) + "/mgsseq/"
    elif pathtype == "DATASET_RESULTS_EXP_SEQIDS":
        return parameters["DATASET_RESULTS_EXP_PHASE2"] +"/"+ str(it) + "/step_" + str(comp_it) + "/seqids/"
    elif pathtype == "DATASET_RESULTS_EXP_UNIANNS":
        return parameters["DATASET_RESULTS_EXP_PHASE2"] +"/" + str(it) + "/step_" + str(comp_it) + "/unianns/"
    elif pathtype == "DATASET_RESULTS_EXP_UNIANNS_GOLDEN":
        return parameters["DATASET_RESULTS_EXP_PHASE2"] +"/" + str(it) + "/step_" + str(comp_it) + "/unianns_golden/"
    
    elif pathtype == "DATASET_RESULTS_EXP_ANALYSIS_POST":
        return parameters["DATASET_RESULTS_EXP_ANALYSIS"] +"/analysis_post_" + str(comp_it) + "/"
    
    elif pathtype == "DATASET_RESULTS_EXP_PHASE2_TMP":
        return parameters["DATASET_RESULTS_EXP_PHASE2"] +"/" + str(it) + "/phase2_tmp/"
    elif pathtype == "DATASET_RESULTS_EXP_FINAL":
        return parameters["DATASET_RESULTS_EXP_PHASE2"] +"/" + str(it) + "/phase2_tmp/final_anns/"
    elif pathtype == "DATASET_RESULTS_EXP_EDGES_SEQ":
        return parameters["DATASET_RESULTS_EXP_PHASE2"] +"/" + str(it) + "/phase2_tmp/edges_seq/"
    elif pathtype == "DATASET_RESULTS_EXP_EDGES_MGS":
        return parameters["DATASET_RESULTS_EXP_PHASE2"] +"/" + str(it) + "/phase2_tmp/edges_mgs/"

    elif pathtype == "DATASET_RESULTS_EXP_CLEANUP":
        return parameters["DATASET_RESULTS_EXP_PHASE2"] +"/" + str(it) + "/phase2_tmp/cleanup/"
    elif pathtype == "DATASET_RESULTS_EXP_REFINEMENT":
        return parameters["DATASET_RESULTS_EXP_PHASE2"] +"/" + str(it) + "/phase2_tmp/refinement/"    
    elif pathtype == "DATASET_RESULTS_EXP_TIS_VOTING":
        return parameters["DATASET_RESULTS_EXP_PHASE2"] +"/" + str(it) + "/phase2_tmp/tis_voting/"
    elif pathtype == "DATASET_RESULTS_EXP_CCREFDET":
        return parameters["DATASET_RESULTS_EXP_PHASE2"] +"/" + str(it) + "/phase2_tmp/cc_refdet/"
    elif pathtype == "DATASET_RESULTS_EXP_CCMGS":
        return parameters["DATASET_RESULTS_EXP_PHASE2"] +"/" + str(it) + "/phase2_tmp/cc_mgs/"    
    elif pathtype == "DATASET_RESULTS_EXP_MGOVER":
        return parameters["DATASET_RESULTS_EXP_PHASE2"] +"/" + str(it) + "/phase2_tmp/mg_overlaps/"  

    elif pathtype == "DATASET_RESULTS_EXP_VOTED":
        return parameters["DATASET_RESULTS_EXP_PHASE2"] +"/" + str(it) + "/phase2_tmp/cc_voted/"
    elif pathtype == "DATASET_RESULTS_EXP_VOTED_STATS":
        return parameters["DATASET_RESULTS_EXP_PHASE2"] +"/" + str(it) + "/phase2_tmp/cc_voted_stats/"
    elif pathtype == "DATASET_RESULTS_EXP_POST_CCDET":
        return parameters["DATASET_RESULTS_EXP_PHASE2"] +"/" + str(it) + "/phase2_tmp/post_cc_det/"         
    elif pathtype == "DATASET_RESULTS_EXP_POST_MGOVER":
        return parameters["DATASET_RESULTS_EXP_PHASE2"] +"/" + str(it) + "/phase2_tmp/post_mg_overlaps/"    

    return ""

def overwriteParameters(sys_argv):
    global parameters_map;
    
    run_path = os.path.abspath(os.curdir)
    cmd_path = sys_argv[0]
    camber_path = getCAMBerPath(run_path, cmd_path)
    os.chdir(camber_path)

    sys.path += ["src/"]

    parameters_map["ECAMBER_PATH"] = camber_path

    n = len(sys_argv)
    i = 1
    while i < n:
        arg = sys_argv[i]
        if arg.startswith("-"):
            if arg.count("=") == 1:
                key_tmp, value = arg[1:].split("=")
                i += 1
            else:
                key_tmp = arg[1:]
                if i < n-1: value = sys_argv[i+1]
                else: value = ""
                i += 2
        elif arg.count("=") == 1:
            key_tmp, value = arg.split("=")
            i += 1
        else:
            value = ""
            i += 1
        
        key = shortKey(key_tmp)
        parameters_map[key] = value
        
    return parameters_map


def readParameters(config_download=False, config_aln=False):
    global parameters_map;
    config_dir = parameters_map["ECAMBER_PATH"] + "/config/"; 
    
    parameters_map = readParametersFromFile(config_dir+"config_params.txt", parameters_map)
    parameters_map = readParametersFromFile(config_dir+"config_resources.txt", parameters_map)
    parameters_map = readParametersFromFile(config_dir+"config_extpaths.txt", parameters_map)
    parameters_map = readParametersFromFile(config_dir+"config_results.txt", parameters_map)
    if config_download:
        parameters_map = readParametersFromFile(config_dir+"config_download.txt", parameters_map)
    if config_aln:
        parameters_map = readParametersFromFile("config/config_aln_params.txt", parameters_map)
        parameters_map = readParametersFromFile("config/config_aln_paths.txt", parameters_map)
        
    return parameters_map

