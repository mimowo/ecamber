import sys
import os

from soft.utils.camber_progress import ShowProgress
from soft.structs.multigene import Multigene
from soft.utils.camber_utils import parameters_map, tail,\
    currentClosureIteration, currentPh2Iteration, readStrainsInfo,\
    readStrainSequences
from soft.utils.camber_seq_utils import complementarySequence
from soft.utils.camber_coding_utils import getCodingMap, mapCodingMapRev,decodeDNAseq, codeDNAseq
from soft.utils.camber_params_utils import getItPath, overwriteParameters,\
    readParameters

def getMappedIDs(blasts_mapping):
    ids = set([])
    for seq_id in list(blasts_mapping):
        ids.add(seq_id)
    return ids

def maxMappedID(blasts_mapping):
    max_id = 0
    for seq_id in blasts_mapping:
        if int(seq_id) > max_id:
            max_id = seq_id
    return max_id

def getMappedSeqs(blasts_mapping):
    seqs = set([])
    for seq in list(blasts_mapping.values()):
        seqs.add(seq)
    return seqs

def getMaxID(input_fn):
    if not os.path.exists(input_fn):
        return -1
    input_fh = open(input_fn)
    lines_found = tail(input_fh, 1)
    input_fh.close()
    
    max_id = -1
    if len(lines_found) == 0:
        max_id = -1
    elif len(lines_found) == 1:
        tokens = lines_found[0].split()
        if len(tokens) > 0:
            max_id = int(tokens[0])
        else:
            max_id = -1
    
    return max_id

def readMaxIDAndMapped(out_map_fn, sequences_subset=None):
    out_map_fh = open(out_map_fn)
    max_id = 0
    seqs = set([])
    for line in out_map_fh.readlines():
        tokens = line.split()
        if len(tokens) < 2:
            continue
        seq_id = int(tokens[0])
        if seq_id > max_id:
            max_id = seq_id
        if sequences_subset == None:
            seqs.add(tokens[1])
        elif tokens[1] in sequences_subset:
            seqs.add(tokens[1])
    out_map_fh.close()
    return max_id, seqs   
    
def readBlastResultsMaxID(out_map_fn):
    out_map_fh = open(out_map_fn)
    max_id = -1
    for line in out_map_fh.readlines():
        tokens = line.split()
        if len(tokens) < 2:
            continue
        seq_id = int(tokens[0])
        if seq_id > max_id:
            max_id = seq_id
    out_map_fh.close()
    return max_id   
    
def readBlastsMappingLens(out_map_fn, subset_ids = None):
    out_map_fh = open(out_map_fn)
    id_map = {}
    for line in out_map_fh.readlines():
        tokens = line.split()
        if len(tokens) < 2:
            continue
        seq_id = int(tokens[0])
        seq_len = int(tokens[1])
        if subset_ids != None and not seq_id in subset_ids:
            continue
        id_map[seq_id] = seq_len
    out_map_fh.close()
    return id_map

def readBlastsMappingAltstarts(out_map_fn, subset_ids = None):
    out_map_fh = open(out_map_fn)
    id_map = {}
    for line in out_map_fh.readlines():
        tokens = line.split()
        if len(tokens) < 2:
            continue
        seq_id = int(tokens[0])
        sltstart = tokens[1]
        if subset_ids != None and not seq_id in subset_ids:
            continue
        id_map[seq_id] = sltstart
    out_map_fh.close()
    return id_map


def readBlastHitsSequencesEncoded(ann_fh, strain):
    
    bp_map = getCodingMap()
    
    seqs_map = {}
    lines = ann_fh.readlines()
    
    for line in lines:
        tokens = line.split()
        seq_id = int(tokens[0])
        start = int(tokens[1]);
        end = int(tokens[2]);
        strand = tokens[3]

        if len(tokens) == 4:
            strain_sequence = strain.sequence
        else:
            contig_id = tokens[4]
            strain_sequence = strain.sequences_map[contig_id]
        if strand == '+':         
            gene_sequence = strain_sequence[start-1:end]
        else:
            gene_sequence = complementarySequence(strain_sequence[end-1:start])
        
        if not seq_id in seqs_map:
            seqs_map[seq_id] = set([])
        encoded_seq = codeDNAseq(gene_sequence, bp_map)
        seqs_map[seq_id].add(encoded_seq)
    return seqs_map;

def readBlastResultsMappingRev(input_fn, verb = False, sequences_subset=None, ids_subset=None, decode = False):
    input_fh = open(input_fn)
    lines = input_fh.readlines()
    input_fh.close()
    n = len(lines)
    if verb:
        progress = ShowProgress("Read BLAST mappings")
        progress.setJobsCount(n)
    map_seq = {}
    
    if decode:
        bp_map = getCodingMap()
        bp_map_rev = mapCodingMapRev(bp_map)
    
    seq_id = -1
    
    for line in lines:
        tokens = line.split()
        if len(tokens) < 2:
            continue
        
        seq_id = int(tokens[0])
        
        if ids_subset != None and not seq_id in ids_subset:
            continue
        seq_encoded = tokens[1]
        if decode:
            seq_decoded = decodeDNAseq(seq_encoded, bp_map_rev)
            if sequences_subset != None and not seq_decoded in sequences_subset:
                continue
            map_seq[seq_decoded] = seq_id
            
        else:
            if sequences_subset != None and not seq_encoded in sequences_subset:
                continue
            map_seq[seq_encoded] = seq_id
        if verb:
            progress.update()
    
    return map_seq, seq_id

def readBlastResultsMapping(input_fn, verb = False, sequences_subset=None, ids_subset=None, decode=False):
    input_fh = open(input_fn)
    lines = input_fh.readlines()
    input_fh.close()
    n = len(lines)
    if verb:
        progress = ShowProgress("Read BLAST mappings")
        progress.setJobsCount(n)
    map_seq = {}
    seq_id = -1
    
    bp_map = getCodingMap()
    bp_map_rev = mapCodingMapRev(bp_map)
    count = 0
    for line in lines:
        tokens = line.split()
        if len(tokens) < 2:
            continue

        seq_id = int(tokens[0])
        if ids_subset != None and not seq_id in ids_subset:
            continue
                
        seq_encoded = tokens[1]
        if decode:
            seq_decoded = decodeDNAseq(seq_encoded, bp_map_rev)
            if sequences_subset != None and not seq_decoded in sequences_subset:
                continue
            map_seq[seq_id] = seq_decoded
        else:
            count += 1
            if sequences_subset != None and not seq_encoded in sequences_subset:
                continue
            map_seq[seq_id] = seq_encoded
        if verb:
            progress.update()
    return map_seq, seq_id

def saveBlastResultsMapping(output_fn, blasts_mapping):
    output_fh = open(output_fn, "w")

    for seq_id in blasts_mapping:
        seq = blasts_mapping[seq_id]
        output_fh.write(str(seq_id) + "\t" + seq + "\n")
    output_fh.close()
    return None

def saveBlastResultsMappingLens(output_fn, blasts_mapping_lens):
    output_fh = open(output_fn, "w")
    for seq_id in sorted(blasts_mapping_lens):
        seq_len = blasts_mapping_lens[seq_id]
        output_fh.write(str(seq_id) + "\t" + str(seq_len) + "\n")
    output_fh.close()
    return None

def readAnnIDMap(input_fh):
    ann_id_map = {}
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split()
        if len(tokens) >= 2:
            ann_id_map[tokens[0]] = tokens[1]

    return ann_id_map

def readMultigeneClusterStrain(params):
    overwriteParameters(sys.argv)
    readParameters()
    
    strain = params[0]
    closure_iteration = params[1]
    ph2_iteration = params[2]
    if len(params) >= 4: org = params[3]
    else: org = False
    if len(params) >= 5: read_gene_names = params[4]
    else: read_gene_names = False
    if len(params) >= 6: read_sequence = params[5]
    else: read_sequence = False
    #if len(params) >= 7: read_overlap = params[6]
    #else: read_overlap = False

    if read_sequence: readStrainSequences(strain)
    
    strain_id = strain.strain_id
    
    ann_input_fn = parameters_map["DATASET_RESULTS_EXP_NAMES"]+"/"+strain_id+".txt"
    if read_gene_names == True and os.path.exists(ann_input_fn):
        ann_input_fh = open(ann_input_fn)
        ann_id_map = readAnnIDMap(ann_input_fh)
        ann_input_fh.close()
    else: ann_id_map = {}
    
    input_fn = getItPath(parameters_map, closure_iteration, "DATASET_RESULTS_EXP_UNIANNS", ph2_iteration) +"/"+ strain_id + ".txt"
    input_fh = open(input_fn)
    lines = input_fh.readlines()
    input_fh.close()
    
    cc_mapping = {}
    for line in lines:
        line = line.strip()
        if line.startswith("#"):
            continue
        tokens = line.split("\t")
        cluster_id = tokens[0]
        mg_tokens = tokens[1].split(" ")
        ann_id = tokens[2]
        if org == True and len(ann_id) <= 1:
            continue
         
        gene_name = ann_id_map.get(ann_id, ann_id)
            
        end = mg_tokens[0]
        strand = mg_tokens[1]
        contig_id = mg_tokens[2]  
        if tokens[3].count(" ") >0: 
            len_tokens = tokens[3].split()
        else:
            len_tokens = tokens[3:]
        
        lengths = set([])
        ann_lengths = set([])
        sel_lengths = set([])
        
        for i in range(0, len(len_tokens), 1):
            
            length_str = len_tokens[i].split(":")[0]
            selected = False
            
            if length_str.startswith("#"):
                selected = True
                length_str = length_str[1:]
            
            if length_str.startswith("*"):
                length_str = length_str[1:]
                length = int(length_str)
                ann_lengths.add(length)
            else:
                length = int(length_str)
            if selected:
                sel_lengths.add(length)
            lengths.add(length)
            
        max_length = max(lengths)
        
        if strand == "+": start = int(end) - max_length + 1
        else: start = int(end) + max_length - 1            
        
        multigene = Multigene(start, end, strand, strain, ann_id=ann_id, gene_name=gene_name, contig_id=contig_id)     
        multigene.setLengths(lengths)
        multigene.setAnnLengths(ann_lengths)
        multigene.setSelLengths(sel_lengths)
        if not cluster_id in cc_mapping:
            cc_mapping[cluster_id] = []
        cc_mapping[cluster_id].append(multigene)
    
    return strain_id, cc_mapping

