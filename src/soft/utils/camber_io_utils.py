import os
import sys

from soft.utils.camber_progress import ShowProgress
from soft.utils.camber_closure_utils import correctGene
from soft.structs.annotation import Annotation
from soft.structs.gene import Gene
from soft.structs.strains import Strains
from soft.structs.blast_hit import BlastHit
from soft.structs.multigene import createStrainMultigeneId, Multigene

def readStrainsFromFile(input_fh, subset=None):
    strains = Strains()

    lines = input_fh.readlines();
    for line in lines:
        line = line.strip().split("#")[0]

        tokens = line.split();
        if len(tokens) == 0:
            continue
        strain_id = tokens[0];
        if subset != None and not strain_id in subset:
            continue        
        if len(tokens)==1 or (len(tokens) >= 2 and tokens[1]=="-"):
            short_name = strain_id
        elif len(tokens) >= 2:
            short_name = tokens[1]
        strains.addStrain(strain_id, short_name)            
    return strains

def readBlastHitsAnnFromFile(ann_fh, strain):
    annotations = Annotation()
    lines = ann_fh.readlines()
    for line in lines:
        tokens = line.split()
        gene_id = "x"
        start = int(tokens[1]);
        end = int(tokens[2]);
        strand = tokens[3]
        if len(tokens) == 4:
            gene = Gene(start, end, strand, strain, gene_id, contig_id = strain.strain_id)
        else:
            gene = Gene(start, end, strand, strain, gene_id, contig_id = tokens[4])
        annotations.addGene(gene)

    return annotations;

def readOrgAnnotationsFromFile(input_fn, strain, check, annotation = None, gt="cds"):
    accept_alt_starts = True
    if annotation == None:
        annotation = Annotation()
        
    if not os.path.exists(input_fn):
        return annotation
    
    if gt == "cds": is_cds = True
    else: is_cds = False

    input_fh = open(input_fn)
    lines = input_fh.readlines()
    input_fh.close()
    
    for line in lines:
        try:
            line = line.strip()
            if line.startswith("#"):
                continue
            tokens = line.split("\t")
            
            if len(tokens) > 2 and tokens[0].count(" ") == 2:
                mg_tokens = tokens[0].split()
                end = int(mg_tokens[0])
                strand = mg_tokens[1]
                contig_id = mg_tokens[2]
                
                for i in range(2, len(tokens), 1):
                    
                    length_str = tokens[i].split(":")[0]
                    
                    if length_str.startswith("*"):
                        length = int(length_str[1:])
                        gene_name = tokens[1]
                        gene_id = gene_name
                    else:
                        length = int(length_str)
                        gene_name = "x"
                        gene_id = "x"
                    if strand == "+":
                        start = end - length + 1
                    else:
                        start = end + length - 1
                        
                    gene = Gene(start, end, strand, strain, gene_id=gene_id, gene_name=gene_name, contig_id=contig_id)
                    if check:
                        if correctGene(gene, accept_alt_starts, is_cds = False) == 0:
                            annotation.addGene(gene)
                        else:
                            annotation.addPseudoGene(gene)
                    else:
                        annotation.addGene(gene)
                        
            elif(len(tokens) in [4,5,6]):
                if tokens[3] in ["+", "-"]:
                    gene_id = tokens[0];
                    start = int(tokens[1]);
                    end = int(tokens[2]);
                    strand = tokens[3]
                    if len(tokens) == 6:
                        gene_name = tokens[4]
                        contig_id = tokens[5]
                    elif len(tokens) == 5:
                        if strain.sequence == None:
                            gene_name = gene_id
                            contig_id = tokens[4]
                        else:
                            gene_name = tokens[4]
                            contig_id = ""
                    else:
                        gene_name = gene_id
                        contig_id = ""
                elif tokens[2] in ["+","-"]:
                    gene_id = "x";
                    start = int(tokens[0]);
                    end = int(tokens[1]);
                    strand = tokens[2]
                    if len(tokens) == 5:
                        gene_name = tokens[3]
                        contig_id = tokens[4]
                    elif len(tokens) == 4:
                        if strain.sequence == None:
                            gene_name = gene_id
                            contig_id = tokens[3]
                        else:
                            gene_name = tokens[3]
                            contig_id = ""
                    else:
                        gene_name = gene_id
                        contig_id = ""
                
                gene = Gene(start, end, strand, strain, gene_id=gene_id, gene_name=gene_name, contig_id=contig_id)
                

                if check:
                    if correctGene(gene, accept_alt_starts, is_cds = False) == 0:
                        annotation.addGene(gene)
                    else:
                        annotation.addPseudoGene(gene)
                else:
                    annotation.addGene(gene)
        except:
            print("Warning: gene annotation described in the following line: ", line, "cannot be matched with the genome sequence.")
            print(input_fn)
    return annotation;

def readGeneAnnotationsFromFile(input_fn, strain, check, annotation = None):
    
    accept_alt_starts = True
    if annotation == None:
        annotation = Annotation()
    
    input_fh = open(input_fn)
    lines = input_fh.readlines()
    input_fh.close()
    
    for line in lines:
        try:
            line = line.strip()
            if line.startswith("#"):
                continue
            tokens = line.split("\t")
            
            if len(tokens) > 2 and tokens[0].count(" ") == 2:
                mg_tokens = tokens[0].split()
                end = int(mg_tokens[0])
                strand = mg_tokens[1]
                contig_id = mg_tokens[2]
                if tokens[2].count(" ") > 0:
                    len_tokens = tokens[2].split()
                else:
                    len_tokens = tokens[2:]
                    
                for i in range(0, len(len_tokens), 1):
                    selected = False
                    
                    length_str = len_tokens[i].split(":")[0]
                    
                    if length_str.startswith("#"):
                        length_str = length_str[1:]
                        selected = True
                    if length_str.startswith("*"):
                        length = int(length_str[1:])
                        gene_name = tokens[1]
                        gene_id = gene_name
                    else:
                        length = int(length_str)
                        gene_name = "x"
                        gene_id = "x"
                    if strand == "+":
                        start = end - length + 1
                    else:
                        start = end + length - 1
                        
                    gene = Gene(start, end, strand, strain, gene_id=gene_id, gene_name=gene_name, contig_id=contig_id, selected= selected)
                    if check:
                        if correctGene(gene, accept_alt_starts) == 0:
                            annotation.addGene(gene)
                        else:
                            annotation.addPseudoGene(gene)
                    else:
                        annotation.addGene(gene)
        except:
            print("Warning: gene annotation described in the following line: ", line, "cannot be matched with the genome sequence.")
            print(input_fn)
    return annotation;

def readMgAnnotationsFromFile(input_fn, strain, check):
    
    accept_alt_starts = True
    annotations = Annotation()
    
    input_fh = open(input_fn)
    lines = input_fh.readlines()
    input_fh.close()
    
    multigenes = {}

    for line in lines:
        line = line.strip()
        if line.startswith("#"):
            continue
        tokens = line.split("\t")
        
        if len(tokens) <= 2: continue
        if tokens[0].count(" ") > 0:
            shift = 0
            cluster_id = None
        else:
            shift = 1
            cluster_id = tokens[0]
            
        mg_tokens = tokens[shift].split()
        end = int(mg_tokens[0])
        strand = mg_tokens[1]
        contig_id = mg_tokens[2]
        if tokens[2+shift].count(" ") > 0:
            len_tokens = tokens[2+shift].split()
        else:
            len_tokens = tokens[2+shift:]
            
        for i in range(0, len(len_tokens), 1):
            
            selected = False
            length_str = len_tokens[i].split(":")[0]
            
            if length_str.startswith("#"):
                selected = True
                length_str = length_str[1:]
            
            if length_str.startswith("*"):
                length = int(length_str[1:])
                gene_name = tokens[1]
                gene_id = gene_name
            else:
                length = int(length_str)
                gene_name = "x"
                gene_id = "x"
                
            if strand == "+":
                start = end - length + 1
            else:
                start = end + length - 1
            
            gene = Gene(start, end, strand, strain, gene_id=gene_id, gene_name=gene_name, contig_id=contig_id, selected=selected)
            
            if check == False or correctGene(gene, accept_alt_starts) == 0:
                multigene_id = createStrainMultigeneId(gene.end, gene.strand, gene.strain_id, gene.contig_id)
                multigene = multigenes.get(multigene_id)
                if multigene == None:
                    multigene = Multigene(start, end, strand, strain, gene_id, gene_name, contig_id)  
                    multigene.cluster_id = cluster_id            
                    multigenes[multigene_id] = multigene
                    annotations.addMultigene(multigene)   
                multigene.addGene(gene)
            
            elif check == True:
                annotations.addPseudoGene(gene)
    return annotations;

def readMgUniAnnotationsFromFile(input_fn, strain, check):
    
    accept_alt_starts = True
    annotations = Annotation()
    
    input_fh = open(input_fn)
    lines = input_fh.readlines()
    input_fh.close()
    
    multigenes = {}

    for line in lines:
        line = line.strip()
        if line.startswith("#"):
            continue
        tokens = line.split("\t")
        
        if len(tokens) <= 2:
            continue
        cluster_id = tokens[0]
        mg_tokens = tokens[1].split()
        end = int(mg_tokens[0])
        strand = mg_tokens[1]
        contig_id = mg_tokens[2]
        mg_lens = tokens[3].split()
        
        for i in range(0, len(mg_lens), 1):
            
            selected = False
            
            length_str = mg_lens[i].split(":")[0]
            
            if length_str.startswith("#"):
                length_str = length_str[1:]
                selected = True
            
            if length_str.startswith("*"):
                length = int(length_str[1:])
                gene_name = tokens[2]
                gene_id = gene_name
            else:
                length = int(length_str)
                gene_name = "x"
                gene_id = "x"
            if strand == "+":
                start = end - length + 1
            else:
                start = end + length - 1
            
            gene = Gene(start, end, strand, strain, gene_id=gene_id, gene_name=gene_name, contig_id=contig_id, selected=selected)
            if check == False or correctGene(gene, accept_alt_starts) == 0:
                multigene_id = createStrainMultigeneId(gene.end, gene.strand, gene.strain_id, gene.contig_id)
                if multigene_id in multigenes:
                    multigene = multigenes[multigene_id]
                    multigene.addGene(gene)
                else:
                    multigene = Multigene(start, end, strand, strain, gene_id, gene_name, contig_id)
                    multigene.addGene(gene)
                    multigene.cluster_id = cluster_id
                    multigenes[multigene_id] = multigene                
                    annotations.addMultigene(multigene)
                
            elif check == True:
                annotations.addPseudoGene(gene)

    return annotations;

def readSequenceFromFile(genome_fn):
    seq_list = []
    seq_map = {}
    genome_fh = open(genome_fn)
    genome_lines = genome_fh.readlines();
    genome_fh.close()
    
    accession = ""
    for line in genome_lines[0:]:
        parts = line.split();
        if len(parts) >= 1:
            if parts[0].startswith(">"):
                if accession != "":
                    seq_map[accession] = ''.join(seq_list)
                    seq_list = []
                acc_tokens = parts[0].split("|")
                if len(acc_tokens) >= 4:
                    accession = acc_tokens[3]
                else:
                    accession = parts[0][1:]
                acc_tok = accession.split(".")
                accession = acc_tok[0]
            elif len(parts[0]) >= 1:
                seq_list.append(parts[0])
    if len(seq_list) > 0:
        seq_map[accession] = ''.join(seq_list)
    return seq_map;

def ensure_dir(path):
    try:
        if not os.path.exists(path):
            os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise
        
def writeFASTAseq(output_fh, seq, annotation, line_length=100):
    output_text = ">" + annotation + "\n"
    if line_length == None:
        output_text += seq + "\n"
    else:
        curr = 0;
        while(curr < len(seq)):
            output_text += seq[curr:min(curr + line_length, len(seq))] + "\n"
            curr += line_length;  
    output_text += "\n"
    output_fh.write(output_text);
    return None

def parseParsedBlastHit(gene, tokens, strain):
    if len(tokens) <= 1:
        return None
    gene_start = int(tokens[8])
    gene_end = int(tokens[9])
    query_id = int(tokens[0])

    if gene_start < gene_end:
        gene_strand = "+"
        query_start = int(tokens[6])
        query_end = int(tokens[7])
    else:
        gene_strand = "-"
        query_start = int(tokens[6])
        query_end = int(tokens[7])

    contig_id = tokens[1]
    evalue = tokens[10]
    aln_len = int(tokens[3])
    identity_b = float(tokens[2])
    identities = int(round(identity_b*aln_len/100))
    mismatches = int(tokens[4])
    residues = identities+mismatches
    hit_gene = Gene(gene_start, gene_end, gene_strand, strain, contig_id=contig_id)
    blast_hit = BlastHit(gene, contig_id, hit_gene, evalue_b=evalue, identities=identities, identity_b=identity_b, aln_len=aln_len, residues=residues, query_start=query_start, query_end=query_end, mismatches=mismatches, query_id=query_id)
    return blast_hit


def parseParsedBlastResultsFile(gene, input_fh, strain):
    lines = input_fh.readlines()
    input_fh.close()
    blast_hits = []
    
    for line in lines:
        tokens = line.split()
        if len(tokens) <= 1:
            continue
        gene_start = int(tokens[8])
        gene_end = int(tokens[9])
        query_id = tokens[0]

        if gene_start < gene_end:
            gene_strand = "+"
            query_start = int(tokens[6])
            query_end = int(tokens[7])
        else:
            gene_strand = "-"
            query_start = int(tokens[6])
            query_end = int(tokens[7])

        contig_id = tokens[1]
        evalue = tokens[10]
        aln_len = int(tokens[3])
        identity_b = float(tokens[2])
        identities = int(round(identity_b*aln_len/100))
        mismatches = int(tokens[4])
        residues = identities+mismatches
        hit_gene = Gene(gene_start, gene_end, gene_strand, strain, contig_id=contig_id)
        blast_hit = BlastHit(gene, contig_id, hit_gene, evalue_b=evalue, identities=identities, identity_b=identity_b, aln_len=aln_len, residues=residues, query_start=query_start, query_end=query_end, mismatches=mismatches, query_id=query_id)
        #blast_hit = BlastHit(gene, contig_id, None, evalue_b=evalue, identities=identities, identity_b=identity_b, aln_len=aln_len, residues=residues, query_start=query_start, query_end=query_end, mismatches=mismatches, query_id=query_id)
       
        blast_hits.append(blast_hit)
    return blast_hits



