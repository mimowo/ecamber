def overlapLength(mg1, mg2):
    
    mg1_left_bound, mg1_right_bound = mg1.getBounds()
    mg2_left_bound, mg2_right_bound = mg2.getBounds()
    
    if mg1_right_bound < mg2_left_bound:
        return 0
    elif mg1_left_bound > mg2_right_bound:
        return 0
    else:
        return min(mg1_right_bound, mg2_right_bound) - max(mg1_left_bound, mg2_left_bound) + 1 
    
    return 0

def findOverlappingMgsContig(mgs_sorted):
    overlapping_pairs = []
    
    js = []
    
    n = len(mgs_sorted)
    for i in range(n):
        mi = mgs_sorted[i]
        mi_left_bound = mi.getLeftBound()
        js_tmp = []
        for j in js:
            mj = mgs_sorted[j]
            mj_right_bound = mj.getRightBound()
            if mj_right_bound >= mi_left_bound:
                js_tmp.append(j)
                overlapping_pairs.append((mj, mi))
        js_tmp.append(i)
        js = js_tmp
    
    return overlapping_pairs

def findOverlappingMgPairs(multigenes):
    overlapping_pairs = []
    
    mgs_sorted = sorted(multigenes, key=lambda multigene: (multigene.contig_id, multigene.getBounds()))
    
    curr_contig = ""
    curr_mgs = []
    
    for multigene in mgs_sorted:
        if curr_contig == "":
            curr_contig = multigene.contig_id
            curr_mgs.append(multigene)
        elif curr_contig == multigene.contig_id:
            curr_mgs.append(multigene)
        else:
            overlapping_pairs += findOverlappingMgsContig(curr_mgs)
            curr_mgs = [multigene]
            curr_contig = multigene.contig_id
    if len(curr_mgs) > 1:
        overlapping_pairs += findOverlappingMgsContig(curr_mgs)
    
    return overlapping_pairs

def findOverlappingMgs(multigenes):
    over_mgs = {}
    
    over_pairs = findOverlappingMgPairs(multigenes)
    
    for mg1, mg2 in over_pairs:
        mg1_id = mg1.mg_unique_id
        mg2_id = mg2.mg_unique_id
        if not mg1_id in over_mgs:
            over_mgs[mg1_id] = []
        if not mg2_id in over_mgs:
            over_mgs[mg2_id] = []
        over_mgs[mg1_id].append(mg2_id)
        over_mgs[mg2_id].append(mg1_id)
  #  print("XXXX", len(over_mgs), len(over_pairs), len(multigenes))
        
    return over_mgs

def classifyOverlaps(overlaps):
    
    short_codir = 0
    short_cover = 0
    short_diver = 0
    long_codir = 0
    long_cover = 0
    long_diver = 0
    
    for (mg1, mg2) in overlaps:
        overlap_length = overlapLength(mg1, mg2)
        if mg1.strand == mg2.strand:
            overlap_type = "co-directional"
        elif (mg1.strand == "+" and mg1.start <= mg2.end) or (mg1.strand == "-" and mg1.start >= mg2.end):
            overlap_type = "convergent"
        else:
            overlap_type = "divergent"
        
        if overlap_length < 60 and overlap_type == "co-directional":
            short_codir += 1
        elif overlap_length < 60 and overlap_type == "convergent":
            short_cover += 1 
        elif overlap_length < 60 and overlap_type == "divergent":
            short_diver += 1
        elif overlap_length >= 60 and overlap_type == "co-directional":
            long_codir += 1
        elif overlap_length >= 60 and overlap_type == "convergent":
            long_cover += 1
        elif overlap_length >= 60 and overlap_type == "divergent":
            long_diver += 1 
        
    return short_codir, short_cover, short_diver, long_codir, long_cover, long_diver
