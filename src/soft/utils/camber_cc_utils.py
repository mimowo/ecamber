import sys
sys.path.append("../../")

from soft.utils.camber_progress import ShowProgress

def connected_components(nodes, verb=False):
    progress = ShowProgress("Connected components")
    n = len(nodes)
    progress.setJobsCount(n)

    visited = set([])
    list_of_curr = []
    ccs = {}
    cluster_id = 0
    
    for node_id in nodes:
        
        if not node_id in visited:
            cluster_id += 1
            ccs[cluster_id] = set([])
            list_of_curr.append(node_id)
            visited.add(node_id)
            
        while len(list_of_curr) > 0:
            curr_node = list_of_curr.pop(0)
            ccs[cluster_id].add(curr_node)
            neighbours = nodes[curr_node]
            for neigh in neighbours:
                if not neigh in visited:
                    visited.add(neigh)
                    list_of_curr.append(neigh)
        progress.update(str(len(visited)) +"/"+str(n))
                 
    return ccs

def saveClusters(output_fh, clusters, inline = False, cluster_ids = False):
    if inline:
        for cluster_id in clusters:
            cc_mgs = clusters[cluster_id]
            text_line = ""
            
            if cluster_ids:
                text_line = str(cluster_id) + "\t"
            
            for mg_node_id in cc_mgs:
                text_line += str(mg_node_id) + ":"
            if len(cc_mgs) > 0: 
                text_line = text_line[:-1]
            output_fh.write(text_line+"\n")        
    else:
        for cluster_id in clusters:
            cc_mgs = clusters[cluster_id]
            for mg_node_id in cc_mgs:
                output_fh.write(str(cluster_id)+"\t"+str(mg_node_id)+"\n")
    return None
