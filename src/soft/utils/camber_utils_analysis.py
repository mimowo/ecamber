def computeLocation(gene1, gene2):
    if gene1.left_bound < gene2.left_bound:
        overlap = max(gene1.right_bound - gene2.left_bound + 1, 0)
        dist = max(gene2.left_bound - gene1.right_bound - 1, 0)
    else:
        overlap = max(gene2.right_bound - gene1.left_bound + 1, 0)
        dist = max(gene1.left_bound - gene2.right_bound - 1, 0)
        
    return overlap, dist 
