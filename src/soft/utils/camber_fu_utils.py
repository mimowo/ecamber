class UnionFind:
    def __init__(self):
        self.num_weights = {}
        self.parent_pointers = {}
        self.num_to_objects = {}
        self.objects_to_num = {}
        self.__repr__ = self.__str__
    def insert_objects(self, objects):
        for obj_t in objects:
            self.find(obj_t);
    def find(self, obj_t):
        if not obj_t in self.objects_to_num:
            obj_num = len(self.objects_to_num)
            self.num_weights[obj_num] = 1
            self.objects_to_num[obj_t] = obj_num
            self.num_to_objects[obj_num] = obj_t
            self.parent_pointers[obj_num] = obj_num
            return obj_t
        stk = [self.objects_to_num[obj_t]]
        par = self.parent_pointers[stk[-1]]
        while par != stk[-1]:
            stk.append(par)
            par = self.parent_pointers[par]
        for i in stk:
            self.parent_pointers[i] = par
        return self.num_to_objects[par]
    def union(self, object1, object2):
        o1p = self.find(object1)
        o2p = self.find(object2)
        if o1p != o2p:
            on1 = self.objects_to_num[o1p]
            on2 = self.objects_to_num[o2p]
            w1 = self.num_weights[on1]
            w2 = self.num_weights[on2]
            if w1 < w2:
                o1p, o2p, on1, on2, w1, w2 = o2p, o1p, on2, on1, w2, w1
            self.num_weights[on1] = w1+w2
            del self.num_weights[on2]
            self.parent_pointers[on2] = on1
    def sets(self):
        sets = {}
        set_map = {}
        set_curr_id = 0
        for i in self.objects_to_num:
            obj_id = self.objects_to_num[self.find(i)]
            if not obj_id in set_map:
                set_map[obj_id] = set_curr_id
                sets[set_curr_id] = []
                set_curr_id += 1
            set_id = set_map[obj_id]
            sets[set_id].append(i)
        return sets
    
    def setsOLD(self):
        sets = {}
        for i in self.objects_to_num:
            set_id = self.objects_to_num[self.find(i)]
            if not set_id in  sets:
                sets[set_id] = []
            sets[set_id].append(i)
        return sets
    def __str__(self):
        sets = {}
        for i in xrange(len(self.objects_to_num)):
            sets[i] = []
        for i in self.objects_to_num:
            sets[self.objects_to_num[self.find(i)]].append(i)
        out = []
        for i in sets.itervalues():
            if i:
                out.append(repr(i))
        return ', '.join(out)

if __name__ == '__main__':
    print("Testing...")
    uf = UnionFind()
    az = "abcdefghijklmnopqrstuvwxyz"
    az += az.upper()
    uf.insert_objects(az)
    import random
    cnt = 0
    while len(uf.num_weights) > 20:
        cnt += 1
        uf.union(random.choice(az), random.choice(az))
    print((uf, cnt))
    print("Testing complete.")
    
    