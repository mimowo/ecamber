import sys

from soft.utils.camber_seq_utils import findProteinSequenceLength,\
    complementarySequence

def convertGenBank(strain, input_fh, ann_fh, acc_map = {}, gt="cds"):
    lines = input_fh.readlines()
    gene_name = ""
    gene_id = ""
    accession = ""
    left_bound = ""
    right_bound = ""
    strand = "+"
    pseudo = False
    cds_defined = False
    out_lines = []
    miss_id_count = 0
    done = set([])

    for i in range(0, len(lines), 1):
        line = lines[i].strip()
        tokens = line.split()

        try:            
            if len(tokens) >= 2 and tokens[0] == "ACCESSION":
                accession = tokens[1]
            elif len(tokens) >= 2 and tokens[0] == "DEFINITION" and tokens[1].count("seqhdr") > 0:
                for token in tokens[1].split(";"):
                    token_tmp = token.split("=")
                    if len(token_tmp) != 2: continue
                    if token_tmp[0] == "seqhdr": 
                        accession = token_tmp[1].strip('"')
            if len(tokens) == 0:
                continue

            if gt == "cds": acc_anns = ["CDS"]
            elif gt == "rna": acc_anns = ["tRNA", "rRNA"]
            elif gt == "rRNA": acc_anns = ["rRNA"]
            elif gt == "tRNA": acc_anns = ["tRNA"]
            else: acc_anns = ["CDS"]
            
            if (tokens[0] in ["gene" , "CDS"] and len(tokens)==2 and tokens[1].count("..")>0) or (i >= len(lines)-2):
                if cds_defined:
                    if len(gene_id) <= 1:
                        miss_id_count += 1
                        gene_id = "x" + str(miss_id_count)
                    if len(gene_name) <= 1 or gene_name == gene_id:
                        gene_name = ""
    
                    #if gene_name.count("RNA")==0 and gene_name.count("PP")==0 and gene_name.count("ncR")==0 and gene_name.count("IS")==0 and gene_name.count("fnm")==0 and gene_name.count("ssrA")==0:
                    text_line = ""
                    if pseudo: text_line += "#"
                    
                    if strand == "+":
                        start, end = left_bound, right_bound
                    else:
                        start, end = right_bound, left_bound
                        
                    if strain.sequence != None:
                        text_line += gene_id+"\t"+str(start)+"\t"+str(end)+"\t"+strand+"\t"+gene_name
                    else:
                        accession_conv = acc_map.get(accession, accession)
                        text_line += gene_id+"\t"+str(start)+"\t"+str(end)+"\t"+strand+"\t"+gene_name+"\t"+accession_conv
                        
                    if not (left_bound, right_bound, strand) in done:
                        done.add((left_bound, right_bound, strand))
                        out_lines.append(text_line)
                    cds_defined = False
                    gene_name = ""
                    gene_id = ""
                    pseudo = False
                    
            if tokens[0] in acc_anns and len(tokens)==2:
                cds_defined = True
                if tokens[1].count(">") > 0 or tokens[1].count("<") > 0:
                    pseudo = True
                    
                if tokens[1].count("join") > 0 or tokens[1].count("order") > 0:
                    pseudo = True
                    left_bound = tokens[1]
                    right_bound = tokens[1]
                elif tokens[1].count("complement") > 0:
                    strand = "-"
                    left_bound, right_bound = tokens[1].strip("complement(").strip(")").split("..")
                else:
                    strand = "+"
                    left_bound, right_bound = tokens[1].split("..")

            if gt == "cds" and tokens[0] == "misc_feature" and cds_defined == False and len(tokens) == 2:
                cds_defined = True
                pseudo = True
                if tokens[1].count("join") > 0 or tokens[1].count("order") > 0:
                    pseudo = True
                    left_bound = tokens[1]
                    right_bound = tokens[1]
                elif tokens[1].count("complement") > 0:
                    strand = "-"
                    left_bound, right_bound = tokens[1].strip("complement(").split("..")
                else:
                    strand = "+"
                    left_bound, right_bound = tokens[1].split("..")                
            
            if left_bound.startswith(">") or left_bound.startswith("<"): pseudo = True
            if right_bound.startswith(">") or right_bound.startswith("<"): pseudo = True

            if cds_defined:
                if len(gene_name) <= 1 and tokens[0].startswith("/gene"):
                    gene_name = tokens[0].strip("/gene=").strip('"').strip(";").strip(".")
                if len(gene_id) <= 1 and tokens[0].startswith("/locus_tag"):
                    gene_id = tokens[0].strip("/locus_tag=").strip('"').strip(";").strip(".")
                if tokens[0].startswith('/note="') and len(gene_id) < 1 and len(gene_name) < 1:
                    if tokens[0].count('"') < 2: continue
                    
                    gene_id = tokens[0].strip("/note=").strip('"').split(";")[0].strip(".").strip("ID=")
                if tokens[0].startswith("/pseudo"):
                    pseudo = True


        except Exception:
            exc = sys.exc_info()[1]
            print("gbk: " + line + ": " + str(exc))
            
    for line in out_lines:
        ann_fh.write(line+"\n")
    return None

def convertFeatureTab(strain, input_fh, ann_fh, acc_map={}, gt="cds"):
    lines = input_fh.readlines()
    out_lines = []
    out_lines_acc = []
    done = set([])

    if gt == "cds": acc_anns = ["CDS"]
    elif gt == "rna": acc_anns = ["tRNA", "rRNA"]
    elif gt == "rRNA": acc_anns = ["rRNA"]
    elif gt == "tRNA": acc_anns = ["tRNA"]
    else: acc_anns = ["CDS"]

    for line in lines[1:]:
        pseudo = False
        tokens = line.split("\t")
        if len(tokens) < 9: continue
        seq_type = tokens[3]
        if not seq_type in acc_anns: continue
        
        accession = tokens[1]
        strand = tokens[8];
        if strand == "+":
            start, end = tokens[6], tokens[7]
        else:
            start, end = tokens[7], tokens[6]
        if start.startswith("<") or start.startswith(">"): pseudo = True
        if end.startswith("<") or end.startswith(">"): pseudo = True
        
        if pseudo: ifpseudohash = "#"
        else: ifpseudohash = "" 
        
        gene_name = tokens[10];
        gene_id = tokens[5];

        accession_conv = acc_map.get(accession, accession)
        
        if not (start, end, strand) in done:
            done.add((start, end, strand))
            if strain.sequence == None:
                text_line = ifpseudohash + gene_id+"\t"+str(start)+"\t"+str(end)+"\t"+strand+"\t"+gene_name+"\t"+accession_conv
            else:
                text_line = ifpseudohash + gene_id+"\t"+str(start)+"\t"+str(end)+"\t"+strand+"\t"+gene_name
            out_lines.append(text_line)
        
    for line in out_lines:
        ann_fh.write(line+"\n")

    return None

def convertMageFeatureTab(strain, input_fh, ann_fh):
    lines = input_fh.readlines()
    out_lines = []
    out_lines_acc = []
    accession_set = set([])
    count = 0
    for line in lines[1:]:
        if count % 100 == 0:
            print(strain.strain_id, count)
            sys.stdout.flush()
        count += 1
     #   print(line)
        tokens = line.split("\t")
        if len(tokens)>=9:
            contig_id = tokens[0]
            gene_id = tokens[1];
            comment = tokens[6]
            
            gene_name = tokens[7]
            
            left_bound = int(tokens[2])
            right_bound = int(tokens[3])
            
            if strain.sequence == None:
                strain_seq = strain.sequences_map[contig_id]
            else:
                strain_seq = strain.sequence
                
            n = len(strain_seq)
            strain_seq_rev = complementarySequence(strain_seq)
            
            gene_seq = strain_seq[left_bound-1:right_bound]
            gene_seq_rev = strain_seq_rev[n-right_bound:n-left_bound+1]

        #    print("FOR", gene_seq)
          #  print("REV", gene_seq_rev)
            
            length = findProteinSequenceLength(gene_seq, 0, alt_starts = True)
            if length == right_bound - left_bound + 1:
                strand = "+"
                start = left_bound;
                end = right_bound;
            else:
                length = findProteinSequenceLength(gene_seq_rev, 0, alt_starts = True)
                if length == right_bound - left_bound + 1:
                    strand = "-"
                    start = right_bound
                    end = left_bound                    
                else:
                    continue
                
            text_line = ""
            if comment.startswith("pseudo"):
                text_line += "#"
            if strain.sequence != None:
                text_line += gene_id+"\t"+str(start)+"\t"+str(end)+"\t"+strand+"\t"+gene_name
            else:
                text_line += gene_id+"\t"+str(start)+"\t"+str(end)+"\t"+strand+"\t"+gene_name+"\t"+contig_id
            out_lines.append(text_line)
    for text_line in out_lines:
        ann_fh.write(text_line+"\n")

    return None
