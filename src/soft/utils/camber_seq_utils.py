import sys
try:
	from string import maketrans
except ImportError:
	pass

def translateCodon(codon):
	gencode = {
    'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
    'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
    'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
    'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
    'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
    'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
    'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
    'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
    'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
    'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
    'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
    'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
    'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
    'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
    'TAC':'Y', 'TAT':'Y', 'TAA':'*', 'TAG':'*',
    'TGC':'C', 'TGT':'C', 'TGA':'*', 'TGG':'W'}
	return gencode.get(codon, "X")

def translateSequence(dna_seq):
	prot_seq = "";
	for i in range(len(dna_seq)//3):
		amino = translateCodon(dna_seq[3*i:3*i+3])
		prot_seq += amino;
		if amino == "*":
			return prot_seq;
	return prot_seq;

def reverseString(s):
	return s[::-1]

def complementarySequence(seq):
	if sys.version.startswith("2"): 
		return seq[::-1].translate(maketrans("TAGC", "ATCG"))
	else:
		return seq[::-1].translate(bytes.maketrans(b"TAGC", b"ATCG"))

#def complementarySequence(seq):
#	return replaceLetters(reverseString(seq));

def findProteinSequenceLength(seq, start=0, alt_starts=False, alt_start = None, max_acceptable_len = 0):
	if alt_starts == False:
		if alt_start == None:
			if not seq[start:start+3] in ["ATG", "GTG", "TTG"]:
				return 0
		else:
			if not seq[start:start+3] in ["ATG", "GTG", "TTG", alt_start]:
				return 0

	n = len(seq)
	i = start + 3
	if max_acceptable_len > 0:
		while i + 3 <= n:
			if i+3-start > max_acceptable_len:
				return 0
			if seq[i] == "T" and (seq[i+1:i+3] in ["GA", "AG", "AA"]):
				return i+3-start;
			i += 3
		return 0
	else:
		while i + 3 <= n:
			if seq[i] == "T" and (seq[i+1:i+3] in ["GA", "AG", "AA"]):
				return i+3-start;
			i += 3
		return 0

def findProteinSequenceLengthOld(seq, start=0, alt_starts=False, max_acceptable_len = 0):

	n = len(seq)
	if not alt_starts:
		if not seq[start:start+3] in ["ATG", "GTG", "TTG"]:
			return 0
	i = start + 3
	if max_acceptable_len > 0:
		while i + 3 <= n:
			i += 3
			if i-start > max_acceptable_len:
				return 0
			if seq[i-3:i] in ["TAG", "TAA", "TGA"]:
				return i-start;
		return 0	
	else:
		while i + 3 <= n:
			i += 3
			if seq[i-3:i] in ["TAG", "TAA", "TGA"]:
				return i-start;
		return 0
	