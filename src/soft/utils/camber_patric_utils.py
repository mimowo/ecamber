import socket
import os
import sys
try:
    from urllib.request import urlopen
except ImportError:
    from urllib2 import urlopen

if sys.version.startswith("3"): raw_input = input

def intparse(string):
    try: return int(string)
    except Exception: return None
        
def selectStrains(all_strains_list):
    clusters, cluster_strains = clusterStrains(all_strains_list)
    classes_sorted = sorted(clusters, key=lambda class_id:(-len(clusters[class_id]), class_id))
    
    page = 0
    while page >= 0:
        for i in range(page*50, min((page+1)*50, len(classes_sorted)), 1):
            class_id = classes_sorted[i]
            print(str(i) +": " + class_id + "\t" + str(len(clusters[class_id])))
        default_skey = "Mycobacterium_tuberculosis"
        sys.stdout.flush()
        skey = raw_input("Press: (q) - quit, (p) - show previous 50 species, (n) - show next 50 species, (Int) - selected species id, (string) representing substring of the selected species (example: "+default_skey+"):").strip()
        
        if skey.lower() == "n":
            page += 1
        elif skey.lower() == "p":
            page = max(0, page-1)
        elif skey.lower() == "q":
            sys.exit()
        elif skey.isdigit():
            index = int(skey)
            if index < len(clusters) and index >= 0:
                class_id = classes_sorted[index]
                strain_det_list = clusters[class_id]
                page = -1
            else:
                print("Wrong index.")
                continue
        elif len(skey) >= 3:
            strain_det_list = filteroutStrains(cluster_strains, skey)
            page = -1
        else:
            continue
        
    if len(strain_det_list) == 0:
        print("No strain names containing " + skey + " as their substring.")
        sys.exit()
        
    selected_subset = set(strain_det_list).copy()
    ans = ""
    n = len(strain_det_list)
    while not ans.lower() in ["q", "d"]:
        for i in range(n):
            full_strain_id = strain_det_list[i]
            if full_strain_id in selected_subset:
                print("[X] "+str(i+1) +": "+ full_strain_id + "\t" + retriveStrainID(full_strain_id))
            else:
                print("[ ] "+str(i+1) +": "+ full_strain_id + "\t" + retriveStrainID(full_strain_id))
        
        print("Press (d)ownload selected / (a)ll /(n)one / (Int) index to toggle selection / (q)uit")
        sys.stdout.flush()
        ans = raw_input("Choose action: ").lower().strip()
        if ans == "d":
            continue
        elif ans == "q":
            exit()
        elif ans == "a":
            selected_subset = set(strain_det_list).copy()
        elif ans == "n":
            selected_subset = set([])
        elif ans.isdigit():
            index = int(ans)-1
            if index < 0 or index >= n:
                continue 
            strain_full_id = strain_det_list[index]
            if strain_full_id in selected_subset:
               # print("JEST: " + str(index) + strain_full_id)
                selected_subset.remove(strain_full_id)
            else:
               # print("NIE MA: " + str(index) + strain_full_id)
                selected_subset.add(strain_full_id)
             
        
    print("Genomes to download: " + str(len(selected_subset)))
    return selected_subset

def choosePatricVersion(patric_db):
    patric_versions = list(getPatricGenomeVersions(patric_db))
    if len(patric_versions) == 0:
        print("No PATRIC database versions found. Exit.")
        sys.exit()
    for i in range(len(patric_versions)):
        link = patric_versions[i][0]
        date = patric_versions[i][1]
        if len(link) <= len("genomes") + 1:
            date += " [current release]"
        print(str(i) +": " + str(link) + "\t date: " + str(date))
    
    index_str = raw_input("Select version's index: (default: 0 -current release):").strip()
    if len(index_str) == 0:
        index_sel = 0
    elif not index_str.isdigit():
        print("The typed string is not a correct index.")
        exit()
    else:
        index_sel = int(index_str)
        if index_sel >= len(patric_versions) or index_sel < 0:
            print("The typed string is not a correct index.")
            exit()
                        
    return patric_versions[index_sel][0]

def clusterStrains(all_strains_list):
    clusters = {}
    cluster_strains = []
    for strain_full_id in  all_strains_list:

        cluster_id = getGenomeClusterID(strain_full_id)
        
        if cluster_id.lower() == strain_full_id.lower():
            continue
        if not cluster_id in clusters:
            clusters[cluster_id] = [] 
        clusters[cluster_id].append(strain_full_id)
        cluster_strains.append(strain_full_id)
    return clusters, cluster_strains

def downloadFile(url, output_fn, timeout=300, trials=3):
    
    socket.setdefaulttimeout(timeout)
    t = 0
    if os.path.exists(output_fn):
        os.remove(output_fn)         
    
    current_log = ""
    while t < trials:
        try:
            u = urlopen(url)
            size_header = 'Content-Length'
            try:
                total_size = int(u.info().getheader(size_header).strip())
            except Exception:
                total_size = 0
                
            output_fh = open(output_fn, 'w')
            output_fh.write(u.read())
            output_fh.close()
            
            real_size = int(os.path.getsize(output_fn))
            #print(output_fn, real_size, total_size)
            #sys.stdout.flush()            
            
            if total_size == 0 or (real_size <= total_size*1.1 and real_size>= 0.9*total_size):
                t = trials 
                current_log = ""
            else:
                t += 1
                current_log = str(t) + ", wrong size" + str(real_size) + " " +str(total_size) + " " + str(output_fn) 
                sys.stdout.flush()
                if os.path.exists(output_fn):
                    os.remove(output_fn)
              #  print(current_log)
        except Exception:
            t += 1
            exc = sys.exc_info()[1]
            if os.path.exists(output_fn):
                os.remove(output_fn)              
            current_log = str(t) +" " + str(exc) + " " +str(t) + " " + url
           # print("Failed download: " + current_log)
    return current_log

def downloadStrainFiles(params):
    strain_id = params[0]
    full_strain_id = params[1]
    genomes_dir = params[2]
    cds_refseq_dir = params[3]
    cds_patric_dir = params[4]
    gb_refseq_dir = params[5]
    gb_patric_dir = params[6]
    patric_db = params[7]    
    
    log_lines = []

    out_seq_fn = genomes_dir + "/"+strain_id+".fasta"    
    if not os.path.exists(out_seq_fn) or os.path.getsize(out_seq_fn)<10:
        http_fn = patric_db + "/" + full_strain_id + "/" + full_strain_id+ ".fna"
        log = downloadFile(http_fn, out_seq_fn)
        if len(log) > 0:
            log_lines.append("SEQUENCE GENOME: " + log)
            if os.path.exists(out_seq_fn):
                os.remove(out_seq_fn)
            
    if len(cds_refseq_dir) > 0:
        out_fea_fn = cds_refseq_dir+"/"+strain_id+".tab"
        if not os.path.exists(out_fea_fn) or os.path.getsize(out_fea_fn)<10:
            http_fn = patric_db + "/" + full_strain_id + "/" + full_strain_id+ ".RefSeq.features.tab"
            log = downloadFile(http_fn, out_fea_fn)
            if len(log) > 0:
                log_lines.append("REFSEQ: " + log)
                if os.path.exists(out_fea_fn):
                    os.remove(out_fea_fn)
    
    if len(cds_patric_dir) > 0:
        out_fea_fn = cds_patric_dir+"/"+strain_id+".tab"            
        if not os.path.exists(out_fea_fn) or os.path.getsize(out_fea_fn)<10:
            #http_fn = patric_db + "/" + full_strain_id + "/" + full_strain_id+ ".PATRIC.cds.tab"
            http_fn = patric_db + "/" + full_strain_id + "/" + full_strain_id+ ".PATRIC.features.tab"
            log = downloadFile(http_fn, out_fea_fn)
            if len(log) > 0:
                log_lines.append("PATRIC: " + log)
                if os.path.exists(out_fea_fn):
                    os.remove(out_fea_fn)

    if len(gb_refseq_dir) > 0:
        out_fea_fn = gb_refseq_dir+"/"+strain_id+".txt"
        if not os.path.exists(out_fea_fn) or os.path.getsize(out_fea_fn)<10:
            http_fn = patric_db + "/" + full_strain_id + "/" + full_strain_id+ ".RefSeq.gbf"
            log = downloadFile(http_fn, out_fea_fn)
            if len(log) > 0:
                log_lines.append("REFSEQ: " + log)
                if os.path.exists(out_fea_fn):
                    os.remove(out_fea_fn)
    
    if len(gb_patric_dir) > 0:
        out_fea_fn = gb_patric_dir+"/"+strain_id+".txt"            
        if not os.path.exists(out_fea_fn) or os.path.getsize(out_fea_fn)<10:
            http_fn = patric_db + "/" + full_strain_id + "/" + full_strain_id+ ".PATRIC.gbf"
            log = downloadFile(http_fn, out_fea_fn)
            if len(log) > 0:
                log_lines.append("PATRIC: " + log)
                if os.path.exists(out_fea_fn):
                    os.remove(out_fea_fn)

            
    return strain_id, log_lines


def retriveStrainID(strain_full_id):
    
    delwords = ["_substr._", "_substr_", "_str_", "_str._", "_subsp._", "_subsp_"]
    strain_id_tmp = strain_full_id
    
    index = strain_id_tmp.find("_")
    if index > 0:
        strain_id_tmp = strain_id_tmp[index+1:]
    index = strain_id_tmp.find("_")
    if index > 0:
        strain_id_tmp = strain_id_tmp[index:]
    for delword in delwords:
        strain_id_tmp = strain_id_tmp.replace(delword, "_")
    strain_id = strain_id_tmp.strip("_")
    
    return strain_id


def getGenomeClusterID(strain_full_id):
    tokens = strain_full_id.split("_")
    
    if len(tokens) == 1:
        return strain_full_id

    return tokens[0] + "_" + tokens[1]

def getPatricGenomeVersions(patric_db, timeout=60):
    try:
        socket.setdefaulttimeout(timeout)
        online_fh = urlopen(patric_db, timeout=timeout)
        lines = online_fh.readlines()
        online_fh.close()
    except Exception:
        print("Cannot connect to the database (timeout="+str(timeout)+"s).")
        return []
    
    print("Server output:")
    ret = []
    for line in lines:
        line = str(line).strip()
        print(line)
        if line.count('genomes') == 0:
            continue
        tokens = line.split()
        version_id = tokens[8]
        date = ''.join(tokens[5:8])
        ret.append((version_id, date))
    return ret

def selectAllStrains(patric_db, timeout = 120):
    try:
        print("Connecting to the database (timeout = " + str(timeout) +"s)...")
        sys.stdout.flush()
        socket.setdefaulttimeout(timeout)
        online_fh = urlopen("ftp://"+patric_db, timeout=timeout)
        lines = online_fh.readlines()
        online_fh.close()
    except Exception:
        exc = sys.exc_info()[1]
        print("Error: " + str(exc))
        print("Error: " + str(sys.exc_info()[0]))
        print("Error: " + str(sys.exc_info()))
        print("Time of "+str(timeout) + "s was exceeded. Cannot connect to the database.")
        return []
    
    ret = []
    for line in lines:
        line = str(line).strip()
        tokens = line.split()
        if len(tokens) < 8: continue
        strain_full_id = tokens[-1]
        if strain_full_id.count(">") > 0 or strain_full_id.count("<") > 0:
            continue
        if len(strain_full_id) <= 5:
            continue
        if strain_full_id.upper() == strain_full_id:
            continue
        if strain_full_id.lower() == strain_full_id:
            continue
        if strain_full_id[0].lower() == strain_full_id[0]:
            continue
        ret.append(strain_full_id)
    return ret

def strainIDs(all_strains):
    ret = []
    for strain_full_id in all_strains:        

        strain_id = retriveStrainID(strain_full_id)

        if len(strain_id) > 0:
            ret.append((strain_id, strain_full_id))
        
    return ret


def filteroutStrains(all_strains, keyword):
    ret = []
    for strain_full_id in all_strains:        
        if strain_full_id.count(keyword) == 0:
            continue

        ret.append(strain_full_id)
        
    return ret
