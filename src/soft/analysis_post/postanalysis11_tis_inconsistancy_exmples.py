import sys
import time
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_graph_utils import *
from soft.structs.clusters import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *


def getSelAnnGenes(mg):
    ann_genes = []
    if len(mg.sel_lengths) > 0:
        for sel_length in mg.sel_lengths:
            if mg.strand == "+":
                start = mg.end - sel_length + 1
            else:
                start = mg.end + sel_length - 1
            gene = Gene(start, mg.end, mg.strand, mg.strain, mg.ann_id, mg.gene_name, mg.contig_id)
            ann_genes.append(gene)        
    else:        
        for ann_length in mg.ann_lengths:
            if mg.strand == "+":
                start = mg.end - ann_length + 1
            else:
                start = mg.end + ann_length - 1
            gene = Gene(start, mg.end, mg.strand, mg.strain, mg.ann_id, mg.gene_name, mg.contig_id)
            ann_genes.append(gene)
        
    return ann_genes
    
if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    
    strains = readStrainsInfo()
    strains_list = strains.allStrains()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default = 3)
    
    ensure_dir(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_ANALYSIS_POST", ph2_iteration))
    
    clusters = MGClusters()
    
    TASKS = []
    for strain_id in strains_list:
        strain = strains.strain(strain_id)
        TASKS.append((strain, closure_iteration, ph2_iteration, False, False, True))
    
    progress = ShowProgress("postanalysis11_tis_inconsistancy_exmples.py")
    progress.setJobsCount(len(TASKS))
        
    pool = multiprocessing.Pool(processes = WORKERS)    
    for strain_id, cc_mapping in pool.imap(readMultigeneClusterStrain, TASKS):
        
        for cluster_id in cc_mapping:
            if not cluster_id in clusters.clusters:
                cluster = MGCluster(cluster_id)
                clusters.addCluster(cluster)
            else:
                cluster = clusters.getCluster(cluster_id)
            for multigene in cc_mapping[cluster_id]:
                cluster.addMultigene(multigene)
        
        progress.update(desc = strain_id)
    pool.close()
    
    clusters.classifyClusters(strains_list)
    k = len(strains_list)
    
    
    text_lines = []
    
    
    check_clusters = []
    
    for cluster in clusters.clusters.values():
        if len(cluster.strains()) < 1:
            continue
        max_tis_number = 0
        cluster_id = cluster.cluster_id
        for multigene in cluster.nodes.values():
            max_tis_number = max(max_tis_number, len(multigene.lengths))
        if max_tis_number > 1:
            check_clusters.append(cluster)
        
    progress = ShowProgress("Clusters checked")
    progress.setJobsCount(len(check_clusters))
       
    inc_cluster = set([])
    inc_mgs_all = 0
    inc_mgs_anchor = 0
     
    for cluster in check_clusters:
        seq_ann_tis_map = {}
        
        cluster_id = cluster.cluster_id
        mgs_list = list(cluster.nodes.values())
        n = len(mgs_list)
        
        if cluster_id == "6379":
            print(cluster_id, n)
        
        for i in range(0, n-1, 1):
            for j in range(i+1, n, 1):
                mg1 = mgs_list[i]
                mg2 = mgs_list[j]
                mg1_ann_genes = getSelAnnGenes(mg1)
                mg2_ann_genes = getSelAnnGenes(mg2)
                if len(mg1_ann_genes) == 0 or len(mg2_ann_genes) == 0:
                    continue
                
                for ann_gene1 in mg1_ann_genes:
                    for ann_gene2 in mg2_ann_genes:
                        if ann_gene1.length() < ann_gene2.length():
                            shorter_gene = ann_gene1
                            longer_gene = ann_gene2
                        elif ann_gene1.length() > ann_gene2.length():
                            shorter_gene = ann_gene2
                            longer_gene = ann_gene1
                        else:
                            continue
                        seq1 = longer_gene.sequence_with_promotor(100)
                        seq2 = shorter_gene.sequence_with_promotor(100 + longer_gene.length() - shorter_gene.length())
                        if seq1 == seq2:
                            inc_cluster.add(cluster_id)
                            inc_mgs_all += 1
                            if cluster.isAnchor():
                                inc_mgs_anchor += 1
                            text_line = mg1.mg_strain_unique_id + "\t" + mg2.mg_strain_unique_id + "\t"+cluster_id+"\t" + str(longer_gene.length()) + "\t" +  str(shorter_gene.length())
                            text_lines.append(text_line)
                            
        progress.update()
    
    print("checked clusters: ", len(check_clusters))
    print("# od inc. mg pairs: ", inc_mgs_all)
    print("# od inc. mg pairs in anchors: ", inc_mgs_anchor)
    print("# of inc. clusters: ", len(inc_cluster))
    
    output_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_ANALYSIS_POST", ph2_iteration) + "analysis_tis_inconsistancy_examples.txt"
    output_fh = open(output_fn, "w")
    for text_line in sorted(text_lines):
        output_fh.write(text_line + "\n")
    output_fh.close()
            
    print("postanalysis11_tis_inconsistancy_exmples.py: Finished.")

        
        