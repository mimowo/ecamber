import sys
import time
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.structs.clusters import MGClusters, MGCluster
from soft.utils.camber_graph_utils import *
from soft.utils.camber_io_utils import ensure_dir
from soft.structs.multigene import *
from soft.utils.camber_utils import *


if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    strains_list = sorted(list(strains.allStrains()), key=lambda strain_id:-strains.strain(strain_id).totalGenomeLength())
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default = 3)
    
    output_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_ANALYSIS_POST", ph2_iteration)
    output_fn = output_dir + "analysis_missing_genes_strains.txt"

    results_map = {}
    
    clusters = MGClusters()
    
    TASKS = []
    for strain_id in strains.allStrains():
        strain = strains.strain(strain_id)
        TASKS.append((strain, closure_iteration, ph2_iteration, False, False))
    
    progress = ShowProgress("postanalysis2_missing_genes_strains.py")
    progress.setJobsCount(len(TASKS))
    
    
    pool = multiprocessing.Pool(processes = WORKERS)    
    
    missing_genes_map = {}
    for strain_id in strains.allStrains():
        missing_genes_map[strain_id] = 0
    
    for strain_id, cc_mapping in pool.imap(readMultigeneClusterStrain, TASKS):
        
        for cluster_id in cc_mapping:
            if not cluster_id in clusters.clusters:
                cluster = MGCluster(cluster_id)
                clusters.addCluster(cluster)
            else:
                cluster = clusters.getCluster(cluster_id)
            for multigene in cc_mapping[cluster_id]:
                cluster.addMultigene(multigene)
        progress.update()
    pool.close()
    pool.join()
    
    clusters.classifyClusters(strains.allStrains())
    
    max_presence_ann_count = 0
    for cluster in clusters.anchors.values():
        presence_ann_count = 0
        for strain_id in strains.allStrains():
            multigenes = cluster.nodesByStrain(strain_id).values()
            annotated = False
            for multigene in multigenes:
                if len(multigene.ann_id) > 1:
                    annotated = True
            if annotated:
                presence_ann_count += 1
        max_presence_ann_count = max(max_presence_ann_count, presence_ann_count)        
    
    print("ANCHORS", len(clusters.anchors))
    
    for cluster in clusters.anchors.values():
        presence_ann_count = 0
        presence_count = 0 
        missing_strains = set([])
        missing_ann_strains = set([])
        for strain_id in strains.allStrains():
            multigenes = cluster.nodesByStrain(strain_id).values()
            if len(multigenes) > 0:
                presence_count += 1
            else:
                missing_strains.add(strain_id)
            annotated = False
            for multigene in multigenes:
                if len(multigene.ann_id) > 1:
                    annotated = True
            if annotated:
                presence_ann_count += 1
            else:
                missing_ann_strains.add(strain_id)
                
     #   print(max_presence_ann_count, presence_count, presence_ann_count, len(strains.allStrains()), len(missing_strains), len(missing_ann_strains))
        
        if abs(presence_count - presence_ann_count) > 0 and abs(max_presence_ann_count - presence_ann_count) <= 1 and presence_count >= len(strains.allStrains()):
        #    print(presence_count, presence_ann_count, max_presence_ann_count, presence_ann_count, presence_count, missing_ann_strains)
            for strain_miss_id in missing_ann_strains:
                #print(strain_miss_id)
                missing_genes_map[strain_miss_id] += 1
                
    output_fh = open(output_fn, "w")
    for strain_miss_id in missing_genes_map:
        text_line = strain_miss_id + "\t" + str(missing_genes_map[strain_miss_id])
        output_fh.write(text_line+ "\n")
    output_fh.close()
    
    print("postanalysis2_missing_genes_strains.py. Finished.")