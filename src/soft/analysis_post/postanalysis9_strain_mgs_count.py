import sys
import time
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_io_utils import ensure_dir
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_stat_utils import mv
from soft.utils.camber_analysis_utils import readGenomeLengths

    
def getMultigenesCount(params):
    overwriteParameters(sys.argv)
    readParameters()
    
    strain_id, closure_iteration, ph2_iteration = params
    
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    
    readUniAnnotationsIter(strain, closure_iteration, ph2_iteration)
    
    return strain_id, closure_iteration, len(strain.uni_annotation.multigenes)

def readORFsCounts(strains):
    input_fh = open(parameters["DATASET_RESULTS_EXP_ANALYSIS"]+"analysis_strain_orf_counts.txt")
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) < 2:
            continue
        strain_id = tokens[0]
        orf_count = int(tokens[2])
        strain = strains.strain(strain_id)
        strain.orf_count = orf_count
    input_fh.close()

    return None

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())

    progress = ShowProgress("postanalysis9_strain_mgs_count.py")

    readGenomeLengths(strains)
    readORFsCounts(strains)
    
    genome_lengths = []
    for strain_id in  strains.allStrains():
        genome_lengths.append(strains.strain(strain_id).genome_length)
    median_genome_length = median(genome_lengths)

    orf_counts = []
    for strain_id in  strains.allStrains():
        orf_counts.append(strains.strain(strain_id).orf_count)
    median_orf_count = median(orf_counts)

    strains_list = sorted(strains.allStrains(), key=lambda strain_id:strains.strain(strain_id).genome_length) 

    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default = 3)
    mg_counts = {}
    
    TASKS = []
    for strain_id in strains_list:
        TASKS.append((strain_id, closure_iteration, ph2_iteration))
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for strain_id, iteration, mg_count in pool.imap_unordered(getMultigenesCount, TASKS):
            mg_counts[(strain_id, iteration)] = mg_count
            progress.update(desc=strain_id + " " + str(iteration) + " " + str(mg_count))
        pool.close()
    else:
        for TASK in TASKS:
            strain_id, iteration, mg_count = getMultigenesCount(TASK)
            mg_counts[(strain_id, iteration)] = mg_count
            progress.update(desc=strain_id + " " + str(iteration) + " " + str(mg_count))
    
    all_output_fh = open(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_ANALYSIS_POST", ph2_iteration)+"post_analysis_strain_mgs_counts.txt", "w")
    line0 = "strain_id"
    line0 += "\tgenome_len\tpost_count\tpost_len_norm\tpost_orf_norm"
    line0 += "\n"
    all_output_fh.write(line0)
    
    for strain_id in strains_list:
        strain = strains.strain(strain_id)
        genome_length = strain.genome_length
        orf_count = strain.orf_count
        mgs_count = mg_counts[(strain_id, closure_iteration)]
        
        
        ratio1 = float(mgs_count) * float(median_genome_length) / float(genome_length)
        ratio2 = float(mgs_count) * float(median_orf_count) / float(orf_count)

        text_line = strain_id + "\t" + str(genome_length)        
        text_line += "\t" + str(mgs_count) + "\t" + str("%.2f" % ratio1) + "\t" + str("%.2f" % ratio2)
        

        text_line += "\n"
        all_output_fh.write(text_line)
    all_output_fh.close()
    
    print("postanalysis9_strain_mgs_count.py. Finished.")