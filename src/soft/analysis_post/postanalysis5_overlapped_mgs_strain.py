import sys
import time
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_io_utils import ensure_dir
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_overlap_utils import findOverlappingMgs, overlapLength

def findOverlappingMgsStrain(params):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strain_id, closure_iteration, ph2_iteration = params
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
        
    readUniAnnotationsIter(strain, closure_iteration, ph2_iteration)
    multigenes = strain.uni_annotation.multigenes
    over_mgs = findOverlappingMgs(strain.uni_annotation.multigenes.values())
    
    text_lines = []
    for mg1_id in over_mgs:
        mg1 = multigenes[mg1_id]
        
        text_line_ex = ""
        max_overlap = 0
        for mg2_id in over_mgs[mg1_id]:
            mg2 = multigenes[mg2_id]

            overlap_length = overlapLength(mg1, mg2)
            max_overlap = max(max_overlap, overlap_length)
            
            text_line_ex += mg2_id + ":" + str(overlap_length) + ";"
        
        text_line = mg1_id + "\t" + str(len(over_mgs[mg1_id])) + "\t" + str(max_overlap) + "\t" + str("%.2f" % (float(max_overlap)/ float(mg1.length()))) + "\t"
        text_line += text_line_ex
        text_line = text_line[:-1]
        text_lines.append(text_line)
            
    cc_det_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_POST_MGOVER", ph2_iteration) + strain_id + ".txt"
    cc_det_fh = open(cc_det_fn, "w")
    
    for text_line in text_lines:
        cc_det_fh.write(text_line + "\n")
    cc_det_fh.close()    

    return strain_id, closure_iteration

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    progress = ShowProgress("postanalysis5_overlapped_mgs_strain.py")
    strains_list = strains.allStrains()
    

    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default = 3)
    
    ensure_dir(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_POST_MGOVER", ph2_iteration))

    TASKS = []
    for strain_id in strains_list:
    #    TASKS.append((strain_id, 0))
        TASKS.append((strain_id, closure_iteration, ph2_iteration))
        
    progress.setJobsCount(len(TASKS))
    
    results = {}
    for strain_id in strains_list:
        results[strain_id] = {}
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for strain_id, iteration, in pool.imap_unordered(findOverlappingMgsStrain, TASKS):
            progress.update(desc=strain_id)
        pool.close()
    else:
        for TASK in TASKS:
            strain_id, iteration = findOverlappingMgsStrain(TASK)
            progress.update(desc=strain_id)

        print("postanalysis5_overlapped_mgs_strain.py. Finished.")
    