import sys
import time
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.structs.clusters import MGClusters, MGCluster
from soft.utils.camber_graph_utils import *
from soft.utils.camber_io_utils import ensure_dir
from soft.structs.multigene import *
from soft.utils.camber_utils import *

def readAnnCounts(strains):
    input_fh = open(parameters["DATASET_RESULTS_EXP_ANALYSIS"]+"analysis_strain_ann_counts.txt")
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) < 2:
            continue
        strain_id = tokens[0]
        ann_count = int(tokens[1])
        strain = strains.strain(strain_id)
        strain.ann_count = ann_count
    input_fh.close()

    return None

def calcCorepanGenomeSizes(strains_list, strain_ccs, alpha=1.0, beta=0.1):
    cc_presence = {}
    coregenome_sizes = [0]*len(strains_list)
    pangenome_sizes = [0]*len(strains_list)

    for i in range(len(strains_list)):
        strain_id = strains_list[i]
        strain_ccs_tmp = strain_ccs.get(strain_id, [])
        for cluster_id in strain_ccs_tmp:
            if not cluster_id in  cc_presence:
                cc_presence[cluster_id] = 0
            cc_presence[cluster_id] += 1
        
        pangenome_size = 0
        coregenome_size = 0
        for strain_count in cc_presence.values():
            if strain_count >= alpha*(i+1):
                coregenome_size += 1
            if strain_count >= beta*(i+1):
                pangenome_size += 1
        
        pangenome_sizes[i] = pangenome_size
        coregenome_sizes[i] = coregenome_size
    return coregenome_sizes, pangenome_sizes

def calcCoregenomeSizes(strains_list, cc_presence, alpha=1.0):   
    core_ccs = set(cc_presence)
    strains_subset = set([])
    coregenome_sizes = [0]*len(strains_list)

    for i in range(len(strains_list)):
        strain_id = strains_list[i]
        strains_subset.add(strain_id)
        core_ccs_tmp = set([])
        
        pangenome_size = 0
        
        for cluster_id in core_ccs:
            cc_strains = set(cc_presence[cluster_id]) & strains_subset
            if len(cc_strains) >= alpha*len(strains_subset):
                core_ccs_tmp.add(cluster_id)
        core_ccs = core_ccs_tmp
        
        coregenome_sizes[i] = len(core_ccs)
    return coregenome_sizes

def calcPangenomeSizes(strains_list, strain_ccs, beta=0.0):   
    core_ccs = set(cc_presence)
    strains_subset = set([])
    pangenome = set([])
    pangenome_sizes = [0]*len(strains_list)

    for i in range(len(strains_list)):
        strain_id = strains_list[i]
        strain_ccs_tmp = strain_ccs[strain_id]
        pangenome.update(strain_ccs_tmp)

        pangenome_sizes[i] = len(pangenome)
    return pangenome_sizes


def readCCDistr(input_fh, only_ann = False):
    
    cc_presence = {}
    
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) < 2:
            continue
        cluster_id = tokens[0]

        tokens_strains = tokens[1].split(";")
        
        found = False
        cc_strains = {}
        
        for strain_token in tokens_strains:
            tokens_elems = strain_token.split(":")
            strain_id = tokens_elems[0]
            if only_ann == False:
                strain_elems = int(tokens_elems[1])
            else:
                strain_elems = int(tokens_elems[2])
            if strain_elems > 0:
                found = True
                cc_strains[strain_id] = strain_elems
                
        if found == True:
            cc_presence[cluster_id] = cc_strains
    
    return cc_presence

def getStrainCCS(cc_presence):
    strain_ccs = {}

    for cluster_id in cc_presence:
        for strain_id in cc_presence[cluster_id]:
            if not strain_id in strain_ccs:
                strain_ccs[strain_id] = set([])
            strain_ccs[strain_id].add(cluster_id)
    return strain_ccs

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default = 3)
    
    readAnnCounts(strains)
    strains_list = sorted(strains.allStrains(), key=lambda strain_id:-strains.strain(strain_id).ann_count) 

    input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_ANALYSIS_POST", ph2_iteration) + "cluster_distr.txt"
    input_fh = open(input_fn)
    cc_presence = readCCDistr(input_fh)
    input_fh.close()
    
    strain_ccs = getStrainCCS(cc_presence)

    coregenome_sizes, pangenome_sizes = calcCorepanGenomeSizes(strains_list, strain_ccs, alpha = 1.0, beta=0.0)
         
    input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_ANALYSIS_POST", ph2_iteration) + "cluster_distr.txt"
    input_fh = open(input_fn)
    cc_presence = readCCDistr(input_fh, only_ann=True)
    input_fh.close()
    
    strain_ccs = getStrainCCS(cc_presence)
    
    coregenome_sizes_ann, pangenome_sizes_ann = calcCorepanGenomeSizes(strains_list, strain_ccs, alpha = 1.0, beta=0.0)
     
    output_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_ANALYSIS_POST", ph2_iteration) + "analysis_core-pan-genome.txt"
    output_fh = open(output_fn, "w")
    
    for i in range(len(strains_list)):
        strain_id = strains_list[i]
        text_line = strain_id +"\t"
        text_line += str(coregenome_sizes[i]) +"\t" + str(pangenome_sizes[i]) + "\t"
        text_line += str(coregenome_sizes_ann[i]) +"\t" + str(pangenome_sizes_ann[i])
        output_fh.write(text_line + "\n")
    
    output_fh.close()
    
    print("postanalysis4_core-pan-genome.py. Finished")