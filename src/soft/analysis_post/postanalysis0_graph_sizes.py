import sys
import time
import os

import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_fu_utils import UnionFind
from soft.utils.camber_graph_utils import readBlastResultsMapping
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *
from soft.utils.camber_io_utils import ensure_dir

def binom(n, k):
    if k <=0:
        return 1
    if 2*k>n:
        k = n-k
    ntok = 1
    for t in range(0, k, 1):
        ntok = ntok*(n-t)//(t+1)
    return ntok;


def createGraphStrain(strain_id):
    
    overwriteParameters(sys.argv)
    parameters = readParameters()
    
    closure_iteration = currentClosureIteration()
    
    g_counts = {}
    m_map = {}
    
    input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_MGSSEQ", 0) + "/" + strain_id + ".txt"
    input_fh = open(input_fn)
    lines = input_fh.readlines()
    input_fh.close()
    
    for line in lines:
        line = line.strip()
        tokens = line.split("\t")
        if len(tokens) < 2:
            continue
        mg_id = tokens[0]
        tokens_seq = tokens[1].split(";")
        for token_seq in tokens_seq:
            tokens_len = token_seq.split(":")
            seq_id = int(tokens_len[1])
            
            if not seq_id in g_counts:
                g_counts[seq_id] = 0
            g_counts[seq_id] += 1
            
            if not seq_id in m_map:
                m_map[seq_id] = set([])
            m_map[seq_id].add(mg_id)
    
    return strain_id, g_counts, m_map


if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    WORKERS = int(parameters["W"])
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default = 3)
    
    strains_list = strains.allStrains()
    
    progress = ShowProgress("postanalysis0_graph_sizes.py")
    progress.setJobsCount(3)
    
    text_lines = []
    
    input_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", 0)
    output_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", 0)
    ensure_dir(output_dir)
    ensure_dir(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_ANALYSIS_POST", ph2_iteration))

    nodes_s_count = 0
    edges_s_count = 0
    edges_b_count = 0
    nodes_o_count = 0
    edges_o_count = 0
    nodes_m_count = 0
    edges_m_count = 0        
    
    edges_b = []
    seq_ids = set([])
    input_fh = open(input_dir + "seqids_all.txt")
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) < 1:
            continue
        seq_ids.add(int(tokens[0]))
        nodes_s_count += 1
    input_fh.close()
    
    
    progress.update(desc = str(closure_iteration) + ": nodes")
    
    input_fh = open(input_dir + "edges_mgs.txt")
    for line in  input_fh.readlines():
        node_tokens = line.split()
        if len(node_tokens) != 2:
            continue
        edges_s_count += 1
    input_fh.close();
    
    progress.update(desc = str(closure_iteration) + ": multigene edges")
    
    input_fh = open(input_dir + "edges_seq.txt")
    lines = input_fh.readlines()
    input_fh.close();
    
    progress = ShowProgress("Read graph")
    progress.setJobsCount(len(lines))
    
    for line in lines:
        node_tokens = line.split()
        node1_id = int(node_tokens[0])
        node2_id = int(node_tokens[1])
        edges_b.append((node1_id, node2_id))
        progress.update()

    TASKS = strains_list
    
    g_counts = {}
    m_map = {}
    
    progress = ShowProgress("Updata seqs")
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap_unordered(createGraphStrain, TASKS):
            strain_id, g_counts_strain, m_map_strain = r
            
            for seq_id in g_counts_strain:
                if not seq_id in g_counts:
                    g_counts[seq_id] = 0
                g_counts[seq_id] += g_counts_strain[seq_id]
                
            for seq_id in m_map_strain:
                if not seq_id in m_map:
                    m_map[seq_id] = set([])                
                m_map[seq_id].update(m_map_strain[seq_id])
                                  
                                  
            progress.update(strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = createGraphStrain(TASK)
            strain_id, g_counts_strain, m_map_strain = r
            
            for seq_id in g_counts_strain:
                if not seq_id in g_counts:
                    g_counts[seq_id] = 0
                g_counts[seq_id] += g_counts_strain[seq_id]
                
            for seq_id in m_map_strain:
                if not seq_id in m_map:
                    m_map[seq_id] = set([])                
                m_map[seq_id].update(m_map_strain[seq_id])
                                  
            progress.update(strain_id) 
    
    nodes_o_count = 0
    edges_b_count = len(edges_b)
    print(len(edges_b))
    
    mg_map_rev = {}
    m_map_help = {}
    
    mg_set = set([])
    for x in m_map:
        for mg_id in m_map[x]:
            mg_set.add(mg_id)
            
    mg_list = list(mg_set)
    for i in range(len(mg_list)):
        mg_map_rev[mg_list[i]] = i
    
    for x in m_map:
        m_map_help[x] = set([])
        for mg_id in m_map[x]:
            m_map_help[x].add(mg_map_rev[mg_id])
            
    m_map = {}
    
    for x in g_counts:
        nodes_o_count += g_counts[x]
        edges_o_count += binom(g_counts[x], 2)
    for (x,y) in edges_b:
        edges_o_count += g_counts[x]*g_counts[y]
        
    nodes_m = set([])    
    
    for x in m_map_help:
        m_list = list(m_map_help[x])
        for mg_id in m_list:
            nodes_m.add(mg_id) 
            
    text_line = str(nodes_s_count) + "\n"
    text_line += str(edges_s_count) + "\n"
    text_line += str(edges_b_count) + "\n"
    text_line += str(nodes_o_count) + "\n"
    text_line += str(edges_o_count) + "\n"
    text_line += str(nodes_m_count) + "\n"
    text_line += str(edges_m_count)
    print(text_line)
    sys.stdout.flush()
        
    g_counts = {}
    
    edges_m = set([])
    progress = ShowProgress("Process edges")
    progress.setJobsCount(len(m_map_help) + len(edges_b))
  
    for x in m_map_help:
        m_list = list(m_map_help[x])
        n = len(m_list)
        for i in range(0, n-1, 1):
            mg1_id = m_list[i]
            for j in range(i+1, n, 1):
                mg2_id = m_list[j]
                if mg1_id <mg2_id:
                    edges_m.add((mg1_id, mg2_id))
                elif mg1_id > mg2_id:
                    edges_m.add((mg2_id, mg1_id))
        progress.update()
                
    for (x, y) in edges_b:
        m1_list = list(m_map_help[x])
        m2_list = list(m_map_help[y])
        for mg1_id in m1_list:
            for mg2_id in m2_list:
                if mg1_id <mg2_id:
                    edges_m.add((mg1_id, mg2_id))
                elif mg1_id > mg2_id:
                    edges_m.add((mg2_id, mg1_id))
        progress.update()
    
    nodes_m_count = len(nodes_m)
    edges_m_count = len(edges_m)
    
    text_line = str(nodes_s_count) + "\n"
    text_line += str(edges_s_count) + "\n"
    text_line += str(edges_b_count) + "\n"
    text_line += str(nodes_o_count) + "\n"
    text_line += str(edges_o_count) + "\n"
    text_line += str(nodes_m_count) + "\n"
    text_line += str(edges_m_count)
    text_lines.append(text_line) 
    
    output_fh = open(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_ANALYSIS_POST", ph2_iteration) + "postanalysis_graph_sizes.txt", "w")
    for text_line in text_lines:
        output_fh.write(text_line+ "\n")
    output_fh.close();
 
    print("postanalysis0_graph_sizes.py: Finished.")
    
