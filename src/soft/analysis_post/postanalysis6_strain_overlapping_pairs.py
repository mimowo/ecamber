import sys
import time
import os
import multiprocessing


sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_io_utils import ensure_dir
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_overlap_utils import overlapLength, findOverlappingMgPairs

def findOverlappingMgsStrain(params):
    overwriteParameters(sys.argv)
    readParameters()
    
    strain_id, closure_iteration, ph2_iteration = params
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    
    readUniAnnotationsIter(strain, closure_iteration, ph2_iteration)
    overlapping_pairs = findOverlappingMgPairs(strain.uni_annotation.multigenes.values())

    return strain_id, closure_iteration, overlapping_pairs

def classifyOverlaps(overlaps):
    
    short_codir = 0
    short_covergent = 0
    short_divergent = 0
    short_covered = 0
    
    long_codir = 0
    long_covergent = 0
    long_divergent = 0
    long_covered = 0
    
    for (mg1, mg2) in overlapping_pairs:
        overlap_length = overlapLength(mg1, mg2)
        
        if mg1.strand == mg2.strand:
            overlap_type = "co-directional"
        elif (mg1.strand == "+" and mg1.end <= mg2.start) or (mg1.strand == "-" and mg1.end >= mg2.start):
            overlap_type = "opp-convergent"
        elif (mg1.strand == "+" and mg2.end <= mg1.start) or (mg1.strand == "-" and mg2.end >= mg1.start):
            overlap_type = "opp-divergent"
        else:
            overlap_type = "opp-covered"
        
        if overlap_length < 60 and overlap_type == "co-directional":
            short_codir += 1
        elif overlap_length < 60 and overlap_type == "opp-convergent":
            short_covergent += 1 
        elif overlap_length < 60 and overlap_type == "opp-divergent":
            short_divergent += 1
        elif overlap_length < 60 and overlap_type == "opp-covered":
            short_covered += 1
        elif overlap_length >= 60 and overlap_type == "co-directional":
            long_codir += 1
        elif overlap_length >= 60 and overlap_type == "opp-convergent":
            long_covergent += 1
        elif overlap_length >= 60 and overlap_type == "opp-divergent":
            long_divergent += 1    
        elif overlap_length >= 60 and overlap_type == "opp-covered":
            long_covered += 1        
            
    return short_codir, short_covergent, short_divergent, short_covered, long_codir, long_covergent, long_divergent, long_covered

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    ensure_dir(parameters["DATASET_RESULTS_EXP_ANALYSIS"])
    
    progress = ShowProgress("postanalysis6_strain_overlapping_pairs.py")
    strains_list = strains.allStrains()

    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default = 3)
    
    
    TASKS = []
    for strain_id in strains_list:
       # TASKS.append((strain_id, 0))
        TASKS.append((strain_id, closure_iteration, ph2_iteration))
        
    progress.setJobsCount(len(TASKS))
    
    results = {}
    for strain_id in strains_list:
        results[strain_id] = {}
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for strain_id, iteration, overlapping_pairs in pool.imap_unordered(findOverlappingMgsStrain, TASKS):
            progress.update(desc=strain_id)
            short_codir, short_covergent, short_divergent, short_covered, long_codir, long_covergent, long_divergent, long_covered = classifyOverlaps(overlapping_pairs)
            text_res = str(short_codir) +"\t"+ str(short_covergent) +"\t"+ str(short_divergent) +"\t"+ str(short_covered) +"\t"+ str(long_codir) +"\t"+ str(long_covergent) +"\t"+ str(long_divergent) +"\t"+ str(long_covered)
            results[strain_id][iteration] = text_res
        pool.close()
    else:
        for TASK in TASKS:
            strain_id, iteration, overlapping_pairs = findOverlappingMgsStrain(TASK)
            short_codir, short_covergent, short_divergent, short_covered, long_codir, long_covergent, long_divergent, long_covered = classifyOverlaps(overlapping_pairs)
            text_res = str(short_codir) +"\t"+ str(short_covergent) +"\t"+ str(short_divergent) +"\t"+ str(short_covered) +"\t"+ str(long_codir) +"\t"+ str(long_covergent) +"\t"+ str(long_divergent) +"\t"+ str(long_covered)
            results[strain_id][iteration] = text_res
            progress.update(desc=strain_id)


    all_output_fh = open(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_ANALYSIS_POST", ph2_iteration) +"strain_over_mgs.txt", "w")
    for strain_id in results:
        text_line = strain_id +"\t"
        text_line += results[strain_id][closure_iteration] + "\t"
        all_output_fh.write(text_line + "\n")
    all_output_fh.close()
      
    print("postanalysis6_strain_overlapping_pairs.py. Finished.")
    