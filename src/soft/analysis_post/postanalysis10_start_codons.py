import sys
import time
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_io_utils import ensure_dir
from soft.structs.multigene import *
from soft.utils.camber_utils import *

def getMultigenesCount(strain_id):
    overwriteParameters(sys.argv)
    readParameters()
    
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    
    start_codons_strain = {}
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default = 3)
    
    readStrainSequences(strain)
    readUniAnnotationsIter(strain, closure_iteration, ph2_iteration)
    
    for multigene in strain.uni_annotation.multigenes.values():
        if len(multigene.sel_lengths) > 0:
            for gene in multigene.genes.values():
                if gene.selected == False:
                    continue                
                start_codon = gene.startCodon()
                if not start_codon in start_codons_strain:
                    start_codons_strain[start_codon] = 0
                start_codons_strain[start_codon] += 1
        else:
            for gene in multigene.genes.values():
                
                start_codon = gene.startCodon()
                if not start_codon in start_codons_strain:
                    start_codons_strain[start_codon] = 0
                start_codons_strain[start_codon] += 1
            
    
    return strain_id, start_codons_strain

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    progress = ShowProgress("postanalysis10_start_codons.py")
    strains_list = sorted(strains.allStrains(), key=lambda strain_id: -strains.strain(strain_id).totalGenomeLength())
    

    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default = 3)

    
    TASKS = strains_list
    progress.setJobsCount(len(TASKS))
    
    strart_codons = {}
    for strain_id in strains_list:
        strart_codons[strain_id] = {} 
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for strain_id, start_codons_strain in pool.imap_unordered(getMultigenesCount, TASKS):
            strart_codons[strain_id] = start_codons_strain
            progress.update(desc=strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            strain_id, start_codons_strain = getMultigenesCount(TASK)
            strart_codons[strain_id] = start_codons_strain
            progress.update(desc=strain_id)
        
    start_codons_counts = {}
    for strain_id in strart_codons:
        for start_codon in strart_codons[strain_id]:
            if not start_codon in start_codons_counts:
                start_codons_counts[start_codon] = 0
            start_codons_counts[start_codon] += strart_codons[strain_id][start_codon]
    
    start_codons_sorted = sorted(start_codons_counts.keys(), key=lambda start_codon: -start_codons_counts[start_codon])
    
    all_output_fh = open(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_ANALYSIS_POST", ph2_iteration)+"postanalysis_start_codons.txt", "w")
    
    line0 = "strain_id\t"
    for start_codon in start_codons_sorted:
        line0 += str(start_codon) + "\t"
    line0 = line0[:-1]
    all_output_fh.write(line0 + "\n")
    
    for strain_id in strains_list:
        text_line = strain_id + "\t"
        for start_codon in start_codons_sorted:
            if not start_codon in strart_codons[strain_id]:
                text_line += str(0)+ "\t"
            else:
                text_line += str(strart_codons[strain_id][start_codon])+ "\t"
        
        text_line = text_line[:-1]
        all_output_fh.write(text_line + "\n")
    all_output_fh.close()

    print("postanalysis10_start_codons.py. Finished.")
