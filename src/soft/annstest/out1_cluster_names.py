import sys
import time
import shutil
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_graph_utils import *
from soft.structs.clusters import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *

def saveClusterGeneNames(output_fh, clusters, strains_ord):
    
    text_line = "cluster_id\tgene_name\n"
    output_fh.write(text_line)
          
    for cluster in list(clusters.clusters.values()):
        cluster.geneName(strains_ord)
        
        if len(cluster.gene_name) <= 1: continue
        text_line  = cluster.cluster_id + "\t"
        text_line += cluster.gene_name + "\t"

        if text_line.endswith("\t"): text_line = text_line[:-1]

        output_fh.write(text_line + "\n")
    return None

if __name__ == '__main__':
    print("out1_cluster_names.py: saves details on multigene clusters.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    ensure_dir(parameters["DATASET_OUTPUT"])
    
    strains = readStrainsInfo()
    strains_list = strains.allStrains()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default = 3)
    
    ensure_dir(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_CLEANUP", ph2_iteration))
    
    clusters = MGClusters()
    
    TASKS = []
    for strain_id in strains_list:
        strain = strains.strain(strain_id)
        TASKS.append((strain, closure_iteration, ph2_iteration, False, True))
    
    progress = ShowProgress("Multigene clusters read")
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes = WORKERS)    
        for strain_id, cc_mapping in pool.imap(readMultigeneClusterStrain, TASKS):
            
            for cluster_id in cc_mapping:
                if not cluster_id in clusters.clusters:
                    cluster = MGCluster(cluster_id)
                    clusters.addCluster(cluster)
                else:
                    cluster = clusters.getCluster(cluster_id)
                for multigene in cc_mapping[cluster_id]:
                    cluster.addMultigene(multigene)
            
            progress.update(desc = strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            strain_id, cc_mapping =readMultigeneClusterStrain(TASK)
            
            for cluster_id in cc_mapping:
                if not cluster_id in clusters.clusters:
                    cluster = MGCluster(cluster_id)
                    clusters.addCluster(cluster)
                else:
                    cluster = clusters.getCluster(cluster_id)
                for multigene in cc_mapping[cluster_id]:
                    cluster.addMultigene(multigene)
            
            progress.update(desc = strain_id)
    
    clusters.classifyClusters(strains_list)    
    
    output_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration) + "cluster_gene_names.txt"
    output_fh = open(output_fn, "w")
    saveClusterGeneNames(output_fh, clusters, strains_list)
    output_fh.close()

    dst_fn = parameters["DATASET_OUTPUT"] + "cluster_gene_names.txt"
    shutil.copy(output_fn, dst_fn)

    print("out1_cluster_names.py. Finished.")
