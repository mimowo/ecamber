import sys
import time
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_graph_utils import *
from soft.structs.clusters import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *

    
if __name__ == '__main__':
    print("at1_anns_test.py: read and saves multigene clusters.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    cu_r = float(parameters["CUR"])
    cu_p = float(parameters["CUP"])
    cu_l = int(parameters["CUL"])
    cu_v = float(parameters["CUV"])
    
    strains = readStrainsInfo()
    n = len(strains.allStrains())
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=3)-1
    
    ensure_dir(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_CLEANUP", ph2_iteration))
    
    lines_out = []
    
    input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_CLEANUP", ph2_iteration) + "cluster_stats.txt"
    input_fh = open(input_fn)
    lines = input_fh.readlines()[1:]
    
    rem1 = 0
    rem2 = 0
    rem3 = 0 
    
    for line in lines:
        line = line.strip()
        tokens = line.split()
        if len(tokens) < 5:
            continue
        cluster_id  = tokens[0]
        cc_name = tokens[1]
        cc_type = tokens[2]
        mg_count = int(tokens[3])
        mg_ann_count = int(tokens[4])
        mg_ann_ratio = float(tokens[5])
        try:
            mg_ann_pvalue = float(tokens[6])
        except:
            mg_ann_pvalue = 0.0
        strain_count = int(tokens[7])
        strain_ann_count = int(tokens[8])
        strain_ann_ratio = float(tokens[9])
        median_overlap = float(tokens[10])
        median_overlap_ratio = float(tokens[11])
        longest_overlap_ratio = float(tokens[12])
        max_tis = int(tokens[13])
        max_len = int(tokens[14])
        median_len = int(float(tokens[15]))
        
        print(cluster_id, cc_name)

    print(rem1, rem2, rem3)
    input_fh.close()
            
    print("at1_anns_test.py: Finished.")


