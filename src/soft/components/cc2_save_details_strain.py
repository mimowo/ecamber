import sys
import time
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_graph_utils import readBlastResultsMappingRev
from soft.utils.camber_coding_utils import getCodingMap, codeDNAseq
from soft.structs.multigene import Multigene, createStrainMultigeneId, mgIntoSortableTriple
from soft.utils.camber_closure_utils import correctGeneSeq

def updateMgComponents(params):
    
    overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strain_id = params[0]
    iteration = params[1]
    
    input_fh = open(getItPath(parameters, iteration, "DATASET_RESULTS_EXP_FINAL") + strain_id+".txt")
    lines = input_fh.readlines()
    input_fh.close()
    
    multigenes = {}
    for line in lines:
        line = line.strip()
        tokens = line.split("\t")
        if len(tokens) < 3:
            continue
        mg_id = tokens[0]
        mg_ann_id = tokens[1]
        mg_lens = tokens[2]
        multigenes[mg_id] = (mg_ann_id, mg_lens)
    
    seqmgs_map = {}
    input_fn = getItPath(parameters, iteration, "DATASET_RESULTS_EXP_MGSSEQ", 0) + "/" + strain_id + ".txt"
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split("\t")
        if len(tokens) < 2:
            continue
        mg_id = tokens[0]
        tokens_seq = tokens[1].split(";")
        for token_seq in tokens_seq:
            tokens_len = token_seq.split(":")
            seq_len = int(tokens_len[0])
            seq_id = int(tokens_len[1])
            if not seq_id in seqmgs_map:
                seqmgs_map[seq_id] = set([])
            seqmgs_map[seq_id].add(mg_id)
    input_fh.close()

    pairs = set([])

    cc_fn = getItPath(parameters, iteration, "DATASET_RESULTS_EXP_PH2_IT" , 0) + "seq_clusters.txt"
    cc_fh = open(cc_fn)
    for line in cc_fh.readlines():
        line = line.strip()
        tokens = line.split()
        if len(tokens) < 2:
            continue
        cc_id = tokens[0]
        seq_id_tokens = tokens[1].split(":")
        for seq_id_token in seq_id_tokens:
            seq_id = int(seq_id_token)
            mgs = seqmgs_map.get(seq_id)
            if mgs != None and len(mgs) > 0:
                for mg_id in mgs:
                    pairs.add((cc_id, mg_id))
    cc_fh.close()

    pairs_sorted = sorted(pairs, key=lambda pair:mgIntoSortableTriple(pair[1]))
    
    output_fn = getItPath(parameters, iteration, "DATASET_RESULTS_EXP_UNIANNS", 0) + strain_id + ".txt"
    output_fh = open(output_fn, "w")
    for (cc_id, mg_id) in pairs_sorted:
        mg_ann_id, mg_lens = multigenes[mg_id]
        output_fh.write(cc_id + "\t" + mg_id + "\t" + mg_ann_id + "\t" + mg_lens + "\n")
    output_fh.close()
    
    return strain_id

if __name__ == '__main__':
    first_id = 1
    print("save_component_multigenes: Converts the sequence connected components into multigene connected components.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())

    closure_iteration = currentClosureIteration()
    
    strains = readStrainsInfo()
    
    strains_list = list(strains.allStrains())
        
    ensure_dir(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_UNIANNS", 0))
    TASKS = []
    for strain_id in strains_list:
        TASKS.append((strain_id, closure_iteration))
        
    progress = ShowProgress("Converted sequence->multigene, it=" + str(closure_iteration))
    n = len(TASKS)
    progress.setJobsCount(n)
    
    seq_ids_mg_map = {} 
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap(updateMgComponents, TASKS):
            strain_id = r
            progress.update(strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = updateMgComponents(TASK)
            strain_id = r
            progress.update(strain_id)
    
    output_fh = open(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PHASE2_IT") + "ph2_step.txt", "w")
    output_fh.write("0\n")
    output_fh.close()
    print("save_component_multigenes: Finished.")
    
