import sys
import time
import os

import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_fu_utils import UnionFind
from soft.utils.camber_graph_utils import readBlastResultsMapping
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *
from soft.utils.camber_io_utils import ensure_dir


if __name__ == '__main__':
    print("compute_components_seq.py: Computes connected components of the sequence consolidation graph.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    closure_iteration = currentClosureIteration()
    
    progress = ShowProgress("Connected components")
    
    progress.setJobsCount(5)
    
    input_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", 0)
    output_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", 0)
    ensure_dir(output_dir)
        
    seq_ids = set([])
    input_fh = open(input_dir + "seqids_all.txt")
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) < 1:
            continue
        seq_ids.add(int(tokens[0]))
    input_fh.close()
    
    fu = UnionFind()
    fu.insert_objects(seq_ids)
    
    progress.update(desc = str(closure_iteration) + ": nodes")
    
    input_fh = open(input_dir + "edges_mgs.txt")
    for line in  input_fh.readlines():
        node_tokens = line.split()
        if len(node_tokens) != 2:
            continue
        node1_id = int(node_tokens[0])
        node2_id = int(node_tokens[1])
        if node1_id in seq_ids and node2_id in seq_ids:
            fu.union(node1_id, node2_id)
    input_fh.close();
    
    progress.update(desc = str(closure_iteration) + ": multigene edges")    
    
    input_fh = open(input_dir + "edges_seq.txt")
    for line in input_fh.readlines():
        node_tokens = line.split()
        if len(node_tokens) != 2:
            continue
        node1_id = int(node_tokens[0])
        node2_id = int(node_tokens[1])
        if node1_id in seq_ids and node2_id in seq_ids:
            fu.union(node1_id, node2_id)                
    input_fh.close();
        
    progress.update(desc = str(closure_iteration) + ": blast edges")
        
    ccs = fu.sets()
    
    progress.update(desc = str(closure_iteration) + ": components")
    
    cluster_fn = output_dir + "seq_clusters.txt"
    cluster_fh = open(cluster_fn, "w")
    saveClusters(cluster_fh, ccs, inline=True, cluster_ids=True)
    cluster_fh.close();
    
    progress.update(desc = str(closure_iteration) + ": saved")
 
    print("compute_components_seq.py: Finished.")
        
