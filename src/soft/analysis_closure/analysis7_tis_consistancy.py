import sys
import time
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_graph_utils import *
from soft.structs.clusters import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *

    
if __name__ == '__main__':
    print("analysis7_tis_consistancy.py: read and saves clusters.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    
    strains = readStrainsInfo()
    strains_list = strains.allStrains()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default = 0)
    
  #  ensure_dir(parameters["DATASET_RESULTS_EXP_ANALYSIS"], ph2_iteration)
    
    clusters = MGClusters()
    
    TASKS = []
    for strain_id in strains_list:
        strain = strains.strain(strain_id)
        TASKS.append((strain, closure_iteration, ph2_iteration, False, False))
    
    progress = ShowProgress("Read clusters")
    progress.setJobsCount(len(TASKS))
        
    pool = multiprocessing.Pool(processes = WORKERS)    
    for strain_id, cc_mapping in pool.imap(readMultigeneClusterStrain, TASKS):
        
        for cluster_id in cc_mapping:
            if not cluster_id in clusters.clusters:
                cluster = MGCluster(cluster_id)
                clusters.addCluster(cluster)
            else:
                cluster = clusters.getCluster(cluster_id)
            for multigene in cc_mapping[cluster_id]:
                cluster.addMultigene(multigene)
        
        progress.update(desc = strain_id)
    pool.close()
    
    clusters.classifyClusters(strains_list)
    k = len(strains_list)
    
    cc_max_tis_map = {}
    mg_max_tis_map = {}
    
    for cluster in clusters.anchors.values(): 
        if len(cluster.strains()) < 0.95*k:
            continue
        max_tis_number = 0
        cluster_id = cluster.cluster_id
        for multigene in cluster.nodes.values():
            max_tis_number = max(max_tis_number, len(multigene.lengths))
            if not len(multigene.lengths) in mg_max_tis_map:
                mg_max_tis_map[len(multigene.lengths)] = 0
            mg_max_tis_map[len(multigene.lengths)] += 1
        if not max_tis_number in cc_max_tis_map:
            cc_max_tis_map[max_tis_number] = 0
        cc_max_tis_map[max_tis_number] += 1
        
    max_tis_number = max(cc_max_tis_map.keys())
    
    output_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_ANALYSIS_POST", ph2_iteration) + "analysis_tis_consistancy.txt"
    output_fh = open(output_fn, "w")
    
    for tis_number in range(1, max_tis_number+1, 1):
        text_line = str(tis_number) + "\t"
        if tis_number in cc_max_tis_map:
            text_line += str(cc_max_tis_map[tis_number])  + "\t"
        else:
            text_line += str(0)  + "\t"
        if tis_number in mg_max_tis_map:
            text_line += str(mg_max_tis_map[tis_number])
        else:
            text_line += str(0)
        output_fh.write(text_line + "\n")
    output_fh.close()
    
    print("analysis7_tis_consistancy.py: Finished.")
        
        