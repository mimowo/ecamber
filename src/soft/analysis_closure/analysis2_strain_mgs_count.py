import sys
import time
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_io_utils import ensure_dir
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_stat_utils import mv
from soft.utils.camber_analysis_utils import readGenomeLengths

    
def readORFsCounts(strains):
    input_fh = open(parameters["DATASET_RESULTS_EXP_ANALYSIS"]+"analysis_strain_orf_counts.txt")
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) < 2:
            continue
        strain_id = tokens[0]
        orf_count = int(tokens[2])
        strain = strains.strain(strain_id)
        strain.orf_count = orf_count
    input_fh.close()

    return None
    
def getMultigenesCount(params):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strain_id = params[0]
    iteration = params[1]
    
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    
    strain_id, annotation = readAllMgAnnotationsIter(strain, iteration)
    
    return strain_id, iteration, len(annotation.multigenes)

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    ensure_dir(parameters["DATASET_RESULTS_EXP_ANALYSIS"])
    
    progress = ShowProgress("analysis2_strain_mgs_count.py")

    readGenomeLengths(strains)
    readORFsCounts(strains)
    
    genome_lengths = []
    for strain_id in  strains.allStrains():
        genome_lengths.append(strains.strain(strain_id).genome_length)
    median_genome_length = median(genome_lengths)

    orf_counts = []
    for strain_id in  strains.allStrains():
        orf_counts.append(strains.strain(strain_id).orf_count)
    median_orf_count = median(orf_counts)

    strains_list = sorted(strains.allStrains(), key=lambda strain_id:strains.strain(strain_id).genome_length) 

    last_iteration = currentClosureIteration()
    mg_counts = {}
    
    iterations = set([0, last_iteration])
    
    TASKS = []
    for iteration in iterations:
        for strain_id in strains_list:
            TASKS.append((strain_id, iteration))
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for strain_id, iteration, mg_count in pool.imap_unordered(getMultigenesCount, TASKS):
            mg_counts[(strain_id, iteration)] = mg_count
            progress.update(desc=strain_id + " " + str(iteration) + " " + str(mg_count))
        pool.close()
    else:
        for TASK in TASKS:
            strain_id, iteration, mg_count = getMultigenesCount(TASK)
            mg_counts[(strain_id, iteration)] = mg_count
            progress.update(desc=strain_id + " " + str(iteration) + " " + str(mg_count))
    
    all_output_fh = open(parameters["DATASET_RESULTS_EXP_ANALYSIS"]+"analysis_strain_mgs_counts.txt", "w")
    line0 = "strain_id"
    line0 += "\tgenome_len\tann_count\tann_len_norm\tann_orf_norm\tclosure_count\tclosure_len_norm\tclosure_orf_norm"
    line0 += "\n"
    all_output_fh.write(line0)
    for strain_id in strains_list:
        strain = strains.strain(strain_id)
        genome_length = strain.genome_length
        orf_count = strain.orf_count
        text_line = strain_id + "\t" + str(genome_length)
        mgs_count = mg_counts[(strain_id, 0)]
        
        ratio1 = float(mgs_count) * float(median_genome_length) / float(genome_length)
        ratio2 = float(mgs_count) * float(median_orf_count) / float(orf_count)
        
        text_line += "\t" + str(mgs_count) + "\t" + str("%.2f" % ratio1) + "\t" + str("%.2f" % ratio2)
        
        mgs_count = mg_counts[(strain_id, last_iteration)]
        
        ratio1 = float(mgs_count) * float(median_genome_length) / float(genome_length)
        ratio2 = float(mgs_count) * float(median_orf_count) / float(orf_count)
        
        text_line += "\t" + str(mgs_count) + "\t" + str("%.2f" % ratio1) + "\t" + str("%.2f" % ratio2)   
        
        text_line += "\n"
        all_output_fh.write(text_line)
    all_output_fh.close()
    
    print("analysis2_strain_mgs_count.py. Finished.")
    