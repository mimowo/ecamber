import sys
import time
import os
import multiprocessing


sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_io_utils import ensure_dir
from soft.structs.multigene import *
from soft.utils.camber_utils import *

def getSequences(params):
    strain_id, iteration = params
    
    overwriteParameters(sys.argv)
    readParameters()
    
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    
    readStrainSequences(strain)
    readAllAnnotationsIter(strain, iteration)
    gene_sequences = set([])
    mgs = set([])

    genes_count = 0
    for gene in list(strain.all_annotation.genes.values()):
        mg_id = createStrainMultigeneId(gene.end, gene.strand, gene.strain, gene.contig_id)
        mgs.add(mg_id)
        gene_sequences.add(gene.sequence())
        genes_count += 1
    
    return strain_id, gene_sequences, genes_count, len(mgs)

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    ensure_dir(parameters["DATASET_RESULTS_EXP_ANALYSIS"])
    last_iteration = currentClosureIteration()
    
    progress = ShowProgress("analysis1_iter_gene_counts.py")
    strains_list = list(strains.allStrains())
    progress.setJobsCount(len(strains_list) * (last_iteration+1))

    gene_counts = [0]*(last_iteration+1)
    
    
    for iteration in range(last_iteration + 1):
        
        dist_sequences = set([])
        tot_genes_count = 0
        tot_mgs_count = 0
        
        TASKS = []
        for strain_id in strains_list:
            TASKS.append((strain_id, iteration))
            
        if WORKERS > 1:
            pool = multiprocessing.Pool(processes=WORKERS) 
            for strain_id, sequences, genes_count, mgs_count in pool.imap_unordered(getSequences, TASKS):
                dist_sequences.update(sequences)
                tot_genes_count += genes_count
                tot_mgs_count += mgs_count
                progress.update(desc=strain_id)
            pool.close()
            pool.join()
        else:
            for TASK in TASKS:
                strain_id, sequences, genes_count, mgs_count = getSequences(TASK)
                dist_sequences.update(sequences)
                tot_genes_count += genes_count
                tot_mgs_count += mgs_count
                progress.update(desc=strain_id)

        gene_counts[iteration] = (len(dist_sequences), tot_genes_count, tot_mgs_count)
        
    all_output_fh = open(parameters["DATASET_RESULTS_EXP_ANALYSIS"]+"analysis_iter_gene_counts.txt", "w")
    for iteration in range(last_iteration + 1):
        sequence_count, genes_count, mgs_count = gene_counts[iteration]
        all_output_fh.write(str(sequence_count) + "\t" + str(genes_count) + "\t" + str(mgs_count)  +"\t"+ str("%.2f" % (float(sequence_count)*100.0/float(genes_count)))+"%" + "\n")
    all_output_fh.close()

    print("analysis1_iter_gene_counts.py. Finished.")