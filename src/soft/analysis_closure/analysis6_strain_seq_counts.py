import sys
import time
import os
import multiprocessing


sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_io_utils import ensure_dir
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_analysis_utils import readGenomeLengths

def getSequences(params):
    strain_id, iteration = params
    
    overwriteParameters(sys.argv)
    readParameters()
    
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    
    readStrainSequences(strain)
    readAllAnnotationsIter(strain, iteration)
    gene_sequences = set([])

    genes_count = 0
    for gene in list(strain.all_annotation.genes.values()):
        gene_sequences.add(gene.sequence())
        genes_count += 1
    
    return strain_id, gene_sequences

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    ensure_dir(parameters["DATASET_RESULTS_EXP_ANALYSIS"])
    last_iteration = currentClosureIteration()
    readGenomeLengths(strains)
    
    progress = ShowProgress("analysis6_strain_seq_counts.py")
    strains_list = sorted(strains.allStrains(), key=lambda strain_id:strains.strain(strain_id).genome_length)
    progress.setJobsCount(len(strains_list)*2)

    strain_counts = {}
    seqs = set([])
    
    for strain_id in strains_list:
        strain_id, strain_seqs = getSequences((strain_id, 0))
        seqs.update(strain_seqs)
        
        strain_counts[strain_id] = len(seqs)
        progress.update()

    all_strain_counts = {}
    all_seqs = set([])
    
    for strain_id in strains_list:
        strain_id, all_strain_seqs = getSequences((strain_id, last_iteration))
        all_seqs.update(all_strain_seqs)
        
        all_strain_counts[strain_id] = len(all_seqs)
        progress.update()
        
    all_output_fh = open(parameters["DATASET_RESULTS_EXP_ANALYSIS"]+"analysis_strain_seq_counts.txt", "w")
    for strain_id in strains_list:
        all_output_fh.write(strain_id + "\t" + str(strain_counts[strain_id]) + "\t" + str(all_strain_counts[strain_id]) +"\n")
    all_output_fh.close()

    print("analysis6_strain_seq_counts.py. Finished.")