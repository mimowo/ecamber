import sys
import time
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_io_utils import ensure_dir
from soft.structs.multigene import *
from soft.utils.camber_utils import *

def getMultigenesCount(strain_id):
    overwriteParameters(sys.argv)
    readParameters()
    
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    
    readStrainSequences(strain)
    
    genome_length = strain.totalGenomeLength()
    
    return strain_id, genome_length

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    ensure_dir(parameters["DATASET_RESULTS_EXP_ANALYSIS"])
    
    progress = ShowProgress("input2_strain_genome_lengths.py")
    strains_list = sorted(strains.allStrains(), key=lambda strain_id: -strains.strain(strain_id).totalGenomeLength())
    

    last_iteration = currentClosureIteration()
    genome_lengths = {}
    
    TASKS = strains_list
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for strain_id, genome_length in pool.imap_unordered(getMultigenesCount, TASKS):
            genome_lengths[strain_id] = genome_length
            progress.update(desc=strain_id)
        pool.close()
    else:
        for TASK in TASKS:
            strain_id, genome_length = getMultigenesCount(TASK)
            genome_lengths[strain_id] = genome_length
            progress.update(desc=strain_id)
        
    all_output_fh = open(parameters["DATASET_RESULTS_EXP_ANALYSIS"]+"analysis_strain_genome_lengths.txt", "w")
    for strain_id in strains_list:
        text_line = strain_id + "\t" + str(genome_lengths[strain_id]) + "\n"
        all_output_fh.write(text_line)
    all_output_fh.close()
      
    print("input2_strain_genome_lengths.py. Finished.")