import sys
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_closure_utils import *
from soft.structs.blast_hit import *
from soft.structs.gene import *
from soft.structs.annotation import *


def countSeqORFs(sequence, min_len=50):
    n = len(sequence)
    orfs_count = 0
    stop_orfs_count = 0
    
    for f in range(3):
        stop_index = 0
        start_found = False
        for i in range(n-2, -1, -1):
            codon = sequence[i+f:i+f+3]
            if codon in ["TAG", "TGA", "TAA"]:
                stop_index = i
                start_found = False
            elif codon in ["ATG", "GTG", "TTG"]:
                if stop_index > 0 and stop_index - i >= min_len:
                    orfs_count += 1
                    if start_found == False:
                        stop_orfs_count += 1
                    start_found = True

    return orfs_count, stop_orfs_count

#def seqORFs(sequence, min_len=50):
#    n = len(sequence)
#    orfs = set([])
#    for f in range(3):
#        stop_index = 0
#        for i in range(n-2, -1, -1):
#            codon = sequence[i+f:i+f+3]
#            if codon in ["TAG", "TGA", "TAA"]:
#                stop_index = i
#            elif codon in ["ATG", "GTG", "TTG"]:
#                if stop_index > 0 and stop_index - i >= min_len:
#                    orf_seq = sequence[i+f:stop_index+3]
#                    orfs.add(orf_seq)                
#    return orfs


def countORFs(strain_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    
    readStrainSequences(strain)
    orfs_count_total = 0
    stop_orfs_count_total = 0
    orfs = set([])
    
    if strain.sequence == None:
        for genome_seq in strain.sequences_map.values():
            genome_seq_rev = complementarySequence(genome_seq)
            orfs_count, stop_orfs_count = countSeqORFs(genome_seq)
            stop_orfs_count_total  += stop_orfs_count
            orfs_count_total += orfs_count
            orfs_count, stop_orfs_count = countSeqORFs(genome_seq_rev)
            stop_orfs_count_total  += stop_orfs_count
            orfs_count_total += orfs_count
            
    else:
        genome_seq = strain.sequence
        genome_seq_rev = complementarySequence(genome_seq)
        orfs_count, stop_orfs_count = countSeqORFs(genome_seq)
        stop_orfs_count_total  += stop_orfs_count
        orfs_count_total += orfs_count
        orfs_count, stop_orfs_count = countSeqORFs(genome_seq_rev)
        stop_orfs_count_total  += stop_orfs_count
        orfs_count_total += orfs_count
    
    return (strain_id, orfs_count_total, stop_orfs_count_total)

#def strainORFs(strain_id):
#    parameters = overwriteParameters(sys.argv)
#    parameters = readParameters()
#    
#    strains = readStrainsInfo()
#    strain = strains.strain(strain_id)
#    
#    readStrainSequences(strain)
#    orfs = set([])
#    
#    if strain.sequence == None:
#        for genome_seq in strain.sequences_map.values():
#            genome_seq_rev = complementarySequence(genome_seq)
#            orfs.update(seqORFs(genome_seq))
#            orfs.update(seqORFs(genome_seq_rev))
#    else:
#        genome_seq = strain.sequence
#        genome_seq_rev = complementarySequence(genome_seq)
#        orfs.update(seqORFs(genome_seq))
#        orfs.update(seqORFs(genome_seq_rev))    
#    
#    return (strain_id, orfs)

if __name__ == '__main__':

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    strains_list = list(strains.allStrains())

    iteration = 0
    print("input0_countORFs.py: count ORFs.")
    print("Current iteration: " + str(iteration))


    ensure_dir(parameters["DATASET_RESULTS_EXP_ANALYSIS"])
    progress = ShowProgress("Counted ORFs")
    
    TASKS = []
    for strain_id in strains.allStrains():
        TASKS.append(strain_id)
    
    progress.setJobsCount(len(TASKS))
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    all_orfs = set([])
    
    text_lines = []
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap_unordered(countORFs, TASKS):
            strain_id, orfs_count, stop_orfs_count = r
            text_lines.append(strain_id + "\t" + str(orfs_count) + "\t" + str(stop_orfs_count))
            progress.update(desc=strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = countORFs(TASK)
            strain_id, orfs_count, stop_orfs_count = r
            text_lines.append(strain_id + "\t" + str(orfs_count) + "\t" + str(stop_orfs_count))
            progress.update(desc=strain_id)
    
    all_output_fh = open(parameters["DATASET_RESULTS_EXP_ANALYSIS"]+"analysis_strain_orf_counts.txt", "w")
    for text_line in text_lines:
        all_output_fh.write(text_line + "\n")
    all_output_fh.close()
    
    print("input0_countORFs.py: Finished.")
    
