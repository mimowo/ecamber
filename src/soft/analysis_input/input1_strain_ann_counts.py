import sys
import time
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_io_utils import ensure_dir
from soft.structs.multigene import *
from soft.utils.camber_utils import *

def getMultigenesCount(strain_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    
    readAllMgAnnotationsIter(strain, 0)
    
    ann_count = len(strain.all_annotation.multigenes)
    
    return strain_id, ann_count

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    ensure_dir(parameters["DATASET_RESULTS_EXP_ANALYSIS"])
    
    progress = ShowProgress("input1_strain_ann_counts.py")
    strains_list = strains.allStrains()
    

    last_iteration = currentClosureIteration()
    ann_count = {}
    
    TASKS = strains_list
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for strain_id, genome_length in pool.imap_unordered(getMultigenesCount, TASKS):
            ann_count[strain_id] = genome_length
            progress.update(desc=strain_id)
        pool.close()
    else:
        for TASK in TASKS:
            strain_id, genome_length = getMultigenesCount(TASK)
            ann_count[strain_id] = genome_length
            progress.update(desc=strain_id)
        
    all_output_fh = open(parameters["DATASET_RESULTS_EXP_ANALYSIS"]+"analysis_strain_ann_counts.txt", "w")
    for strain_id in strains_list:
        text_line = strain_id + "\t" + str(ann_count[strain_id]) + "\n"
        all_output_fh.write(text_line)
    all_output_fh.close()
      
    print("input1_strain_ann_counts.py. Finished.")