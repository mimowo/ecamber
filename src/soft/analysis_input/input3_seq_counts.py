import sys
import time
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_io_utils import ensure_dir
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_analysis_utils import readGenomeLengths


def getSequences(params):
    strain_id = params
    
    overwriteParameters(sys.argv)
    readParameters()
    
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    
    readStrainSequences(strain)
    readAllAnnotationsIter(strain, 0)
    gene_sequences = set([])

    genes_count = len(strain.all_annotation.genes)
    for gene in list(strain.all_annotation.genes.values()):
        gene_sequences.add(gene.sequence())
    
    return strain_id, gene_sequences, genes_count

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    ensure_dir(parameters["DATASET_RESULTS_EXP_ANALYSIS"])
    last_iteration = currentClosureIteration()
    readGenomeLengths(strains)
    
    progress = ShowProgress("input3_seq_counts.py")
    strains_list_sorted = sorted(strains.allStrains(), key=lambda strain_id:strains.strain(strain_id).genome_length)
    progress.setJobsCount(len(strains_list_sorted))

    all_gene_count = 0
    all_seqs = set([])
    
    strain_seq_counts = {}
    strain_genes_counts = {}
    
    TASKS = strains_list_sorted
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for strain_id, sequences, genes_count in pool.imap(getSequences, TASKS):
            all_seqs.update(sequences)
            all_gene_count += genes_count
            strain_seq_counts[strain_id] = len(all_seqs)
            strain_genes_counts[strain_id] = all_gene_count
            progress.update(desc=strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            strain_id, sequences, genes_count = getSequences(TASK)
            all_seqs.update(sequences)
            all_gene_count += genes_count
            strain_seq_counts[strain_id] = len(all_seqs)
            strain_genes_counts[strain_id] = all_gene_count
            progress.update(desc=strain_id)

        
    all_output_fh = open(parameters["DATASET_RESULTS_EXP_ANALYSIS"]+"analysis_strain_seq_counts.txt", "w")
    for strain_id in strains_list_sorted:
        all_output_fh.write(strain_id + "\t" + str(strain_seq_counts[strain_id]) + "\t" + str(strain_genes_counts[strain_id]) +"\n")
    all_output_fh.close()

    print("input3_seq_counts.py. Finished.")
    