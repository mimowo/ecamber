import sys
import os
import multiprocessing


sys.path.append("../../")
from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_graph_utils import *

def chunks(l, n):
    i = 0
    chunk = l[:n]
    while chunk:
        yield chunk
        i += n
        chunk = l[i:i+n]

def readSeqEdgesStrain(params):
    overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strain_chunk = params[0]
    iteration = params[1]

    new_edges = set([])
    closure_iteration = currentClosureIteration()
    
    for strain_id in strain_chunk:
        strain_edges = []
        input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_EDGES_SEQ") + "edges_"+str(iteration) + "/" + strain_id +"-" + str(iteration) + ".txt"
        if not os.path.exists(input_fn):
            continue
        
        input_fh = open(input_fn)
        for line in input_fh.readlines():
            edges_pair = line.strip()
            if len(edges_pair) <= 1: continue
            strain_edges.append(edges_pair)
        input_fh.close()
        new_edges.update(strain_edges)
        
    return new_edges

def combineSeqEdgesIt(iteration, strains_list, output_dir, WORKERS):
    strain_chunks = list(chunks(strains_list, 1 + int(len(strains_list) / WORKERS)))
    
    TASKS = []
    for strain_chunk in strain_chunks:
        TASKS.append((strain_chunk, iteration))
        
    progress = ShowProgress("Combine edges, it=" + str(iteration))
    progress.setJobsCount(len(strain_chunks))

    graph_fn = output_dir + "edges_seq_"+str(iteration) + ".txt"
    if os.path.exists(graph_fn) and os.path.getsize(graph_fn) > 0:
        return iteration
    
    all_edges = set([])
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)
        for r in pool.imap_unordered(readSeqEdgesStrain, TASKS):
            new_edges = r
            all_edges.update(new_edges)                
            progress.update("it=" + str(iteration))
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = readSeqEdgesStrain(TASK)
            new_edges = r
            all_edges.update(new_edges)                
            progress.update("it=" + str(iteration))

    graph_fh = open(graph_fn, "w")
    for edges_pair in all_edges:
        graph_fh.write(edges_pair + "\n")
    graph_fh.close()
    del all_edges
    
    return iteration
    
if __name__ == '__main__':
    print("graph4_comb_it_blast_edges.py: Combines graph edges between BLAST hits.")
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()

    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())

    strains_list = list(strains.allStrains())
    
    closure_iteration = currentClosureIteration()

    output_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_EDGES_SEQ")        

    s_time = time.time()
    for iteration in range(closure_iteration + 1):
        
        combineSeqEdgesIt(iteration, strains_list, output_dir, WORKERS)
        
    e_time = time.time()
    
    print("graph4_comb_it_blast_edges.py: Finished.", e_time-s_time)
    
