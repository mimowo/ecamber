import sys
import os
import multiprocessing

sys.path.append("../../")
from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_graph_utils import *
from soft.utils.camber_progress import ShowProgress


if __name__ == '__main__':
    print("graph5_comb_blast_edges.py: Combines graph edges between BLAST hits.")
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()

    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())

    strains_list = list(strains.allStrains())

    closure_iteration = currentClosureIteration()
    output_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", 0)
    ensure_dir(output_dir)
        
    all_edges = set([])
    
    progress = ShowProgress("Processed edges")
    progress.setJobsCount(closure_iteration+1)
    input_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_EDGES_SEQ")
    
    for iteration in range(closure_iteration + 1): 
        it_edges = []  
        input_fn = input_dir + "edges_seq_"+str(iteration)+".txt"
        input_fh = open(input_fn)
        for line in input_fh.readlines():
            edges_pair = line.strip()
            if len(edges_pair) < 2: continue
            it_edges.append(edges_pair)
        input_fh.close()
        all_edges.update(it_edges)
        progress.update("it=" + str(iteration))
                
    graph_fn = output_dir + "edges_seq.txt"
    graph_fh = open(graph_fn, "w")
    for edges_pair in all_edges:
        graph_fh.write(edges_pair + "\n")
    graph_fh.close()
            
    print("graph5_comb_blast_edges.py: Finished.")
    
