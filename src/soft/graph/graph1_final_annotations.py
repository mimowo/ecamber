import sys
import os
import time
import multiprocessing

sys.path.append("../../")
from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_closure_utils import *
from soft.structs.blast_hit import *
from soft.structs.gene import *
from soft.structs.annotation import *
from soft.structs.multigene import mgIntoSortableTriple

def filteroutShortAnnotations(strain_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    
    strain = strains.strain(strain_id)
    readStrainSequences(strain)   
    
    last_iteration = currentClosureIteration()
    
    output_dir = getItPath(parameters, last_iteration, "DATASET_RESULTS_EXP_FINAL")
    ensure_dir(output_dir)
    
    strain_id, annotation = readAllMgAnnotationsIter(strain, last_iteration)

    if "SMR" in parameters: smr = float(parameters["SMR"])
    else: smr = 0.5
    
    final_orfs_fh = open(output_dir + strain_id + ".txt", "w")
    saveMgsAnnotations(final_orfs_fh, genes=None, multigenes=annotation.multigenes.values(), show_start_codons=True, smr=smr)
    final_orfs_fh.close();

    return strain_id
    
if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    print("save_final_annotations.py: Saves final annotations removing short multigene elements.")

    strains = readStrainsInfo()
    progress = ShowProgress("Saved final annotations")
    strains_list = list(strains.allStrains())
    
    closure_iteration = currentClosureIteration()
    
    ensure_dir(parameters["DATASET_RESULTS_EXP_PHASE2"])
    ensure_dir(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PHASE2_IT"))
    ensure_dir(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_FINAL"))
    
    TASKS = []
    for strain_id in strains_list:
        TASKS.append(strain_id)
        
    n = len(TASKS)
    progress.setJobsCount(n)
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)   
        for r in pool.imap_unordered(filteroutShortAnnotations, TASKS):
            strain_id = r
            progress.update(desc=strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            strain_id = filteroutShortAnnotations(TASK)
            progress.update(desc=strain_id)
    
    print("save_final_annotations.py: Finished.")

