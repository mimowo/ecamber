import sys
import os
import multiprocessing


sys.path.append("../../")
from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_graph_utils import *
     
def chunks(l, n):
    i = 0
    chunk = l[:n]
    while chunk:
        yield chunk
        i += n
        chunk = l[i:i+n]
        
def readSeqEdgesStrain(params):
    overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strain_chunk = params[0]
    iteration = params[1]

    new_edges = set([])
    closure_iteration = currentClosureIteration()
    
    for strain_id in strain_chunk:
    
        input_fn = input_fn = getItPath(parameters, iteration, "DATASET_RESULTS_EXP_EDGES_MGS") + "/" + strain_id+".txt"
        if not os.path.exists(input_fn):
            return (strain_id, new_edges)
        
        input_fh = open(input_fn)
        
        for line in input_fh.readlines():
            edges_pair = line.strip()
            if len(edges_pair) < 2: continue
            new_edges.add(edges_pair)
        input_fh.close();
        
    return new_edges
    
if __name__ == '__main__':
    print("graph7_comb_multigene_edges.py: Combines the sequence graph edges between genes within the same multigene.")
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()

    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())

    strains_list = list(strains.allStrains())
    
    closure_iteration = currentClosureIteration()
        
    output_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", 0)
    ensure_dir(output_dir)

    strain_chunks = list(chunks(strains_list, 1 + int(len(strains_list) / WORKERS)))
    progress = ShowProgress("Combined multigene elt. edges")
    
    TASKS = []
    for strain_chunk in strain_chunks:
        TASKS.append((strain_chunk, closure_iteration))
        
    progress.setJobsCount(len(TASKS))
        
    all_edges = set([])
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap_unordered(readSeqEdgesStrain, TASKS):
            new_edges = r
            all_edges.update(new_edges)
            
            progress.update(str(len(all_edges)))
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = readSeqEdgesStrain(TASK)
            new_edges = r
            all_edges.update(new_edges)
            
            progress.update(str(len(all_edges)))

    graph_fn = output_dir +"edges_mgs.txt"
    graph_fh = open(graph_fn, "w")
    for edges_pair in all_edges:
        graph_fh.write(edges_pair + "\n")
    graph_fh.close()
    
    print("graph7_comb_multigene_edges.py: Finished.")
    
    