import sys
import os
import multiprocessing




sys.path.append("../../")
from soft.utils.camber_params_utils import *
from soft.structs.multigene import createStrainMultigeneId, Multigene
from soft.utils.camber_graph_utils import readBlastResultsMappingRev
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_coding_utils import getCodingMap, codeDNAseq
     
def createMGGraphStrain(strain_id):
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    readStrainSequences(strain)
    
    last_iteration = currentClosureIteration()
    multigenes = {}
    new_edges = set([])
    
    input_fn = getItPath(parameters, last_iteration, "DATASET_RESULTS_EXP_MGSSEQ", 0) + "/" + strain_id + ".txt"
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split("\t")
        if len(tokens) < 2:
            continue
        mg_id = tokens[0]
        multigenes[mg_id] = []
        tokens_seq = tokens[1].split(";")
        for token_seq in tokens_seq:
            tokens_len = token_seq.split(":")
            seq_id = int(tokens_len[1])
            multigenes[mg_id].append(seq_id)
    input_fh.close()

    for multigene_id in multigenes:   
        seq_ids_list = multigenes[multigene_id]
        if len(seq_ids_list) < 2:
            continue
        seq1_id = seq_ids_list[0]
        for i in range(1, len(seq_ids_list), 1):    
            seq2_id = seq_ids_list[i]
            if seq1_id < seq2_id:
                new_edges.add((seq1_id, seq2_id))
            else:
                new_edges.add((seq2_id, seq1_id))
    
    #if iteration in iterations:
    graph_fn = getItPath(parameters, last_iteration, "DATASET_RESULTS_EXP_EDGES_MGS") + "/" + strain_id+".txt"
    graph_fh = open(graph_fn, "w")
    for (node1_id, node2_id) in new_edges:
        graph_fh.write(str(node1_id) + "\t" + str(node2_id) + "\n")
    graph_fh.close()    
    
    return strain_id
    
if __name__ == '__main__':
    print("graph6_seq_multigene_edges.py: Computes the sequence graph edges between genes within the same multigene.")
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    closure_iteration = currentClosureIteration()

    strains = readStrainsInfo()
    ensure_dir(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_EDGES_MGS"))

    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    progress = ShowProgress("Saved multigene edges")
    n = len(strains.allStrains())
    progress.setJobsCount(n)

    strains_list = list(strains.allStrains())

    TASKS = []
    for strain_id in strains_list:
        TASKS.append(strain_id)
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap(createMGGraphStrain, TASKS):
            strain_id = r
            progress.update(strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = createMGGraphStrain(TASK)
            strain_id = r
            progress.update(strain_id)
                        
    print("graph6_seq_multigene_edges.py: Finished.")
    
    