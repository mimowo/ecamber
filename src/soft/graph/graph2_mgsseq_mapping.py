import sys
import os
import multiprocessing



sys.path.append("../../")
from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_graph_utils import *
from soft.utils.camber_coding_utils import codeDNAseq
from soft.structs.multigene import mgIntoSortableTriple
     
def getStrainAllSequences(params):
    
    overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    
    strain_id = params[0]
    iteration = params[1]
    
    strain = strains.strain(strain_id)
    
    readStrainSequences(strain)
    closure_iteration = currentClosureIteration()
    
    input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_FINAL") + strain_id+".txt"
    annotation = readGeneAnnotationsFromFile(input_fn, strain, False)
    gene_sequences = set([])

    bp_map = getCodingMap()
    mgs = {}
    
    for gene in list(annotation.genes.values()):
        mg_id = createStrainMultigeneId(gene.end, gene.strand, gene.strain, gene.contig_id)
        seq = gene.sequence()
        encoded_seq = codeDNAseq(seq, bp_map)
        gene_sequences.add(encoded_seq)
        if not mg_id in mgs:
            mgs[mg_id] = []
        mgs[mg_id].append(encoded_seq)

    blast_mapping = readBlastResultsMappingRev(parameters["DATASET_BLAST"]+"/blasts_mapping.txt", verb=False, sequences_subset=gene_sequences, decode=False)[0]
    seq_ids = set(blast_mapping.values())
    seq_lens = readBlastsMappingLens(parameters["DATASET_BLAST"]+"/blasts_mapping_lens.txt", subset_ids=seq_ids)
    text_lines = []
    
    mgs_sorted = sorted(mgs, key=lambda mg_id: mgIntoSortableTriple(mg_id))

    for mg_id in mgs_sorted:
        encoded_seqs = mgs[mg_id]
        text_line = mg_id + "\t"
        seq_pairs = []
        for encoded_seq in encoded_seqs:
            seq_id = blast_mapping.get(encoded_seq)
            seq_len = seq_lens[seq_id]
            seq_pairs.append((seq_len, seq_id))
        seq_pairs_sorted = sorted(seq_pairs)
        for (seq_len, seq_id) in seq_pairs_sorted:
            text_line += str(seq_len) + ":" + str(seq_id) + ";"
        text_line = text_line[:-1]
        text_lines.append(text_line)
    
    output_fn = getItPath(parameters, iteration, "DATASET_RESULTS_EXP_MGSSEQ", 0) + "/" + strain_id + ".txt"
    output_fh = open(output_fn, "w")
    for text_line in text_lines:
        output_fh.write(text_line + "\n")
    output_fh.close()
#    
    return strain_id, iteration, seq_ids
    
if __name__ == '__main__':
    print("save_mgsseq_mapping_all.py: Saves sequence ids by strains after the last iteration.")
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()

    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    closure_iteration = currentClosureIteration() 
    all_seq_ids = set([])
 
    strains_list = list(strains.allStrains())

    ensure_dir(parameters["DATASET_RESULTS_EXP_PHASE2"])
    ensure_dir(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PHASE2_IT"))

    output_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", 0)
    ensure_dir(output_dir)  
    
    ensure_dir(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_MGSSEQ", 0))

    progress = ShowProgress("Saved sequence ids, it=" + str(closure_iteration))
    n = len(strains.allStrains())
    progress.setJobsCount(n)
         
    TASKS = []
    for strain_id in strains_list:
        TASKS.append((strain_id, closure_iteration))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)        
        for r in pool.imap_unordered(getStrainAllSequences, TASKS):
            strain_id, iteration, new_seq_ids = r
            all_seq_ids.update(new_seq_ids)                      
            progress.update(strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = getStrainAllSequences(TASK)
            strain_id, iteration, new_seq_ids = r  
            all_seq_ids.update(new_seq_ids)
            progress.update(strain_id)
    
    output_fn = output_dir + "seqids_all.txt"
    output_fh = open(output_fn, "w")
    
    for seq_id in sorted(all_seq_ids):
        output_fh.write(str(seq_id) + "\n")
    
    output_fh.close()

    print("save_mgsseq_mapping_all.py: Finished.")

