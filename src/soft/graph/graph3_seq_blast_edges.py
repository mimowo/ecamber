import sys
import os
import time
import multiprocessing

sys.path.append("../../")
from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_graph_utils import *
from soft.utils.camber_coding_utils import codeDNAseq
     
def createGraphStrain(strain_id):
    
    overwriteParameters(sys.argv)
    parameters = readParameters()
    
    closure_iteration = currentClosureIteration()
    
    multigenes = {}
    seqids_final = set([])
    
    input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", 0) + "seqids_all.txt"
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        line = line.strip()
        if len(line) > 0:
            seqids_final.add(int(line))
    input_fh.close()   
    
    input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_MGSSEQ", 0) + "/" + strain_id + ".txt"
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split("\t")
        if len(tokens) < 2:
            continue
        mg_id = tokens[0]
        multigenes[mg_id] = {}
        tokens_seq = tokens[1].split(";")
        for token_seq in tokens_seq:
            tokens_len = token_seq.split(":")
            seq_len = int(tokens_len[0])
            seq_id = int(tokens_len[1])
            
            multigenes[mg_id][seq_len] = seq_id
    input_fh.close()

    for iteration in range(closure_iteration+1):
        all_edges = set([])
        folder_out = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_EDGES_SEQ") + "edges_"+str(iteration) + "/"
        
        input_fn = getItPath(parameters, iteration, "DATASET_RESULTS_EXP_BLAST_ACC") + "/" + strain_id + ".txt"
        if not os.path.exists(input_fn):
            continue
        
        output_fn = folder_out + "/" + strain_id + "-" + str(iteration) + ".txt"

        blast_orfs_fh = open(input_fn)
        lines = blast_orfs_fh.readlines()
        blast_orfs_fh.close()
        
        
        for line in lines:
            tokens = line.split()
            seq1_id = int(tokens[0])
            start = int(tokens[1])
            end = int(tokens[2])
            seq_len = max(start, end) - min(start, end) + 1
            strand = tokens[3]
                        
            if len(tokens) >= 5: contig_id = tokens[4]
            else: contig_id = strain_id
                
            if not seq1_id in seqids_final:
                continue
                
            mg_id = createStrainMultigeneId(end, strand, strain_id, contig_id)
            
            mg_lens_map = multigenes.get(mg_id)
            if mg_lens_map == None:
                continue
            seq2_id = mg_lens_map.get(seq_len)
            if seq2_id == None:
                continue
            
            elif seq1_id < seq2_id: all_edges.add((seq1_id, seq2_id))
            elif seq2_id < seq1_id: all_edges.add((seq2_id, seq1_id))
#        
        output_fh = open(output_fn, "w")
        for (node1_id, node2_id) in all_edges:
            output_fh.write(str(node1_id) + "\t" + str(node2_id) + "\n")
        output_fh.close()
        
    return strain_id
    
if __name__ == '__main__':
    print("graph3_seq_blast_edges.py: Computes the sequence graph edges between BLAST hits.")
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()

    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())

    progress = ShowProgress("Saved BLAST edges")
    n = len(strains.allStrains())
    progress.setJobsCount(n)

    strains_list = list(strains.allStrains())

    closure_iteration = currentClosureIteration()
    folder_out = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_EDGES_SEQ")
    ensure_dir(folder_out)
    
    for iteration in range(0, closure_iteration + 1, 1):
        folder_out_it = folder_out + "edges_"+str(iteration) + "/"
        ensure_dir(folder_out_it)
    
    TASKS = strains_list
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap_unordered(createGraphStrain, TASKS):
            strain_id = r                      
            progress.update(strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = createGraphStrain(TASK)
            strain_id = r  
            progress.update(strain_id)        
    
    print("graph3_seq_blast_edges.py: Finished.")
    