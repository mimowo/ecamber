import sys
import os

import multiprocessing
import shutil

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *

def removePrefixesInput(parameters, strains):
    progress = ShowProgress("Removed prefixes")
    n = len(strains.allStrains())
    progress.setJobsCount(n)
    
    for strain_id in strains.allStrains():
        src_fn = parameters["DATASET_GENOMES"] + "seq-" + strain_id + ".fasta"
        dst_fn = parameters["DATASET_GENOMES"] + strain_id + ".fasta"
        if os.path.exists(src_fn) and not os.path.exists(dst_fn): os.rename(src_fn, dst_fn)

        src_fn = parameters["DATASET_GENOMES_DBS"] + "seq-" + strain_id + ".nhr"
        if os.path.exists(src_fn): os.remove(src_fn) 
        src_fn = parameters["DATASET_GENOMES_DBS"] + "seq-" + strain_id + ".nin"
        if os.path.exists(src_fn): os.remove(src_fn)
        src_fn = parameters["DATASET_GENOMES_DBS"] + "seq-" + strain_id + ".nsq"
        if os.path.exists(src_fn): os.remove(src_fn)
        
        src_fn = parameters["DATASET_GENOMES_TMP"] + "seq-" + strain_id + ".fasta"
        dst_fn = parameters["DATASET_GENOMES_TMP"] + strain_id + ".fasta"
        if os.path.exists(src_fn) and not os.path.exists(dst_fn): os.rename(src_fn, dst_fn)


        src_fn = parameters["DATASET_GENBANKS_REFSEQ"] + "gbf-" + strain_id + ".txt"
        dst_fn = parameters["DATASET_GENBANKS_REFSEQ"] + strain_id + ".txt"
        if os.path.exists(src_fn) and not os.path.exists(dst_fn): os.rename(src_fn, dst_fn)

        src_fn = parameters["DATASET_GENBANKS_PATRIC"] + "gbf-" + strain_id + ".txt"
        dst_fn = parameters["DATASET_GENBANKS_PATRIC"] + strain_id + ".txt"
        if os.path.exists(src_fn) and not os.path.exists(dst_fn): os.rename(src_fn, dst_fn)
    
        src_fn = parameters["DATASET_GENBANKS_OTHER"] + "gbf-" + strain_id + ".txt"
        dst_fn = parameters["DATASET_GENBANKS_OTHER"] + strain_id + ".txt"
        if os.path.exists(src_fn) and not os.path.exists(dst_fn): os.rename(src_fn, dst_fn)

        src_fn = parameters["DATASET_FEATURES_REFSEQ"] + "cds-" + strain_id + ".tab"
        dst_fn = parameters["DATASET_FEATURES_REFSEQ"] + strain_id + ".tab"
        if os.path.exists(src_fn) and not os.path.exists(dst_fn): os.rename(src_fn, dst_fn)

        src_fn = parameters["DATASET_FEATURES_PATRIC"] + "cds-" + strain_id + ".tab"
        dst_fn = parameters["DATASET_FEATURES_PATRIC"] + strain_id + ".tab"
        if os.path.exists(src_fn) and not os.path.exists(dst_fn): os.rename(src_fn, dst_fn)

        src_fn = parameters["DATASET_FEATURES_OTHER"] + "cds-" + strain_id + ".tab"
        dst_fn = parameters["DATASET_FEATURES_OTHER"] + strain_id + ".tab"
        if os.path.exists(src_fn) and not os.path.exists(dst_fn): os.rename(src_fn, dst_fn)

        src_fn = parameters["DATASET_ANN_CDS_PATRIC"] + "ann-" + strain_id + ".txt"
        dst_fn = parameters["DATASET_ANN_CDS_PATRIC"] + strain_id + ".txt"
        if os.path.exists(src_fn) and not os.path.exists(dst_fn): os.rename(src_fn, dst_fn)

        src_fn = parameters["DATASET_ANN_CDS_REFSEQ"] + "ann-" + strain_id + ".txt"
        dst_fn = parameters["DATASET_ANN_CDS_REFSEQ"] + strain_id + ".txt"
        if os.path.exists(src_fn) and not os.path.exists(dst_fn): os.rename(src_fn, dst_fn)

        src_fn = parameters["DATASET_ANN_CDS_OTHER"] + "ann-" + strain_id + ".txt"
        dst_fn = parameters["DATASET_ANN_CDS_OTHER"] + strain_id + ".txt"
        if os.path.exists(src_fn) and not os.path.exists(dst_fn): os.rename(src_fn, dst_fn)

        src_fn = parameters["DATASET_ANN_GBF_PATRIC"] + "ann-" + strain_id + ".txt"
        dst_fn = parameters["DATASET_ANN_GBF_PATRIC"] + strain_id + ".txt"
        if os.path.exists(src_fn) and not os.path.exists(dst_fn): os.rename(src_fn, dst_fn)

        src_fn = parameters["DATASET_ANN_GBF_REFSEQ"] + "ann-" + strain_id + ".txt"
        dst_fn = parameters["DATASET_ANN_GBF_REFSEQ"] + strain_id + ".txt"
        if os.path.exists(src_fn) and not os.path.exists(dst_fn): os.rename(src_fn, dst_fn)

        src_fn = parameters["DATASET_ANN_GBF_OTHER"] + "ann-" + strain_id + ".txt"
        dst_fn = parameters["DATASET_ANN_GBF_OTHER"] + strain_id + ".txt"
        if os.path.exists(src_fn) and not os.path.exists(dst_fn): os.rename(src_fn, dst_fn)

        progress.update()

    
 
if __name__ == '__main__':
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    input1_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains.txt"
    input2_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains-all.txt"
    if not os.path.exists(input1_fn) and os.path.exists(input2_fn): input_fn = input2_fn
    else: input_fn = input1_fn
    
    input_fh = open(input_fn)
    strains = readStrainsFromFile(input_fh);
    input_fh.close()
        
    removePrefixesInput(parameters, strains)
            