import sys
import os
import multiprocessing


sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import ensure_dir, readStrainsFromFile
from soft.utils.camber_format_utils import convertFeatureTab, convertGenBank


def convertAnns(strain_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains_tmp.txt"
    if not os.path.exists(input_fn):
        input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains.txt"
    input_fh = open(input_fn)
    strains = readStrainsFromFile(input_fh)
    input_fh.close()
    
    acc_map = {}
    
    input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "contig_acc_map.txt"
    if os.path.exists(input_fn):
        input_fh = open(input_fn)
        for line in input_fh.readlines():
            tokens = line.split()
            if len(tokens) < 2:
                continue
            acc_map[tokens[0]] = tokens[1]
        input_fh.close()
    
    strain = strains.strain(strain_id)
    readStrainSequences(strain)
    
    for src in ["patric", "refseq"]:
        input_dir = parameters["DATASET_ANNS_INPUT"] + src + "/"
        output_dir = parameters["DATASET_ANNS_PARSED"] + src + "/"
        
        input_fn = input_dir + strain_id + ".tab"
        if not os.path.exists(input_fn): input_fn = input_dir + strain_id + ".txt"
        if not os.path.exists(input_fn): input_fn = input_dir + strain_id + ".cds"
        
        output_fn = output_dir + strain_id + ".txt"
        
        if os.path.exists(input_fn) and os.path.getsize(input_fn) > 10:
            output_fh = open(output_fn, "w")
            input_fh = open(input_fn)
            convertFeatureTab(strain, input_fh, output_fh, acc_map, "cds")
            input_fh.close()
            output_fh.close()
            
        input_dir = parameters["DATASET_ANNS_INPUT"] + src + "_gbk/"
        output_dir = parameters["DATASET_ANNS_PARSED"] + src + "_gbk/"
        
        input_fn = input_dir + strain_id + ".gbk"
        if not os.path.exists(input_fn): input_fn = input_dir + strain_id + ".txt"

        output_fn = output_dir + strain_id + ".txt"
        
        if os.path.exists(input_fn) and os.path.getsize(input_fn) > 10:
            output_fh = open(output_fn, "w")
            input_fh = open(input_fn)
            convertGenBank(strain, input_fh, output_fh, acc_map, "cds")
            input_fh.close()
            output_fh.close()        
    return strain_id

if __name__ == '__main__':

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains_tmp.txt"
    if not os.path.exists(input_fn):
        input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains.txt"
    input_fh = open(input_fn)
    strains = readStrainsFromFile(input_fh)
    input_fh.close()
    
    strains_list = strains.allStrains()
    print("format_annotations.py: Formats annotations.")

    for src in ["patric", "refseq"]:

        input_dir = parameters["DATASET_ANNS_INPUT"] + src + "/"
        output_dir = parameters["DATASET_ANNS_PARSED"] + src + "/"

        if os.path.exists(input_dir):
            ensure_dir(output_dir)

        input_dir = parameters["DATASET_ANNS_INPUT"] + src + "_gbk/"
        output_dir = parameters["DATASET_ANNS_PARSED"] + src + "_gbk/"
        if os.path.exists(input_dir):
            ensure_dir(output_dir)
      #  print(output_dir)

    n = len(strains_list)
    progress = ShowProgress("Formatted annotations")
    progress.setJobsCount(n)
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    TASKS = strains_list
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)
        for r in pool.imap_unordered(convertAnns, TASKS):
            progress.update(desc=r)    
        pool.close() 
        pool.join()
    else:
        for TASK in TASKS:
            r = convertAnns(TASK)
            progress.update(desc=r)
            
    print("format_annotations.py: Finished.")
    
