import sys
import os

import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *


def updateParameters(parameters):
    
    parameters["DATASET_NEW"] = parameters["D"] + "_new"
    parameters["ECAMBER_DATASET_PATH_NEW"] = parameters["ECAMBER_DATASET_PATH"] 
    parameters["DATASET_ANN_OTHER"] = parameters["ECAMBER_DATASET_PATH"] + "/annotations/"
    parameters["DATASET_GENBANKS_NEW"] = parameters["ECAMBER_DATASET_PATH_NEW"] + "/genbanks/"
    parameters["DATASET_GENOMES_TMP_NEW"] = parameters["ECAMBER_DATASET_PATH_NEW"] + "/genomes_tmp/"
    parameters["DATASET_GENOMES_OLD"] = parameters["ECAMBER_DATASET_PATH_NEW"] + "/genomes_old/"
    parameters["DATASET_FEATURES_NEW"] = parameters["ECAMBER_DATASET_PATH_NEW"] + "/features/"
    parameters["DATASET_ANN_OTHER_NEW"] = parameters["ECAMBER_DATASET_PATH_NEW"] + "/anns_cds/"
    return parameters

def readOldStrainsInfo(input_fn):
    input_fh = open(input_fn)
    lines = input_fh.readlines()
    input_fh.close()
    strains_old = {}
    
    for line in lines:
        tokens = line.split()
        if len(tokens) == 1:
            strain1_id = tokens[0]
            if not strain1_id in strains_old:
                strains_old[strain1_id] = []
            strains_old[strain1_id].append(strain1_id)
        elif len(tokens) >= 2:
            strain1_id = tokens[0]
            strain2_id = tokens[1]
            if strain2_id == "-":
                if not strain1_id in strains_old:
                    strains_old[strain1_id] = []
                strains_old[strain1_id].append(strain1_id)
            else:
                if not strain2_id in strains_old:
                    strains_old[strain2_id] = []
                strains_old[strain2_id].append(strain1_id)
    return strains_old


def convertInputStrain(strain_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    parameters = updateParameters(parameters)
    
    old_strains = readOldStrainsInfo(parameters_map["ECAMBER_DATASET_PATH"] + "strains-all.txt")        
    
    strain_contigs = old_strains[strain_id] 
    ensure_dir(parameters["ECAMBER_DATASET_PATH_NEW"] + "/genbanks/")
        
    output_fh = open(parameters["DATASET_ANN_OTHER_NEW"] + strain_id+".txt", "w")    
    for contig_id in strain_contigs:
        input_fn = parameters["DATASET_ANN_OTHER"] + "ann-"+contig_id+".txt"
        if not os.path.exists(input_fn): continue
        input_fh = open(input_fn)

        for line in input_fh.readlines():
            line = line.strip()
            tokens = line.split("\t")
            line_upper = line.upper()
            if line_upper.count("LENGTH") > 0 and line_upper.count("NAME") > 0 and line_upper.count("LOCUS") > 0:
                continue
            if line_upper.count("GENOME") > 0 and line_upper.count("COMPLETE") > 0:
                continue
            if len(tokens) < 4:
                continue
            elif len(tokens) in [4, 5]:
                gene_id = tokens[0]
                start = int(tokens[1])
                end = int(tokens[2])
                strand = tokens[3]
                if len(tokens) >= 5:
                    ann_id = tokens[4]
                else:
                    ann_id = ""
                output_fh.write(gene_id + "\t" + str(start) + "\t" + str(end) + "\t" + strand +"\t" + ann_id + "\t" + contig_id + "\n")
            elif len(tokens) in [8, 9, 10]:
             #   print(line)
                gene_id = tokens[8]
                start = int(tokens[1])
                end = int(tokens[2])
                strand = tokens[3]
                ann_id = tokens[7]
                if len(ann_id) < 2: ann_id = ""
                output_fh.write(gene_id + "\t" + str(start) + "\t" + str(end) + "\t" + strand +"\t" + ann_id + "\t" + contig_id + "\n")
                
        input_fh.close()
    output_fh.close()

    output_fh = open(parameters["ECAMBER_DATASET_PATH_NEW"] + "/genbanks/" + strain_id+".txt", "w")    
    for contig_id in strain_contigs:
        input_fn = parameters["ECAMBER_DATASET_PATH"] + "/genbanks_tmp/" + "gb-"+contig_id+".gb"
        if not os.path.exists(input_fn): continue

        
        input_fh = open(input_fn)
        for line in input_fh.readlines():
            line = line.strip()
            
            tokens = line.split()
            
            if len(tokens) > 0 and tokens[0] == "ACCESSION":
                output_fh.write(tokens[0] + "\t" + contig_id + "\n")
            else:
                output_fh.write(line + "\n")
        input_fh.close()
    output_fh.close()
    
    output_fh = open(parameters["DATASET_GENOMES_TMP_NEW"] + strain_id+".fasta", "w")
    
    for contig_id in strain_contigs:
        input_fn = parameters["DATASET_GENOMES_OLD"]+"seq-"+contig_id+".fasta"
        if not os.path.exists(input_fn):
            input_fn = parameters["DATASET_GENOMES_OLD"] + contig_id+".fasta"
        seq_map = readSequenceFromFile(input_fn)
        
        #print("error", seq_map, parameters["DATASET_GENOMES_TMP"] + contig_id+".fasta")
        
        sequence = list(seq_map.values())[0]
        writeFASTAseq(output_fh, sequence, contig_id)
    output_fh.close()
    
    return strain_id

def saveNewStrains(output_fn, strains_list):
    output_fh = open(output_fn, "w")
    for strain_id in old_strains:
        output_fh.write(strain_id + "\n")
    
    output_fh.close()
    return None
        
if __name__ == '__main__':

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    parameters = updateParameters(parameters)
 
    print("format_input_2to3.py: Converts CAMBer input 2 to 3.")
    
    old_strains = readOldStrainsInfo(parameters_map["ECAMBER_DATASET_PATH"] + "strains-all.txt")
    
    print(old_strains)
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    ensure_dir(parameters["ECAMBER_DATASET_PATH_NEW"])
    ensure_dir(parameters["DATASET_GENOMES_TMP_NEW"])
    ensure_dir(parameters["DATASET_ANN_OTHER_NEW"])
    
    strains_list = list(old_strains)
  
    progress = ShowProgress("Formatted CAMBer input 2 to 3")
    n = len(strains_list)
    progress.setJobsCount(n)
    
    TASKS = strains_list
    
    saveNewStrains(parameters_map["ECAMBER_DATASET_PATH_NEW"] + "strains.txt", old_strains)
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap_unordered(convertInputStrain, TASKS):
            progress.update(desc=r)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = convertInputStrain(TASK)
            progress.update(desc=r)

    print("format_input_2to3.py: Finished.")
    