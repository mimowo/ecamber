import sys
import os

import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *

def updateParameters(parameters): 
    parameters["DATASET_OLD"] = parameters["D"] + "_old"
    parameters["ECAMBER_DATASET_PATH_OLD"] = parameters["ECAMBER_DATASETS"] + "/" + parameters["DATASET_OLD"]
    parameters["DATASET_GENOMES_TMP_OLD"] = parameters["ECAMBER_DATASET_PATH_OLD"] + "/genomes/"
    parameters["DATASET_GENBANKS_OLD"] = parameters["ECAMBER_DATASET_PATH_OLD"] + "/genbanks/"
    parameters["DATASET_FEATURES_OLD"] = parameters["ECAMBER_DATASET_PATH_OLD"] + "/features/"
    parameters["DATASET_ANN_OTHER_OLD"] = parameters["ECAMBER_DATASET_PATH_OLD"] + "/anns_cds/"
    return parameters

def convertInputStrain(strain_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    parameters = updateParameters(parameters)
    
    strains = readStrainsInfo() 
    strain = strains.strain(strain_id)       
    readStrainSequences(strain)
    strain_id, annotation = readOrgAnnotations(strain)
    
    ensure_dir(parameters["DATASET_GENBANKS_OLD"])
    ensure_dir(parameters["DATASET_FEATURES_OLD"])
    ensure_dir(parameters["DATASET_ANN_OTHER_OLD"])
    
    if strain.sequence == None:
        contigs_sorted = sorted(strain.sequences_map, key=lambda contig_id: -len(strain.sequences_map[contig_id]))
        
        for i in range(0, len(contigs_sorted), 1):
            contig_id = contigs_sorted[i]
            contig_id_out = contig_id
            if i == 0: contig_id_out = strain_id
            
            output_fn = parameters["DATASET_ANN_OTHER_OLD"] + "ann-" + contig_id_out + ".txt"
            output_fh = open(output_fn, "w")
            for gene in annotation.genes.values():
                if gene.contig_id != contig_id:
                    continue
                text_line = gene.gene_id + "\t"
                text_line += str(gene.start) + "\t"
                text_line += str(gene.end) + "\t"
                text_line += gene.strand + "\t"
                if len(gene.gene_name) > 1:
                    text_line += "\t" + gene.gene_name
                text_line += "\n"
                output_fh.write(text_line)
            output_fh.close()
    else:
        output_fn = parameters["DATASET_ANN_OTHER_OLD"] + "ann-"+strain_id+".txt"
        output_fh = open(output_fn, "w")
        for gene in annotation.genes.values():
            text_line = gene.gene_id + "\t"
            text_line += str(gene.start) + "\t"
            text_line += str(gene.end) + "\t"
            text_line += gene.strand
            if len(gene.gene_name) > 1:
                text_line += "\t" + gene.gene_name
            text_line += "\n"
            output_fh.write(text_line)
        output_fh.close() 

    if strain.sequence == None:
        contigs_sorted = sorted(strain.sequences_map, key=lambda contig_id: -len(strain.sequences_map[contig_id]))
        
        contig0_id = contigs_sorted[0]
        sequence = strain.sequences_map[contig0_id]
        
        output_fh = open(parameters["DATASET_GENOMES_TMP_OLD"] + "seq-" + strain_id + ".fasta", "w")    
        writeFASTAseq(output_fh, sequence, strain_id)
        output_fh.close()        
        
        for contig_id in contigs_sorted[1:]:
            output_fh = open(parameters["DATASET_GENOMES_TMP_OLD"] + "seq-" + contig_id + ".fasta", "w")
            
            sequence = strain.sequences_map[contig_id]
            writeFASTAseq(output_fh, sequence, contig_id)
            output_fh.close()
    else:
        output_fh = open(parameters["DATASET_GENOMES_TMP_OLD"] + "seq-" + strain_id + ".fasta", "w")
        sequence = strain.sequence
        writeFASTAseq(output_fh, sequence, strain_id)
        output_fh.close()
    
    
    return strain_id

def saveOldStrains(output_fn, strains):
    output_fh = open(output_fn, "w")
    for strain_id in strains.allStrains():
        output_fh.write(strain_id + "\t-\n")
    for strain_id in strains.allStrains():
        strain = strains.strain(strain_id)
        readStrainSequences(strain)
        print(strain_id, strain.sequences_map.keys())
        if strain.sequence == None:
            contigs_sorted = sorted(strain.sequences_map, key=lambda contig_id: -len(strain.sequences_map[contig_id]))
            for contig_id in contigs_sorted[1:]:
                output_fh.write(contig_id + "\t" + strain_id + "\n")
        else:
            continue
    output_fh.close()
    return None
        
if __name__ == '__main__':

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    parameters = updateParameters(parameters)
 
    print("format_input_3to2.py: Converts eCAMBer input to CAMBer.")
    
    new_strains = readStrainsInfo()
    
    print(new_strains)
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    ensure_dir(parameters["ECAMBER_DATASET_PATH_OLD"])
    ensure_dir(parameters["DATASET_GENOMES_TMP_OLD"])
    ensure_dir(parameters["DATASET_ANN_OTHER_OLD"])
    
    strains_list = list(new_strains.allStrains())
  
    progress = ShowProgress("Formatted CAMBer input 2 to 3")
    n = len(strains_list)
    progress.setJobsCount(n)
    
    TASKS = strains_list
    
    saveOldStrains(parameters_map["ECAMBER_DATASET_PATH_OLD"] + "/strains-all.txt", new_strains)
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap_unordered(convertInputStrain, TASKS):
            progress.update(desc=r)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = convertInputStrain(TASK)
            progress.update(desc=r)

    print("format_input_3to2.py: Finished.")
        