import sys
import os
import multiprocessing


sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import ensure_dir, readStrainsFromFile

def runFormatDB(formatdb_exe, genome_seq_dir, genome_db_dir, strain_id, log_fn = ""):
    
    genome_fn  =genome_seq_dir + strain_id + ".fasta"
    command = formatdb_exe + " -p F -i " + genome_fn
    if len(log_fn) > 0:
        command += " -l" + log_fn
    os.system(command)
    
    in_fn  = genome_seq_dir + strain_id + ".fasta.nhr"
    out_fn = genome_db_dir + strain_id + ".nhr"
    if os.path.exists(out_fn):
        os.remove(out_fn)
    os.rename(in_fn, out_fn)

    in_fn  = genome_seq_dir + strain_id + ".fasta.nin"
    out_fn = genome_db_dir + strain_id + ".nin"
    if os.path.exists(out_fn):
        os.remove(out_fn)
    os.rename(in_fn, out_fn)
    
    in_fn  = genome_seq_dir + strain_id + ".fasta.nsq"
    out_fn = genome_db_dir + strain_id + ".nsq"
    if os.path.exists(out_fn):
        os.remove(out_fn)
    os.rename(in_fn, out_fn)    
    
    return None

def runMakeBlastDB(makedb_exe, genome_seq_dir, genome_db_dir, strain_id, log_fn = ""):
    genome_fn  = genome_seq_dir + strain_id + ".fasta"
    
    command = makedb_exe + " -in " + genome_fn + " -dbtype nucl"
    command += " -out " + genome_db_dir + strain_id
    command += " -logfile "+ log_fn
    
    os.system(command)
    return None

def prepareDatabase(strain_id):
    overwriteParameters(sys.argv)
    parameters = readParameters()

    genome_seq_dir = parameters["DATASET_GENOMES"]
    genome_db_dir = parameters["DATASET_GENOMES_DBS"]
    log_fn = parameters["ECAMBER_DATASET_PATH"] + "formatdb.log"

    if parameters["BLAST_VER"].upper() == "BLAST+":
        makedb_exe = parameters["MAKEBLASTDB_PATH"]
        runMakeBlastDB(makedb_exe, genome_seq_dir, genome_db_dir, strain_id, log_fn)
    elif parameters["BLAST_VER"].upper() == "BLASTALL":
        formatdb_exe = parameters["FORMATDB_PATH"]
        runFormatDB(formatdb_exe, genome_seq_dir, genome_db_dir, strain_id, log_fn)
    
    return strain_id    

if __name__ == '__main__':
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    print("prepare_databases.py: Formats sequences databases for BLAST.")
    print("Logs are saved to: " + parameters["DATASET_GENOMES"] + "formatdb.log")
    
    ensure_dir(parameters["DATASET_GENOMES_DBS"])
    
    input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains_tmp.txt"
    if not os.path.exists(input_fn):
        input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains.txt"
    input_fh = open(input_fn)
    strains = readStrainsFromFile(input_fh)
    input_fh.close()
    
    strains_list = list(strains.allStrains())
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    n = len(strains_list)
    
    progress = ShowProgress("Formatted BLAST databases")
    progress.setJobsCount(n)
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)   
        for r in pool.imap_unordered(prepareDatabase, strains_list):
            progress.update(desc=r)
        pool.close()
        pool.join()
    else:
        for strain_id in strains_list:
            r = prepareDatabase(strain_id)
            progress.update(desc=r)        
        
    print("prepare_databases.py: Finished.")
    