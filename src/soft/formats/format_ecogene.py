import sys
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import ensure_dir, readStrainsFromFile
from soft.utils.camber_format_utils import *


def convertAnns(strain_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains_tmp.txt"
    if not os.path.exists(input_fn):
        input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains.txt"
    input_fh = open(input_fn)
    strains = readStrainsFromFile(input_fh)
    input_fh.close()
    
    strain = strains.strain(strain_id)
    readStrainSequences(strain)
    
    input_fn = parameters["DATASET_ANNS_INPUT"] + "/ecogene/" + strain_id+".txt"
    output_fn = parameters["DATASET_ANNS_PARSED"] + "/ecogene/" + strain_id+".txt"
    
    if os.path.exists(input_fn):
        output_fh = open(output_fn, "w")
        annotation = readGeneAnnotationsFromFile(input_fn, strain, check=False)
        
        for gene in annotation.genes.values():
            text_line = gene.gene_id + "\t"
            text_line += str(gene.start) + "\t"
            text_line += str(gene.end) + "\t"
            text_line += str(gene.strand) + "\t"
            text_line += str(gene.contig_id) + "\n"
            output_fh.write(text_line)
        output_fh.close()
        
    if os.path.exists(output_fn) and os.path.getsize(output_fn)< 10:
        os.remove(output_fn)
    return strain_id

if __name__ == '__main__':

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains_tmp.txt"
    if not os.path.exists(input_fn):
        input_fn = parameters_map["ECAMBER_DATASET_PATH"] + "strains.txt"
    input_fh = open(input_fn)
    strains = readStrainsFromFile(input_fh)
    input_fh.close()
    
    ensure_dir(parameters["DATASET_ANNS_PARSED"] + "/coliscope/")
        
    strains_list = strains.allStrains()
    print("format_coliscope.py: formats ColiScope annotations")
                 
    n = len(strains_list)
    progress = ShowProgress("Formatted ColiScope annotations")
    progress.setJobsCount(n)
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())

    TASKS = strains_list
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)
        for r in pool.imap_unordered(convertAnns, TASKS):
            progress.update(desc=r)    
        pool.close() 
        pool.join()
    else:
        for TASK in TASKS:
            r = convertAnns(TASK)
            progress.update(desc=r)
            
    print("format_coliscope.py: Finished.")

