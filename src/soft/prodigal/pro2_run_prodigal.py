import sys
import os
import time
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_closure_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_progress import *

def runProdigal(strain_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    genomes_dir = parameters["DATASET_GENOMES"]
    output_dir = parameters["DATASET_PRODIGAL"]
    train_dir = parameters["DATASET_PRODIGAL_TRAIN"]
    scores_dir = parameters["DATASET_PRODIGAL_SCORES"]
    #genes_dir = parameters["DATASET_PRODIGAL_GENES"]
    prodigal_exe = parameters["PRODIGAL_PATH"]
    
    
    command = prodigal_exe + " -q -c -f gbk"
    command += " -t " + train_dir + strain_id
    command += " -o " + parameters["DATASET_ANNS_INPUT"] + "prodigal/" + strain_id + ".txt"
    command += " -s " + scores_dir + strain_id + ".txt"
    command += " -i " + genomes_dir + strain_id + ".fasta"

    os.system(command);
    return strain_id

if __name__ == '__main__':
    s_time = time.time()
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()

    print("pro2_run_prodigal.py: computes Prodigal to generate annotations.")


    ensure_dir(parameters["DATASET_PRODIGAL"])
    ensure_dir(parameters["DATASET_PRODIGAL_TRAIN"])
    ensure_dir(parameters["DATASET_PRODIGAL_SCORES"])
    ensure_dir(parameters["DATASET_ANNS_INPUT"] + "prodigal/")
   
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    strains_list = list(strains.allStrains())
    n = len(strains_list)
    
    progress = ShowProgress("Computed Prodigal")
    progress.setJobsCount(n)
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap_unordered(runProdigal, strains_list):
            progress.update(desc=r)
        pool.close()
        pool.join()
    else:
        for strain_id in strains_list:
            r = runProdigal(strain_id)
            progress.update(desc=r)
            
    print("pro2_run_prodigal.py: Finished.")
    e_time = time.time()
    print("Exe time: " + str(e_time - s_time))

