import sys
import os
import time
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_closure_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_progress import *

def runProdigal(strain_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    genomes_dir = parameters["DATASET_GENOMES"]
    output_dir = parameters["DATASET_PRODIGAL_TRAIN"]
    prodigal_exe = parameters["PRODIGAL_PATH"]
    
    output_fn = output_dir + strain_id
    if os.path.exists(output_fn):
        os.remove(output_fn)
    
    command = prodigal_exe + " -q "
    command += " -t " + output_dir + strain_id
    command += " -i " + genomes_dir + strain_id + ".fasta"
    
    os.system(command);
    return strain_id

if __name__ == '__main__':
    s_time = time.time()
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()

    print("pro1_train_prodigal.py: generates Prodigal training files.")

    ensure_dir(parameters["DATASET_PRODIGAL"])
    ensure_dir(parameters["DATASET_PRODIGAL_TRAIN"])
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    strains_list = list(strains.allStrains())
    n = len(strains_list)
    
    progress = ShowProgress("Prodigal training")
    progress.setJobsCount(n)
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap_unordered(runProdigal, strains_list):
            progress.update(desc=r)
        pool.close()
        pool.join()
    else:
        for strain_id in strains_list:
            r = runProdigal(strain_id)
            progress.update(desc=r)
            
    print("pro1_train_prodigal.py: Finished.")
    e_time = time.time()
    print("Exe time: " + str(e_time - s_time))

