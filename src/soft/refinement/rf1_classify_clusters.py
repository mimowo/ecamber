import sys
import time
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_graph_utils import *
from soft.structs.clusters import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *

def saveNodesStrain(params):
    strain, non_anchor_ccs, closure_iteration, ph2_iteration = params
    overwriteParameters(sys.argv)
    parameters = readParameters() 

    strain_id = strain.strain_id
    output_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_REFINEMENT", ph2_iteration)
    
    non_anchor_mgs = {}
    input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_UNIANNS", ph2_iteration) + "/" +  strain_id +".txt"
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        tokens = line.split("\t")
        if len(tokens) < 2:
            continue
        cluster_id = tokens[0]
        if not cluster_id in non_anchor_ccs:
            continue
        non_anchor_mgs[tokens[1]] = cluster_id
    input_fh.close()
    
    mg_seqs_map = {}
    input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_MGSSEQ", ph2_iteration) + "/" +  strain_id +".txt"
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split("\t")
        if len(tokens) < 2:
            continue
        mg_id = tokens[0]
        if not mg_id in non_anchor_mgs:
            continue
        mg_seqs_map[mg_id] = []
        seq_lenid_tokens = tokens[1].split(";")
        for seq_lenid_token in seq_lenid_tokens:
            seq_id = int(seq_lenid_token.split(":")[1])
            mg_seqs_map[mg_id].append(seq_id)
    input_fh.close()
    
    ensure_dir(output_dir + "rf_nodes/")
    
    output_fh = open(output_dir + "rf_nodes/" + strain_id + ".txt", "w")
    for mg_id in non_anchor_mgs:
        cluster_id = non_anchor_mgs[mg_id]
        text_line = cluster_id + "\t" + mg_id + "\t"
        for seq_id in mg_seqs_map[mg_id]:
            text_line += str(seq_id) + ":"
        text_line = text_line[:-1]
        output_fh.write(text_line + "\n")
    output_fh.close()
    
    return strain_id

if __name__ == '__main__':
    print("rf1_classify_components.py: classify clusters.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    strains_list = strains.allStrains()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())

    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=1)-1
    
    output_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_REFINEMENT", ph2_iteration)
    cc_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration)
    ensure_dir(output_dir)
    
    clusters = MGClusters()
    
    TASKS = []
    for strain_id in strains_list:
        strain = strains.strain(strain_id)
        TASKS.append((strain, closure_iteration, ph2_iteration, False, False))
    
    progress = ShowProgress("Read clusters")
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes = WORKERS) 
        for strain_id, cc_mapping in pool.imap(readMultigeneClusterStrain, TASKS):
            
            for cluster_id in cc_mapping:
                if not cluster_id in clusters.clusters:
                    cluster = MGCluster(cluster_id)
                    clusters.addCluster(cluster)
                else:
                    cluster = clusters.getCluster(cluster_id)
                for multigene in cc_mapping[cluster_id]:
                    cluster.addMultigene(multigene)
            
            progress.update(desc = strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            strain_id, cc_mapping = readMultigeneClusterStrain(TASK)
            
            for cluster_id in cc_mapping:
                if not cluster_id in clusters.clusters:
                    cluster = MGCluster(cluster_id)
                    clusters.addCluster(cluster)
                else:
                    cluster = clusters.getCluster(cluster_id)
                for multigene in cc_mapping[cluster_id]:
                    cluster.addMultigene(multigene)
            
            progress.update(desc = strain_id)

    clusters.classifyClusters(strains_list)
    
    output_fn = output_dir + "rf_anchors.txt"
    output_fh = open(output_fn, "w")
    for cluster_id in clusters.anchors:
        output_fh.write(str(cluster_id) + "\n")
    output_fh.close()
    
    output_fn = output_dir + "rf_nonanchors.txt"
    output_fh = open(output_fn, "w")
    for cluster_id in clusters.non_anchors:
        output_fh.write(str(cluster_id) + "\n")
    output_fh.close()
    
    output_fn = output_dir + "rf_anochor_cc_strains.txt"
    output_fh = open(output_fn, "w")
    
    for cluster in clusters.anchors.values():
        cluster_id = cluster.cluster_id
        strains_subset = sorted(cluster.strains())
        
        text_line = cluster_id + "\t"
        if len(strains_subset) <= 0.5*len(strains_list):
            text_line += "+\t"
            for strain_id in strains_subset:
                text_line += strain_id + ";"
        else:
            text_line += "-\t"
            for strain_id in sorted(strains_list):
                if not strain_id in strains_subset:
                    text_line += strain_id + ";"
            
        text_line = text_line[:-1]
        
        output_fh.write(text_line + "\n")
    output_fh.close()    
    
    text_lines = []
    for cluster in clusters.non_anchors.values():
        for mg_id in cluster.nodes:
            text_lines.append(mg_id + "\t" + cluster.cluster_id + "\n")
    
    output_fh = open(output_dir + "rf_nonanchor_nodes.txt", "w")
    for text_line in sorted(text_lines):
        output_fh.write(text_line)
    output_fh.close()

    rf_seq_ids = set([])
    
    input_fn = cc_dir + "seq_clusters.txt"
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) < 1:
            continue
        cluster_id = tokens[0]
        
        if not cluster_id in clusters.non_anchors:
            continue
        seq_ids_str = tokens[1].split(":")
        for seq_id_str in seq_ids_str:
            rf_seq_ids.add(int(seq_id_str))
    input_fh.close()
       
    input_fh = open(cc_dir + "edges_seq.txt")
    output_fh = open(output_dir + "rf_edges_seq.txt", "w")
    
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) < 2:
            continue
        seq1_id = int(tokens[0])
        seq2_id = int(tokens[1])
            
        if not seq1_id in rf_seq_ids or not seq2_id in rf_seq_ids:
            continue
        output_fh.write(line)
    output_fh.close()
    input_fh.close()
    
    non_anchor_ccs = set(clusters.non_anchors.keys())
    
    TASKS = []
    for strain_id in strains_list:
        strain = strains.strain(strain_id)
        TASKS.append((strain, non_anchor_ccs, closure_iteration, ph2_iteration))
    
    progress = ShowProgress("Saved nodes")
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes = WORKERS) 
        for strain_id in pool.imap(saveNodesStrain, TASKS):
            progress.update(desc = strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            strain_id = saveNodesStrain(TASK)
            progress.update(desc = strain_id)
    print("rf1_classify_components.py: Finished.")
        
    