import sys
import time
import os

import multiprocessing
import shutil

sys.path.append("../../")


from soft.utils.camber_params_utils import *
from soft.utils.camber_fu_utils import UnionFind
from soft.utils.camber_graph_utils import readBlastResultsMapping
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *
from soft.utils.camber_io_utils import ensure_dir


def saveRefAnnotations(params):
    strain_id = params[0]
    mg_cc_map = params[1]
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()    

    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=1)-1
    
    rf_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_REFINEMENT")

    text_lines = []
    cc_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_UNIANNS", ph2_iteration) + strain_id + ".txt"
    cc_fh = open(cc_fn)
    
    for line in cc_fh.readlines():
        line = line.strip()
        tokens = line.split("\t")
        mg_id = tokens[1]
        mg_ann_id = tokens[2]
        mg_lens = tokens[3]
        
        cluster_id = mg_cc_map.get(mg_id)
        if cluster_id == None:
            cluster_id = tokens[0]
        
        text_line = cluster_id + "\t" + mg_id + "\t" + mg_ann_id + "\t" + mg_lens
        text_lines.append(text_line)
    cc_fh.close()

    cc_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_UNIANNS", ph2_iteration+1) + strain_id + ".txt"
    cc_fh = open(cc_fn, "w")
    for text_line in text_lines:
        cc_fh.write(text_line + "\n")
    cc_fh.close()
    
    input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_MGSSEQ", ph2_iteration) + "/" + strain_id + ".txt"
    output_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_MGSSEQ", ph2_iteration+1) + "/" + strain_id + ".txt"
    if os.path.exists(output_fn):
        os.remove(output_fn)
        
    shutil.copyfile(input_fn, output_fn)
    
    return strain_id

if __name__ == '__main__':
    print("rf5_save_ref_annotations.py: saves refined annotations.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=1)-1
    
    
    ensure_dir(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_MGSSEQ", ph2_iteration+1))
    ensure_dir(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_UNIANNS", ph2_iteration+1))
    rf_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_REFINEMENT")
    
    strains = readStrainsInfo()
    strains_list = list(sorted(strains.allStrains()))
    k = len(strains_list)
    
    contig_map = {}
    input_fn = parameters["DATASET_RESULTS_EXP"]+"contigs_mapping.txt"
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) < 2:
            continue
        contig_id = tokens[0]
        strain_id = tokens[1]
        contig_map[contig_id] = strain_id
    input_fh.close()
    
    
    mg_cc_map = {}
    for strain_id in strains_list:
        mg_cc_map[strain_id] = {}
    
    input_fh = open(rf_dir + "rf_nonanchor_nodes_new.txt")
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split("\t")
        mg_id = tokens[0]
        cluster_id = tokens[1]
        contig_id = mg_id.split()[2]
        strain_id = contig_map[contig_id]
        mg_cc_map[strain_id][mg_id] = cluster_id
    input_fh.close()

    TASKS = []
    for strain_id in strains_list:
        TASKS.append((strain_id, mg_cc_map[strain_id]))
    
    progress = ShowProgress("rf5_save_ref_annotations.py")
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)   
        for r in pool.imap_unordered(saveRefAnnotations, TASKS):
            strain_id = r            
            progress.update(desc=strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = saveRefAnnotations(TASK)
            strain_id = r            
            progress.update(desc=strain_id)


    print("rf5_save_ref_annotations.py: Finished.")
    
