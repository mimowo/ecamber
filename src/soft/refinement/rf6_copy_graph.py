import sys
import time
import os

import multiprocessing
from shutil import copyfile
import shutil

sys.path.append("../../")


from soft.utils.camber_params_utils import *
from soft.utils.camber_fu_utils import UnionFind
from soft.utils.camber_graph_utils import readBlastResultsMapping
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *
from soft.utils.camber_io_utils import ensure_dir


def splitGraphClusters(strain_id):
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()    

    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=1)-1
    
    cc_map = {}
    mgs_seq_map = {}
    
    input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_MGSSEQ", ph2_iteration+1) + "/" + strain_id + ".txt"
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split("\t")
        if len(tokens) < 2:
            continue
        mg_id = tokens[0]
        mgs_seq_map[mg_id] = []
        tokens_seq = tokens[1].split(";")
        for token_seq in tokens_seq:
            tokens_len = token_seq.split(":")
            seq_id = int(tokens_len[1])
            mgs_seq_map[mg_id].append(seq_id)
    input_fh.close()
    
    cc_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_UNIANNS", ph2_iteration+1) + strain_id + ".txt"
    cc_fh = open(cc_fn)
    
    for line in cc_fh.readlines():
        line = line.strip()
        tokens = line.split("\t")
        cluster_id = tokens[0]
        mg_id = tokens[1]
        if not cluster_id in cc_map:
            cc_map[cluster_id] = set([])
        cc_map[cluster_id].update(mgs_seq_map[mg_id])
    cc_fh.close()
    
    return strain_id, cc_map

if __name__ == '__main__':
    print("rf6_copy_graph.py: Computes connected components of the sequence consolidation graph.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=1)-1
    
    ensure_dir(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_UNIANNS", ph2_iteration+1))
    rf_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_REFINEMENT")
    
    curr_it_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration)
    next_it_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration+1)
    
    curr_ms_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_MGSSEQ", ph2_iteration)
    next_ms_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_MGSSEQ", ph2_iteration+1)
    
    ensure_dir(next_ms_dir)
    
    strains = readStrainsInfo()
    strains_list = list(sorted(strains.allStrains()))
    k = len(strains_list)
    
    input_fn = curr_it_dir + "seq_clusters.txt"
    
    
    TASKS = strains_list
    
    progress = ShowProgress("Split components")
    progress.setJobsCount(len(strains_list))
    
    clusters = {}
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)   
        for r in pool.imap_unordered(splitGraphClusters, TASKS):
            strain_id, cc_map_strain = r
            for cluster_id in cc_map_strain:
                if not cluster_id in clusters:
                    clusters[cluster_id] = set([])
                clusters[cluster_id].update(cc_map_strain[cluster_id])
            progress.update(desc=strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = splitGraphClusters(TASK)
            strain_id, cc_map_strain = r       
            for cluster_id in cc_map_strain:
                if not cluster_id in clusters:
                    clusters[cluster_id] = set([])
                clusters[cluster_id].update(cc_map_strain[cluster_id])     
            progress.update(desc=strain_id)

    output_fh = open(next_it_dir + "seq_clusters.txt", "w")
    saveClusters(output_fh, clusters, inline=True, cluster_ids = True)
    output_fh.close()
 
    print("rf6_copy_graph.py: Finished.")
    