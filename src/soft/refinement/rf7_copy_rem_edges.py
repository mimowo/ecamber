import sys
import time
import os

import multiprocessing
from shutil import copyfile
import shutil

sys.path.append("../../")


from soft.utils.camber_params_utils import *
from soft.utils.camber_fu_utils import UnionFind
from soft.utils.camber_graph_utils import readBlastResultsMapping
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *
from soft.utils.camber_io_utils import ensure_dir

if __name__ == '__main__':
    print("rf7_copy_rem_edges.py: save refined graph.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=1)-1
    
    ensure_dir(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_UNIANNS", ph2_iteration+1))
    rf_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_REFINEMENT")
    
    curr_it_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration)
    next_it_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration+1)
    
    curr_ms_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_MGSSEQ", ph2_iteration)
    next_ms_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_MGSSEQ", ph2_iteration+1)
    
    ensure_dir(next_ms_dir)
    
    strains = readStrainsInfo()
    strains_list = list(sorted(strains.allStrains()))
    k = len(strains_list)
    
    input_fn = curr_it_dir + "seqids_all.txt"
    output_fn = next_it_dir + "seqids_all.txt"
    
    shutil.copyfile(input_fn, output_fn)

    input_fn = curr_it_dir + "edges_mgs.txt"
    output_fn = next_it_dir + "edges_mgs.txt"
    
    shutil.copyfile(input_fn, output_fn)

    input_fn = curr_it_dir + "edges_seq.txt"
    output_fn = next_it_dir + "edges_seq.txt"
    
    shutil.copyfile(input_fn, output_fn)

    for strain_id in strains_list:
        input_fn = curr_ms_dir + strain_id + ".txt"
        output_fn = next_ms_dir + strain_id + ".txt"
        
        shutil.copyfile(input_fn, output_fn)
    
    output_fh = open(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PHASE2_IT") + "ph2_step.txt", "w")
    output_fh.write(str(ph2_iteration+1) + "\n")
    output_fh.close()
    
    dir_path = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PHASE2_TMP")
    #shutil.rmtree(dir_path, True, None)
    
    print("rf7_copy_rem_edges.py: Finished.")

