import sys
import time
import os

import multiprocessing
from shutil import copyfile

sys.path.append("../../")


from soft.utils.camber_params_utils import *
from soft.utils.camber_fu_utils import UnionFind
from soft.utils.camber_graph_utils import readBlastResultsMapping
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *
from soft.utils.camber_io_utils import ensure_dir

if __name__ == '__main__':
    print("rf4_compute_clusters.py/: Computes connected components of the sequence consolidation graph.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=1)-1
    
    rf_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_REFINEMENT", ph2_iteration)
    
    strains = readStrainsInfo()
    strains_list = list(sorted(strains.allStrains()))
    k = len(strains_list)
    
    nodes = {}
    cluster_ids = set([])
    
    input_fh = open(rf_dir + "rf_nonanchor_nodes.txt")
    lines = input_fh.readlines()
    for line in lines:
        tokens = line.strip().split("\t")
        if len(tokens) < 2: continue
        node_id = tokens[0]
        cluster_id = tokens[1]
        cluster_ids.add(cluster_id)
        nodes[node_id] = cluster_id
        
    input_fh.close()
    
    print(len(nodes))
    
    fu = UnionFind()
    fu.insert_objects(list(nodes.keys()))

    output_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_REFINEMENT", ph2_iteration)
    input_fh = open(output_dir + "rf_edge_pairs_comb.txt")
    lines = input_fh.readlines()
    input_fh.close()
    
    progress = ShowProgress("Union edges")
    progress.setJobsCount(len(lines))
    for line in lines:
        tokens = line.strip().split("\t")
        if len(tokens) != 2: continue
        fu.union(tokens[0], tokens[1])
        progress.update()
                  
    ccs = {}
    ccs_old_counter = {}

    ccs_new = fu.sets()
    cc_old_counter = {}
    cc_new_old_map = {}
    cc_mg_map = {}
    
    for cluster_id in cluster_ids:
        cc_old_counter[cluster_id] = 0
    
    for cluster_id_new in ccs_new:
        cc_old_id = nodes[list(ccs_new[cluster_id_new])[0]]
        cc_old_counter[cc_old_id] += 1
        
        new_cluster_id =  cc_old_id +":" + str(cc_old_counter[cc_old_id])
        ccs[new_cluster_id] = ccs_new[cluster_id_new] 
        cc_new_old_map[new_cluster_id] = cc_old_id
    
    not_ref_count = 0
    ref_count = 0
    
    text_lines = []
    for cluster_id_new in ccs:
        cluster_id_old = cc_new_old_map[cluster_id_new]
        if cc_old_counter[cluster_id_old] == 1:
            not_ref_count += 1
            cluster_id = cluster_id_old
        else:
            ref_count += 1
            cluster_id = cluster_id_new
        for node_id in ccs[cluster_id_new]:
            text_lines.append(node_id + "\t" + cluster_id + "\n")
              
    output_fh = open(rf_dir + "rf_nonanchor_nodes_new.txt", "w")
    for text_line in sorted(text_lines):
        output_fh.write(text_line)
    output_fh.close()

    print("split clusters: ", ref_count)
    print("not split clusters: ", not_ref_count)
    print("rf4b_compute_clusters.py: Finished.")

    
