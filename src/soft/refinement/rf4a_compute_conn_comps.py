import sys
import time
import os

import multiprocessing
from shutil import copyfile

sys.path.append("../../")


from soft.utils.camber_params_utils import *
from soft.utils.camber_fu_utils import UnionFind
from soft.utils.camber_graph_utils import readBlastResultsMapping
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *
from soft.utils.camber_io_utils import ensure_dir

def chunks(l, n):
    i = 0
    chunk = l[:n]
    while chunk:
        yield chunk
        i += n
        chunk = l[i:i+n]

def readEdges(strain_pairs_chunks):

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()    

    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=1)-1
    
    rf_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_REFINEMENT", ph2_iteration)
    output_dir = rf_dir + "/strain_pair_edges_rem/" 

    rem_edges = []
    
    for (strain1_id, strain2_id) in strain_pairs_chunks:
        input_fh = open(output_dir + strain1_id + "/" + strain2_id + ".txt")
        for line in input_fh.readlines():
            nodes_pair = line.strip()
            rem_edges.append(nodes_pair)
        input_fh.close()
    
    return rem_edges

if __name__ == '__main__':
    print("rf4_compute_clusters.py/: Computes connected components of the sequence consolidation graph.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=1)-1
    
    rf_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_REFINEMENT", ph2_iteration)
    
    strains = readStrainsInfo()
    strains_list = list(sorted(strains.allStrains()))
    k = len(strains_list)
    
    strain_pairs = []
    
    for i in range(0, k-1, 1):
        strain1_id = strains_list[i]
        for j in range(i+1, k, 1):
            strain2_id = strains_list[j]
            strain_pairs.append((strain1_id, strain2_id)) 
            
    TASKS = list(chunks(strain_pairs, 1 + int(len(strain_pairs) / WORKERS)))
    
    output_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_REFINEMENT", ph2_iteration)
    output_fn = output_dir + "rf_edge_pairs_comb.txt"
    open(output_fn, "w").close()

    print("strains count: " + str(len(strains_list)))
    print("strain pairs count: " + str(len(strain_pairs)))
    print("workers count: " + str(WORKERS))
    print("chunks(TASKS) count: " + str(len(TASKS)))
    print("chunks len: " + str(len(strain_pairs) / WORKERS))
    
    all_edges_count = 0
    
    progress = ShowProgress("rf4a_compute_clusters.py")
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)   
        for r in pool.imap_unordered(readEdges, TASKS):
            rem_edges = r

            output_fh = open(output_fn, "a")
            for nodes_pair in rem_edges:
                output_fh.write(nodes_pair + "\n")
            output_fh.close()
            
            all_edges_count += len(rem_edges)
            progress.update(desc=str(len(rem_edges)) + " "+ str(all_edges_count))
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = readEdges(TASK)
            rem_edges = r
            
            output_fh = open(output_fn, "a")
            for nodes_pair in rem_edges:
                output_fh.write(nodes_pair + "\n")
            output_fh.close()

            all_edges_count += len(rem_edges)
            progress.update(desc=str(len(rem_edges)) + " "+ str(all_edges_count))

    print("rf4a_compute_clusters.py: Finished.")

