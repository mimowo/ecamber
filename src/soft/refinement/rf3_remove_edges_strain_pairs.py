import sys
import time
import os

import multiprocessing
from shutil import copyfile

sys.path.append("../../")


from soft.utils.camber_params_utils import *
from soft.utils.camber_fu_utils import UnionFind
from soft.utils.camber_graph_utils import readBlastResultsMapping
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *
from soft.utils.camber_io_utils import ensure_dir

def readCCstrainPres(input_fh, strain1_id, strain2_id):
    cc_strain1_map = set([])
    cc_strain2_map = set([])
    for line in input_fh.readlines():
        tokens = line.strip().split("\t")
        if len(tokens) < 1:
            continue
        cluster_id = tokens[0]
        rel = tokens[1]
        if rel == "+":
            strain_tokens = set(tokens[2].split(";"))
            if strain1_id in strain_tokens:
                cc_strain1_map.add(cluster_id)
            if strain2_id in strain_tokens:
                cc_strain2_map.add(cluster_id)
        else:
            if len(tokens) < 3:
                strain_tokens = set([])
            else:
                strain_tokens = set(tokens[2].split(";"))
            if not strain1_id in strain_tokens:
                cc_strain1_map.add(cluster_id)
            if not strain2_id in strain_tokens:
                cc_strain2_map.add(cluster_id)            
    return cc_strain1_map, cc_strain2_map

def readMultigenesCCstrain(input_fh):
    mgs_strain = []
    cc_strain = []
    mgs_strain_rev = {}
    
    count = 0
    for line in input_fh.readlines():
        tokens = line.strip().split("\t")
        if len(tokens) < 2:
            continue
        mg_id = tokens[1]
        cluster_id = tokens[0]
        
        mgs_strain.append(mg_id)
        cc_strain.append(cluster_id)
        mgs_strain_rev[mg_id] = count
        count += 1
        
    return mgs_strain, mgs_strain_rev, cc_strain

def findLeftAnchors(mgs_strain, mgs_strain_rev, cc_strain, cc_strain_pres):
    left_mgs_list = []
    
    left_index = None
    left_contig_id = ""
    
    n = len(mgs_strain)
    
    for i in range(n):
        mg_id = mgs_strain[i]
        cluster_id = cc_strain[i]
        contig_id = mg_id.split()[2]
        if left_contig_id == contig_id:
            left_mgs_list.append(left_index)
        else:
            left_mgs_list.append(None)
        if cluster_id in cc_strain_pres:
            left_contig_id = contig_id
            left_index = i
    
    return left_mgs_list

def findRightAnchors(mgs_strain, mgs_strain_rev, cc_strain, cc_strain_pres):
    right_mgs_list = []

    right_index = None
    right_contig_id = ""
    
    n = len(mgs_strain)

    for i in range(n-1, -1, -1):
        mg_id = mgs_strain[i]
        cluster_id = cc_strain[i]
        contig_id = mg_id.split()[2]
        if right_contig_id == contig_id:
            right_mgs_list.append(right_index)
        else:
            right_mgs_list.append(None)
        if cluster_id in cc_strain_pres:
            right_contig_id = contig_id
            right_index = i
    right_mgs_list.reverse()    
    return right_mgs_list 
    
def checkEdgeSupport(left1_index, right1_index, left2_index, right2_index, cc_strain1, cc_strain2):
    if left1_index == None:
        l1 = -1
    else:
        l1 = cc_strain1[left1_index]
    if right1_index == None:
        r1 = -1
    else:
        r1 = cc_strain1[right1_index]
    if left2_index == None:
        l2 = -1
    else:
        l2 = cc_strain2[left2_index]
    if right2_index == None:
        r2 = -1
    else:
        r2 = cc_strain2[right2_index]
    
    is_supported = False
    
    if (l1==l2 or l1==-1 or l2==-1) and (r1==r2 or r1==-1 or r2==-1):
        is_supported = True
    elif (l1==r2 or l1==-1 or r2==-1) and (l2==r1 or r1==-1 or l2==-1):
        is_supported = True 
    
    return is_supported

def removeEdges(params):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=1)-1
    
    
    rf_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_REFINEMENT", ph2_iteration)
    
    strain1_id = params[0]
    strain2_id = params[1]
    input_dir = rf_dir + "/strain_pair_edges/"
    output_dir = rf_dir + "/strain_pair_edges_rem/" 
    
    edges = set([])
    rem_edges = set([])
    
    input_fh = open(input_dir  + strain1_id + "/" + strain2_id + ".txt")         
    for line in input_fh.readlines():
        tokens = line.strip().split("\t")
        if len(tokens) < 2:
            continue
        node1_id = tokens[0]
        node2_id = tokens[1]
        edges.add((node1_id, node2_id))
    input_fh.close()
    
    input_fh = open(rf_dir +  "rf_anochor_cc_strains.txt")
    cc_strain1_pres, cc_strain2_pres = readCCstrainPres(input_fh, strain1_id, strain2_id)
    input_fh.close()
    
    mg_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_UNIANNS", ph2_iteration)
    
    input_fh = open(mg_dir + strain1_id + ".txt")
    mgs_strain1, mgs_strain1_rev, cc_strain1 = readMultigenesCCstrain(input_fh)
    input_fh.close()

    input_fh = open(mg_dir + strain2_id + ".txt")
    mgs_strain2, mgs_strain2_rev, cc_strain2 = readMultigenesCCstrain(input_fh)
    input_fh.close()
    
    left_anchors_strain1 = findLeftAnchors(mgs_strain1, mgs_strain1_rev, cc_strain1, cc_strain2_pres)
    right_anchors_strain1 = findRightAnchors(mgs_strain1, mgs_strain1_rev, cc_strain1, cc_strain2_pres)
    
    left_anchors_strain2 = findLeftAnchors(mgs_strain2, mgs_strain2_rev, cc_strain2, cc_strain1_pres)
    right_anchors_strain2 = findRightAnchors(mgs_strain2, mgs_strain2_rev, cc_strain2, cc_strain1_pres)
    
    for (node1_id, node2_id) in edges:
        node1_index = mgs_strain1_rev[node1_id]
        node2_index = mgs_strain2_rev[node2_id]
        
        left1_index = left_anchors_strain1[node1_index]
        right1_index = right_anchors_strain1[node1_index]
        left2_index = left_anchors_strain2[node2_index]
        right2_index = right_anchors_strain2[node2_index]
        
        if checkEdgeSupport(left1_index, right1_index, left2_index, right2_index, cc_strain1, cc_strain2):
            rem_edges.add((node1_id, node2_id))
    
    output_fh = open(output_dir + strain1_id + "/" + strain2_id + ".txt", "w")
    for (node1_id, node2_id) in rem_edges:
        output_fh.write(node1_id + "\t" + node2_id + "\n")
    output_fh.close()
    
    return strain1_id, strain2_id

if __name__ == '__main__':
    print("rf3_remove_edges_strain_pairs.py: Remove multigene edges.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=1)-1
    
    rf_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_REFINEMENT", ph2_iteration)
    
    output_dir = rf_dir + "/strain_pair_edges_rem/" 
    ensure_dir(output_dir)    
    
    strains = readStrainsInfo()
    strains_list = list(sorted(strains.allStrains()))
    k = len(strains_list)
    
    TASKS = []
    
    for i in range(0, k-1, 1):
        strain1_id = strains_list[i]
        ensure_dir(output_dir + strain1_id)
        for j in range(i+1, k, 1):
            strain2_id = strains_list[j]
            TASKS.append((strain1_id, strain2_id)) 
    
    progress = ShowProgress("rf3_remove_edges_strain_pairs.py")
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)   
        for r in pool.imap_unordered(removeEdges, TASKS):
            strain1_id = r[0]
            strain2_id = r[1]
            progress.update(desc=strain1_id +" " + strain2_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = removeEdges(TASK)
            strain1_id = r[0]
            strain2_id = r[1]            
            progress.update(desc=strain1_id +" " + strain2_id)     

    print("rf3_remove_edges_strain_pairs.py: Finished.")
    