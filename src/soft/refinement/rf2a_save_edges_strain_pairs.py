import sys
import time
import os

import multiprocessing
from shutil import copyfile
import shutil

sys.path.append("../../")


from soft.utils.camber_params_utils import *
from soft.utils.camber_fu_utils import UnionFind
from soft.utils.camber_graph_utils import readBlastResultsMapping
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *
from soft.utils.camber_io_utils import ensure_dir

def readSeqIDs(params):
    strain_index, strain_id, input_fn = params 
    seq_ids = set([])
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        tokens = line.strip().split("\t")
        if len(tokens) < 1:
            continue
        seq_id_tokens = tokens[2].split(":")
        for seq_id_token in seq_id_tokens:
            seq_id = int(seq_id_token)
            seq_ids.add(seq_id)
    input_fh.close()
    
    return strain_index, strain_id, seq_ids

def saveSelectedEdges(params):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strain1_id = params[0]
    strain2_id = params[1]
    lines = params[2]
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=1)-1
    
    rf_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_REFINEMENT", ph2_iteration)

    t6 = time.time()
    output_dir  = rf_dir + "/strain_pair_edges_seq/" + strain1_id
    ensure_dir(output_dir)
    
    output_fh = open(output_dir + "/" + strain2_id + ".txt", "w")         
    for line in lines:
        output_fh.write(line)
    output_fh.close()
    
#    print(strain1_id, strain2_id, t1-t0, t2-t1, t3-t2, t4-t3, t5-t4, t6-t5)

    return strain1_id, strain2_id

if __name__ == '__main__':
    print("rf2a_save_edges_strain_pairs.py: Saves sequence graph.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=1)-1
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    strains = readStrainsInfo()
    strains_list = list(sorted(strains.allStrains()))
    k = len(strains_list)
    
    rf_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_REFINEMENT", ph2_iteration)
    
    TASKS = []
    
    for strain_index in range(k):
        strain_id = strains_list[strain_index]
        TASKS.append((strain_index, strain_id, rf_dir + "rf_nodes/" + strain_id+ ".txt"))

    seq_strains_map = {}
    
    progress = ShowProgress("Read seq ids")
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)   
        for r in pool.imap_unordered(readSeqIDs, TASKS):
            strain_index, strain_id, seqs = r
            
            for seq_id in seqs:
                if not seq_id in seq_strains_map:
                    seq_strains_map[seq_id] = set([])
                seq_strains_map[seq_id].add(strain_index)
            progress.update(desc=strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            strain_index, strain_id, seqs = readSeqIDs(TASK)
            for seq_id in seqs:
                if not seq_id in seq_strains_map:
                    seq_strains_map[seq_id] = set([])
                seq_strains_map[seq_id].add(strain_index)
            
            progress.update(desc=strain_id)
    
    edges_strains = []     
    for i in range(0, k-1, 1):
        edges_strains.append([])
        for j in range(i+1, k, 1):
            edges_strains[i].append([])
   # print(len(edges_strains), len(edges_strains[0]), len(edges_strains[1]))
    
    rf_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_REFINEMENT", ph2_iteration)
    output_dir  = rf_dir + "/strain_pair_edges_seq/"
    ensure_dir(output_dir)   
    
    progress = ShowProgress("rf2a_save_edges_strain_pairs.py")
    
    input_fn = rf_dir + "rf_edges_seq.txt"
    input_fh = open(input_fn)
    lines = input_fh.readlines()
    progress.setJobsCount(len(lines)) 
    
    for line in lines:
        tokens = line.split()
        if len(tokens) < 2:
            continue
        seq1_id = int(tokens[0])
        seq2_id = int(tokens[1])
        
        strains1 = seq_strains_map.get(seq1_id)
        strains2 = seq_strains_map.get(seq2_id)
     #   print(seq1_id, seq2_id, strains1, strains2)
        progress.update()
        if len(strains1) == 0 or len(strains2) == 0:
            continue

        for i in strains1:
            for j in strains2:
                if i < j:
                    edges_strains[i][j-i-1].append(line)
                elif i > j:
                    edges_strains[j][i-j-1].append(line)

    input_fh.close()

    
    
    TASKS = []
    
    for i in range(0, k-1, 1):
        strain1_id = strains_list[i]
        ensure_dir(output_dir  + strain1_id)
        for j in range(i+1, k, 1):
            strain2_id = strains_list[j]
            TASKS.append((strain1_id, strain2_id, edges_strains[i][j-i-1])) 
    
    progress = ShowProgress("Processed strain pairs")
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)   
        for r in pool.imap_unordered(saveSelectedEdges, TASKS):
            strain1_id = r[0]
            strain2_id = r[1]
            progress.update(desc=strain1_id +" " + strain2_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = saveSelectedEdges(TASK)
            strain1_id = r[0]
            strain2_id = r[1]            
            progress.update(desc=strain1_id +" " + strain2_id)     

    print("rf2a_save_edges_strain_pairs.py: Finished.")
