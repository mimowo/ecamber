import sys
import time
import os

import multiprocessing
from shutil import copyfile

sys.path.append("../../")


from soft.utils.camber_params_utils import *
from soft.utils.camber_fu_utils import UnionFind
from soft.utils.camber_graph_utils import readBlastResultsMapping
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *
from soft.utils.camber_io_utils import ensure_dir

def readMgsInfo(input_fn): 
    seq_cc_mgs_map = {}
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        tokens = line.strip().split("\t")
        if len(tokens) < 1:
            continue
        cluster_id = tokens[0]
        mg_id = tokens[1]
        seq_id_tokens = tokens[2].split(":")
        for seq_id_token in seq_id_tokens:
            seq_id = int(seq_id_token)
            if not seq_id in seq_cc_mgs_map:
                seq_cc_mgs_map[seq_id] = {}
            if not cluster_id in seq_cc_mgs_map[seq_id]:
                seq_cc_mgs_map[seq_id][cluster_id] = set([])
            seq_cc_mgs_map[seq_id][cluster_id].add(mg_id)
    input_fh.close()
    
    return seq_cc_mgs_map

def saveEdges(params):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    
    strain1_id = params[0]
    strain2_id = params[1]
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=1)-1
    
    rf_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_REFINEMENT", ph2_iteration)
    t0 = time.time()
    
    contig_map = {}
    input_fn = parameters["DATASET_RESULTS_EXP"]+"contigs_mapping.txt"
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) < 2:
            continue
        contig_id = tokens[0]
        strain_id = tokens[1]
        contig_map[contig_id] = strain_id
    input_fh.close()
    
    t1 = time.time()
    
    strain1_seq_mgs_map = readMgsInfo(rf_dir + "rf_nodes/" + strain1_id+ ".txt")
    strain2_seq_mgs_map = readMgsInfo(rf_dir + "rf_nodes/" + strain2_id+ ".txt") 
    
    edge_strains_map = set([])
    
    for seq_id in set(strain1_seq_mgs_map.keys()) & set(strain2_seq_mgs_map.keys()):
        ccs1 = strain1_seq_mgs_map[seq_id]
        ccs2 = strain2_seq_mgs_map[seq_id]
        for cluster_id in set(ccs1.keys()) & set(ccs2.keys()):
            mgs1 = ccs1[cluster_id]
            mgs2 = ccs2[cluster_id]
            for mg1_id in mgs1:
                for mg2_id in mgs2:
                    edge_strains_map.add((mg1_id, mg2_id))
    
  #  seqids_subset = set(seq_mgs_map).copy()
#
    input_fn = rf_dir + "strain_pair_edges_seq" + "/" + strain1_id + "/"+ strain2_id + ".txt"
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) < 2:
            continue
        seq1_id = int(tokens[0])
        seq2_id = int(tokens[1])
        
        ccs1 = strain1_seq_mgs_map.get(seq1_id)
        ccs2 = strain2_seq_mgs_map.get(seq2_id)
        
        if ccs1 == None or ccs2 == None:
            continue

        for cluster_id in set(ccs1.keys()) & set(ccs2.keys()):
            mgs1 = ccs1[cluster_id]
            mgs2 = ccs2[cluster_id]
            for mg1_id in mgs1:
                for mg2_id in mgs2:
                    edge_strains_map.add((mg1_id, mg2_id))
    
        ccs1 = strain1_seq_mgs_map.get(seq2_id)
        ccs2 = strain2_seq_mgs_map.get(seq1_id)
        
        if ccs1 == None or ccs2 == None:
            continue
        
        for cluster_id in set(ccs1.keys()) & set(ccs2.keys()):
            mgs1 = ccs1[cluster_id]
            mgs2 = ccs2[cluster_id]
            for mg1_id in mgs1:
                for mg2_id in mgs2:
                    edge_strains_map.add((mg1_id, mg2_id))
    
    input_fh.close()

    t6 = time.time()
    output_dir  = rf_dir + "/strain_pair_edges/"
    output_fh = open(output_dir  + strain1_id + "/" + strain2_id + ".txt", "w")         
    for (node1_id, node2_id) in edge_strains_map:
        output_fh.write(node1_id + "\t" + node2_id + "\n")
    output_fh.close()
    
#    print(strain1_id, strain2_id, t1-t0, t2-t1, t3-t2, t4-t3, t5-t4, t6-t5)

    return strain1_id, strain2_id

if __name__ == '__main__':
    print("rf2b_save_edges_strain_pairs.py: Saves multigene graph.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=1)-1
    
    rf_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_REFINEMENT", ph2_iteration)
    output_dir  = rf_dir + "/strain_pair_edges/"
    ensure_dir(output_dir)    
    
    strains = readStrainsInfo()
    strains_list = list(sorted(strains.allStrains()))
    k = len(strains_list)
    
    TASKS = []
    
    for i in range(0, k-1, 1):
        strain1_id = strains_list[i]
        ensure_dir(output_dir  + strain1_id)
        for j in range(i+1, k, 1):
            strain2_id = strains_list[j]
            TASKS.append((strain1_id, strain2_id)) 
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    progress = ShowProgress("rf2b_save_edges_strain_pairs.py")
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)   
        for r in pool.imap_unordered(saveEdges, TASKS):
            strain1_id = r[0]
            strain2_id = r[1]
            progress.update(desc=strain1_id +" " + strain2_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = saveEdges(TASK)
            strain1_id = r[0]
            strain2_id = r[1]            
            progress.update(desc=strain1_id +" " + strain2_id)     

    print("rf2b_save_edges_strain_pairs.py: Finished.")
    