import sys
import os
import time
import multiprocessing


sys.path.append("../../")

from soft.utils.camber_coding_utils import getCodingMap, mapCodingMapRev,codeDNAseq
from soft.utils.camber_params_utils import *
from soft.utils.camber_closure_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_progress import ShowProgress
from soft.utils.camber_graph_utils import *

def readQueries(input_fn):
    seq_parts_list = []
    seq_map = {}
    genome_fh = open(input_fn)
    genome_lines = genome_fh.readlines();
    genome_fh.close()
    accession_str = ""
    for line in genome_lines:
        line = line.strip()
        parts = line.split();
        if(len(parts) >= 1):
            if parts[0][0] == ">":
                if accession_str != "":
                    seq_map[int(accession_str)] = ''.join(seq_parts_list)
                    seq_parts_list = []
                acc_tokens = parts[0].split("|")
                if len(acc_tokens) >= 4:
                    accession_str = acc_tokens[3]
                else:
                    accession_str = parts[0][1:]
                acc_tok = accession_str.split(".")
                accession_str = acc_tok[0]
            else:
                seq_parts_list.append(parts[0])
    if len(seq_parts_list) >= 1:
        seq_map[int(accession_str)] = ''.join(seq_parts_list)
    return seq_map

def updateBlasts(strain_id):
    overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    readStrainSequences(strain)
    strain.calcRevSequence()
    iteration = currentClosureIteration()

    is_cds = True

    max1_id = getMaxID(parameters["DATASET_BLAST_RESULTS"] + "/blast-" + strain_id + ".txt")
    max2_id = getMaxID(parameters["DATASET_BLAST_PSEUDO_RESULTS"] + "/blast-" + strain_id + ".txt")  
    max_id = max(max1_id, max2_id)
    
    blast_mapping_lens = readBlastsMappingLens(parameters["DATASET_RESULTS_EXP_BLAST"]+"/lens-new-"+str(iteration)+".txt")
    blast_mapping_altstarts = readBlastsMappingAltstarts(parameters["DATASET_RESULTS_EXP_BLAST"]+"/alt_starts-new-"+str(iteration)+".txt")
    
    input_fn = parameters["DATASET_RESULTS_EXP_BLAST_TMP"] + "/blast-" + strain_id +"-"+str(iteration)+ ".txt";
    if not os.path.exists(input_fn) or os.path.getsize(input_fn) < 2:
        return strain_id
    
    input_fh = open(input_fn)
    acc_out_text_lines = []
    pseudo_out_text_lines = []
    for line in input_fh.readlines():
        tokens = line.split()
        blast_hit = parseParsedBlastHit(None, tokens, strain)
        if blast_hit.query_id <= max_id:
            continue
        if blast_hit == None:
            continue
        blast_hit.query_len = int(blast_mapping_lens[blast_hit.query_id])
        alt_start = blast_mapping_altstarts.get(blast_hit.query_id)
        blast_hit = rateBlastHitDna(blast_hit, strain, 50, 0.3, 1e-10, pid_method=0, hssp_b = True, id_p_pseudo=70, alt_start=alt_start, is_cds=is_cds)
        if blast_hit == None:
            continue
        elif blast_hit.is_pseudo:
            pseudo_out_text_lines.append(line)
        else:
            acc_out_text_lines.append(line)
    
    input_fh.close()    
    
    output_fn = parameters["DATASET_BLAST_RESULTS"] + "/blast-" + strain_id + ".txt"
    output_fh = open(output_fn, "a")
    output_fh.write(''.join(acc_out_text_lines))
    output_fh.close()

    output_fn = parameters["DATASET_BLAST_PSEUDO_RESULTS"] + "/blast-" + strain_id + ".txt"
    output_fh = open(output_fn, "a")
    output_fh.write(''.join(pseudo_out_text_lines))
    output_fh.close()
    
    return strain_id

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    
    iteration = currentClosureIteration()

    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    n = len(strains.allStrains())
    
    print("loop3_update_seqs_mapping.py: updates 'blasts_mapping.txt' file.")
    
    ensure_dir(parameters["DATASET_BLAST_RESULTS"])
    ensure_dir(parameters["DATASET_BLAST_PSEUDO_RESULTS"])
    progress = ShowProgress("Updated sequences mapping")
    progress.setJobsCount(n)
    

    diff_genes = set([])    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)
        for r in pool.imap_unordered(updateBlasts, list(strains.allStrains())):
            strain_id = r
            progress.update(desc=strain_id)
        pool.close()
        pool.join()
    else:
        for strain_id in list(strains.allStrains()):
            strain_id = updateBlasts(strain_id)
            progress.update(desc=strain_id)

    max_id = getMaxID(parameters["DATASET_BLAST"] + "blasts_mapping_lens.txt")
    
    queries_fn = parameters["DATASET_RESULTS_EXP_BLAST"] + "/queries-"+str(iteration)+".fasta"
    seq_mapping = readQueries(queries_fn)

    bp_map = getCodingMap()
    
    out_text = ""
    out_text_lens = ""
    out_text_starts = ""

    for seq_id in sorted(seq_mapping, key=lambda seq_id:int(seq_id)):
        if seq_id <= max_id:
            continue
        seq = seq_mapping[seq_id]
        seq_encoded = codeDNAseq(seq, bp_map)
        out_text += str(seq_id) + " "+seq_encoded+"\n"
        out_text_lens += str(seq_id) + " "+str(len(seq))+"\n"
        if not seq_encoded[0] in ["(", "c", "8"]:
            out_text_starts += str(seq_id) + " " + str(seq[:3]) + "\n"

    os.remove(queries_fn)
    
    if len(out_text) > 0:
        out_map_fn = parameters["DATASET_BLAST"] + "blasts_mapping.txt"
        out_map_fh = open(out_map_fn, "a")
        out_map_fh.write(out_text)
        out_map_fh.close()

    if len(out_text_lens) > 0:
        out_map_fn = parameters["DATASET_BLAST"] + "blasts_mapping_lens.txt"
        out_map_fh = open(out_map_fn, "a")
        out_map_fh.write(out_text_lens)
        out_map_fh.close()
        
    if len(out_text_starts) > 0:
        out_map_fn = parameters["DATASET_BLAST"] + "blasts_mapping_altstarts.txt"
        out_map_fh = open(out_map_fn, "a")
        out_map_fh.write(out_text_starts)
        out_map_fh.close()


    print("loop3_update_seqs_mapping.py: Finished.")
