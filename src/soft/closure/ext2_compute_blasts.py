import sys
import os
import time
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_closure_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_progress import ShowProgress

def blastDnaExt(strain_id):
    overwriteParameters(sys.argv)
    parameters = readParameters()
  
    tr_evalue = parameters["BE"]
    
    genome_db = parameters["DATASET_GENOMES_DBS"] + strain_id
    queries_dir = parameters["DATASET_RESULTS_EXP_BLAST"]
    results_tmp_dir = parameters["DATASET_RESULTS_EXP_BLAST_TMP"]
    
    query_fn = queries_dir + "/queries-ext.fasta"
    
    src_output_fn = results_tmp_dir + "/tmp-" + strain_id + ".txt";
    dst_output_fn = results_tmp_dir + "/blast-" + strain_id + ".txt";
    
    if os.path.exists(dst_output_fn) or not os.path.exists(query_fn) or os.path.getsize(query_fn) < 2:
        return strain_id
    
    masking = False
    if "LCM" in parameters and parameters["LCM"].upper() == "Y":
        masking =True    
    
    if parameters["BLAST_VER"].upper() == "BLAST+":
        blastn_exe = parameters["BLASTN_PATH"]
        runBlastn(blastn_exe, query_fn, genome_db, src_output_fn, tr_evalue, masking);
    elif parameters["BLAST_VER"].upper() == "BLASTALL": 
        blastall_exe = parameters["BLASTALL_PATH"]
        runBlastallBlastn(blastall_exe, query_fn, genome_db, src_output_fn, tr_evalue, masking);
        
    os.rename(src_output_fn, dst_output_fn)
    
    return strain_id

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()

    print("ext2_compute_blasts.py: computes BLASTs to transfer annotations between strains.")
    
    ensure_dir(parameters["DATASET_RESULTS_EXP_BLAST"])
    ensure_dir(parameters["DATASET_RESULTS_EXP_BLAST_TMP"] )  
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    TASKS = []
    
    input_fn = parameters["DATASET_BLAST"] + "blasts_mapping.txt"
    if not os.path.exists(input_fn):
        open(input_fn, "w").close()
        print("The BLAST mapping file is created (no annotations to transfer).")
        exit()
    elif os.path.getsize(input_fn) < 2:
        print("The BLAST mapping file is empty (no annotations to transfer).")
        exit()
    else:
        all_blasts_computed = True
        for strain_id in strains.allStrains():
            
            blast_fn = parameters["DATASET_BLAST_RESULTS"] + "/blast-" + strain_id + ".txt"
            if not os.path.exists(blast_fn):
                all_blasts_computed = False
                print("Missing BLAST to be computed: " + blast_fn)
                TASKS.append(strain_id)
        if all_blasts_computed:
            print("All BLASTs are pre-computed!")
            exit()
    
    n = len(TASKS)
    
    progress = ShowProgress("Computed BLASTs")
    progress.setJobsCount(n)
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap_unordered(blastDnaExt, TASKS):
            progress.update(desc=r)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = blastDnaExt(TASK)
            progress.update(desc=r)
            
    query_fn = parameters["DATASET_RESULTS_EXP_BLAST"] + "/queries-ext.fasta"
    os.remove(query_fn)
    print("ext2_compute_blasts.py: Finished.")
    
    