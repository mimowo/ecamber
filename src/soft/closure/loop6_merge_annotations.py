import sys
import os
import multiprocessing
import shutil

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_closure_utils import *
from soft.structs.blast_hit import *
from soft.structs.gene import *
from soft.structs.annotation import *


def joinAnnotations(blast_orfs, new_annotation, all_annotation):
    for blast_gene in list(blast_orfs.genes.values()):
        blast_gene_id = blast_gene.unique_id
        if not blast_gene_id in all_annotation.genes:
            all_annotation.genes[blast_gene_id] = blast_gene
            new_annotation.genes[blast_gene_id] = blast_gene

def mergeBlastResultsStrains(strain_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    
    iteration = currentClosureIteration()
    
    new_annotation = Annotation();
    readAllAnnotationsIter(strain, iteration)
    readStrainSequences(strain)
    all_annotation = strain.all_annotation

    input_fn = getItPath(parameters, iteration, "DATASET_RESULTS_EXP_BLAST_ACC") + "/"+strain_id + ".txt"
    if os.path.exists(input_fn):
        blast_orfs_fh = open(input_fn)
        blast_orfs = readBlastHitsAnnFromFile(blast_orfs_fh, strain)
        blast_orfs_fh.close();
    else:
        blast_orfs = Annotation()
        
    joinAnnotations(blast_orfs, new_annotation, all_annotation)

    new_orfs_fn = getItPath(parameters, iteration+1, "DATASET_RESULTS_EXP_NEW") + strain_id + ".txt"
    all_orfs_fn = getItPath(parameters, iteration+1, "DATASET_RESULTS_EXP_ALL") + strain_id + ".txt"
    
    if os.path.exists(new_orfs_fn):
        os.remove(new_orfs_fn)
    if os.path.exists(all_orfs_fn):
        os.remove(all_orfs_fn)
        
    new_orfs_tmp_fn = getItPath(parameters, iteration+1, "DATASET_RESULTS_EXP_NEW") + "tmp-new-" + strain_id + ".txt"
    all_orfs_tmp_fn = getItPath(parameters, iteration+1, "DATASET_RESULTS_EXP_ALL") + "tmp-all-" + strain_id + ".txt"
    
    new_blasts = len(new_annotation.genes)
    new_orfs_fh = open(new_orfs_tmp_fn, "w")
    saveMgsAnnotations(new_orfs_fh, new_annotation.genes, show_start_codons=True)
    new_orfs_fh.close();
    
    all_orfs_fh = open(all_orfs_tmp_fn, "w")
    saveMgsAnnotations(all_orfs_fh, all_annotation.genes, show_start_codons=True)
    all_orfs_fh.close();
    
    os.rename(new_orfs_tmp_fn, new_orfs_fn)
    os.rename(all_orfs_tmp_fn, all_orfs_fn)
    
    return (strain_id, new_blasts)

if __name__ == '__main__':

    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()

    iteration = currentClosureIteration()
    print("loop6_merge_annotations.py: merges accepted BLAST hits with current annotations.")
    print("Current iteration: " + str(iteration))

    ensure_dir(getItPath(parameters, iteration+1, "DATASET_RESULTS_EXP_PHASE1_IT"))
    ensure_dir(getItPath(parameters, iteration+1, "DATASET_RESULTS_EXP_NEW"))
    ensure_dir(getItPath(parameters, iteration+1, "DATASET_RESULTS_EXP_ALL"))

    progress = ShowProgress("Merged BLAST accepted hits")
    
    TASKS = []
    for strain_id in strains.allStrains():
        TASKS.append(strain_id)
    
    progress.setJobsCount(len(TASKS))
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    tot_blasts = 0
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap_unordered(mergeBlastResultsStrains, TASKS):
            strain_id = r[0]
            new_blasts = r[1]
            tot_blasts += new_blasts
            progress.update(desc=strain_id +": "+str(new_blasts))
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = mergeBlastResultsStrains(TASK)
            strain_id = r[0]
            new_blasts = r[1]
            tot_blasts += new_blasts
            progress.update(desc=strain_id +": "+str(new_blasts))
        
        
    if tot_blasts > 0:
        print("The closure procedure is still not computed. Total number of new accepted BLAST hits: " +str(tot_blasts))
        iteration_fh = open(parameters_map["DATASET_RESULTS_EXP_PHASE1"] +"ph1_it.txt", "w")
        iteration_fh.write(str(iteration + 1)+"\n")
        iteration_fh.close();

        if iteration > 0:
            for strain_id in TASKS:
                all_fn = getItPath(parameters, iteration, "DATASET_RESULTS_EXP_ALL") + strain_id + ".txt"
                if os.path.exists(all_fn):
                    os.remove(all_fn)
    else:
        print("The closure procedure is already computed.")
        iteration_fh = open(parameters_map["DATASET_RESULTS_EXP_PHASE1"] +"ph1_it.txt", "w")
        iteration_fh.write(str(iteration)+" "+"C"+"\n")
        iteration_fh.close();
        
        rem_dir = getItPath(parameters, iteration+1, "DATASET_RESULTS_EXP_PHASE1_IT")
        shutil.rmtree(rem_dir, True, None)
        
    print("loop6_merge_annotations.py: Finished.")
    
