import sys
import os
import time
import multiprocessing


sys.path.append("../../")

from soft.utils.camber_coding_utils import getCodingMap, mapCodingMapRev,codeDNAseq
from soft.utils.camber_params_utils import *
from soft.utils.camber_closure_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_progress import ShowProgress
from soft.utils.camber_graph_utils import *



def updateBlasts(strain_id):
    overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    readStrainSequences(strain)
    strain.calcRevSequence()

    max1_id = getMaxID(parameters["DATASET_BLAST_RESULTS"] + "/blast-" + strain_id + ".txt")
    max2_id = getMaxID(parameters["DATASET_BLAST_PSEUDO_RESULTS"] + "/blast-" + strain_id + ".txt")  
    max_id = max(max1_id, max2_id)

    blast_mapping_lens = readBlastsMappingLens(parameters["DATASET_RESULTS_EXP_BLAST"]+"/lens-ext.txt")    
    blast_mapping_altstarts = readBlastsMappingAltstarts(parameters["DATASET_RESULTS_EXP_BLAST"]+"/alt_starts-ext.txt")  
    
    input_fn = parameters["DATASET_RESULTS_EXP_BLAST_TMP"] + "/blast-" + strain_id + ".txt";    
    input_fh = open(input_fn)
    
    acc_out_text_lines = []
    pseudo_out_text_lines = []
    for line in input_fh.readlines():
        tokens = line.split()
        blast_hit = parseParsedBlastHit(None, tokens, strain)
        if blast_hit == None:
            continue
        if blast_hit.query_id <= max_id:
            continue
        alt_start = blast_mapping_altstarts.get(blast_hit.query_id)
        blast_hit.query_len = int(blast_mapping_lens[blast_hit.query_id])
        blast_hit = rateBlastHitDna(blast_hit, strain, 30, 0.3, 1e-10, pid_method=0, hssp_b = True, id_p_pseudo=70, alt_start=alt_start)
        if blast_hit == None:
            continue
        elif blast_hit.is_pseudo:
            pseudo_out_text_lines.append(line)
        else:
            acc_out_text_lines.append(line)
    
    input_fh.close()    
    
    output_fn = parameters["DATASET_BLAST_RESULTS"] + "/blast-" + strain_id + ".txt"
    output_fh = open(output_fn, "a")
    output_fh.write(''.join(acc_out_text_lines))
    output_fh.close()

    output_fn = parameters["DATASET_BLAST_PSEUDO_RESULTS"] + "/blast-" + strain_id + ".txt"
    output_fh = open(output_fn, "a")
    output_fh.write(''.join(pseudo_out_text_lines))
    output_fh.close()    
    
    os.remove(input_fn)
    
    return strain_id

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    print("ext3_update_blast_results.py: updates 'blasts_mapping.txt' file.")
    
    strains = readStrainsInfo()
    strains_list = strains.allStrains()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    n = len(strains.allStrains())
    
    ensure_dir(parameters["DATASET_BLAST_RESULTS"])
    ensure_dir(parameters["DATASET_BLAST_PSEUDO_RESULTS"])
    ensure_dir(parameters["DATASET_RESULTS_EXP_BLAST"])
    ensure_dir(parameters["DATASET_RESULTS_EXP_BLAST_TMP"])  
    
    TASKS = []
    input_fn = parameters["DATASET_BLAST"] + "blasts_mapping.txt"
    if not os.path.exists(input_fn):
        open(input_fn, "w").close()
        print("The BLAST mapping file is created (no annotations to transfer).")
        exit()
    elif os.path.getsize(input_fn) < 2:
        print("The BLAST mapping file is empty (no annotations to transfer).")
        exit()
    else:
        for strain_id in strains_list:
            blast_fn = parameters["DATASET_BLAST_RESULTS"] + "/blast-" + strain_id + ".txt"
            if not os.path.exists(blast_fn):
                TASKS.append(strain_id)
                print("BLAST to be computed: " + blast_fn)
        if len(TASKS) == 0:
            print("All BLASTs are pre-computed!")
            exit()    
    
    progress = ShowProgress("Updated sequences mapping")
    progress.setJobsCount(n)

    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)  
        for r in pool.imap(updateBlasts, TASKS):
            strain_id = r
            progress.update(desc=strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            strain_id = updateBlasts(TASK)
            progress.update(desc=strain_id)

    print("ext3_update_blast_results.py: Finished")
    