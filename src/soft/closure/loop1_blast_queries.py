import sys
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_graph_utils import *
from soft.utils.camber_closure_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_io_utils import ensure_dir
from soft.utils.camber_progress import ShowProgress
from soft.utils.camber_coding_utils import codeDNAseq, decodedDNAseqLength

def getSequencesEncoded(strain_id):
    overwriteParameters(sys.argv)
    readParameters()
    
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    iteration = currentClosureIteration()
    readStrainSequences(strain)
    
    bp_map = getCodingMap()

    all_seq_set = set([])
    if iteration > 0:
        readNewAnnotationsIter(strain, iteration)
        annotation = strain.new_annotation
    else:
        readAllAnnotationsIter(strain, 0)
        annotation = strain.all_annotation

    for gene in list(annotation.genes.values()):
        seq = gene.sequence()
        encoded_seq = codeDNAseq(seq, bp_map)
        all_seq_set.add(encoded_seq)

    return strain_id, all_seq_set

def saveQueries(out_queries_fn, blasts_mapping, decode = False):
    bp_map = getCodingMap()
    bp_map_rev = mapCodingMapRev(bp_map)
    
    out_queries_fh = open(out_queries_fn, "w")
    
    for seq_id in sorted(blasts_mapping):
        seq = blasts_mapping[seq_id]
        if decode == False:
            writeFASTAseq(out_queries_fh, seq, str(seq_id))
        else:
            seq_decoded = decodeDNAseq(seq, bp_map_rev)
            writeFASTAseq(out_queries_fh, seq_decoded, str(seq_id))
    out_queries_fh.close()

def saveAltStarts(out_starts_fn, blasts_mapping_alt_start, decode=True):
    bp_map = getCodingMap()
    
    out_queries_fh = open(out_starts_fn, "w")
    
    for seq_id in sorted(blasts_mapping_alt_start):
        seq_start = blasts_mapping_alt_start[seq_id]
        out_queries_fh.write(str(seq_id) + "\t" + seq_start + "\n")
    out_queries_fh.close()
    
    
    

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()

    is_cds =True
    
    iteration = currentClosureIteration()
    
    print("loop1_blast_queries.py: prepares BLAST queries to transfer annotations between strains.")
    print("Current iteration: " + str(iteration))

    ensure_dir(parameters["DATASET_BLAST"])
    ensure_dir(parameters["DATASET_BLAST_RESULTS"])
    ensure_dir(parameters["DATASET_RESULTS_EXP_BLAST"])
    ensure_dir(getItPath(parameters, iteration, "DATASET_RESULTS_EXP_PHASE1_IT"))
    
    strains = readStrainsInfo()
    strains_list = list(strains.allStrains())
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())

    progress = ShowProgress("Prepared BLAST queries")
    n = len(strains_list)
    progress.setJobsCount(n)

    new_seqs = set([])

    TASKS = strains_list
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)
        for strain_id, new_gene_seqs in pool.imap_unordered(getSequencesEncoded, TASKS):
            new_seqs.update(new_gene_seqs)
            progress.update(desc=strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            strain_id, new_gene_seqs = getSequencesEncoded(TASK)
            new_seqs.update(new_gene_seqs)
            progress.update(desc=strain_id)
    
    input_fn = parameters["DATASET_BLAST"] + "blasts_mapping.txt"
    if not os.path.exists(input_fn) or os.path.getsize(input_fn) < 2:
        print("The BLAST mapping file is empty")
        open(input_fn, "w").close()    
    
    blasts_mapping, max_id = readBlastResultsMapping(input_fn, sequences_subset=new_seqs, decode=False)
    computed_seqs = set(blasts_mapping.values())
     
    diff_genes = new_seqs - computed_seqs
    if len(diff_genes) > 0:
        print("The closure procedure is still not computed. Number of BLAST queries in next iteration:" +str(len(diff_genes)))    
    else:
        print("The closure procedure is already computed.")
    
    print("Saving sequences mapping...")
 
    new_blasts_mapping = {}
    
    count = max_id + 1
    for new_seq in diff_genes:
        new_blasts_mapping[count] = new_seq
        count += 1
    
    print("Number of pre-computed sequences (as BLAST queries): " + str(len(blasts_mapping)))
    print("Number of new sequences (as BLAST queries): " + str(len(new_blasts_mapping)))    
    
    bp_map = getCodingMap()
    bp_map_rev = mapCodingMapRev(bp_map)
    
    comp_blasts_altstarts = {}
    blasts_mapping_lens = {}
    for seq_id in blasts_mapping:
        seq = blasts_mapping[seq_id]
        seq_start = seq[0]
        seq_docoded_length = decodedDNAseqLength(seq, bp_map_rev)
        blasts_mapping_lens[seq_id] = seq_docoded_length
        if is_cds and not seq_start in ["8", "c", "("]:
            comp_blasts_altstarts[seq_id] = decodeDNAseq(seq[:3], bp_map_rev)[:3]

    new_blasts_altstarts = {}
    new_blasts_mapping_lens = {}
    for seq_id in new_blasts_mapping:
        seq = new_blasts_mapping[seq_id]
        seq_start = seq[0]
        seq_docoded_length = decodedDNAseqLength(seq, bp_map_rev)
        new_blasts_mapping_lens[seq_id] = seq_docoded_length
        if is_cds and not seq_start in ["8", "c", "("]:
            new_blasts_altstarts[seq_id] = decodeDNAseq(seq[:3], bp_map_rev)[:3]

    saveQueries(parameters["DATASET_RESULTS_EXP_BLAST"] + "/queries-"+str(iteration)+".fasta", new_blasts_mapping, decode=True)
    
    saveAltStarts(parameters["DATASET_RESULTS_EXP_BLAST"] + "/alt_starts-comp-"+str(iteration)+".txt", comp_blasts_altstarts, decode=True)
    saveAltStarts(parameters["DATASET_RESULTS_EXP_BLAST"] + "/alt_starts-new-"+str(iteration)+".txt", new_blasts_altstarts, decode=True)
    
    saveBlastResultsMappingLens(parameters["DATASET_RESULTS_EXP_BLAST"] + "/lens-comp-"+str(iteration)+".txt", blasts_mapping_lens)
    saveBlastResultsMappingLens(parameters["DATASET_RESULTS_EXP_BLAST"] + "/lens-new-"+str(iteration)+".txt", new_blasts_mapping_lens)
    print("loop1_blast_queries.py: Finished")


