import sys
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_graph_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_coding_utils import decodedDNAseqLength


def saveQueries(out_queries_fn, blasts_mapping, decode = False):
    bp_map = getCodingMap()
    bp_map_rev = mapCodingMapRev(bp_map)
    
    out_queries_fh = open(out_queries_fn, "w")
    
    for seq_id in sorted(blasts_mapping):
        seq = blasts_mapping[seq_id]
        if decode == False:
            writeFASTAseq(out_queries_fh, seq, str(seq_id))
        else:
            seq_decoded = decodeDNAseq(seq, bp_map_rev)
            writeFASTAseq(out_queries_fh, seq_decoded, str(seq_id))
    out_queries_fh.close()

def saveAltStarts(out_starts_fn, blasts_mapping_alt_start, decode=True):
    bp_map = getCodingMap()
    bp_map_rev = mapCodingMapRev(bp_map)
    
    out_queries_fh = open(out_starts_fn, "w")
    
    for seq_id in sorted(blasts_mapping_alt_start):
        seq_start = blasts_mapping_alt_start[seq_id]
        out_queries_fh.write(str(seq_id) + "\t" + seq_start + "\n")
    out_queries_fh.close()
    
    
    

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    iteration = currentClosureIteration()
    print("ext1_prepare_blast_queries.py: prepares BLAST queries to transfer annotations to newly added strains.")
    print("Current iteration: " + str(iteration))
    
    ensure_dir(parameters["DATASET_BLAST"])
    ensure_dir(parameters["DATASET_BLAST_RESULTS"])
    ensure_dir(parameters["DATASET_RESULTS_EXP_BLAST"])
    print(parameters["DATASET_BLAST"])
    print(parameters["DATASET_BLAST_RESULTS"])
    print(parameters["DATASET_RESULTS_EXP_BLAST"])
    
    strains = readStrainsInfo()
    strains_list = list(strains.allStrains())
    
    input_fn = parameters["DATASET_BLAST"] + "blasts_mapping.txt"
    if not os.path.exists(input_fn):
        open(input_fn, "w").close()
        print("The BLAST mapping file is created (no annotations to transfer).")
        exit()
    elif os.path.getsize(input_fn) < 2:
        print("The BLAST mapping file is empty (no annotations to transfer).")
        exit()
    else:
        all_blasts_computed = True
        for strain_id in strains_list:
            blast_fn = parameters["DATASET_BLAST_RESULTS"] + "/blast-" + strain_id + ".txt"
            if not os.path.exists(blast_fn):
                all_blasts_computed = False
                print("BLAST to be computed: " + blast_fn)
        if all_blasts_computed:
            print("All BLASTs are pre-computed.")
            exit()
            
    bp_map = getCodingMap()
    bp_map_rev = mapCodingMapRev(bp_map)
    
    blasts_mapping, max_id = readBlastResultsMapping(input_fn, sequences_subset=None, decode=False)
    
    blasts_mapping_altstarts = {}
    blasts_mapping_lens = {}
    for seq_id in blasts_mapping:
        seq = blasts_mapping[seq_id]
        seq_docoded_length = decodedDNAseqLength(seq, bp_map_rev)
        blasts_mapping_lens[seq_id] = seq_docoded_length
        
        decodeDNAseq(seq[:3], bp_map_rev)[:3]
        
        seq_start = seq[0]
        if not seq_start in ["8", "c", "("]:
            blasts_mapping_altstarts[seq_id] = decodeDNAseq(seq[:3], bp_map_rev)[:3]

    
    saveQueries(parameters["DATASET_RESULTS_EXP_BLAST"] + "/queries-ext.fasta", blasts_mapping, decode=True)
        
    saveAltStarts(parameters["DATASET_RESULTS_EXP_BLAST"] + "/alt_starts-ext.txt", blasts_mapping_altstarts, decode=True)
    saveBlastResultsMappingLens(parameters["DATASET_RESULTS_EXP_BLAST"] + "/lens-ext.txt", blasts_mapping_lens)
    print("ext1_prepare_blast_queries.py: Finished.")


