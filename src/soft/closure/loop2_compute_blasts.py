import sys
import os
import time
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_closure_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_seq_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_progress import ShowProgress

def blastDna(strain_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    iteration = currentClosureIteration()
    queries_dir = parameters["DATASET_RESULTS_EXP_BLAST"]
    results_tmp_dir = parameters["DATASET_RESULTS_EXP_BLAST_TMP"] 
  
    tr_evalue = parameters["BE"]
    #genome_fn = parameters["DATASET_GENOMES"]+'seq-' + strain_id + ".fasta";
    genome_db = parameters["DATASET_GENOMES_DBS"] + strain_id
    
    query_fn = queries_dir + "/queries-" + str(iteration)+".fasta"
    src_output_fn = results_tmp_dir + "/tmp-" + strain_id +"-"+str(iteration) + ".txt";
    dst_output_fn = results_tmp_dir + "/blast-" + strain_id +"-"+str(iteration)+ ".txt";
    
    if os.path.exists(dst_output_fn) or not os.path.exists(query_fn) or os.path.getsize(query_fn) < 2:
        return strain_id
    
    masking = False
    if "LCM" in parameters and parameters["LCM"].upper() == "Y":
        masking =True
    
    if parameters["BLAST_VER"].upper() == "BLAST+":
        blastn_exe = parameters["BLASTN_PATH"]
        runBlastn(blastn_exe, query_fn, genome_db, src_output_fn, tr_evalue, masking=masking);

    elif parameters["BLAST_VER"].upper() == "BLASTALL": 
        blastall_exe = parameters["BLASTALL_PATH"]
        runBlastallBlastn(blastall_exe, query_fn, genome_db, src_output_fn, tr_evalue, masking=masking);
    os.rename(src_output_fn, dst_output_fn)
    return strain_id

if __name__ == '__main__':
    s_time = time.time()
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    iteration = currentClosureIteration()
    strains = readStrainsInfo()

    print("loop2_compute_blasts.py: computes BLASTs to transfer annotations between strains.")
    print("Current iteration: " + str(iteration))
    
    ensure_dir(parameters["DATASET_RESULTS_EXP_BLAST"])
    ensure_dir(parameters["DATASET_RESULTS_EXP_BLAST_TMP"] )  
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    strains_list = list(strains.allStrains())
    n = len(strains_list)
    
    progress = ShowProgress("Computed BLASTs")
    progress.setJobsCount(n)
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap_unordered(blastDna, strains_list):
            progress.update(desc=r)
        pool.close()
        pool.join()
    else:
        for strain_id in strains_list:
            r = blastDna(strain_id)
            progress.update(desc=r)
    print("loop2_compute_blasts.py: Finished.")
    e_time = time.time()
    print("Exe time: " + str(e_time - s_time))


    