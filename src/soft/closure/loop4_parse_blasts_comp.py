import sys
import os
import multiprocessing
import time

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_closure_utils import *
from soft.structs.blast_hit import *
from soft.structs.gene import *
from soft.utils.camber_graph_utils import readBlastsMappingLens, getMaxID,\
    readBlastsMappingAltstarts
    
def parseBlastResultsStrains(strain_id):
    overwriteParameters(sys.argv)
    parameters = readParameters()

    iteration = currentClosureIteration()
    
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    readStrainSequences(strain)
    strain.calcRevSequence()    
    
    is_cds = True

    len_p = float(parameters["PLC"])/float(100.0)
    id_p = float(parameters["PID"])
    evalue_tr = float(parameters["TE"])
    if parameters["HSSP"] == "NO":
        hssp_b = False
    else:
        hssp_b = True
    pid_method = 0
    if parameters["PID_TYPE"] == "Q":
        pid_method = 0
    elif parameters["PID_TYPE"] == "B":
        pid_method = 1

    output_dst_fn = getItPath(parameters, iteration, "DATASET_RESULTS_EXP_BLAST_ACC") + "/"+strain_id + ".txt"
    max_id = getMaxID(output_dst_fn)

    input_lens_fn = parameters["DATASET_RESULTS_EXP_BLAST"]+"/lens-comp-"+str(iteration)+".txt"
    input_altstarts_fn = parameters["DATASET_RESULTS_EXP_BLAST"]+"/alt_starts-comp-"+str(iteration)+".txt"
    input_fn = parameters["DATASET_BLAST_RESULTS"]+"/blast-" + strain.strain_id + ".txt"

    blast_mapping_lens = readBlastsMappingLens(input_lens_fn)
    blast_mapping_altstarts = readBlastsMappingAltstarts(input_altstarts_fn)
    
    if len(blast_mapping_lens) == 0 or not os.path.exists(input_fn):
        return strain_id

    input_fh = open(input_fn)
    out_text_list = []
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) == 0:
            continue
        query_id = int(tokens[0])
        if query_id <= max_id:
            continue
        query_len = blast_mapping_lens.get(query_id)
        if query_len == None:
            continue
        blast_hit = parseParsedBlastHit(None, tokens, strain)
        if blast_hit == None:
            continue
        alt_start = blast_mapping_altstarts.get(query_id)
        blast_hit.query_len = query_len
        blast_hit = rateBlastHitDna(blast_hit, strain, len_p, id_p, evalue_tr, pid_method, hssp_b, alt_start=alt_start, is_cds=is_cds)
        if blast_hit == None:
            continue
        out_text_list.append(blast_hit.toString())
    input_fh.close()
    
    output_dst_fn = getItPath(parameters, iteration, "DATASET_RESULTS_EXP_BLAST_ACC") + "/"+strain_id + ".txt"
    output_fh = open(output_dst_fn, "a")
    output_fh.write(''.join(out_text_list))
    output_fh.close()
    
    return strain_id
    
if __name__ == '__main__':
    overwriteParameters(sys.argv)
    parameters = readParameters()
    
    iteration = currentClosureIteration()
    strains = readStrainsInfo()
    ensure_dir(parameters["DATASET_RESULTS_EXP_BLAST"])
    ensure_dir(getItPath(parameters, iteration, "DATASET_RESULTS_EXP_BLAST_ACC"))

    print("loop5_parse_blasts_comp.py: Parses and accepts or rejects pre-computed BLAST hits.")

    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    TASKS = []
    for strain_id in strains.allStrains():
        TASKS.append(strain_id)
    
    n = len(TASKS)

    progress = ShowProgress("Processed pre-computed BLASTs")
    progress.setJobsCount(n)
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)
        for r in pool.imap_unordered(parseBlastResultsStrains, TASKS):
            progress.update(desc=r)
        pool.close()
        pool.join()
    else:
        for strain_id in TASKS:
            r = parseBlastResultsStrains(strain_id)
            progress.update(desc=r)
            
    input_lens_fn = parameters["DATASET_RESULTS_EXP_BLAST"]+"/lens-comp-"+str(iteration)+".txt"
    os.remove(input_lens_fn)
    input_altstarts_fn = parameters["DATASET_RESULTS_EXP_BLAST"]+"/alt_starts-comp-"+str(iteration)+".txt"
    os.remove(input_altstarts_fn)
    
    print("loop5_parse_blasts_comp.py: Finished.")
    