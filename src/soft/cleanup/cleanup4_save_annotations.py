import sys
import time
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *
from soft.utils.camber_io_utils import ensure_dir


def readMultigeneDetails(params):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strain, closure_iteration, ph2_iteration = params[:3]
    strain_id = strain.strain_id
    
    acc_ccs = set([])
    
    input_fh = open(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_CLEANUP", ph2_iteration) + "selected_clusters.txt")
    for line in input_fh.readlines():
        line = line.strip()
        if line.startswith("#"):
            continue
        tokens = line.split()
        if len(tokens) < 1:
            continue
        acc_ccs.add(tokens[0])
    input_fh.close()
    #print(text_line.split("\t"))
    text_lines = []
    
    input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_UNIANNS", ph2_iteration) + strain_id + ".txt"
    input_fh = open(input_fn)
    
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split()
        if len(tokens) < 2:
            continue
        cluster_id = tokens[0]
        if cluster_id in acc_ccs:
            text_lines.append(line)
        elif line.startswith("#"):
            text_lines.append(line)
        else:
            text_lines.append("#" + line)
    input_fh.close()    

    output_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_UNIANNS", ph2_iteration+1) + strain_id + ".txt"
    output_fh = open(output_fn, "w")
    for text_line in text_lines:
        output_fh.write(text_line + "\n")
    output_fh.close()  

    return strain_id


if __name__ == '__main__':
    print("cleanup4_save_annotations.py: Saves annotations for remaining components.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()

    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())  
    strains = readStrainsInfo()
    strains_list = list(strains.allStrains())
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=3)-1
    
    ensure_dir(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_CLEANUP", ph2_iteration))
    
    TASKS = []
    for strain_id in strains_list:
        strain = strains.strain(strain_id)
        TASKS.append((strain, closure_iteration, ph2_iteration))
    ensure_dir(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_UNIANNS", ph2_iteration+1))
    
    progress = ShowProgress("Saved cc details, it=" + str(closure_iteration))
    progress.setJobsCount(len(TASKS))
    
    multigenes = {}
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap(readMultigeneDetails, TASKS):
            strain_id = r
            progress.update(strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = readMultigeneDetails(TASK)
            strain_id = r    
            progress.update(strain_id)  

    print("cleanup4_save_annotations.py: Finished.")
    
