import sys
import time
import os
import shutil

import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_fu_utils import UnionFind
from soft.utils.camber_graph_utils import readBlastResultsMapping
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *
from soft.utils.camber_io_utils import ensure_dir

def updateCompGraph(params):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    gt, closure_iteration, ph2_iteration = params[:3]
    
    seq_ids = set([])
    
    input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration+1) + "seqids_all.txt"
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) == 0:
            continue
        seq_ids.add(int(tokens[0]))
    input_fh.close()    
    
    if gt == "b":
        input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration) + "edges_seq.txt"
        output_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration+1) + "edges_seq.txt"
    elif gt == "m":
        input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration) + "edges_mgs.txt"
        output_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration+1) + "edges_mgs.txt"
    elif gt == "g":
        input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration) + "seq_clusters.txt"
        output_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration+1) + "seq_clusters.txt"

    text_lines = []
        
    if gt in ["b", "m"]:
        input_fh = open(input_fn)
        for line in input_fh.readlines():
            tokens = line.split()
            if len(tokens) != 2:
                continue
            seq1_id = int(tokens[0])
            seq2_id = int(tokens[1])
            if seq1_id in seq_ids and seq2_id in seq_ids:
                text_lines.append(str(seq1_id) + "\t" + str(seq2_id))
        input_fh.close()
    elif gt == "g":
        input_fh = open(input_fn)
        for line in input_fh.readlines():
            tokens = line.split()
            if len(tokens) < 2:
                continue
            cluster_id = tokens[0]
            text_line = cluster_id + "\t"
            seq_ids_tokens = tokens[1].split(":")
            any_found = False
            for seq_id_token in seq_ids_tokens:
                seq_id = int(seq_id_token)
                if seq_id in seq_ids:
                    text_line += str(seq_id) + "\t"
                    any_found = True
            text_line = text_line[:-1]
            if any_found:
                text_lines.append(text_line)
        input_fh.close()
    all_orfs_fh = open(output_fn, "w")
    for text_line in text_lines:
        all_orfs_fh.write(text_line + "\n")
    all_orfs_fh.close();
        
    return gt

if __name__ == '__main__':
    print("cleanup6_update_graphs.py: Update graphs.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=3)-1
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())   
    
    TASKS = [("b",closure_iteration, ph2_iteration),  
             ("m",closure_iteration, ph2_iteration), 
             ("g",closure_iteration, ph2_iteration)]
    
    
    progress = ShowProgress("Update graphs")
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes = WORKERS)    
        for t in pool.imap(updateCompGraph, TASKS):
 
            progress.update(desc = t)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            t = updateCompGraph(TASK) 
            ##print(strain_id, len(sub_cc_ann_counts_strain), len(sub_cc_cons_counts_strain))

            progress.update(desc = t)

    output_fh = open(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PHASE2_IT") + "ph2_step.txt", "w")
    output_fh.write(str(ph2_iteration+1) + "\n")
    output_fh.close()
    
    dir_path = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PHASE2_TMP")
    #shutil.rmtree(dir_path, True, None)
    
    print("cleanup6_update_graphs.py: Finished.")
        