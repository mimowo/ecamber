import sys
import os
import multiprocessing

sys.path.append("../../")
from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_graph_utils import *
from soft.utils.camber_coding_utils import codeDNAseq
from soft.structs.multigene import mgIntoSortableTriple

def getStrainAllSequences(params):
    
    overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strain, closure_iteration, ph2_iteration = params[:3]
    strain_id = strain.strain_id
    
    readUniAnnotationsIter(strain, closure_iteration, ph2_iteration+1)

    multigenes = strain.uni_annotation.multigenes
    mgs = {}
    
    for mg_id_short in list(multigenes):
        multigene = multigenes[mg_id_short]
        mg_id = multigene.mg_strain_unique_id
  #      if strain_id in ["IAI1", "S88"]:
  #          print(mg_id, multigene.mg_strain_unique_id, multigene.mg_unique_id)
        for gene in multigene.genes.values():
            gene_len = gene.length()
            if not mg_id in mgs:
                mgs[mg_id] = set([])
            mgs[mg_id].add(gene_len)

    seqids_final = set([])
    mg_pairs = {}
    
    input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_MGSSEQ", ph2_iteration) + "/" + strain_id + ".txt"
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split("\t")
        if len(tokens) < 2:
            continue
        mg_id = tokens[0]
     #   if strain_id == "IAI1":
      #      print(mg_id, list(mgs.keys())[:3])
        if not mg_id in mgs:
            continue
        mg_pairs[mg_id] = []
        tokens_seq = tokens[1].split(";")
        for token_seq in tokens_seq:
            tokens_len = token_seq.split(":")
            seq_len = int(tokens_len[0])
            seq_id = int(tokens_len[1])
            if not seq_len in mgs[mg_id]:
                continue
            seqids_final.add(seq_id)
            mg_pairs[mg_id].append((seq_len, seq_id))
    input_fh.close()
    
    text_lines = []
    
    mgs_sorted = sorted(mg_pairs, key=lambda mg_id: mgIntoSortableTriple(mg_id))

    for mg_id in mgs_sorted:
        seq_pairs_sorted = sorted(mg_pairs[mg_id])
        if len(seq_pairs_sorted) > 0:
            text_line = mg_id + "\t"
            for (seq_len, seq_id) in seq_pairs_sorted:
                text_line += str(seq_len) + ":" + str(seq_id) + ";"
            text_line = text_line[:-1]
        text_lines.append(text_line)
    
    output_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_MGSSEQ", ph2_iteration+1) + "/" + strain_id + ".txt"
    output_fh = open(output_fn, "w")
    for text_line in text_lines:
        output_fh.write(text_line + "\n")
    output_fh.close()
    
#    output_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_SEQIDS", ph2_iteration+1) + "/" + strain_id + ".txt"
#    output_fh = open(output_fn, "w")
#    for seq_id in sorted(seqids_final):
#        output_fh.write(str(seq_id) + "\n")
#    output_fh.close()    
    
    return strain_id, closure_iteration, set(seqids_final)
    
if __name__ == '__main__':
    print("cleanup5_mgsseq_mapping.py: Saves sequence ids by strains after the last iteration.")
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()

    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    closure_iteration = currentClosureIteration() 
    ph2_iteration = currentPh2Iteration(default=3)-1
    output_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration+1)
    ensure_dir(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_MGSSEQ", ph2_iteration+1))
   # ensure_dir(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_SEQIDS", ph2_iteration+1))
    all_seq_ids = set([])
 
    strains_list = list(strains.allStrains())

    progress = ShowProgress("Saved sequence ids, it=" + str(closure_iteration))
    n = len(strains.allStrains())
    progress.setJobsCount(n)
         
    TASKS = []
    for strain_id in strains_list:
        strain = strains.strain(strain_id)
        TASKS.append((strain, closure_iteration, ph2_iteration))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)        
        for r in pool.imap_unordered(getStrainAllSequences, TASKS):
            strain_id, iteration, new_seq_ids = r
            all_seq_ids.update(new_seq_ids)                      
            progress.update(strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = getStrainAllSequences(TASK)
            strain_id, iteration, new_seq_ids = r  
            all_seq_ids.update(new_seq_ids)
            progress.update(strain_id)


    output_fn = output_dir+ "seqids_all.txt"
    output_fh = open(output_fn, "w")
    
    for seq_id in sorted(all_seq_ids):
        output_fh.write(str(seq_id) + "\n")
    
    output_fh.close()
    
    print("cleanup5_mgsseq_mapping.py: Finished.")
        
