import sys
import time
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_graph_utils import *
from soft.structs.clusters import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *

def binom(n, k):
    if k <=0:
        return 1
    if 2*k>n:
        k = n-k
    ntok = 1
    for t in range(0, k, 1):
        ntok = ntok*(n-t)//(t+1)
    return ntok;


def getPvalueAnn(k, n, p):
    try:
        q = 1-p
        pvalue = 0.0
        for i in range(k, n +1, 1):
            pvalue += binom(n, i)*p**i*q**(n-i)
    except:
        return None    
    return pvalue 

def gcContent(seq):
    gc_count = 0
    for c in seq:
        if c in ["G", "C"]:
            gc_count += 1
    seq_len = len(seq)
    return float(gc_count) / float(seq_len)

def saveClustersStats(output_fh, clusters, strains):

    strains_ordered = sorted(strains.allStrains())
    
    mgs_all_count = 0
    mgs_ann_count = 0
    
    for cluster in list(clusters.clusters.values()):
        for multigene in cluster.nodes.values():
            mg_length = multigene.length() 
            if not multigene.ann_id in ["x", ""]:
                mgs_ann_count += 1
            mgs_all_count += 1
    
    if mgs_all_count > 0: p = float(mgs_ann_count) / float(mgs_all_count)
    else: p = 1.0
        
    text_line = "cluster_id\tcc_name\tcc_type\tmg_pres\tmg_ann\tmg_ratio\tmg_pvalue\tstrain_pres\tstrain_ann\tstrain_ratio\toverlap\tmedian_overlap_ratio\tlongest_overlap_ratio\tmax_tis\tmax_length\tmedian_length"
    output_fh.write(text_line + "\n")
          
    for cluster in list(clusters.clusters.values()):
        cluster.geneName(strains_ordered)
        cc_type = "ANCHOR"
        max_length = 0
        median_length = 0
        text_line  = cluster.cluster_id + "\t"
        text_line += cluster.gene_name + "\t"
        str_pres = set([])
        str_ann = set([])
        mg_overlaps = []
        mg_overlap_ratios = []
        lengths = []
        longest_yes_count = 0
        longest_no_count = 0
        #untypical_starts = 0
        for multigene in cluster.nodes.values():
            mg_length = multigene.length() 
            lengths.append(mg_length)
            if multigene.strain.strain_id in str_pres:
                cc_type = "NON_ANCHOR"
            else:
                str_pres.add(multigene.strain.strain_id)
            if not len(multigene.ann_id) <= 1:
                str_ann.add(multigene.strain.strain_id)

            if multigene.longest_overlap == "Y":
                longest_yes_count += 1
            else:
                longest_no_count += 1
                mg_overlaps.append(multigene.overlap)
                mg_overlap_ratios.append(multigene.overlap_ratio)
        
        longest_overlap_ratio = float(longest_yes_count) / float(longest_yes_count + longest_no_count) 
        max_length = max(lengths)
        median_length = median(lengths) 
        
        max_tis = 0
        for multigene in cluster.nodes.values():
            max_tis = max(max_tis, len(multigene.lengths)) 
        
        mg_count = len(cluster.nodes)
        mg_ann_count = cluster.ann_mgs_count()
        
        pvalue = getPvalueAnn(mg_ann_count, mg_count, p)
        
        mg_ann_ratio = float(mg_ann_count) / float(mg_count)
        
        strain_count = len(str_pres)
        strain_ann_count = len(str_ann)
        strain_ann_ratio = float(strain_ann_count) / float(strain_count)
        
        median_overlap = median(mg_overlaps)
        median_overlap_ratio = median(mg_overlap_ratios) 
        
        text_line += cc_type + "\t"
        text_line += str(mg_count) + "\t"
        text_line += str(mg_ann_count) + "\t"

        text_line += str("%.2f" % mg_ann_ratio) + "\t"
        if pvalue == None:
            text_line += "NA" + "\t"
        else:
            text_line += str("%.6f" % pvalue) + "\t"

        text_line += str(strain_count) + "\t"
        text_line += str(strain_ann_count) + "\t"
        
        text_line += str("%.2f" % strain_ann_ratio) + "\t"     
        
        text_line += str(median_overlap) + "\t"        
        text_line += str("%.2f" % median_overlap_ratio) + "\t"
        text_line += str("%.2f" % longest_overlap_ratio) + "\t"
        text_line += str(max_tis) + "\t"
        text_line += str(max_length) + "\t"
        text_line += str(median_length)
    
        output_fh.write(text_line + "\n")
    return None

def readMultigeneClusterStrainExt(params):
    overwriteParameters(sys.argv)
    parameters = readParameters() 
    
    strain, closure_iteration, ph2_iteration = params[:3]
    
    strain_id, cc_mapping = readMultigeneClusterStrain((strain, closure_iteration, ph2_iteration))
    
    mg_overlaps = {}
    mg_overlap_ratios = {}
    mg_longest_overlap = {}
    
    input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_MGOVER", ph2_iteration) + strain_id + ".txt"
    input_fh = open(input_fn)
    
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split("\t")
        if len(tokens) < 3:
            continue
        mg_id = tokens[0]
        
        mg_overlaps[mg_id] = int(tokens[2]) 
        mg_overlap_ratios[mg_id] = float(tokens[3])
        mg_longest_overlap[mg_id] = tokens[4]
    input_fh.close() 
    
    for multigenes in cc_mapping.values():
        for multigene in multigenes:
            mg_id = multigene.mg_unique_id
            if mg_id in mg_overlap_ratios:
                multigene.overlap = mg_overlaps[mg_id]
                multigene.overlap_ratio = mg_overlap_ratios[mg_id]
                multigene.longest_overlap = mg_longest_overlap[mg_id]
            else:
                multigene.overlap = 0
                multigene.overlap_ratio = 0.0
                multigene.longest_overlap = "Y"
    
    return strain_id, cc_mapping
    
    
if __name__ == '__main__':
    print("cleanup2_clusters_stats.py: saves details on multigene clusters.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    strains_list = strains.allStrains()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=3)-1
    
    ensure_dir(getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_CLEANUP", ph2_iteration))
    
    clusters = MGClusters()
    
    TASKS = []
    for strain_id in strains_list:
        strain = strains.strain(strain_id)
        TASKS.append((strain, closure_iteration, ph2_iteration))
    
    progress = ShowProgress("cleanup2_component_stats.py")
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes = WORKERS)    
        for strain_id, cc_mapping in pool.imap(readMultigeneClusterStrainExt, TASKS):
            
            for cluster_id in cc_mapping:
                if not cluster_id in clusters.clusters:
                    cluster = MGCluster(cluster_id)
                    clusters.addCluster(cluster)
                else:
                    cluster = clusters.getCluster(cluster_id)
                for multigene in cc_mapping[cluster_id]:
                    cluster.addMultigene(multigene)
            
            progress.update(desc = strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            strain_id, cc_mapping =readMultigeneClusterStrainExt(TASK)
            
            for cluster_id in cc_mapping:
                if not cluster_id in clusters.clusters:
                    cluster = MGCluster(cluster_id)
                    clusters.addCluster(cluster)
                else:
                    cluster = clusters.getCluster(cluster_id)
                for multigene in cc_mapping[cluster_id]:
                    cluster.addMultigene(multigene)
            
            progress.update(desc = strain_id)
    
    clusters.classifyClusters(strains_list)    
    
    output_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_CLEANUP", ph2_iteration) + "cluster_stats.txt"
    output_fh = open(output_fn, "w")
    saveClustersStats(output_fh, clusters, strains)
    output_fh.close()
    
    print("cleanup2_clusters_stats.py: Finished.")
