class BlastHit:
    def __init__(self, query_gene, contig_id, hit_gene, evalue_b, aln_len=0, residues=0, identity_b=0, query_start=0, query_end=0,  identities=0, mismatches=0,query="", query_id=None, query_len=None):
        self.contig_id = contig_id;
        self.query_gene = query_gene
        self.hit_gene = hit_gene
        self.evalue_b = evalue_b
        self.identity_b = identity_b
        self.identities = identities
        self.residues = residues
        self.aln_len = aln_len
        self.query_start = int(query_start)
        self.query_end = int(query_end)
        self.mismatches = mismatches
        self.query = query
        self.query_len = query_len
        self.query_id = query_id
    def toString(self):
        out = str(self.query_id)
        out += "\t" + str(self.hit_gene_ext.start)
        out += "\t" + str(self.hit_gene_ext.end)
        out += "\t" + self.hit_gene_ext.strand
        #out += "\t" + self.hit_gene_ext.strain_id
        if self.contig_id != self.hit_gene.strain_id:
            out += "\t" + self.contig_id
    #    if out.count("NZ9555") > 0:
     #       print(self.query_id, self.hit_gene_ext.start, self.hit_gene_ext.end, self.hit_gene_ext.strand, self.contig_id)
        out += "\n"
        return out;
    def addBlastHitExtended(self, hit_gene_ext):
        self.hit_gene_ext = hit_gene_ext
