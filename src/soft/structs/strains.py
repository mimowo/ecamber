class Genome(object):
    def __init__(self, strain_id, short_name = ""):
        self.name = strain_id;
        if short_name == "":
            self.short_name = strain_id
        else:
            self.short_name = short_name 
        self.strain_id = strain_id
        self.annotation = None
        self.new_annotation = None
        self.all_annotation = None
        self.final_annotation = None
        self.uni_annotation = None
        self.sequences_map = {}
        self.rev_sequences_map = {}
        self.sequence = None
        self.rev_sequence = None
    def calcRevSequence(self):
        from soft.utils.camber_seq_utils import complementarySequence
        if self.sequence != None:
            self.rev_sequence = complementarySequence(self.sequence)
        else:
            if len(self.sequences_map) >= 1:
                for strain_id in self.sequences_map:
                    seq = self.sequences_map[strain_id]
                    self.rev_sequences_map[strain_id] = complementarySequence(seq)        
    def setSequence(self, sequence):
        self.sequence = sequence;
    def setSequences(self, sequences_map):
        self.sequences_map = sequences_map;
    def getSequencesMap(self):
        return self.sequences_map
    def getSequence(self):
        return self.sequence;
    def setAnnotation(self, annotation):
        self.annotation = annotation;
    def annotation(self):
        return self.annotation;
    def setNewAnnotation(self, new_annotation):
        self.new_annotation = new_annotation;
    def setFinalAnnotation(self, final_annotation):
        self.final_annotation = final_annotation;
    def setUniAnnotation(self, uni_annotation):
        self.uni_annotation = uni_annotation;
    def setAllAnnotation(self, all_annotation):
        self.all_annotation = all_annotation;
    def newAnnotation(self):
        return self.new_annotation;
    def totalGenomeLength(self):
        if self.sequence != None:
            return len(self.sequence)
        else:
            tot_length = 0
            for seq in self.sequences_map.values():
                tot_length += len(seq)
            return tot_length
    #def name(self):
    #    return self.name;

class Strain(Genome):
    def __init__(self, strain_id, short_name= ""):
        super(Strain, self).__init__(strain_id, short_name)
        
class Strains:
    def __init__(self):
        self.strains = {}
        self.ref_strain = ""
        self.strains_ordered = list()
        self.strains_by_short_names = {}
    def addStrain(self, strain_id, short_name = ""):
        if short_name == "":
            short_name = strain_id
        strain = Strain(strain_id, short_name)
        self.strains[strain_id] = strain
        self.strains_by_short_names[strain.short_name] = strain#.getGenome()
        self.strains_ordered.append(strain_id)
    def setRefStrain(self, strain_name):
        self.ref_strain = strain_name;
    def refStrain(self):
        if self.ref_strain == "":
            return list(self.strains)[0]
        return self.ref_strain
    def allStrains(self):
        return list(self.strains_ordered);
    def strain(self, strain_id):
        return self.strains[strain_id]
    def count(self):
        return len(self.strains)
    def strainsCount(self):
        return len(self.strains)
    def getStrainByIndex(self, index):
        return self.strains_ordered[index]

