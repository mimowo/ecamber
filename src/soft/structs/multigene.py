from soft.utils.camber_seq_utils import complementarySequence

class Multigene:
    def __init__(self, start, end, strand, strain, ann_id = "x", gene_name = "x", contig_id = ""):
        if strand =="+":
            self.end = max(int(start), int(end))
            self.start = min(int(start), int(end))
        else:
            self.end = min(int(start), int(end))
            self.start = max(int(start), int(end))
        self.strand = strand;
        self.strain = strain;
        self.strain_id = strain.strain_id;
        self.left_bound = min(self.start, self.end)
        self.right_bound = max(self.start, self.end)
        self.ann_id = ann_id
        self.gene_name = gene_name
        self.contig_id = contig_id
        self.mg_unique_id = createMultigeneId(self.end, self.strand, self.contig_id, self.strain_id)
        self.mg_strain_unique_id = createStrainMultigeneId(self.end, self.strand, self.strain_id, self.contig_id)
        self.genes = {}
        self.lengths = set([])
        self.ann_lengths = set([])
        self.sel_lengths = set([])

    def setLengths(self, lengths):
        self.lengths = lengths
    def setAnnLengths(self, lengths):
        self.ann_lengths = lengths
    def setSelLengths(self, lengths):
        self.sel_lengths = lengths    
    
    def getGeneID(self):
        left_bound, right_bound = self.getBounds()
        if self.strand == "+":
            if self.contig_id == "":
                return str(left_bound) + "." + str(self.end) + "." + self.strand + "." + self.strain_id
            else:
                return str(left_bound) + "." + str(self.end) + "." + self.strand + "." + self.contig_id
        else:
            if self.contig_id == "":
                return str(right_bound) + "." + str(self.end) + "." + self.strand + "." + self.strain_id
            else:
                return str(right_bound) + "." + str(self.end) + "." + self.strand + "." + self.contig_id
            
    def getGeneStrainID(self):
        left_bound, right_bound = self.getBounds()
        if self.strand == "+":
            if self.contig_id == "":
                return str(left_bound) + "." + str(self.end) + "." + self.strand + "." + self.strain_id
            else:
                return str(left_bound) + "." + str(self.end) + "." + self.strand + "." + self.strain_id + "." + self.contig_id
        else:
            if self.contig_id == "":
                return str(right_bound) + "." + str(self.end) + "." + self.strand + "." + self.strain_id
            else:
                return str(right_bound) + "." + str(self.end) + "." + self.strand + "." + self.strain_id + "." + self.contig_id
            
    def addGene(self, gene):
        gene_length = gene.length()
        
        if len(gene.gene_id) > 1:
            self.ann_lengths.add(gene_length)
            self.ann_id = gene.gene_id
        if self.strand =="+":
            self.start = min(gene.start, self.start)
            self.left_bound = self.start
        else:
            self.start = max(gene.start, self.start)
            self.right_bound = self.start
        
        self.lengths.add(gene_length)
        if gene.selected == True:
            self.sel_lengths.add(gene_length)
        
        self.genes[gene_length] = gene

    def getBounds(self):
        if len(self.sel_lengths) > 0:
            sel_length = max(self.sel_lengths)
            if self.strand == "+":
                return (self.end - sel_length + 1, self.end)
            else:
                return (self.end, self.end + sel_length - 1)
        return (self.left_bound, self.right_bound)
    
    def getStartEnd(self):
        if len(self.sel_lengths) > 0:
            sel_length = max(self.sel_lengths)
            if self.strand == "+":
                return (self.end - sel_length + 1, self.end)
            else:
                return (self.end + sel_length - 1, self.end)
        return (self.start, self.end)
    
    def getLeftBound(self):
        if len(self.sel_lengths) > 0:
            sel_length = max(self.sel_lengths)
            if self.strand == "+":
                return self.end - sel_length + 1
            else:
                return self.end
        return self.left_bound

    def getRightBound(self):
        if len(self.sel_lengths) > 0:
            sel_length = max(self.sel_lengths)
            if self.strand == "+":
                return self.end
            else:
                return self.end + sel_length - 1
        return self.right_bound

    def length(self):
        if len(self.sel_lengths) > 0:
            return max(self.sel_lengths)
        return self.right_bound - self.left_bound + 1
        
    def fulllength(self):
        return self.right_bound - self.left_bound + 1

    def sequence(self, length=None):
        start, end = self.getStartEnd()
        
        strain = self.strain
        
        if strain.sequence != None:
            strain_sequence = strain.sequence
        else:
            strain_sequence = strain.sequences_map[self.contig_id]

        if self.strand == '+':
            if length == None:
                seq_from = max(0, start-1)
            else:
                seq_from = max(0, end - length)
            seq_to = min(end, len(strain_sequence))
            gene_sequence = strain_sequence[seq_from:seq_to]
        else:
            seq_from = min(end-1, len(strain_sequence))
            if length == None:
                seq_to = max(0, start)
            else:
                seq_to = max(0, end + length)
                            
            gene_sequence = complementarySequence(strain_sequence[seq_from:seq_to])
        return gene_sequence
    
    def sequence_with_promotor(self, length=None, promotor_length=100):
        strain = self.strain
        start, end = self.getStartEnd()
        left_bound, right_bound = self.getBounds()
        
        if strain.sequence != None:
            strain_sequence = strain.sequence
        else:
            strain_sequence = strain.sequences_map[self.contig_id]
            
        if self.strand == '+':
            if length == None:
                seq_from = max(0, start - 1 - promotor_length)
            else:
                seq_from = max(0, end - length - promotor_length)
                
            seq_to = min(right_bound, len(strain_sequence))
            gene_sequence = strain_sequence[seq_from:seq_to]
        else:
            if length == None:
                seq_to = max(0, start + promotor_length)
            else:
                seq_to = max(0, end + length + promotor_length)
                
            seq_from = min(end-1, len(strain_sequence))
            
            gene_sequence = complementarySequence(strain_sequence[seq_from:seq_to])
            
            #gene_sequence = complementarySequence(strain_sequence[self.left_bound-1:self.right_bound+promotor_length])

        return gene_sequence    


    def promotor(self, length=None, promotor_length=100):
        
        start, end = self.getStartEnd()
        #left_bound, right_bound = self.getBounds()
        
        strain = self.strain
        if strain.sequence != None:
            strain_sequence = strain.sequence
        else:
            strain_sequence = strain.sequences_map[self.contig_id]
            
        if self.strand == '+':
            if length == None:
                seq_from = max(0, start - 1 - promotor_length)
            else:
                seq_from = max(0, end - length - promotor_length)
                
            seq_to = min(seq_from + promotor_length, len(strain_sequence))
            gene_sequence = strain_sequence[seq_from:seq_to]
        else:
            if length == None:
                seq_to = max(0, start + promotor_length)
            else:
                seq_to = max(0, end + length + promotor_length)
                
            seq_from = min(start, len(strain_sequence))
            gene_sequence = complementarySequence(strain_sequence[seq_from:seq_to])
            
        return gene_sequence 
    
def mgIntoSortableTriple(mg_id):
    tokens = mg_id.split()
    if len(tokens) >= 3: return (tokens[2], int(tokens[0]), tokens[1])
    else: return ("", int(tokens[0]), tokens[1])

def createMultigeneId(end, strand, contig_id, strain_id=""):
    if contig_id == "" or contig_id == strain_id:
        return str(end)+" "+strand
    else:
        return str(end)+" "+strand+" "+contig_id

def createStrainMultigeneId(end, strand, strain_id, contig_id):
    if contig_id == "":
        return str(end)+" "+strand+" "+strain_id
    else:
        return str(end)+" "+strand+" "+contig_id
