class Annotation:
    genes = {}
    multigenes = {}
    pseudo_genes = {}
    def __init__(self):
        self.genes = {}
        self.multigenes = {}
        self.pseudo_genes = {}
    def addGene(self, gene):
        self.genes[gene.unique_id] = gene
    def addMultigene(self, multigene):
        self.multigenes[multigene.mg_unique_id] = multigene
    def addPseudoGene(self, pseudo_gene):
        self.pseudo_genes[pseudo_gene.unique_id] = pseudo_gene
    def multigenes(self):
        return self.multigenes
    def genes(self):
        return self.genes;
    def pseudo_genes(self):
        return self.pseudo_genes
