from soft.utils.camber_utils import strains
from soft.utils.camber_progress import ShowProgress

class MGCluster:
    def __init__(self, cluster_id):
        self.cluster_id = cluster_id
        self.is_anchor = False
        self.nodes = {}
        self.nodes_by_strain = {}
        self.gene_name = ""
        
    def isAnchor(self):
        if self.is_anchor: return True
        else: return False
        
    def geneName(self, strains_list):
        gene_name_counts = {}
        if len(self.gene_name) > 1: return self.gene_name
        
        for strain_id in strains_list:
            multigenes = self.nodesByStrain(strain_id)
            if len(multigenes) > 0:
                for multigene in multigenes.values():
                    gene_name = multigene.gene_name
                    if len(gene_name) <= 1 or multigene.gene_name == multigene.ann_id:
                        continue
                    if not gene_name in gene_name_counts:
                        gene_name_counts[gene_name] = 0
                    gene_name_counts[gene_name] += 1
        max_count = 0
        max_count_name = "x"
        
        for gene_name in gene_name_counts:
            count = gene_name_counts[gene_name]
            if count > max_count:
                max_count = count
                max_count_name = gene_name
        self.gene_name = max_count_name
        
        return self.gene_name;
                 
    def nodes(self):
        return self.nodes
     
    def nodesByStrain(self, strain_id):
        return self.nodes_by_strain.get(strain_id, {})
                
    def addMultigene(self, multigene):
        self.nodes[multigene.mg_strain_unique_id] = multigene
        if not multigene.strain_id in self.nodes_by_strain:
            self.nodes_by_strain[multigene.strain_id] = {}
        self.nodes_by_strain[multigene.strain_id][multigene.mg_strain_unique_id] = multigene
        
    def strains(self):
        return set(self.nodes_by_strain)
    
    def ann_mgs_count(self):
        count = 0
        for multigene in self.nodes.values():
            if len(multigene.ann_id) > 1:
                count+=1
        return count
    
    def ann_strains(self):
        ret = []
        for multigenes_map in self.nodes_by_strain.values():
            ann_strain_id = ""
            for multigene in multigenes_map.values():
                if len(multigene.ann_id) > 1:
                    ann_strain_id = multigene.strain_id
            if len(ann_strain_id) > 0:
                ret.append(ann_strain_id)
        return ret


class MGClusters:
    def __init__(self):
        self.clusters = {}
        self.anchors = {}
        self.non_anchors = {}
    def addCluster(self, cluster):
        self.clusters[cluster.cluster_id] = cluster
    def getCluster(self, cluster_id):
        return self.clusters[cluster_id]
    
    def classifyClusters(self, strains_subset=None):
        self.anchors = {}
        self.non_anchors = {}

        for cluster in list(self.clusters.values()):
            self.classifyCluster(cluster, strains_subset)

    def classifyCluster(self, cluster, strains_subset=None):
        cluster.is_anchor = True
        
        if strains_subset == None: strains_subset = cluster.nodes_by_strain.keys()
        
        for strain_id in strains_subset:
            mg_count = len(cluster.nodesByStrain(strain_id))
            if mg_count > 1:
                cluster.is_anchor = False
        if cluster.is_anchor == False:
            self.non_anchors[cluster.cluster_id] = cluster
        else:
            self.anchors[cluster.cluster_id] = cluster
        
