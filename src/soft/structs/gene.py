from soft.utils.camber_seq_utils import complementarySequence, translateSequence

class Gene:
    def __init__(self, start, end, strand, strain, gene_id="x", gene_name="x", contig_id = "", selected=False):
        if strand == "+":
            self.start = min(int(start), int(end))
            self.end = max(int(start), int(end))
            self.left_bound = self.start
            self.right_bound = self.end
        else:
            self.start = max(int(start), int(end))
            self.end = min(int(start), int(end))
            self.left_bound = self.end
            self.right_bound = self.start
        self.strand = strand
        self.strain = strain
        self.strain_id = strain.strain_id
        self.contig_id = contig_id
        self.unique_id = self.createGeneId(gene_id, self.left_bound, self.right_bound, strand, self.strain_id, contig_id)
        self.strain_unique_id = self.createStrainGeneId(gene_id, self.left_bound, self.right_bound, strand, self.strain_id, contig_id)
        self.gene_id = gene_id
        self.gene_name = gene_name
        self.selected = selected
        
    def createGeneId(self, gene_id, start, end, strand, strain_id, contig_id=""):
        if contig_id == "":
            return str(start)+"."+str(end)+"."+strand+"."+strain_id
        else:
            return str(start)+"."+str(end)+"."+strand+"."+contig_id
        
    def createStrainGeneId(self, gene_id, start, end, strand, strain_id, contig_id=""):
        if contig_id == "":
            return str(start)+"."+str(end)+"."+strand+"."+strain_id
        else:
            return str(start)+"."+str(end)+"."+strand+"."+strain_id+"."+contig_id

    def startCodon(self):
        strain = self.strain
        if strain.sequence != None:
            strain_sequence = strain.sequence
        else:
            strain_sequence = strain.sequences_map[self.contig_id]
        if self.strand == '+':
            gene_sequence = strain_sequence[self.left_bound-1:self.left_bound+2]
        else:
            gene_sequence = complementarySequence(strain_sequence[self.right_bound-3:self.right_bound])
        return gene_sequence
    
    def stopCodon(self):
        strain = self.strain
        if strain.sequence != None:
            strain_sequence = strain.sequence
        else:
            strain_sequence = strain.sequences_map[self.contig_id]
        if self.strand == '+':
            gene_sequence = strain_sequence[self.right_bound-3:self.right_bound]
        else:
            gene_sequence = complementarySequence(strain_sequence[self.left_bound-1:self.left_bound+2])
        return gene_sequence
    
    def sequence(self):
        strain = self.strain
        if strain.sequence != None:
            strain_sequence = strain.sequence
        else:
            strain_sequence = strain.sequences_map[self.contig_id]
        if self.strand == '+':
            gene_sequence = strain_sequence[self.left_bound-1:self.right_bound]
        else:
            gene_sequence = complementarySequence(strain_sequence[self.left_bound-1:self.right_bound])
        return gene_sequence

    def sequence_with_promotor(self, promotor_length=100):
        strain = self.strain
        
        if strain.sequence != None:
            strain_sequence = strain.sequence
        else:
            strain_sequence = strain.sequences_map[self.contig_id]
        
        if self.strand == '+':
            gene_sequence = strain_sequence[self.left_bound-1-promotor_length:self.right_bound]
        else:
            gene_sequence = complementarySequence(strain_sequence[self.left_bound-1:self.right_bound+promotor_length])
        return gene_sequence    

    def promotor(self, length=None, promotor_length=100):
        start, end = self.start, self.end
        
        strain = self.strain
        if strain.sequence != None:
            strain_sequence = strain.sequence
        else:
            strain_sequence = strain.sequences_map[self.contig_id]
            
        if self.strand == '+':
            if length == None:
                seq_from = max(0, start - 1 - promotor_length)
            else:
                seq_from = max(0, end - length - promotor_length)
                
            seq_to = min(seq_from + promotor_length, len(strain_sequence))
            gene_sequence = strain_sequence[seq_from:seq_to]
        else:
            if length == None:
                seq_to = max(0, start + promotor_length)
            else:
                seq_to = max(0, end + length + promotor_length)
                
            seq_from = min(start, len(strain_sequence))
            gene_sequence = complementarySequence(strain_sequence[seq_from:seq_to])
            
        return gene_sequence 

    def upstream(self, length=None, up_length=100):
        return self.promotor(length=length, promotor_length=up_length)

    def downstream(self, down_length=100):
        start, end = self.start, self.end
        
        strain = self.strain
        if strain.sequence != None:
            strain_sequence = strain.sequence
        else:
            strain_sequence = strain.sequences_map[self.contig_id]
            
        if self.strand == '+':
            seq_from = min(end, len(strain_sequence))
            seq_to = min(seq_from + down_length, len(strain_sequence))
            gene_sequence = strain_sequence[seq_from:seq_to]
        else:
            seq_to = max(end, 0)
            seq_from = max(seq_to - down_length, 0)
            gene_sequence = complementarySequence(strain_sequence[seq_from:seq_to])
            
        return gene_sequence 


    def proteinSequence(self):
        return translateSequence(self.sequence())

    def length(self):
        return self.right_bound - self.left_bound + 1

    def lengthAA(self):
        return len(self.proteinSequence())

    def toString(self):
        out = "";
        out += self.gene_id
        out += "\t" + str(self.start)
        out += "\t" + str(self.end)
        out += "\t" + self.strand
        if self.contig_id != "":
            out += "\t" + self.contig_id
        else:
            out += "\t" + self.strain_id
        out += "\n"
        return out;

    def asString(self):
        out = "";
        out += self.gene_id
        out += "\t" + str(self.start)
        out += "\t" + str(self.end)
        out += "\t" + self.strand
        if self.contig_id != "":
            out += "\t" + self.contig_id
        else:
            out += "\t" + self.strain_id
        return out;

