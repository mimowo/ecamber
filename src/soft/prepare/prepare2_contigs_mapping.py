import sys
import os
import multiprocessing

sys.path.append("../../")
from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import ensure_dir

def getTargetMapping(strain_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)

    contigs = set([])
    genome_fh = open(parameters["DATASET_GENOMES"] + strain.strain_id + ".fasta")
    genome_lines = genome_fh.readlines();
    genome_fh.close()
    
    accession = ""
    for line in genome_lines:
        parts = line.split();
        if len(parts) >= 1:
            if parts[0].startswith(">"):
                if accession != "":
                    contigs.add(accession)
                acc_tokens = parts[0].split("|")
                if len(acc_tokens) >= 4:
                    accession = acc_tokens[3]
                else:
                    accession = parts[0][1:]
                acc_tok = accession.split(".")
                accession = acc_tok[0]
    contigs.add(accession)
    
    contigs_list = list(contigs)
    target_desc_list = []
    
    if len(contigs_list) <= 1:
        target_desc_list.append(strain_id +"\t"+strain_id + "\t" +"m")
    else:
        if len(contigs_list) > 10:
            for contig_id in contigs_list:
                target_desc_list.append(contig_id +"\t"+strain_id + "\t" + "c")
        else:
            for contig_id in contigs_list:
                if contig_id == strain_id:
                    target_desc_list.append(contig_id +"\t"+strain_id + "\t" + "m")
                else:
                    target_desc_list.append(contig_id +"\t"+strain_id + "\t" + "c")
    
    return strain_id, max(1, len(contigs_list)), target_desc_list

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    print("prepare_contigs_mapping.py: Prepares mapping of contig names.")
    
    strains = readStrainsInfo()
    strains_list = list(strains.allStrains())
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())    
    ensure_dir(parameters["DATASET_RESULTS_EXP"])
    
    target_desc_map = {}
    
    progress = ShowProgress("Contig names mapping")
    n = len(strains_list)
    progress.setJobsCount(n)
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS)
        for strain_id, k, targets_list in pool.imap_unordered(getTargetMapping, strains_list):
            target_desc_map[strain_id] = (k, targets_list)
            progress.update(desc=strain_id)
        pool.close()
        pool.join()
    else:
        for strain_id in strains_list:
            strain_id, k, targets_list = getTargetMapping(strain_id)
            target_desc_map[strain_id] = (k, targets_list)
            progress.update(desc=strain_id)

    output_fn = parameters["DATASET_RESULTS_EXP"]+"contigs_mapping.txt"
    output_fh = open(output_fn, "w")
    
    strains_sorted = sorted(strains_list, key=lambda strain_id: target_desc_map[strain_id][0])
    for strain_id in strains_sorted:
        targets_list = target_desc_map[strain_id][1]
        for target_line in targets_list:
            output_fh.write(target_line + "\n")
    output_fh.close()        
    
    print("prepare_contigs_mapping.py: Finished")
    
    