import sys
import os
import multiprocessing


sys.path.append("../..")
from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import ensure_dir

def getGeneNameMappings(strain_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    #strains = readStrainsInfo()
    mappings_list = []

    input_fn = parameters["DATASET_ANNS_PARSED_SRC"] + strain_id+".txt"
    if not os.path.exists(input_fn):
        return strain_id, []

    all_input_fh = open(input_fn)
    lines = all_input_fh.readlines()
    for line in lines:
        line = line.strip()
        if not line:
            break
        tokens = line.split("\t")
        
        if len(tokens)>=5:
            if len(tokens[4]) < 2:
                continue
            if tokens[4] == strain_id or tokens[0] == tokens[4]:
                continue
            mappings_list.append(tokens[0]+"\t"+tokens[4])
    all_input_fh.close()

    strain_output_fh = open(parameters["DATASET_RESULTS_EXP_NAMES"]+"/"+strain_id + ".txt", "w")
    for text_line in mappings_list:
        strain_output_fh.write(text_line + "\n")
    strain_output_fh.close()

    return strain_id, mappings_list

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())

    print("prepare_ann_names.py: Combines gene names")
    
    ensure_dir(parameters["DATASET_RESULTS_EXP_NAMES"])
    progress = ShowProgress("Combined gene names")
    strains_list = list(strains.allStrains())
    progress.setJobsCount(len(strains_list))

    all_mappings_list = []
    strain_mappings_list = {}
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for strain_id, mappings_list in pool.imap_unordered(getGeneNameMappings, strains_list):
            all_mappings_list += mappings_list
            strain_mappings_list[strain_id] = mappings_list
            progress.update(desc=strain_id)
        pool.close()
        pool.join()
    else:
        for strain_id in strains_list:
            strain_id, mappings_list = getGeneNameMappings(strain_id)
            all_mappings_list += mappings_list
            strain_mappings_list[strain_id] = mappings_list
            progress.update(desc=strain_id)

    print("prepare_ann_names.py: Finished")
    
    