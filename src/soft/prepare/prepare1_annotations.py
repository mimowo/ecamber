import sys
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_seq_utils import translateSequence
from soft.utils.camber_io_utils import ensure_dir
from soft.utils.camber_closure_utils import *


#strains_ordered = sorted(strains_list, key=lambda strain_id:-len(strains.strain(strain_id).sequence))
def prepareAnnotations(strain_id):
    overwriteParameters(sys.argv)
    parameters = readParameters()
    iteration = 0#currentClosureIteration()
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    
    is_cds = True

    wrong_start = 0
    wrong_stop = 0
    wrong_length = 0
    stop_inside = 0
    
    readStrainSequences(strain)
    readOrgAnnotations(strain)
    
    #all_output_tmp_fn = getItPath(parameters, iteration, "DATASET_RESULTS_EXP_ALL")+"tmp-all-"  + strain_id+"-"+ str(iteration)+".txt"
    all_output_fn = getItPath(parameters, iteration, "DATASET_RESULTS_EXP_ALL") + strain_id + ".txt"
    all_output_fh = open(all_output_fn, "w");
    saveMgsAnnotations(all_output_fh, strain.annotation.genes, show_start_codons=True)
    all_output_fh.close()
    
    pseudo_output_fn = parameters["DATASET_RESULTS_EXP_PSEUDO"] + strain_id+".txt"
    pseudo_output_fh = open(pseudo_output_fn, "w");
    for pseudo_gene in list(strain.annotation.pseudo_genes.values()):
        res_code = correctGene(pseudo_gene, alt_starts=True, is_cds=is_cds)

        if res_code == -1:
            wrong_length += 1
        elif res_code == -2:
            wrong_start += 1
        elif res_code == -3:
            wrong_stop += 1
        elif res_code == -4:
            stop_inside += 1
            
        pseudo_output_fh.write(str(res_code)+" "+pseudo_gene.gene_id + " "+pseudo_gene.strain_unique_id+"\n")
        
        try: dna_sequence = pseudo_gene.sequence()
        except: dna_sequence = ""
        
        pseudo_output_fh.write(dna_sequence+"\n")
        pseudo_output_fh.write(translateSequence(dna_sequence)+"\n")
        pseudo_output_fh.write("\n")
    pseudo_output_fh.close()
    
    return strain_id, len(strain.annotation.genes)

if __name__ == '__main__':
    print("prepare_annotations.py: Initialization of the closure procedure.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()

    is_cds = True
        
    strains = readStrainsInfo()
    strains_list = strains.allStrains()
     
    ensure_dir(parameters["DATASET_BLAST"])
    ensure_dir(parameters["DATASET_RESULTS"]);
    ensure_dir(parameters["DATASET_RESULTS_EXP"]);
    ensure_dir(parameters["DATASET_RESULTS_EXP_PSEUDO"])
    ensure_dir(parameters["DATASET_RESULTS_EXP_PHASE1"])

    iteration_fh = open(parameters_map["DATASET_RESULTS_EXP_PHASE1"] +"ph1_it.txt", "w")
    iteration_fh.write("0\n")
    iteration_fh.close();

    ensure_dir(getItPath(parameters, 0, "DATASET_RESULTS_EXP_PHASE1_IT"))
    ensure_dir(getItPath(parameters, 0, "DATASET_RESULTS_EXP_ALL"))
    ensure_dir(getItPath(parameters, 0, "DATASET_RESULTS_EXP_NEW"))
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    TASKS = strains_list
    
    genes_count = 0
    
    progress = ShowProgress("Prepared annotations")
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap_unordered(prepareAnnotations, TASKS):
            strain_id, genes_count_strain = r
            genes_count += genes_count_strain
            progress.update(desc=strain_id)
        pool.close()
        pool.join()
    else:
        for strain_id in TASKS:
            strain_id, genes_count_strain = prepareAnnotations(strain_id)    
            genes_count += genes_count_strain   
            progress.update(desc=strain_id) 
        
    if genes_count == 0 and is_cds:
        print("The set of valid protein-coding annotations is empty, used parameter: as=" + parameters["AS"])
        print("Try to change the source of annotations ('as' parameter) or reformat the annotations.")
        print("AS=patric, annotations from PATRIC: " + parameters["DATASET_ANNS_PARSED"] + "patric/")
        print("AS=refseq, annotations from RefSeq: " + parameters["DATASET_ANNS_PARSED"] + "refseq/")
        print("AS=prodigal, annotations from PRODIGAL: " + parameters["DATASET_ANNS_PARSED"] + "prodigal/")
        print("AS=<other>, other source of annotations: "+ parameters["DATASET_ANNS_PARSED"] + "<other>/")
        print("prepare_annotations.py: Finished.")
        exit(-1)
        
    print("prepare_annotations.py: Finished.")
