import sys
import os
import shutil
sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *

def converToCAMBerVis(src_fn, dst_fn):
    dst_fh = open(dst_fn, "w")
    src_fh = open(src_fn)
    
    for line in src_fh.readlines():
        line = line.strip()
        conv_line = ""
        tokens = line.split("\t")
        for i in range(len(tokens)):
            if i == 2:
                conv_line += tokens[i].replace(" ",".") + "\t"
            else:
                conv_line += tokens[i] + "\t"
        
        conv_line = conv_line[:-1]
        dst_fh.write(conv_line + "\n")
        
    dst_fh.close()
    src_fh.close()
    return None


def convertECAMBerToCAMBerVis(strains, closure_iteration, ph2_iteration, output_fn):
    text_lines = []
    
    
    for strain_id in strains.allStrains():
        
        input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_UNIANNS", ph2_iteration) + strain_id + ".txt"

        input_fh = open(input_fn)
        for line in input_fh.readlines():
            line = line.strip()
            tokens = line.split("\t")
            out_line = ""
            out_line = tokens[0] + "\t" + tokens[1] + "\t" + tokens[2] + "\t"
            for len_token in tokens[3].split():
                out_line += len_token.split(":")[0] + "\t"
            out_line = out_line[:-1]
            text_lines.append(out_line)
        input_fh.close()
    
    output_fh = open(output_fn, "w")
    for text_line in text_lines:
        output_fh.write(text_line + "\n")
    output_fh.close()
    
    return None

if __name__ == '__main__':
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    
    print("Creates input for CAMBerVis")
    print("Input folder will be stored at: "+parameters["DATASET_CAMBERVIS"])
    
    ensure_dir(parameters["DATASET_CAMBERVIS"])
    ensure_dir(parameters["DATASET_CAMBERVIS"]+"/genomes")
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default = 3)
    
    
    progress = ShowProgress("CAMBerVis output")
    n = len(strains.allStrains())
    progress.setJobsCount(n)
    
    for strain_id in strains.allStrains():
        progress.update()
        src_fn = parameters["DATASET_GENOMES"] + strain_id + ".fasta"
        dst_fn = parameters["DATASET_CAMBERVIS"]+'/genomes/seq-' + strain_id + ".fasta"
        shutil.copy(src_fn, dst_fn)
    
    print("Copying unified annotations.")
    
    #src_fn = getItPath(parameters, iteration, "DATASET_RESULTS_EXP_PHASE2S") + "cluster-comb-det.txt"
    #if not os.path.exists(src_fn):
    #    src_fn = getItPath(parameters, iteration, "DATASET_RESULTS_EXP_PHASE2S") + "cluster-det.txt"
    
    output_fn = parameters["DATASET_CAMBERVIS"]+"/clustering.txt"
    
    convertECAMBerToCAMBerVis(strains, closure_iteration, ph2_iteration, output_fn)
    
    print("Copying the file with the list of strains.")
    input_fh = open(parameters["DATASET_RESULTS_EXP"]+"contigs_mapping.txt")
    output_fh = open(parameters["DATASET_CAMBERVIS"]+"/strains.txt", "w")
    
    for strain_id in strains.allStrains():
        output_fh.write(strain_id + "\n")
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split()
        if len(tokens) < 3:
            continue
        if tokens[2] in ["c"]:
            continue
        if tokens[0] == tokens[1]:
            continue
        output_fh.write(line +"\n") 
    
    output_fh.close()
    input_fh.close()
    
    print("The CAMBerVis input is already generated.")
    print("Copy (and rename) the folder 'cambervis-input' into 'examples/' folder in your CAMBerVis installation.")
    

