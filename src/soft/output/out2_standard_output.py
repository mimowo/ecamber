import sys
import os
import shutil
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *


def saveOutputAnnotations(strain_id):
    overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default = 3)

    strain = strains.strain(strain_id)
    src_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_UNIANNS", ph2_iteration) + strain_id + ".txt"
    dst_fn = parameters["DATASET_OUTPUT"]+'/ann_ecamber/' + strain_id + ".txt"
    
    shutil.copy(src_fn, dst_fn)
    
    cluster_gene_names = readClusterGeneNames()
    
    strain_id, annotations = readUniAnnotationsIter(strain, closure_iteration, ph2_iteration)
    output_fn = parameters["DATASET_OUTPUT"]+'/ann_output/' + strain_id + ".txt"
    output_fh = open(output_fn, "w")
    saveMgsAnnotations(output_fh, genes=None, multigenes=annotations.multigenes.values(), show_start_codons=False, cluster_ids=True, smr=0.0, std=True, cluster_gene_names=cluster_gene_names)
    output_fh.close()
    
    return strain_id

if __name__ == '__main__':
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    
    print("Creates input for CAMBerVis")
    print("Input folder will be stored at: "+parameters["DATASET_OUTPUT"])
    
    ensure_dir(parameters["DATASET_OUTPUT"])
    #ensure_dir(parameters["DATASET_OUTPUT"]+"/genomes/")
    ensure_dir(parameters["DATASET_OUTPUT"]+"/ann_ecamber/")
    ensure_dir(parameters["DATASET_OUTPUT"]+"/ann_output/")
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=3)
        
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    progress = ShowProgress("Saved output annotations")
    n = len(strains.allStrains())
    progress.setJobsCount(n)
    
    TASKS = strains.allStrains()
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap_unordered(saveOutputAnnotations, TASKS):
            progress.update(desc=r)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = saveOutputAnnotations(TASK)
            progress.update(desc=r)
      
    print("Saved output annotations.")



