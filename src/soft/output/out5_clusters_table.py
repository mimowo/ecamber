import sys
import time
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_graph_utils import *
from soft.structs.clusters import *
from soft.utils.camber_io_utils import *
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *

def saveClustersTable(output_fh, clusters, strains):
    strains_ordered = sorted(strains.allStrains())
    cluster_gene_names = readClusterGeneNames()
    clusters.classifyClusters(strains_ordered)

    text_line = "cluster_id\tcluster_gene_name\tcluster_type\tcluster_mg_count\tcluster_mg_ann\tcluster_strain_count\tcluster_strain_ann\t"
    for strain_id in  strains_ordered:
        text_line += strain_id + "\t"
        
    text_line = text_line[:-1] + "\n"
    output_fh.write(text_line)        
          
    for cluster in list(clusters.clusters.values()):
        text_line  = cluster.cluster_id + "\t"
        text_line += cluster_gene_names.get(cluster.cluster_id, "x") + "\t"
        
        if cluster.isAnchor(): text_line += "ANCHOR" + "\t"
        else: text_line += "NON_ANCHOR" + "\t"
            
        text_line += str(len(cluster.nodes)) + "\t"
        text_line += str(cluster.ann_mgs_count()) + "\t"

        text_line += str(len(cluster.strains())) + "\t"
        text_line += str(len(cluster.ann_strains())) + "\t"
        
        text_strains = ""
        
        for strain_id in strains_ordered:
            for multigene in cluster.nodesByStrain(strain_id).values():
                start, end = multigene.getStartEnd()
                if len(multigene.contig_id) > 0 and multigene.contig_id != multigene.strain_id:
                    text_strains += str(multigene.ann_id)+"."+str(start)+"."+str(end)+"."+multigene.strand+"."+multigene.contig_id + ";"
                else:
                    text_strains += str(multigene.ann_id)+"."+str(start)+"."+str(end)+"."+multigene.strand + ";"
                
            if text_strains.endswith(";"): text_strains = text_strains[:-1] + "\t"
            else: text_strains += "\t"
            
        text_line += text_strains
    
        output_fh.write(text_line + "\n")
    return None
    
if __name__ == '__main__':
    print("out5_clusters_table.py: read and saves connected components.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    strains_list = strains.allStrains()
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default = 3)
    
    clusters = MGClusters()
    
    TASKS = []
    for strain_id in strains_list:
        strain = strains.strain(strain_id)
        TASKS.append((strain, closure_iteration, ph2_iteration, False, False))
    
    progress = ShowProgress("Read connected components")
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes = WORKERS)    
        for strain_id, cc_mapping in pool.imap(readMultigeneClusterStrain, TASKS):
            
            for cluster_id in cc_mapping:
                if not cluster_id in clusters.clusters:
                    cluster = MGCluster(cluster_id)
                    clusters.addCluster(cluster)
                else:
                    cluster = clusters.getCluster(cluster_id)
                for multigene in cc_mapping[cluster_id]:
                    cluster.addMultigene(multigene)
            
            progress.update(desc = strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            strain_id, cc_mapping =readMultigeneClusterStrain(TASK)
            
            for cluster_id in cc_mapping:
                if not cluster_id in clusters.clusters:
                    cluster = MGCluster(cluster_id)
                    clusters.addCluster(cluster)
                else:
                    cluster = clusters.getCluster(cluster_id)
                for multigene in cc_mapping[cluster_id]:
                    cluster.addMultigene(multigene)
            
            progress.update(desc = strain_id)
    
    clusters.classifyClusters(strains_list)
    
    output_fn = parameters["DATASET_OUTPUT"] + "clusters_table.txt"
    output_fh = open(output_fn, "w")
    saveClustersTable(output_fh, clusters, strains)
    output_fh.close()
    
    print("out5_clusters_table.py: Finished.")
