import sys
import os
import shutil
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_seq_utils import translateSequence

def saveGeneSequences(strain_id):
    overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default = 3)

    strain = strains.strain(strain_id)
    readStrainSequences(strain)
    
    cluster_gene_names = readClusterGeneNames()

    strain_id, annotations = readUniAnnotationsIter(strain, closure_iteration, ph2_iteration)
    output_aa_fn = parameters["DATASET_OUTPUT"]+'/genes_aa/' + strain_id + ".fasta"
    output_dna_fn = parameters["DATASET_OUTPUT"]+'/genes_dna/' + strain_id + ".fasta"
    output_aa_fh = open(output_aa_fn, "w")
    output_dna_fh = open(output_dna_fn, "w")
    mutligenes_sorted = sorted(annotations.multigenes.values(), key=lambda multigene:mgIntoSortableTriple(multigene.mg_unique_id))
    for multigene in mutligenes_sorted:
        start, end = multigene.getStartEnd()
        ann_id =  multigene.ann_id + "|" + multigene.cluster_id + "|"+ cluster_gene_names.get(multigene.cluster_id,"x") + "|" + multigene.strand + "|" + str(start) + "|" + str(end) + "|" + str(multigene.contig_id)
        sequence_dna = multigene.sequence()
        sequence_aa = translateSequence(sequence_dna)
        writeFASTAseq(output_aa_fh, sequence_aa, ann_id)
        writeFASTAseq(output_dna_fh, sequence_dna, ann_id)
    output_aa_fh.close()
    output_dna_fh.close()
    
    return strain_id

if __name__ == '__main__':
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    strains = readStrainsInfo()
    
    print("Creates input for CAMBerVis")
    print("Input folder will be stored at: "+parameters["DATASET_OUTPUT"])
    
    ensure_dir(parameters["DATASET_OUTPUT"])
    ensure_dir(parameters["DATASET_OUTPUT"]+"/genes_aa/")
    ensure_dir(parameters["DATASET_OUTPUT"]+"/genes_dna/")
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=3)
        
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    progress = ShowProgress("Gene sequences saved")
    n = len(strains.allStrains())
    progress.setJobsCount(n)
    
    TASKS = strains.allStrains()
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for r in pool.imap_unordered(saveGeneSequences, TASKS):
            progress.update(desc=r)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            r = saveGeneSequences(TASK)
            progress.update(desc=r)

    print("Gene sequences saved.")

