import sys
import os
import shutil
sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *

def converToCAMBerVis(src_fn, dst_fn):
    dst_fh = open(dst_fn, "w")
    src_fh = open(src_fn)
    
    for line in src_fh.readlines():
        line = line.strip()
        conv_line = ""
        tokens = line.split("\t")
        for i in range(len(tokens)):
            if i == 2:
                conv_line += tokens[i].replace(" ",".") + "\t"
            else:
                conv_line += tokens[i] + "\t"
        
        conv_line = conv_line[:-1]
        dst_fh.write(conv_line + "\n")
        
    dst_fh.close()
    src_fh.close()
    return None


def convertECAMBerToCAMBerVis(strains, closure_iteration, ph2_iteration, output_fn):
    text_lines = []
    
    
    for strain_id in strains.allStrains():
        
        input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_UNIANNS", ph2_iteration) + strain_id + ".txt"

        input_fh = open(input_fn)
        for line in input_fh.readlines():
            line = line.strip()
            tokens = line.split("\t")
            out_line = ""
            out_line = tokens[0] + "\t" + tokens[1] + "\t" + tokens[2] + "\t"
            for len_token in tokens[3].split():
                out_line += len_token.split(":")[0] + "\t"
            out_line = out_line[:-1]
            text_lines.append(out_line)
        input_fh.close()
    
    output_fh = open(output_fn, "w")
    for text_line in text_lines:
        output_fh.write(text_line + "\n")
    output_fh.close()
    
    return None

if __name__ == '__main__':
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters(config_aln=True)
    strains = readStrainsInfo()
    
    print("Creates input for GWAMAR")
    print("Input folder will be stored at: "+parameters["DATASET_GWAMAR"])
    
    ensure_dir(parameters["DATASET_GWAMAR"])
    ensure_dir(parameters["DATASET_GWAMAR"]+"/input/")
    
    print("Copying the file with the list of strains.")
    output_fh = open(parameters["DATASET_GWAMAR"]+"/input/strains.txt", "w")
    for strain_id in strains.allStrains():
        output_fh.write(strain_id + "\n")
    output_fh.close()

    src_fn = parameters["MUTATIONS_RESULTS"] + 'gene_profiles.txt'
    dst_fn = parameters["DATASET_GWAMAR"]+"/input/gene_profiles.txt"
    if not os.path.exists(src_fn):
        print("The file: "+src_fn+ " does not exist. First generate the list of gene profiles.")
    else:
        shutil.copy2(src_fn, dst_fn)    

    src_fn = parameters["MUTATIONS_RESULTS"] + 'point_mutations.txt'
    dst_fn = parameters["DATASET_GWAMAR"]+"/input/point_mutations.txt"
    if not os.path.exists(src_fn):
        print("The file: "+src_fn+ " does not exist. First generate the list of point mutations.")
    else:
        shutil.copy2(src_fn, dst_fn)    


    src_fn = parameters["PHYLOGENY_FOLDER_MUTATIONS"] + 'tree.txt'
    dst_fn = parameters["DATASET_GWAMAR"]+"/input/tree.txt"
    if not os.path.exists(src_fn):
        print("The file: "+src_fn+ " does not exist. First generate the list of point mutations.")
    else:
        shutil.copy2(src_fn, dst_fn)   

    print("The GWAMAR input has been already generated.")
    print("Copy (and rename) the folder 'gwamar' into your {GWAMAR}/datasets/ folder .")
    print("Please not that eCAMBer does not generate the 'res_profiles.txt'.")
    print("This file provides the drug resistance status for the strains.")
    print("For more information please read the manual of GWAMAR.")

