import time
import sys
import os

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_patric_utils import *

if sys.version.startswith("3"):
    raw_input = input
    
if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)

    patric_db = parameters["PATRIC_FTP"]
    
    print("Show_patric_genomes.py: Outputs the list of genomes in the PATRIC database.")
    print("PATRIC: " + patric_db)
    sel_version = choosePatricVersion(patric_db)                    
    print(patric_db + sel_version)
    strain_det_list = selectAllStrains(patric_db + sel_version)
    
    clusters, cluster_strains = clusterStrains(strain_det_list)
    if len(clusters) == 0:
        print("There are no strain genomes in the selected folder: " + patric_db + sel_version)
        exit()
    
    classes_sorted = sorted(clusters, key=lambda class_id:(-len(clusters[class_id]), class_id))
    
    print("Total number of available strain genomes: " + str(len(strain_det_list)))
    
    for i in range(0, len(classes_sorted), 1):
        class_id = classes_sorted[i]
        strains_count = len(clusters[class_id])
        if strains_count > 1: 
            print(str(i) + ": " + class_id + "\t" + str(strains_count))
        
    index_str = raw_input("Select dataset index: (default: 0 -"+classes_sorted[0]+"):").strip()
    if len(index_str) == 0:
        index_sel = 0
    elif not index_str.isdigit():
        print("The typed string is not a correct index.")
        exit()
    else:
        index_sel = int(index_str)
        if index_sel >= len(classes_sorted) or index_sel < 0:
            print("The typed string is not a correct index.")
            exit()                    
    class_sel = classes_sorted[index_sel]
    strain_full_ids = clusters[class_sel]
    for i in range(len(strain_full_ids)):
        strain_full_id = strain_full_ids[i]
        print(str(i+1) + ": "+ strain_full_id + "\t" + getGenomeClusterID(strain_full_id) + "\t" + retriveStrainID(strain_full_id))
    print("show_patric_genomes.py: Finished.")
    