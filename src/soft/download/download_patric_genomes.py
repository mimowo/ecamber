import time
import sys
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_patric_utils import *

        
if sys.version.startswith("3"): raw_input = input

def defaultDatasetName(strain_list):
    prefix_tmp = ""
    cons = True
    for strain_full_id in strain_list:
        name_tokens = strain_full_id.split("_")
        if len(name_tokens) <= 1 or len(name_tokens[1]) < 3:
            strain_prefix = name_tokens[0][:3].lower()
        else:
            strain_prefix = name_tokens[0][0].lower() + name_tokens[1][:2].lower()
        if prefix_tmp == "":
            prefix_tmp = strain_prefix
        elif prefix_tmp != strain_prefix:
            cons = False
    
    if cons:
        default_dataset_name = prefix_tmp + "" + str(len(strain_list))
    else:
        default_dataset_name = "exp" + "" + str(len(strain_list))
        
    return default_dataset_name

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()

    patric_db = parameters.get("PATRIC_FTP", "ftp://ftp.patricbrc.org/patric2/genomes/")
    
    print("PATRIC: " + patric_db)
    print("download_patric_genomes.py: downloads genome sequences and annotations from the PATRIC database.")

#    sel_version = choosePatricVersion(patric_db)
 #   print(patric_db + sel_version)

    all_strains_list = selectAllStrains(patric_db)

    print("Total number of seqenced genomes in this version: " + str(len(all_strains_list)))
    if len(all_strains_list) == 0:
        print("There are no strain genomes in the selected folder: " + patric_db)
        exit()
    print("Selected strains:")
    sys.stdout.flush()
        
    selected_strains = selectStrains(all_strains_list)
    strain_ids = strainIDs(selected_strains)
    
    default_dataset = defaultDatasetName(selected_strains)
    dataset = raw_input("Input folder name to store the data (default: "+default_dataset+"):").strip()
    if len(dataset) == 0:
        dataset = default_dataset

    parameters["D"] = dataset
    parameters = readParameters(config_download=True)
    
    print("Dataset download directory: " + parameters["ECAMBER_DATASET_PATH"])
    if os.path.exists(parameters["ECAMBER_DATASET_PATH"]):
        print("This folder already exists: " + parameters["ECAMBER_DATASET_PATH"])
        ans = ""
        while not ans.lower() in ["n", "y"]:  
            ans = raw_input('Continue? [y/n]')
            if (ans.lower() == "n"):
                print("")
                exit()
     
    ensure_dir(parameters["ECAMBER_DATASET_PATH"])
    ensure_dir(parameters["DATASET_GENOMES_TMP"])

    if parameters["DOWNLOAD_PATRIC_ANNS"].upper() == "Y":
        patric_cds_dir = parameters["DATASET_ANNS_INPUT"] + "patric/"
        ensure_dir(patric_cds_dir)
        if parameters["DOWNLOAD_GBK_FORMAT_ANNS"] == "Y":
            patric_gb_dir = parameters["DATASET_ANNS_INPUT"] + "patric_gbk/"
            ensure_dir(patric_gb_dir)
        else: patric_gb_dir = ""
    else: patric_cds_dir = ""
    if parameters["DOWNLOAD_REFSEQ_ANNS"].upper() == "Y":
        refseq_cds_dir = parameters["DATASET_ANNS_INPUT"] + "refseq/"
        ensure_dir(refseq_cds_dir)
        if parameters["DOWNLOAD_GBK_FORMAT_ANNS"] == "Y":
            refseq_gb_dir = parameters["DATASET_ANNS_INPUT"] + "refseq_gbk/"
            ensure_dir(refseq_gb_dir)
        else: refseq_gb_dir = ""
    else: refseq_cds_dir = ""
        
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    progress = ShowProgress("Downloaded strain data")
    progress.setJobsCount(len(strain_ids))
      
    TASKS = []
    for (strain_id, full_strain_id) in strain_ids:
        TASKS.append((strain_id, 
                      full_strain_id, 
                      parameters["DATASET_GENOMES_TMP"], 
                      refseq_cds_dir,
                      patric_cds_dir,
                      refseq_gb_dir,
                      patric_gb_dir,
                      patric_db))
        
    log_lines = []
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for strain_id, log_lines_strain in pool.imap_unordered(downloadStrainFiles, TASKS):
            log_lines += log_lines_strain
            progress.update(desc=strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            strain_id, log_lines_strain = downloadStrainFiles(TASK)
            log_lines += log_lines_strain
            progress.update(desc=strain_id)

    output_fh = open(parameters["ECAMBER_DATASET_PATH"] + "/strains_tmp.txt", "w")
    
    for (strain_id, full_strain_id) in strain_ids:
        output_fh.write(strain_id + "\n")
    output_fh.close()
    
    output_fh = open(parameters["ECAMBER_DATASET_PATH"] + "/strains_name_details.txt", "w")
    for (strain_id, full_strain_id) in strain_ids:
        output_fh.write(strain_id + "\t" + full_strain_id + "\n")
    output_fh.close()

    output_fh = open(parameters["ECAMBER_DATASET_PATH"] + "/missing_files.txt", "w")
    for log_line in log_lines:
        output_fh.write(log_line + "\n")
    output_fh.close()
    
    
    print("download_patric_genomes.py: Finished.")

