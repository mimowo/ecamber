import time
import sys
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_ncbi_utils import *

if sys.version.startswith("3"):
    raw_input = input

def defaultDatasetName(species_name):
    prefix_tmp = species_name[:3].lower()
    default_dataset_name = prefix_tmp + str(len(strain_list))
        
    return default_dataset_name

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)

    ncbi_db = parameters.get("NCBI_HTTP", "http://www.ncbi.nlm.nih.gov/genomes/Genome2BE/genome2srv.cgi")
    
    print("NCBI: " + ncbi_db)
    print("download_ncbi_genomes.py: downloads genome sequences and annotations from the PATRIC database.")

    species_name = raw_input("Provide a species name (default: Mycobacterium tuberculosis):").strip()
    if len(species_name) <= 1:
        species_name = "Mycobacterium tuberculosis"
    species_name = species_name.replace(" ", "%20")

    sel_species_link = ncbi_db + "?action=download&orgn="+species_name+"[orgn]&status=50|40||30|20&report=proks&group=--%20All%20Prokaryotes%20--&subgroup=--%20All%20Prokaryotes%20--"
    all_strains_list = downloadStrainsList(sel_species_link, species_name=species_name)
        
    if len(all_strains_list) == 0:
        print("There are no strain genomes in the selected folder: " + sel_species_link)
        exit()
        
    selected_strains = selectStrains(all_strains_list, species_name)
    strain_ids = strainIDs(selected_strains)
    
    default_dataset = defaultDatasetName(species_name)
    dataset = raw_input("Provide a folder name to store the data (default: "+default_dataset+"):").strip()
    if len(dataset) == 0:
        dataset = default_dataset

    parameters["D"] = dataset
    parameters = readParameters()
    
    print("Dataset download directory: " + parameters["ECAMBER_DATASET_PATH"])
    if os.path.exists(parameters["ECAMBER_DATASET_PATH"]):
        print("This folder already exists: " + parameters["ECAMBER_DATASET_PATH"])
    
    ans = ""
    while not ans.lower() in ["n", "y"]:  
        ans = raw_input('Continue? [y/n]')
        if (ans.lower() == "n"):
            print("")
            exit()
    
    ensure_dir(parameters["ECAMBER_DATASET_PATH"])
    ensure_dir(parameters["DATASET_GENOMES_TMP"])
    ensure_dir(parameters["DATASET_FEATURES_REFSEQ"])
    ensure_dir(parameters["DATASET_FEATURES_PATRIC"])
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    progress = ShowProgress("Downloaded strain data")
    progress.setJobsCount(len(strain_ids))
      
    TASKS = []
    for (strain_id, full_strain_id) in strain_ids:
        TASKS.append((strain_id, 
                      full_strain_id, 
                      parameters["DATASET_GENOMES_TMP"], 
                      parameters["DATASET_FEATURES_REFSEQ"], 
                      parameters["DATASET_FEATURES_PATRIC"], 
                      patric_db + sel_version))
    
    log_lines = []
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for strain_id, log_lines_strain in pool.imap_unordered(downloadStrainFiles, TASKS):
            log_lines += log_lines_strain
            progress.update(desc=strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            strain_id, log_lines_strain = downloadStrainFiles(TASK)
            log_lines += log_lines_strain
            progress.update(desc=strain_id)

    output_fh = open(parameters["ECAMBER_DATASET_PATH"] + "/strains_tmp.txt", "w")
    for (strain_id, full_strain_id) in strain_ids:
        output_fh.write(strain_id + "\n")
    output_fh.close()
    
    output_fh = open(parameters["ECAMBER_DATASET_PATH"] + "/strains_name_details.txt", "w")
    for (strain_id, full_strain_id) in strain_ids:
        output_fh.write(strain_id + "\t" + full_strain_id + "\n")
    output_fh.close()

    output_fh = open(parameters["ECAMBER_DATASET_PATH"] + "/missing_files.txt", "w")
    for log_line in log_lines:
        output_fh.write(log_line + "\n")
    output_fh.close()
    
    print("download_ncbi_genomes.py: Finished.")
