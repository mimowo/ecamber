import time
import sys
import os
import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_utils import *
from soft.utils.camber_io_utils import *
from soft.utils.camber_patric_utils import *

patric_db = "http://brcdownloads.vbi.vt.edu/patric2/"  
        
if sys.version.startswith("3"):
    raw_input = input

def downloadGffStrainFiles(params):
    strain_id = params[0]
    full_strain_id = params[1]
    genomes_dir = params[2]
    gff_refseq_dir = params[3]
    gff_patric_dir = params[4]
    patric_db = params[5]    
    
    log_lines = []
    
    out_fea_fn = gff_refseq_dir+"/"+strain_id+".txt"
    if not os.path.exists(out_fea_fn) or os.path.getsize(out_fea_fn)<10:
        http_fn = patric_db + "/" + full_strain_id + "/" + full_strain_id+ ".RefSeq.gff"
        log = downloadFile(http_fn, out_fea_fn)
        if len(log) > 0:
            log_lines.append("REFSEQ: " + log)
            if os.path.exists(out_fea_fn):
                os.remove(out_fea_fn)
            
    out_fea_fn = gff_patric_dir+"/"+strain_id+".txt"            
    if not os.path.exists(out_fea_fn) or os.path.getsize(out_fea_fn)<10:
        http_fn = patric_db + "/" + full_strain_id + "/" + full_strain_id+ ".PATRIC.gff"
        log = downloadFile(http_fn, out_fea_fn)
        if len(log) > 0:
            log_lines.append("PATRIC: " + log)
            if os.path.exists(out_fea_fn):
                os.remove(out_fea_fn)
            
    return strain_id, log_lines


def defaultDatasetName(strain_list):
    prefix_tmp = ""
    cons = True
    for strain_full_id in strain_list:
        strain_prefix = strain_full_id.lower()[:3]
        if prefix_tmp == "":
            prefix_tmp = strain_prefix
        elif prefix_tmp != strain_prefix:
            cons = False
    
    if cons:
        default_dataset_name = prefix_tmp + "_" + str(len(strain_list))
    else:
        default_dataset_name = "exp" + "_" + str(len(strain_list))
        
    return default_dataset_name

if __name__ == '__main__':
    parameters = overwriteParameters(sys.argv)
    print("PATRIC: " + patric_db)
    print("download_patric_genomes.py: downloads genome sequences and annotations from the PATRIC database.")

    sel_version = choosePatricVersion(patric_db)
    print(patric_db + sel_version)
    
    all_strains_list = selectAllStrains(patric_db + sel_version)
        
    if len(all_strains_list) == 0:
        print("There are no strain genomes in the selected folder: " + patric_db + sel_version)
        exit()
    print("Selected strains:")
    sys.stdout.flush()
        
    selected_strains = selectStrains(all_strains_list)
    strain_ids = strainIDs(selected_strains)
    
    default_dataset = defaultDatasetName(selected_strains)
    dataset = raw_input("Input folder name to store the data (default: "+default_dataset+"):").strip()
    if len(dataset) == 0:
        dataset = default_dataset

    parameters["D"] = dataset
    parameters = readParameters()
    

    print("Dataset download directory: " + parameters["ECAMBER_DATASET_PATH"])
    if os.path.exists(parameters["ECAMBER_DATASET_PATH"]):
        print("This folder already exists: " + parameters["ECAMBER_DATASET_PATH"])
        ans = ""
        while not ans.lower() in ["n", "y"]:  
            ans = raw_input('Continue? [y/n]')
            if (ans.lower() == "n"):
                print("")
                exit()
        
    ensure_dir(parameters["ECAMBER_DATASET_PATH"])
    ensure_dir(parameters["DATASET_GENOMES_TMP"])
    ensure_dir(parameters["DATASET_FEATURES_REFSEQ"])
    ensure_dir(parameters["DATASET_FEATURES_PATRIC"])
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())
    
    progress = ShowProgress("Downloaded strain data")
    progress.setJobsCount(len(strain_ids))
      
    TASKS = []
    for (strain_id, full_strain_id) in strain_ids:
        TASKS.append((strain_id, 
                      full_strain_id, 
                      parameters["DATASET_GENOMES_TMP"], 
                      parameters["ECAMBER_DATASET_PATH"] + "/gff/", 
                      parameters["DATASET_GFF_PATRIC"], 
                      patric_db + sel_version))
        
    log_lines = []
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes=WORKERS) 
        for strain_id, log_lines_strain in pool.imap_unordered(downloadGffStrainFiles, TASKS):
            log_lines += log_lines_strain
            progress.update(desc=strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            strain_id, log_lines_strain = downloadGffStrainFiles(TASK)
            log_lines += log_lines_strain
            progress.update(desc=strain_id)

    output_fh = open(parameters["ECAMBER_DATASET_PATH"] + "/strains_tmp.txt", "w")
    
    for (strain_id, full_strain_id) in strain_ids:
        output_fh.write(strain_id + "\n")
    output_fh.close()
    
    output_fh = open(parameters["ECAMBER_DATASET_PATH"] + "/strains_name_details.txt", "w")
    for (strain_id, full_strain_id) in strain_ids:
        output_fh.write(strain_id + "\t" + full_strain_id + "\n")
    output_fh.close()

    output_fh = open(parameters["ECAMBER_DATASET_PATH"] + "/missing_files.txt", "w")
    for log_line in log_lines:
        output_fh.write(log_line + "\n")
    output_fh.close()
    
    
    print("download_patric_genomes.py: Finished.")
    
    