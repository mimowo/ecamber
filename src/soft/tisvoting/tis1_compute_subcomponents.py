import sys
import time
import os

import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_fu_utils import UnionFind
from soft.utils.camber_graph_utils import readBlastResultsMapping
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *
from soft.utils.camber_io_utils import ensure_dir


if __name__ == '__main__':
    print("tis1_compute_subcomponents.py: Computes connected sub-components.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=2)-1
    
    progress = ShowProgress("tis1_compute_subcomponents.py")
    
    progress.setJobsCount(5)
    
    g_input_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration)
    cc_input_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration)
    output_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_TIS_VOTING")
    ensure_dir(output_dir)

    seq_cc_map = {}
    seq_ids = set([])
    
    input_fn = cc_input_dir + "seqids_all.txt"
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) < 1:
            continue
        seq_ids.add(int(tokens[0]))
    
    input_fh.close()
    
    ccs = {}
    ccs_old = {}

    input_fn = cc_input_dir + "seq_clusters.txt"
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        tokens = line.split()
        if len(tokens) < 2:
            continue
        cluster_id = tokens[0]
        ccs[cluster_id] = set([])
        ccs_old[cluster_id] = set([])
        
        seq_id_tokens = tokens[1].split(":")
        for seq_id_token in seq_id_tokens:
            seq_id = int(seq_id_token)
            if not seq_id in seq_cc_map:
                seq_cc_map[seq_id] = set([])
            seq_cc_map[seq_id].add(cluster_id)
            ccs_old[cluster_id].add(seq_id)
    input_fh.close();
    
    fu = UnionFind()
    fu.insert_objects(seq_ids)
    
    progress.update(desc = str(closure_iteration) + ": nodes")
    
    input_fh = open(g_input_dir + "edges_seq.txt")
    for line in input_fh.readlines():
        node_tokens = line.split()
        if len(node_tokens) != 2:
            continue
        node1_id = int(node_tokens[0])
        node2_id = int(node_tokens[1])
        if node1_id in seq_ids and node2_id in seq_ids:
            fu.union(node1_id, node2_id)                
    input_fh.close();
        
    progress.update(desc = str(closure_iteration) + ": blast edges")
        
    sub_ccs = fu.sets()
    
    progress.update(desc = str(closure_iteration) + ": components")
        
    for sub_cluster_id in sub_ccs:
        sub_cc_seqs = list(sub_ccs[sub_cluster_id])
        for seq_id in sub_cc_seqs:
            cluster_ids = seq_cc_map[seq_id]
            for cluster_id in cluster_ids:
                common_seq_ids = sorted(ccs_old[cluster_id] & set(sub_cc_seqs))
                common_seq_ids_str = ""
                for seq_id in common_seq_ids:
                    common_seq_ids_str += str(seq_id) + ":"
                common_seq_ids_str = common_seq_ids_str[:-1]
                ccs[cluster_id].add(common_seq_ids_str)
    
    cluster_fn = output_dir + "tis_conn_subcomp_seq.txt"
    cluster_fh = open(cluster_fn, "w")
    for cluster_id in ccs:
        text_line = cluster_id + "\t"
        for common_seq_ids_str in ccs[cluster_id]:
            text_line += common_seq_ids_str
            text_line += ";"
        text_line = text_line[:-1]
        cluster_fh.write(text_line + "\n")
    cluster_fh.close();
    
    progress.update(desc = str(closure_iteration) + ": saved")
 
    print("tis1_compute_subcomponents.py: Finished.")
        
