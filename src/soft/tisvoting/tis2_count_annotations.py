import sys
import time
import os

import multiprocessing

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_fu_utils import UnionFind
from soft.utils.camber_graph_utils import readBlastResultsMapping
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *
from soft.utils.camber_io_utils import ensure_dir

def tisCounting(strain_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=2)-1
    
    readUniAnnotationsIter(strain, closure_iteration, ph2_iteration)
    multigenes = strain.uni_annotation.multigenes
    
    seqid_ann_counts = {}
    seqid_cons_counts = {}
    cc_mgs_counts = {}
    mg_len_seq_map = {}
    
    input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_MGSSEQ", ph2_iteration) + "/" + strain_id + ".txt"
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split("\t")
        if len(tokens) < 2:
            continue
        mg_id = tokens[0]
        tokens_seq = tokens[1].split(";")
        for token_seq in tokens_seq:
            tokens_len = token_seq.split(":")
            seq_len = int(tokens_len[0])
            seq_id = int(tokens_len[1])
            mg_len_seq_map[(mg_id, seq_len)] = seq_id
    input_fh.close()
    
    for multigene in multigenes.values():
        mg_id = multigene.mg_strain_unique_id
        cluster_id = multigene.cluster_id
        if not cluster_id in cc_mgs_counts:
            cc_mgs_counts[cluster_id] = 0
        cc_mgs_counts[cluster_id] += 1
        
        seq_ids = set([])
        for gene in multigene.genes.values():
            seq_len = gene.length()
            seq_id = mg_len_seq_map.get((mg_id, seq_len))
            if seq_id == None:
                continue
            seq_ids.add(seq_id)

            if not (seq_id, cluster_id) in seqid_ann_counts:
                seqid_ann_counts[(seq_id, cluster_id)] = 0
            if len(gene.gene_id) > 1:
                seqid_ann_counts[(seq_id, cluster_id)] += 1
        for seq_id in seq_ids:
            if not (seq_id, cluster_id) in seqid_cons_counts:
                seqid_cons_counts[(seq_id, cluster_id)] = 0
            seqid_cons_counts[(seq_id, cluster_id)] += 1
    
    return strain_id, seqid_ann_counts, seqid_cons_counts, cc_mgs_counts

if __name__ == '__main__':
    print("tis2_count_annotations.py: count annotation for multigene clusters.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=2)-1
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())   
    
    g_input_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration)
    cc_input_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_PH2_IT", ph2_iteration)
    tis_input_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_TIS_VOTING")

    ccs = {}
    ccs_rev = {}
    sub_cc_map = {}
    sub_cc_ann_counts = {}
    sub_cc_cons_counts = {}
    sub_cc_mgs_counts = {}
    
    cluster_fn = tis_input_dir + "tis_conn_subcomp_seq.txt"
    cluster_fh = open(cluster_fn)
    for line in cluster_fh.readlines():
        tokens = line.split()
        if len(tokens) < 2:
            continue
        cluster_id = tokens[0]
        sub_cluster_ids = tokens[1].split(";")
        ccs[cluster_id] = sub_cluster_ids
        #ccs_rev[sub_cluster_ids] = cluster_id
        for sub_cluster_id in sub_cluster_ids:
            sub_cc_ann_counts[(cluster_id, sub_cluster_id)] = 0
            sub_cc_cons_counts[(cluster_id, sub_cluster_id)] = 0
            sub_cc_mgs_counts[cluster_id] = 0
            seq_ids = sub_cluster_id.split(":")
            for seq_id_token in seq_ids:
                seq_id = int(seq_id_token)
                if not (cluster_id, seq_id) in sub_cc_map:
                    sub_cc_map[(cluster_id, seq_id)] = set([])
                sub_cc_map[(cluster_id, seq_id)].add(sub_cluster_id)
    cluster_fh.close();
    
    strains_list = strains.allStrains()
    TASKS = strains_list
    
    progress = ShowProgress("tis2_count_annotations.py")
    progress.setJobsCount(len(TASKS))

    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes = WORKERS)    
        for strain_id, sub_cc_ann_counts_strain, sub_cc_cons_counts_strain, sub_cc_mgs_counts_strain in pool.imap(tisCounting, TASKS):
          #  print(strain_id, len(sub_cc_ann_counts_strain), len(sub_cc_cons_counts_strain))
            for (seq_id, cluster_id) in sub_cc_ann_counts_strain:
                sub_cluster_ids = sub_cc_map[(cluster_id, seq_id)]
                for sub_cluster_id in sub_cluster_ids:
                    sub_cc_ann_counts[(cluster_id, sub_cluster_id)] += sub_cc_ann_counts_strain[(seq_id, cluster_id)]
            for (seq_id, cluster_id) in sub_cc_cons_counts_strain:
                sub_cluster_ids = sub_cc_map[(cluster_id, seq_id)]
                for sub_cluster_id in sub_cluster_ids:
                    sub_cc_cons_counts[(cluster_id, sub_cluster_id)] += sub_cc_cons_counts_strain[(seq_id, cluster_id)]
            for cluster_id in sub_cc_mgs_counts_strain:
                sub_cc_mgs_counts[cluster_id] += sub_cc_mgs_counts_strain[cluster_id]
            progress.update(desc = strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            strain_id, sub_cc_ann_counts_strain, sub_cc_cons_counts_strain, sub_cc_mgs_counts_strain = tisCounting(TASK) 
            for (seq_id, cluster_id) in sub_cc_ann_counts_strain:
                sub_cluster_ids = sub_cc_map[(cluster_id, seq_id)]
                for sub_cluster_id in sub_cluster_ids:
                    sub_cc_ann_counts[(cluster_id, sub_cluster_id)] += sub_cc_ann_counts_strain[(seq_id, cluster_id)]
            for (seq_id, cluster_id) in sub_cc_cons_counts_strain:
                sub_cluster_ids = sub_cc_map[(cluster_id, seq_id)]
                for sub_cluster_id in sub_cluster_ids:
                    sub_cc_cons_counts[(cluster_id, sub_cluster_id)] += sub_cc_cons_counts_strain[(seq_id, cluster_id)]
            for cluster_id in sub_cc_mgs_counts_strain:
                sub_cc_mgs_counts[cluster_id] += sub_cc_mgs_counts_strain[cluster_id]
            progress.update(desc = strain_id)

    cluster_fn = tis_input_dir + "tis_conn_subcomp_anns.txt"
    cluster_fh = open(cluster_fn, "w")
    for cluster_id in ccs:
        text_line = cluster_id + "\t" + str(sub_cc_mgs_counts[cluster_id]) + "\t"
        for cc_sub_id in ccs[cluster_id]:
            ann_count = sub_cc_ann_counts[(cluster_id, cc_sub_id)]
            cons_count = sub_cc_cons_counts[(cluster_id, cc_sub_id)]
            
            text_line += str(ann_count) + ":" + str(cons_count) + ":" + cc_sub_id

            text_line += ";"
        text_line = text_line[:-1]
        cluster_fh.write(text_line + "\n")
    cluster_fh.close();
     
    print("tis2_count_annotations.py: Finished.")
        