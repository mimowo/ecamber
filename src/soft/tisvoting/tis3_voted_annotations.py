import sys
import time
import os

import multiprocessing
import shutil

sys.path.append("../../")

from soft.utils.camber_params_utils import *
from soft.utils.camber_fu_utils import UnionFind
from soft.utils.camber_graph_utils import readBlastResultsMapping
from soft.structs.multigene import *
from soft.utils.camber_utils import *
from soft.utils.camber_cc_utils import *
from soft.utils.camber_io_utils import ensure_dir

def selectTISs(tis_anns_map, tis_cons_map, tis_starts_map, mg_count, ann_lens, tvr=0.8, tvc="r"):
    sel_lens = set([])
    
    if len(tis_anns_map) == 1:
        return set(tis_anns_map.keys())
    
    elif len(tis_anns_map) == 0:
        return set([])

    if tvc.lower() in ["r","c"]:    
        len_anns_sorted = sorted(tis_anns_map, key=lambda seq_len: -tis_anns_map[seq_len])
        sec_ann_len = len_anns_sorted[1]
        sec_ann_count = tis_anns_map[sec_ann_len]

        for seq_len in tis_anns_map:
            ann_count = tis_anns_map[seq_len]
            cons_count = tis_cons_map[seq_len]
            if cons_count >= tvr*mg_count and (2*ann_count >= mg_count and ann_count >= 2*sec_ann_count):
                sel_lens.add(seq_len)
                
        if len(sel_lens) > 1:
            sel_ann_lens = sel_lens & ann_lens
            if len(sel_ann_lens) > 1:
                return set([sorted(sel_ann_lens, key = lambda seq_len:-seq_len)[0]])
            elif len(sel_ann_lens) == 1:
                return sel_ann_lens
            elif len(ann_lens) == 0:
                return set([sorted(sel_lens, key = lambda seq_len:-seq_len)[0]])
            elif max(ann_lens) > max(sel_lens):
                return set([sorted(ann_lens, key = lambda seq_len:-seq_len)[0]])
            else:
                return set([sorted(sel_lens, key = lambda seq_len:-seq_len)[0]])
    else:
        tis_scores_map = {}
        tis_anns_max = max(tis_anns_map.values())
        tis_cons_max = max(tis_cons_map.values())
        tis_len_max = max(tis_cons_map.keys())
        for sl in tis_anns_map:
            if tis_starts_map[sl] == "ATG":
                cw = 0.82
            elif tis_starts_map[sl] == "GTG":
                cw = 0.14
            elif tis_starts_map[sl] == "TTG":
                cw = 0.03
            else:
                cw = 0.01
            if tis_anns_max > 0:
                tis_scores_map[sl] = tis_anns_map[sl]*tis_cons_map[sl]*sl*cw/(tis_anns_max*tis_cons_max*tis_len_max)
            else:
                tis_scores_map[sl] = tis_cons_map[sl]*sl*cw/(tis_cons_max*tis_len_max)
        res = set([sorted(tis_scores_map, key = lambda seq_len:-tis_scores_map[seq_len])[0]])
        return res
        
    if len(sel_lens) == 1:
        return sel_lens
    elif len(ann_lens) >= 1:
        return set([sorted(ann_lens, key = lambda seq_len:-seq_len)[0]])
    else:
        return set([sorted(tis_anns_map, key = lambda seq_len:-seq_len)[0]])
        
    return sel_lens
        
def saveVotedAnnotations(strain_id):
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    tvr = float(parameters["TVR"])
    tvc = parameters["TVC"]
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=2)-1
    
    tis_input_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_TIS_VOTING")
    
    strains = readStrainsInfo()
    strain = strains.strain(strain_id)
    
    readStrainSequences(strain)
    readUniAnnotationsIter(strain, closure_iteration, ph2_iteration)
    multigenes = strain.uni_annotation.multigenes
    
    sub_cc_map = {}
    sub_cc_ann_counts = {}
    sub_cc_cons_counts = {}
    cc_mg_count = {}
    
    cluster_fn = tis_input_dir + "tis_conn_subcomp_anns.txt"
    cluster_fh = open(cluster_fn)
    for line in cluster_fh.readlines():
        tokens = line.split()
        if len(tokens) < 2:
            continue
        cluster_id = tokens[0]
        cc_mg_count[cluster_id] = int(tokens[1])
        sub_cluster_ids_tokens = tokens[2].split(";")

        for sub_cluster_ids_token in sub_cluster_ids_tokens:
            seq_stats_tokens = sub_cluster_ids_token.split(":")
            sub_cluster_id = ""
            for seq_token in seq_stats_tokens[2:]:
                sub_cluster_id += seq_token + ":"
            sub_cluster_id = sub_cluster_id[:-1]

            sub_cc_ann_counts[(cluster_id, sub_cluster_id)] = int(seq_stats_tokens[0])
            sub_cc_cons_counts[(cluster_id, sub_cluster_id)] = int(seq_stats_tokens[1])
            
            seq_ids = sub_cluster_id.split(":")
            for seq_id_token in seq_ids:
                seq_id = int(seq_id_token)
                sub_cc_map[(cluster_id, seq_id)] = sub_cluster_id
                
    cluster_fh.close();
    
    mg_len_seq_map = {}
    
    input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_MGSSEQ", ph2_iteration) + "/" + strain_id + ".txt"
    input_fh = open(input_fn)
    for line in input_fh.readlines():
        line = line.strip()
        tokens = line.split("\t")
        if len(tokens) < 2:
            continue
        mg_id = tokens[0]
        tokens_seq = tokens[1].split(";")
        for token_seq in tokens_seq:
            tokens_len = token_seq.split(":")
            seq_len = int(tokens_len[0])
            seq_id = int(tokens_len[1])
            mg_len_seq_map[(mg_id, seq_len)] = seq_id
    input_fh.close()
    
    text_stat_lines = []
    
    for multigene in multigenes.values():
        mg_id = multigene.mg_strain_unique_id
        cluster_id = multigene.cluster_id
        tis_anns = {}
        tis_cons = {}
        ann_lens = set([])
        tis_starts = {}
        mg_count = cc_mg_count[cluster_id]
        for gene in multigene.genes.values():
            seq_len = gene.length()
            tis_starts[seq_len] = gene.startCodon()
            
            if len(gene.gene_id) > 1:
                ann_lens.add(seq_len)
                
            seq_id = mg_len_seq_map.get((mg_id, seq_len))
            if seq_id== None:
                continue
            sub_cluster_id = sub_cc_map[(cluster_id, seq_id)]
            tis_anns[seq_len] = sub_cc_ann_counts[(cluster_id, sub_cluster_id)]
            tis_cons[seq_len] = sub_cc_cons_counts[(cluster_id, sub_cluster_id)]
        
        sel_tiss = selectTISs(tis_anns, tis_cons, tis_starts, mg_count, ann_lens, tvr=tvr, tvc=tvc.lower())

        if tvc.lower() == "c":
            change = True
            for sel_len in sel_tiss:
                ann_len = sel_len
                for ann_len in ann_lens:
                    if sel_len < ann_len: change = False
            for sel_len in sel_tiss:
                sel_start = tis_starts[seq_len]
                ann_start = sel_start
                for ann_len in ann_lens:
                    ann_start  = tis_starts[ann_len]
                    if ann_start == "ATG" and not sel_start in ["ATG"]: change = False
                    elif ann_start == "GTG" and not sel_start in ["GTG", "ATG"]: change = False
                    elif ann_start == "TTG" and not sel_start in ["GTG", "ATG", "TTG"]: change = False
        else:
            change = True
            
        if change: multigene.sel_lengths = sel_tiss
        else: multigene.sel_lengths = multigene.ann_lengths
        
        if len(multigene.lengths) > 0:                
            text_line = cluster_id + "\t" + mg_id +"\t" + str(mg_count) +"\t" 
            for seq_len in tis_anns:
                text_line += str(seq_len) + ":" + str(tis_anns[seq_len]) + ":"+ multigene.genes[seq_len].startCodon()+ ";"
            text_line = text_line[:-1] + "\t"
            
            for seq_len in tis_cons:
                text_line += str(seq_len) + ":" + str(tis_cons[seq_len]) + ";"
            text_line = text_line[:-1] + "\t"
            
            if len(ann_lens) == 0:
                text_line += "D\t"
            elif not sel_tiss.issubset(ann_lens) and change:
                text_line += "C\t"
            else:
                text_line += "S\t"

            for seq_len in sel_tiss:
                text_line += str(seq_len) + ";"
            text_line = text_line[:-1] + "\t"
                
            for seq_len in ann_lens:
                text_line += str(seq_len) + ";"
            text_line = text_line[:-1] + "\t"
            text_stat_lines.append(text_line)
        
    output_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_UNIANNS", ph2_iteration+1)
    
    output_fh = open(output_dir + strain_id + ".txt", "w")
    saveMgsAnnotations(output_fh, genes=None, multigenes=multigenes.values(), show_start_codons=True, cluster_ids = True)
    output_fh.close();
        
    output_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_VOTED_STATS", ph2_iteration+1)

    all_orfs_fh = open(output_dir + strain_id + ".txt", "w")
    for text_line in text_stat_lines:
        all_orfs_fh.write(text_line + "\n")
    all_orfs_fh.close();
    
    input_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_MGSSEQ", ph2_iteration) + "/" + strain_id + ".txt"
    output_fn = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_MGSSEQ", ph2_iteration+1) + "/" + strain_id + ".txt"
    if os.path.exists(output_fn):
        os.remove(output_fn)
        
    shutil.copyfile(input_fn, output_fn)
        
    return strain_id

if __name__ == '__main__':
    print("tis3_voted_annotations.py: selects TISs.")
    
    parameters = overwriteParameters(sys.argv)
    parameters = readParameters()
    
    strains = readStrainsInfo()
    
    closure_iteration = currentClosureIteration()
    ph2_iteration = currentPh2Iteration(default=2)-1
    output_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_VOTED", ph2_iteration+1)
    ensure_dir(output_dir)
    output_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_VOTED_STATS", ph2_iteration+1)
    ensure_dir(output_dir)
    output_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_MGSSEQ", ph2_iteration+1)
    ensure_dir(output_dir)
    output_dir = getItPath(parameters, closure_iteration, "DATASET_RESULTS_EXP_UNIANNS", ph2_iteration+1)
    ensure_dir(output_dir)
    
    WORKERS = min(int(parameters["W"]), multiprocessing.cpu_count())   
    
    strains_list = strains.allStrains()
    TASKS = strains_list
    
    progress = ShowProgress("tis3_voted_annotations.py")
    progress.setJobsCount(len(TASKS))
    
    if WORKERS > 1:
        pool = multiprocessing.Pool(processes = WORKERS)    
        for strain_id in pool.imap(saveVotedAnnotations, TASKS):
 
            progress.update(desc = strain_id)
        pool.close()
        pool.join()
    else:
        for TASK in TASKS:
            strain_id = saveVotedAnnotations(TASK) 
            ##print(strain_id, len(sub_cc_ann_counts_strain), len(sub_cc_cons_counts_strain))

            progress.update(desc = strain_id)

    print("tis3_voted_annotations.py: Finished.")
